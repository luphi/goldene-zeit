Maps contain custom properties "EncounterRate" and "Encounters" which are parsed into Scenes from their respective TMX file.

The encounter rate ("EncounterRate") is a float value between 0.0 and 1.0 defining the probability that walking on a new tile will produce an encounter. For
example, 0.1 means a 10% chance, 0.0 means no encounters, and 1.0 means the player will be furious. If no property is defined, 0.0 is assumed.

The encounters list ("Encounters") is a string using a specific format that defines which opponents may be encountered by their IDs (integer or name), their
center level, level margin, and the weight dictating the probability that an encounter will be that particular opponent.
Encounters are defined in the newline- and colon-separated format:
    <ID>:<weight>:<center level>:<level margin>
for which
    ID - an integer or string (name) identifying the opponent
    weight - an integer defining the relative probably that this opponent will be selected when an encounter occurs (e.g. given the weights 2 and 1, the
        opponent with weight 2 will have a 67% probability of appearing and that with 1 will have a 33% chance)
    center level - an integer defining the center, or average, level of this opponent
    level margin - an integer indicating the amount the opponents level can range that can be thought of as a "plus or minus" value (e.g. given center level
        10 and margin 2, the levels can range from 8 to 12)
and additional encounters are separated by newlines.

The map properties, as an example, are:
  <properties>
    <property name="EncounterRate" type="float" value="0.1"/>
    <property name="Encounters">0:5:10:2
1:1:8:3</property>
  </properties>