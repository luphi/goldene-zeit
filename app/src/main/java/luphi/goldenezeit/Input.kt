package luphi.goldenezeit

@Suppress("PropertyName")
class Input(opcode: Button, isPress: Boolean) {
    enum class Button { NO_OP, A, B, SELECT, START, DPAD_UP, DPAD_RIGHT, DPAD_DOWN, DPAD_LEFT }

    var Opcode: Button = opcode
    var IsPress: Boolean = isPress

    override fun toString(): String = "{ Opcode = $Opcode, IsPress = $IsPress }"
}