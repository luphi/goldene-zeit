package luphi.goldenezeit

import java.util.LinkedList
import java.util.Locale
import kotlin.collections.HashMap

@Suppress("PropertyName")
class Catalog {
    private val mEntries = HashMap<String, LinkedList<Entry>>()

    val Keys: LinkedList<String>
        get() = LinkedList(mEntries.keys)

    fun get(key: String, verse: Int): String {
        val lower = key.toLowerCase(Locale.getDefault())
        if (!mEntries.containsKey(lower) || (mEntries[lower] == null))
            return ""
        var result: Entry? = null
        for (entry in mEntries[lower]!!) {
            if (verse >= entry.Verse)
                result = entry
        }
        return result?.Value ?: ""
    }

    operator fun get(key: String): String {
        return get(key, gGZ.Verse)
    }

    fun set(key: String, value: String, verse: Int) {
        val lower = key.toLowerCase(Locale.getDefault())
        if (!mEntries.containsKey(lower) || (mEntries[lower] == null))
            mEntries[lower] = LinkedList()
        mEntries[lower]?.add(Entry(verse, value))
    }

    private inner class Entry internal constructor(internal var Verse: Int, internal var Value: String)
}