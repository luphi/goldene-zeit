package luphi.goldenezeit

import java.util.*

@Suppress("PropertyName")
class Addition(val ID: Int, val Name: String, val Style: GZ.AttackStyle, val Level: Int, val Modifier: Float) {
    val Adds = LinkedList<Long>()

    override fun toString(): String {
        fun strongAgainst(attackStyle: GZ.AttackStyle): String = when (attackStyle) {
            GZ.AttackStyle.NONE -> "all/no"
            GZ.AttackStyle.CRUSH -> "PLATE"
            GZ.AttackStyle.SLASH -> "LEATHER"
            GZ.AttackStyle.STAB -> "CHAIN"
        }
        return "${Adds.size}-add attack strong against ${strongAgainst(Style)} defenses"
    }
}