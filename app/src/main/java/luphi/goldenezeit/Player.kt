package luphi.goldenezeit

import luphi.goldenezeit.item.ITAccessory
import luphi.goldenezeit.item.ITArmor
import luphi.goldenezeit.item.ITWeapon
import kotlin.math.floor

@Suppress("PropertyName")
class Player {
    val Name = "Leid"
    var Cash = 0

    // Level and experience properties
    var Level = 1
    var Experience = 0 // Total experience gained throughout the game (refer to Utils.experienceForLevel() to know what level this should correlate to)

    // HP properties
    var HP = MaxHP
        set(value) {
            field = value
            if (field > MaxHP)
                field = MaxHP
        }
    val MaxHP
        get() = floor((((2 * StatBase) * Level / 100) + Level + 10).toDouble()).toInt()

    // Stat properties
    var AvailableStatPoints = 0
    val StatBase // Level-dependent value for any stat prior to applying user-allotted points
        get() = 9 + Level
    val Strength
        get() = StatBase + StrengthStatAllotted
    val Vitality
        get() = StatBase + VitalityStatAllotted
    val Agility
        get() = StatBase + AgilityStatAllotted
    val Sense
        get() = StatBase + SenseStatAllotted
    var StrengthStatAllotted = 0
    var VitalityStatAllotted = 0
    var AgilityStatAllotted = 0
    var SenseStatAllotted = 0

    // Item properties
    val Inventory = LinkedHashMap<Int, Int>() // Item ID -> non-zero quantity
    val Cards = LinkedHashMap<Int, Int>() // Card ID -> non-zero quantity
    var EquippedWeapon: ITWeapon? = null // If null, nothing is equipped
    var EquippedArmor: ITArmor? = null // If null, nothing is equipped
    var EquippedAccessory1: ITAccessory? = null // If null, nothing is equipped
    var EquippedAccessory2: ITAccessory? = null // If null, nothing is equipped

    // Stat bonus from equipped item(s) properties
    val HPBonus: Int
        get() = 0 // TODO - derived from accessories and armors
    val StrengthBonus: Int
        get() = (EquippedWeapon?.StrengthBonus ?: 0) +
                (EquippedArmor?.StrengthBonus ?: 0) +
                (EquippedAccessory1?.StrengthBonus ?: 0) +
                (EquippedAccessory2?.StrengthBonus ?: 0)
    val VitalityBonus: Int
        get() = (EquippedWeapon?.VitalityBonus ?: 0) +
                (EquippedArmor?.VitalityBonus ?: 0) +
                (EquippedAccessory1?.VitalityBonus ?: 0) +
                (EquippedAccessory2?.VitalityBonus ?: 0)
    val AgilityBonus: Int
        get() = (EquippedWeapon?.AgilityBonus ?: 0) +
                (EquippedArmor?.AgilityBonus ?: 0) +
                (EquippedAccessory1?.AgilityBonus ?: 0) +
                (EquippedAccessory2?.AgilityBonus ?: 0)
    val SenseBonus: Int
        get() = (EquippedWeapon?.SenseBonus ?: 0) +
                (EquippedArmor?.SenseBonus ?: 0) +
                (EquippedAccessory1?.SenseBonus ?: 0) +
                (EquippedAccessory2?.SenseBonus ?: 0)

    // Addition properties
    var Addition1: Addition? = null
    var Addition2: Addition? = null
    var Addition3: Addition? = null
    var Addition4: Addition? = null

    // Scene coordinates (in tiles), refering to the global scene (gGZ.Scene)
    var X = 0
    var Y = 0

    fun clone(): Player {
        val player = Player()
        player.Inventory.putAll(Inventory)
        player.EquippedWeapon = EquippedWeapon
        player.EquippedArmor = EquippedArmor
        player.EquippedAccessory1 = EquippedAccessory1
        player.EquippedAccessory2 = EquippedAccessory2
        return player
    }

    fun zeroize() {
        Cash = 0
        Level = 1
        Experience = 0
        HP = MaxHP
        AvailableStatPoints = 0
        StrengthStatAllotted = 0
        VitalityStatAllotted = 0
        AgilityStatAllotted = 0
        SenseStatAllotted = 0
        Inventory.clear()
        Cards.clear()
        EquippedWeapon = null
        EquippedArmor = null
        EquippedAccessory1 = null
        EquippedAccessory2 = null
        Addition1 = null
        Addition2 = null
        Addition3 = null
        Addition4 = null
        X = 0
        Y = 0
    }

    fun minimize() {
        zeroize()
        Cash = 1000
        Level = 1
        Experience = gGZ.Utils.experienceForLevel(Level)
        HP = MaxHP
        Inventory[0] = 10 // Potion
        Inventory[1] = 10 // Super potion
        Inventory[2] = 10 // Hyper potion
        EquippedWeapon = gGZ.Resources.loadItem(100) as ITWeapon // Schwert
        EquippedArmor = gGZ.Resources.loadItem(200) as ITArmor // Lederweste
        Addition1 = gGZ.Resources.loadAddition(0)
        Addition2 = gGZ.Resources.loadAddition(1)
        Addition3 = gGZ.Resources.loadAddition(2)
        Addition4 = gGZ.Resources.loadAddition(3)
    }

    fun maximize() {
        zeroize()
        Cash = Int.MAX_VALUE / 2 // Halved to avoid wraparound
        Level = 100
        Experience = gGZ.Utils.experienceForLevel(Level)
        HP = MaxHP
        Inventory[3] = 50 // Fairy dust
        Inventory[10] = 50 // X Health
        Inventory[11] = 50 // X Strength
        Inventory[12] = 50 // X Vitality
        Inventory[13] = 50 // X Agility
        Inventory[14] = 50 // X Sense
        Inventory[20] = 50 // Dart
        Inventory[21] = 50 // Sand
        Inventory[22] = 50 // Mini bomb
        Inventory[23] = 50 // Mask
        Inventory[24] = 50 // Caltrops
        Inventory[25] = 1 // Knife
        Inventory[30] = 50 // Bandage
        Inventory[31] = 50 // Eye drops
        Inventory[32] = 50 // Smelling salts
        Inventory[33] = 50 // Bravery charm
        Inventory[50] = 50 // Protein
        Inventory[51] = 50 // Calcium
        Inventory[52] = 50 // Carbohydrate
        Inventory[53] = 50 // Caffeine
        Inventory[54] = 50 // Rare candy
        Inventory[100] = 1 // Schwert
        Inventory[101] = 1 // Zweihänder
        Inventory[200] = 1 // Lederweste
        Inventory[300] = 1 // Schnabelmaske
        for (cardID in gGZ.Resources.CardCatalog.Keys)
            Cards[cardID.toInt()] = if (gGZ.Utils.isCardUnique(cardID.toInt())) 1 else 5
        EquippedWeapon = gGZ.Resources.loadItem(199) as ITWeapon // Drachentöter
        EquippedArmor = gGZ.Resources.loadItem(299) as ITArmor // Berserker
        EquippedAccessory1 = gGZ.Resources.loadItem(398) as ITAccessory // Behelit
        EquippedAccessory2 = gGZ.Resources.loadItem(399) as ITAccessory // Band
        Addition1 = gGZ.Resources.loadAddition(0)
        Addition2 = gGZ.Resources.loadAddition(1)
        Addition3 = gGZ.Resources.loadAddition(2)
        Addition4 = gGZ.Resources.loadAddition(3)
    }

    fun addExperience(experiencePoints: Int) {
        Experience += experiencePoints
        while (Experience >= gGZ.Utils.experienceForLevel(Level + 1)) {
            val hpDiff = MaxHP - HP // Remember how many points below max the HP currently is
            Level += 1 // This will modify MaxHP
            HP = MaxHP - hpDiff // Apply the difference
        }
    }

    fun addItem(id: Int, quantity: Int = 1) {
        Inventory[id] = (Inventory[id] ?: 0) + quantity
    }

    fun removeItem(id: Int, quantity: Int = 1): Boolean {
        if (!Inventory.containsKey(id))
            return false
        Inventory[id] = (Inventory[id] ?: 0) - quantity
        if (Inventory[id] ?: 0 <= 0) {
            Inventory.remove(id)
            return true
        }
        return false
    }

    fun addCard(id: Int, quantity: Int = 1) {
        Cards[id] = (Cards[id] ?: 0) + quantity
    }

    fun removeCard(id: Int, quantity: Int = 1): Boolean {
        if (!Cards.containsKey(id))
            return false
        Cards[id] = (Cards[id] ?: 0) - quantity
        if (Cards[id] ?: 0 <= 0) {
            Cards.remove(id)
            return true
        }
        return false
    }
}