package luphi.goldenezeit

import android.content.Context
import luphi.goldenezeit.`interface`.IMode
import luphi.goldenezeit.gl.GLCamera
import luphi.goldenezeit.gl.GLRenderer
import luphi.goldenezeit.gl.GLView
import luphi.goldenezeit.scene.Scene
import java.util.concurrent.ConcurrentLinkedQueue

@SuppressWarnings("StaticFieldLeak")
val gGZ = GZ()

@Suppress("PropertyName")
class GZ {
    enum class Cardinal { NORTH, EAST, SOUTH, WEST }

    enum class Town { ERSTEBURG, ZWEITEBURG, DRITTEBURG, KRONE, SCHICKSALBURG, SCHLACHTFELD, ENDBURG, ROTERSEE, EINFACHESLEBEN, BERGDORF, HULLE_GRANZ }

    enum class AttackStyle {
        NONE,
        STAB, // Inserting pointy things into stuff (strong against plate armor, weak against leather armor)
        SLASH, // Like a really big paper cut (strong against leather armor, weak against chain armor)
        CRUSH // Imagine running into a wall, but the wall hits you (strong against chain armor, weak against plate armor)
    }

    enum class DefenseStyle {
        NONE,
        PLATE, // Strong against crush, weak against stab
        CHAIN, // Strong against slash, weak against crush
        LEATHER // Strong against stab, weak against slash
    }

    val Camera = GLCamera()
    var Chapter = 1
    var Context: Context? = null
    var FontSize = 0f
    val InputQueue = ConcurrentLinkedQueue<Input>()
    var IsActivityReady = false
    var IsViewReady = false
    var IsRendererReady = false
    var Mode: IMode? = null
    val Player = Player()
    val Record = Record()
    var Renderer: GLRenderer? = null
    val Resources = Resources()
    var Scene: Scene? = null
    val Utils = Utils()
    var Verse = 1
    var View: GLView? = null
}