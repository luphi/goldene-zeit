package luphi.goldenezeit

import android.util.Log
import luphi.goldenezeit.battle.BTOpponent
import luphi.goldenezeit.battle.BTOpponentParser
import luphi.goldenezeit.card.Card
import luphi.goldenezeit.gl.GLTexture
import luphi.goldenezeit.item.*
import luphi.goldenezeit.scene.*
import org.xml.sax.Attributes
import org.xml.sax.InputSource
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler
import java.io.FileNotFoundException
import java.io.IOException
import java.io.StringReader
import java.util.LinkedList
import java.util.Locale
import javax.xml.parsers.ParserConfigurationException
import javax.xml.parsers.SAXParserFactory
import kotlin.collections.HashMap

@Suppress("PropertyName")
class Resources {
    private var mItemCatalog: Catalog? = null
    private var mSceneCatalog: Catalog? = null
    private var mPCCatalog: Catalog? = null
    private var mNpcCatalog: Catalog? = null
    private var mOpponentCatalog: Catalog? = null
    private var mCutsceneCatalog: Catalog? = null
    private var mCardCatalog: Catalog? = null
    private var mShopCatalog: Catalog? = null
    private var mSignCatalog: Catalog? = null
    private var mChapterCatalog: Catalog? = null
    private val mTextures = HashMap<String, GLTexture>()
    private val mCatalogs = HashMap<String, Catalog>()
    private val mScenes = HashMap<String, Scene>()
    private val mNPCs = HashMap<String, SCNpc>()
    private val mOpponents = HashMap<String, BTOpponent>()
    private val mCutscenes = HashMap<String, SCCutscene>()
    private val mCards = HashMap<Int, Card>()
    private val mAdditions = HashMap<Int, Addition>()

    fun free() {
        Log.i("Resources", "Freeing all loaded resources")
        for (texture in mTextures.values)
            texture.free()
        mTextures.clear()
        for (scene in mScenes.values)
            scene.free()
        mScenes.clear()
        mNPCs.clear()
        mCards.clear()
        mCutscenes.clear()
        mCatalogs.clear()
    }

    val ItemCatalog: Catalog
        get() {
            if (mItemCatalog == null) {
                mItemCatalog = loadCatalog("items/catalog.xml")
                if (mItemCatalog == null) {
                    Log.w("Resources", "Could not load the item catalog")
                    mItemCatalog = Catalog() // A dummy to prevent null exceptions
                }
            }
            return mItemCatalog!!
        }

    val SceneCatalog: Catalog
        get() {
            if (mSceneCatalog == null) {
                mSceneCatalog = loadCatalog("scenes/catalog.xml")
                if (mSceneCatalog == null) {
                    Log.w("Resources", "Could not load the scene catalog")
                    mSceneCatalog = Catalog() // A dummy to prevent null exceptions
                }
            }
            return mSceneCatalog!!
        }

    val PCCatalog: Catalog
        get() {
            if (mPCCatalog == null) {
                mPCCatalog = loadCatalog("PC/catalog.xml")
                if (mPCCatalog == null) {
                    Log.w("Resources", "Could not load the PC catalog")
                    mPCCatalog = Catalog() // A dummy to prevent null exceptions
                }
            }
            return mPCCatalog!!
        }

    val NpcCatalog: Catalog
        get() {
            if (mNpcCatalog == null) {
                mNpcCatalog = loadCatalog("NPCs/catalog.xml")
                if (mNpcCatalog == null) {
                    Log.w("Resources", "Could not load the NPC catalog")
                    mNpcCatalog = Catalog() // A dummy to prevent null exceptions
                }
            }
            return mNpcCatalog!!
        }

    val OpponentCatalog: Catalog
        get() {
            if (mOpponentCatalog == null) {
                mOpponentCatalog = loadCatalog("opponents/catalog.xml")
                if (mOpponentCatalog == null) {
                    Log.w("Resources", "Could not load the opponent catalog")
                    mOpponentCatalog = Catalog() // A dummy to prevent null exception
                }
            }
            return mOpponentCatalog!!
        }

    val CutsceneCatalog: Catalog
        get() {
            if (mCutsceneCatalog == null) {
                mCutsceneCatalog = loadCatalog("cutscenes/catalog.xml")
                if (mCutsceneCatalog == null) {
                    Log.w("Resources", "Could not load the cutscene catalog")
                    mCutsceneCatalog = Catalog() // A dummy to prevent null exceptions
                }
            }
            return mCutsceneCatalog!!
        }

    val CardCatalog: Catalog
        get() {
            if (mCardCatalog == null) {
                mCardCatalog = loadCatalog("cards/catalog.xml")
                if (mCardCatalog == null) {
                    Log.w("Resources", "Could not load the card catalog")
                    mCardCatalog = Catalog() // A dummy to prevent null exceptions
                }
            }
            return mCardCatalog!!
        }

    val ShopCatalog: Catalog
        get() {
            if (mShopCatalog == null) {
                mShopCatalog = loadCatalog("shops/catalog.xml")
                if (mShopCatalog == null) {
                    Log.w("Resources", "Could not load the shop catalog")
                    mShopCatalog = Catalog() // A dummy to prevent null exceptions
                }
            }
            return mShopCatalog!!
        }

    val SignCatalog: Catalog
        get() {
            if (mSignCatalog == null) {
                mSignCatalog = loadCatalog("signs/catalog.xml")
                if (mSignCatalog == null) {
                    Log.w("Resources", "Could not load the sign catalog")
                    mSignCatalog = Catalog() // A dummy to prevent null exceptions
                }
            }
            return mSignCatalog!!
        }

    val ChapterCatalog: Catalog
        get() {
            if (mChapterCatalog == null) {
                mChapterCatalog = loadCatalog("chapters/catalog.xml")
                if (mChapterCatalog == null) {
                    Log.w("Resources", "Could not load the chapter catalog")
                    mChapterCatalog = Catalog() // A dummy to prevent null exceptions
                }
            }
            return mChapterCatalog!!
        }

    fun loadTexture(file: String): GLTexture? {
        if (mTextures.containsKey(file))
            return mTextures[file]
        val texture = GLTexture()
        texture.init(file)
        Log.i("Resources", "Loaded texture $texture")
        mTextures[file] = texture
        return texture
    }

    fun loadItem(id: Int): Item {
        val itemCatalog = ITCatalog()
        val parser: DefaultHandler
        val catalogFile: String
        when (ItemCatalog[id.toString()].toLowerCase(Locale.getDefault())) {
            "weapon" -> {
                parser = WeaponCatalogParser(itemCatalog)
                catalogFile = "items/weapons.xml"
            }
            "armor" -> {
                parser = ArmorCatalogParser(itemCatalog)
                catalogFile = "items/armors.xml"
            }
            "accessory" -> {
                parser = AccessoryCatalogParser(itemCatalog)
                catalogFile = "items/accessories.xml"
            }
            else -> {
                parser = ItemCatalogParser(itemCatalog)
                catalogFile = "items/items.xml"
            }
        }
        val saxParserFactory = SAXParserFactory.newInstance()
        val dummy = Item(-1, Item.ItemCategory.SPECIAL, "MissingNo.", "Unknown item ID", 999999)
        try {
            val saxParser = saxParserFactory.newSAXParser()
            val xmlReader = saxParser.xmlReader
            val assetManager = gGZ.Context?.assets
            val inputStream = assetManager?.open(catalogFile)
            xmlReader.contentHandler = parser
            xmlReader.parse(InputSource(inputStream))
        } catch (e: FileNotFoundException) {
            Log.e("Resources", "Could not find item catalog \"$catalogFile\"", e)
            return dummy
        } catch (e: ParserConfigurationException) {
            Log.e("Resources", "Item catalog parse error", e)
            return dummy
        } catch (e: SAXException) {
            Log.e("Resources", "Item catalog error", e)
            return dummy
        } catch (e: IOException) {
            Log.e("Resources", "Item catalog I/O error", e)
            return dummy
        } catch (e: NullPointerException) {
            Log.e("Resources", "Null pointer during load", e)
            return dummy
        }
        Log.i("Resources", "Loaded item catalog $catalogFile")
        return itemCatalog.get(id) ?: dummy
    }

    fun loadScene(id: String): Scene? {
        if (mScenes.containsKey(id))
            return mScenes[id]
        val sceneParser = SCSceneParser(id, SceneCatalog[id])
        val saxParserFactory = SAXParserFactory.newInstance()
        try {
            val saxParser = saxParserFactory.newSAXParser()
            val xmlReader = saxParser.xmlReader
            val assetManager = gGZ.Context?.assets
            val inputStream = assetManager?.open(SceneCatalog[id])
            xmlReader.contentHandler = sceneParser
            xmlReader.parse(InputSource(inputStream))
        } catch (e: FileNotFoundException) {
            Log.e("Resources", "Could not find scene file \"${SceneCatalog[id]}\"", e)
            return null
        } catch (e: ParserConfigurationException) {
            Log.e("Resources", "Scene parse error", e)
            return null
        } catch (e: SAXException) {
            Log.e("Resources", "Scene error", e)
            return null
        } catch (e: IOException) {
            Log.e("Resources", "Scene I/O error", e)
            return null
        } catch (e: NullPointerException) {
            Log.e("Resources", "Null pointer during load", e)
            return null
        }
        val scene = sceneParser.Scene // the scene parser generates the scene from the data it parsed when getting the Scene property
        Log.i("Resources", "Loaded scene $scene")
        mScenes[id] = scene
        return scene
    }

    fun loadNpc(file: String): SCNpc? {
        if (mNPCs.containsKey(file))
            return mNPCs[file]
        val npcParser = SCNpcParser(file)
        val saxParserFactory: SAXParserFactory = SAXParserFactory.newInstance()
        try {
            val saxParser = saxParserFactory.newSAXParser()
            val xmlReader = saxParser.xmlReader
            val assetManager = gGZ.Context?.assets
            val inputStream = assetManager?.open(file)
            xmlReader.contentHandler = npcParser
            xmlReader.parse(InputSource(inputStream))
        } catch (e: FileNotFoundException) {
            Log.e("Resources", "Could not find NPC file \"$file\"", e)
            return null
        } catch (e: ParserConfigurationException) {
            Log.e("Resources", "NPC parse error", e)
            return null
        } catch (e: SAXException) {
            Log.e("Resources", "NPC error", e)
            return null
        } catch (e: IOException) {
            Log.e("Resources", "NPC I/O error", e)
            return null
        } catch (e: NullPointerException) {
            Log.e("Resources", "Null pointer during NPC load", e)
            return null
        }
        val npc = npcParser.NPC
        Log.i("Resources", "Loaded NPC $npc")
        mNPCs[file] = npc
        return npc
    }

    fun loadOpponent(file: String): BTOpponent? {
        if (mOpponents.containsKey(file))
            return mOpponents[file]
        val opponentParser = BTOpponentParser(file)
        val saxParserFactory: SAXParserFactory = SAXParserFactory.newInstance()
        try {
            val saxParser = saxParserFactory.newSAXParser()
            val xmlReader = saxParser.xmlReader
            val assetManager = gGZ.Context?.assets
            val inputStream = assetManager?.open(file)
            xmlReader.contentHandler = opponentParser
            xmlReader.parse(InputSource(inputStream))
        } catch (e: FileNotFoundException) {
            Log.e("Resources", "Could not find opponent file \"$file\"", e)
            return null
        } catch (e: ParserConfigurationException) {
            Log.e("Resources", "Opponent parse error", e)
            return null
        } catch (e: SAXException) {
            Log.e("Resources", "Opponent error", e)
            return null
        } catch (e: IOException) {
            Log.e("Resources", "Opponent I/O error", e)
            return null
        } catch (e: NullPointerException) {
            Log.e("Resources", "Null pointer during opponent load", e)
            return null
        }
        val oppponent = opponentParser.Opponent
        Log.i("Resources", "Loaded opponent $oppponent")
        mOpponents[file] = oppponent
        return oppponent
    }

    fun loadCutscene(fileOrContent: String, isFile: Boolean = true): SCCutscene? {
        if (mCutscenes.containsKey(fileOrContent))
            return mCutscenes[fileOrContent]
        //val cutscene = SCCutscene()
        //val cutsceneParser = SCCutsceneParser(cutscene)
        val cutsceneParser = SCCutsceneParser(fileOrContent, isFile)
        val saxParserFactory = SAXParserFactory.newInstance()
        try {
            val saxParser = saxParserFactory.newSAXParser()
            val xmlReader = saxParser.xmlReader
            xmlReader.contentHandler = cutsceneParser
            // If the parameter is a file containing XML
            if (isFile) {
                val assetManager = gGZ.Context?.assets
                val inputStream = assetManager?.open(fileOrContent)
                xmlReader.parse(InputSource(inputStream))
            }
            // If the parameter is a string of XML
            else {
                xmlReader.parse(InputSource(StringReader(fileOrContent)))
            }
        } catch (e: FileNotFoundException) {
            Log.e("Resources", "Could not find cutscene file \"$fileOrContent\"", e)
            return null
        } catch (e: ParserConfigurationException) {
            Log.e("Resources", "cutscene parse error", e)
            return null
        } catch (e: SAXException) {
            Log.e("Resources", "cutscene error", e)
            return null
        } catch (e: IOException) {
            Log.e("Resources", "cutscene I/O error", e)
            return null
        } catch (e: NullPointerException) {
            Log.e("Resources", "Null pointer during cutscene load", e)
            return null
        }
        Log.i("Resources", "Loaded cutscene \"$fileOrContent\"")
        val cutscene = cutsceneParser.Cutscene
        mCutscenes[fileOrContent] = cutscene
        return cutscene
    }

    fun loadCard(id: Int): Card {
        val dummy = Card(-1, 0, 0, 0, 0, 0)
        dummy.init()
        if (mCards.containsKey(id))
            return mCards[id] ?: dummy
        val values = CardCatalog[id.toString()]
        val splitValues = values.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        if (splitValues.size < 5) {
            Log.e("Resources", "Could not load a cataloged card with the ID \"$id\" or its missing values")
            return dummy
        }
        val card = Card(
            id,
            splitValues[0].toIntOrNull() ?: 0, // Level
            splitValues[1].toIntOrNull() ?: 0, // Left
            splitValues[2].toIntOrNull() ?: 0, // Top
            splitValues[3].toIntOrNull() ?: 0, // Right
            splitValues[4].toIntOrNull() ?: 0 // Bottom
        )
        card.init()
        mCards[id] = card
        return card
    }

    fun loadAddition(id: Int): Addition {
        val dummy = Addition(-1, "MissingNo.", GZ.AttackStyle.STAB, 1, 1f)
        if (mAdditions.containsKey(id))
            return mAdditions[id] ?: dummy
        val additionsParser = AdditionsParser(mAdditions)
        val saxParserFactory = SAXParserFactory.newInstance()
        try {
            val saxParser = saxParserFactory.newSAXParser()
            val xmlReader = saxParser.xmlReader
            val assetManager = gGZ.Context?.assets
            val inputStream = assetManager?.open("additions/additions.xml")
            xmlReader.contentHandler = additionsParser
            xmlReader.parse(InputSource(inputStream))
        } catch (e: FileNotFoundException) {
            Log.e("Resources", "Could not find additions file \"additions/additions.xml\"", e)
            return dummy
        } catch (e: ParserConfigurationException) {
            Log.e("Resources", "additions parse error", e)
            return dummy
        } catch (e: SAXException) {
            Log.e("Resources", "additions error", e)
            return dummy
        } catch (e: IOException) {
            Log.e("Resources", "additions I/O error", e)
            return dummy
        } catch (e: NullPointerException) {
            Log.e("Resources", "Null pointer during additions load", e)
            return dummy
        }
        return mAdditions[id] ?: dummy
    }

    fun loadAdditions(): LinkedList<Addition> {
        // If the additions have not been parsed yet
        if (mAdditions.isEmpty())
            loadAddition(0) // Loading a single addition actually involves caching the whole list
        return LinkedList(mAdditions.values)
    }

    fun loadShop(id: Int): LinkedList<Item> {
        val items = LinkedList<Item>()
        val values = ShopCatalog[id.toString()] // Returns an empty string if the ID is not an existing key
        val splitValues = values.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (itemID in splitValues) {
            try {
                val item = loadItem(itemID.toInt())
                items.add(item)
            } catch (e: Exception) {
                Log.e("Resources", "An item listed in shop ID $id did not have a correctly-formatted integer ID")
            }
        }
        return items
    }

    private fun loadCatalog(file: String): Catalog? {
        if (mCatalogs.containsKey(file))
            return mCatalogs[file]
        val catalog = Catalog()
        val catalogParser = CatalogParser(catalog)
        val saxParserFactory = SAXParserFactory.newInstance()
        try {
            val saxParser = saxParserFactory.newSAXParser()
            val xmlReader = saxParser.xmlReader
            val assetManager = gGZ.Context?.assets
            val inputStream = assetManager?.open(file)
            xmlReader.contentHandler = catalogParser
            xmlReader.parse(InputSource(inputStream))
        } catch (e: FileNotFoundException) {
            Log.e("Resources", "Could not find catalog file \"${file}\"", e)
            return catalog
        } catch (e: ParserConfigurationException) {
            Log.e("Resources", "catalog parse error", e)
            return catalog
        } catch (e: SAXException) {
            Log.e("Resources", "catalog error", e)
            return catalog
        } catch (e: IOException) {
            Log.e("Resources", "catalog I/O error", e)
            return catalog
        } catch (e: NullPointerException) {
            Log.e("Resources", "Null pointer during catalog load", e)
            return catalog
        }
        Log.i("Resources", "Loaded catalog $file")
        mCatalogs[file] = catalog
        // CatalogParser will modify the catalog with all the attributes it parsed
        return catalog
    }

    private class ItemCatalogParser constructor(private val catalog: ITCatalog) :
        DefaultHandler() {
        override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
            if (localName == "item") {
                val id = atts.getValue("id")
                if (id != null)
                    catalog.set(
                        id.toInt(), Item(
                            id.toInt(),
                            when (atts.getValue("category")) {
                                "buff" -> Item.ItemCategory.BUFF
                                "offense" -> Item.ItemCategory.OFFENSE
                                "defense" -> Item.ItemCategory.DEFENSE
                                "held" -> Item.ItemCategory.HELD
                                "vitamin" -> Item.ItemCategory.VITAMIN
                                "special" -> Item.ItemCategory.SPECIAL
                                "weapon" -> Item.ItemCategory.WEAPON
                                "armor" -> Item.ItemCategory.ARMOR
                                "accessory" -> Item.ItemCategory.ACCESSORY
                                else -> Item.ItemCategory.POTION
                            },
                            atts.getValue("name") ?: "",
                            atts.getValue("description") ?: "",
                            atts.getValue("cost")?.toInt() ?: 0
                        )
                    )
            }
        }
    }

    private class WeaponCatalogParser constructor(private val catalog: ITCatalog) :
        DefaultHandler() {
        override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
            if (localName == "weapon") {
                val id = atts.getValue("id")
                if (id != null)
                    catalog.set(
                        id.toInt(), ITWeapon(
                            id.toInt(),
                            atts.getValue("name") ?: "",
                            atts.getValue("description") ?: "",
                            atts.getValue("cost")?.toInt() ?: 0,
                            atts.getValue("bonus")?.toInt() ?: 0,
                            when (atts.getValue("effect")) {
                                // TODO
                                else -> ITWeapon.WeaponEffect.NONE
                            },
                            atts.getValue("bleed")?.toBoolean() ?: false,
                            atts.getValue("blind")?.toBoolean() ?: false,
                            atts.getValue("stun")?.toBoolean() ?: false,
                            atts.getValue("fear")?.toBoolean() ?: false
                        )
                    )
            }
        }
    }

    private class ArmorCatalogParser constructor(private val catalog: ITCatalog) :
        DefaultHandler() {
        override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
            if (localName == "armor") {
                val id = atts.getValue("id")
                if (id != null)
                    catalog.set(
                        id.toInt(), ITArmor(
                            id.toInt(),
                            atts.getValue("name") ?: "",
                            atts.getValue("description") ?: "",
                            atts.getValue("cost")?.toInt() ?: 0,
                            atts.getValue("bonus")?.toInt() ?: 0,
                            when (atts.getValue("effect")) {
                                // TODO
                                else -> ITArmor.ArmorEffect.NONE
                            },
                            when (atts.getValue("style")) {
                                "chain" -> GZ.DefenseStyle.CHAIN
                                "leather" -> GZ.DefenseStyle.LEATHER
                                else -> GZ.DefenseStyle.PLATE
                            },
                            atts.getValue("bleed")?.toBoolean() ?: false,
                            atts.getValue("blind")?.toBoolean() ?: false,
                            atts.getValue("stun")?.toBoolean() ?: false,
                            atts.getValue("fear")?.toBoolean() ?: false
                        )
                    )
            }
        }
    }

    private class AccessoryCatalogParser constructor(private val catalog: ITCatalog) :
        DefaultHandler() {
        override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
            if (localName == "accessory") {
                val id = atts.getValue("id")
                if (id != null)
                    catalog.set(
                        id.toInt(), ITAccessory(
                            id.toInt(),
                            atts.getValue("name") ?: "",
                            atts.getValue("description") ?: "",
                            atts.getValue("cost")?.toInt() ?: 0,
                            when (atts.getValue("effect")) {
                                // TODO
                                else -> ITAccessory.AccessoryEffect.NONE
                            },
                            when (atts.getValue("effect")) {
                                // TODO
                                else -> ITAccessory.AccessoryEffect.NONE
                            },
                            when (atts.getValue("effect")) {
                                // TODO
                                else -> ITAccessory.AccessoryEffect.NONE
                            },
                            when (atts.getValue("effect")) {
                                // TODO
                                else -> ITAccessory.AccessoryEffect.NONE
                            }
                        )
                    )
            }
        }
    }

    private class AdditionsParser constructor(private val additions: HashMap<Int, Addition>) :
        DefaultHandler() {
        private var mCurrentAddition: Addition? = null
        override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
            if (localName == "addition") {
                try {
                    mCurrentAddition = Addition(
                        atts.getValue("id").toInt(),
                        atts.getValue("name"),
                        when (atts.getValue("style")) {
                            "crush" -> GZ.AttackStyle.CRUSH
                            "slash" -> GZ.AttackStyle.SLASH
                            "stab" -> GZ.AttackStyle.STAB
                            else -> GZ.AttackStyle.NONE
                        },
                        atts.getValue("level").toInt(),
                        atts.getValue("modifier").toFloat()
                    )
                } catch (e: java.lang.Exception) {
                    mCurrentAddition = null
                }
            } else if ((mCurrentAddition != null) && (localName == "add"))
                mCurrentAddition?.Adds?.add(atts.getValue("delay")?.toLong() ?: 0)
        }

        override fun endElement(uri: String?, localName: String?, qName: String?) {
            if ((mCurrentAddition != null) && (localName == "addition")) {
                additions[mCurrentAddition!!.ID] = mCurrentAddition!!
                mCurrentAddition = null
            }
        }
    }

    private class CatalogParser constructor(private val catalog: Catalog) :
        DefaultHandler() {
        override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
            if (localName == "entry") {
                val key = atts.getValue("key")
                val value = atts.getValue("value")
                val verse = atts.getValue("verse")?.toInt() ?: 0
                if (key != null)
                    catalog.set(key, value, verse)
            }
        }
    }
}