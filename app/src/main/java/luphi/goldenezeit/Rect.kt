package luphi.goldenezeit

class Rect(var X: Float, var Y: Float, var Width: Float, var Height: Float) {
    override fun toString(): String {
        return "{ position ($X, $Y), dimensions [$Width x $Height] }"
    }
}