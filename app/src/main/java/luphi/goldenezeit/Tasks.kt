package luphi.goldenezeit

import luphi.goldenezeit.`interface`.IProgressive
import java.util.LinkedList
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class Tasks : IProgressive {
    private var mID = -1
    private val mPeriodics = HashMap<Int, DelayedCallback>()
    private val mOneOffs = HashMap<Int, DelayedCallback>()
    private val mLock = ReentrantLock()

    override fun step(dt: Long) {
        mLock.withLock {
            val toCall = LinkedList<(() -> Unit)?>()
            for (periodic in mPeriodics.values) {
                periodic.Ticks += dt
                if (periodic.Ticks >= periodic.Delay) {
                    toCall.add(periodic.Callback)
                    periodic.Ticks -= periodic.Delay
                }
            }
            val idsToRemove = LinkedList<Int>()
            for (oneOffPair in mOneOffs) {
                oneOffPair.value.Ticks += dt
                if (oneOffPair.value.Ticks >= oneOffPair.value.Delay) {
                    toCall.add(oneOffPair.value.Callback)
                    idsToRemove.add(oneOffPair.key)
                }
            }
            for (toRemove in idsToRemove)
                mOneOffs.remove(toRemove)
            for (callback in toCall)
                callback?.invoke()
        }
    }

    fun schedulePeriodic(period: Long, callback: () -> Unit) : Int {
        mLock.withLock {
            mID += 1
            mPeriodics[mID] = DelayedCallback(0, period, callback)
            return mID
        }
    }

    fun scheduleOneOff(delay: Long, callback: () -> Unit) : Int {
        mLock.withLock{
            mID += 1
            mOneOffs[mID] = DelayedCallback(0, delay, callback)
            return mID
        }
    }

    fun cancelByID(id: Int) {
        mLock.withLock {
            if (mPeriodics.containsKey(id))
                mPeriodics.remove(id)
            if (mOneOffs.containsKey(id))
                mPeriodics.remove(id)
        }
    }

    fun clear() {
        mLock.withLock {
            mPeriodics.clear()
            mOneOffs.clear()
        }
    }

    fun zero() {
        mLock.withLock {
            for (periodic in mPeriodics.values)
                periodic.Ticks = 0L
            for (oneOff in mOneOffs.values)
                oneOff.Ticks = 0L
        }
    }

    @Suppress("unused")
    private inner class DelayedCallback(var Ticks: Long, val Delay: Long, val Callback: (() -> Unit)?)
}