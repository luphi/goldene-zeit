package luphi.goldenezeit.`interface`

import luphi.goldenezeit.Input

interface IInteractable {
    fun handleInput(input: Input)
}