package luphi.goldenezeit.`interface`

@Suppress("PropertyName")
interface IEquipment {
    val StrengthBonus: Int
    val VitalityBonus: Int
    val AgilityBonus: Int
    val SenseBonus: Int
}