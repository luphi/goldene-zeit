package luphi.goldenezeit.`interface`

interface IProgressive {
    fun step(dt: Long)
}