package luphi.goldenezeit.`interface`

import luphi.goldenezeit.Rect

@Suppress("PropertyName")
interface IDrawable {
    // Defines the position and dimensions of the this drawable, can be thought of as an axis-aligned bounding box.
    val Rect: Rect
    // Performs the drawing; always executed on the draw (OpenGL) thread. The parameter represents the difference in time, in milliseconds, since the last draw.
    fun draw(dt: Long)
}