package luphi.goldenezeit.`interface`

import luphi.goldenezeit.Input
import luphi.goldenezeit.mode.MTransition

@Suppress("PropertyName")
interface IMode {
    val Transition: MTransition?
    fun init(parameters: MTransition)
    fun draw(dt: Long)
    fun step(dt: Long)
    fun handleInput(input: Input)
}