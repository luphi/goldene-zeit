package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.Tasks
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import java.util.LinkedList
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.math.*

class UITransition(private val mOnDone: () -> Unit) : IDrawable, IProgressive {
    enum class Type { SINGLE_CIRCLE, DOUBLE_CIRCLE, HORIZONTAL_STRIPES, VERTICAL_STRIPES, INWARD_SPIRAL, OUTWARD_SPIRAL, SHRINK, SPLIT }

    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    private val mTasks = Tasks()
    private val mPrimitives = LinkedList<GLPrimitive>()
    private val mPrimitivesLock = ReentrantLock()
    private var mGridHeight = 0 // Height of the grid of "pixels" used to black out the screen
    private var mGridWidth = 0 // Width of the grid of "pixels" used to black out the screen
    private var mGridPixelHeightNDC = 0f // Width of a grid "pixel" in NDC
    private var mGridPixelWidthNDC = 0f // Height of a grid "pixel" in NDC
    private var mIsInitialized = false

    fun init(type: Type) {
        if (gGZ.Camera.AspectRatio > 1.0) {
            mGridHeight = 18
            mGridWidth = (mGridHeight.toFloat() * gGZ.Camera.AspectRatio).toInt()
            // In order to make things easier by avoiding rounding down due to integer division (e.g. a grid width of 21 resulting in 21 / 2 = 10 for some
            // steps), the grid dimensions will be incremented to a multiple of two
            if (mGridWidth % 2 == 1)
                mGridWidth += 1
        } else {
            mGridWidth = 18
            mGridHeight = (mGridWidth.toFloat() / gGZ.Camera.AspectRatio).toInt()
            if (mGridHeight % 2 == 1)
                mGridHeight += 1
        }
        mGridPixelHeightNDC = gGZ.Camera.Height / mGridHeight.toFloat()
        mGridPixelWidthNDC = gGZ.Camera.Width / mGridWidth.toFloat()
        // Initializations
        when (type) {
            Type.HORIZONTAL_STRIPES -> {
                for (i in 0 until mGridHeight) {
                    val bar = GLPrimitive()
                    bar.init(GLPrimitive.Shape.QUAD)
                    bar.Color = floatArrayOf(0f, 0f, 0f)
                    bar.IsStatic = true
                    bar.size(0f, mGridPixelHeightNDC)
                    bar.move(if (i % 2 == 0) gGZ.Camera.LeftEdge else gGZ.Camera.RightEdge, gGZ.Camera.TopEdge - mGridPixelHeightNDC * (i + 1).toFloat())
                    mPrimitivesLock.withLock { mPrimitives.add(bar) }
                }
            }
            Type.VERTICAL_STRIPES -> {
                for (i in 0 until mGridWidth) {
                    val bar = GLPrimitive()
                    bar.init(GLPrimitive.Shape.QUAD)
                    bar.Color = floatArrayOf(0f, 0f, 0f)
                    bar.IsStatic = true
                    bar.size(mGridPixelWidthNDC, 0f)
                    bar.move(gGZ.Camera.LeftEdge + mGridPixelWidthNDC * i.toFloat(), if (i % 2 == 0) gGZ.Camera.TopEdge else gGZ.Camera.BottomEdge)
                    mPrimitivesLock.withLock { mPrimitives.add(bar) }
                }
            }
            Type.SHRINK -> {
                for (i in 0..3) {
                    val quad = GLPrimitive()
                    quad.init(GLPrimitive.Shape.QUAD)
                    quad.IsStatic = true
                    quad.Color = floatArrayOf(0f, 0f, 0f)
                    mPrimitivesLock.withLock { mPrimitives.add(quad) }
                }
            }
            Type.SPLIT -> {
                for (i in 0..1) {
                    val quad = GLPrimitive()
                    quad.init(GLPrimitive.Shape.QUAD)
                    quad.IsStatic = true
                    quad.Color = floatArrayOf(0f, 0f, 0f)
                    mPrimitivesLock.withLock { mPrimitives.add(quad) }
                }
            }
            else -> {
            }
        }
        // Periodic tasks
        val duration = 2f // Seconds
        val frequency = 25f // Hz
        when (type) {
            Type.SINGLE_CIRCLE -> {
                var theta = 0.0
                val points = LinkedList<Point>()
                mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                    // dTheta = 2 * pi / (dt / duration ticks)
                    val dTheta = 2.0 * Math.PI / (frequency * duration)
                    val fill = pointsBetween(theta, theta + dTheta)
                    if (theta >= 2f * Math.PI) {
                        mTasks.clear()
                        mTasks.schedulePeriodic(100L, mOnDone) // Let the screen stay black for a moment
                    }
                    theta += dTheta
                    for (point in fill) {
                        if (!points.contains(point)) {
                            points.add(point)
                            // Note: the pixel must be created here or the primitives list mutex lock and renderer mutex lock could cause a deadlock
                            val pixel = pixelAt(point.X, point.Y)
                            mPrimitivesLock.withLock { mPrimitives.add(pixel) }
                        }
                    }
                }
            }
            Type.DOUBLE_CIRCLE -> {
                var theta = 0.0
                val dtheta = Math.PI / (frequency * duration) // Change in theta per callback
                val points1 = LinkedList<Point>()
                val points2 = LinkedList<Point>()
                mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                    val fill1 = pointsBetween(theta, theta + dtheta)
                    val fill2 = pointsBetween(theta + Math.PI, theta + dtheta + Math.PI)
                    if (theta >= Math.PI) {
                        mTasks.clear()
                        mTasks.schedulePeriodic(100L, mOnDone) // Let the screen stay black for a moment
                    }
                    theta += dtheta
                    for (point in fill1) {
                        if (!points1.contains(point)) {
                            points1.add(point)
                            // Note: the pixel must be created here or the primitives list mutex lock and renderer mutex lock could cause a deadlock
                            val pixel = pixelAt(point.X, point.Y)
                            mPrimitivesLock.withLock { mPrimitives.add(pixel) }
                        }
                    }
                    for (point in fill2) {
                        if (!points2.contains(point)) {
                            points2.add(point)
                            val pixel = pixelAt(point.X, point.Y)
                            mPrimitivesLock.withLock { mPrimitives.add(pixel) }
                        }
                    }
                }
            }
            Type.HORIZONTAL_STRIPES -> {
                var t = 0f
                mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                    if (t > duration) {
                        mTasks.clear()
                        mOnDone.invoke()
                    } else {
                        t += (1f / frequency)
                        val barWidth = ((t / duration) * mGridWidth.toFloat()).roundToInt().toFloat() * mGridPixelWidthNDC
                        for ((i, bar) in mPrimitives.withIndex()) {
                            bar.size(barWidth, bar.Rect.Height)
                            if (i % 2 != 0)
                                bar.move(gGZ.Camera.RightEdge - barWidth, bar.Rect.Y)
                        }
                    }
                }
            }
            Type.VERTICAL_STRIPES -> {
                var t = 0f
                mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                    if (t > duration) {
                        mTasks.clear()
                        mTasks.schedulePeriodic(100L, mOnDone) // Let the screen stay black for a moment
                    } else {
                        t += (1f / frequency)
                        val barHeight = ((t / duration) * mGridHeight.toFloat()).roundToInt().toFloat() * mGridPixelHeightNDC
                        for ((i, bar) in mPrimitives.withIndex()) {
                            bar.size(bar.Rect.Width, barHeight)
                            if (i % 2 == 0)
                                bar.move(bar.Rect.X, gGZ.Camera.TopEdge - barHeight)
                        }
                    }
                }
            }
            Type.INWARD_SPIRAL, Type.OUTWARD_SPIRAL -> {
                val pixelQueue = LinkedList<Pair<Int, Int>>() // FILO queue of pixels to draw in a spiralling order
                val totalPixels = mGridWidth * mGridHeight
                val pixelsPerCallback = (totalPixels.toFloat() / (frequency * duration)).toInt() // Number of pixels to remove from the queue per callback
                var edge = Edge.LEFT
                var x = -mGridWidth / 2
                var y = -mGridHeight / 2
                // Fill the pixel queue with the total number of pixels in the grid
                for (i in 0 until totalPixels) {
                    pixelQueue.add(Pair(x, y))
                    val nextX = if (edge == Edge.TOP) x + 1 else if (edge == Edge.BOTTOM) x - 1 else x
                    val nextY = if (edge == Edge.RIGHT) y - 1 else if (edge == Edge.LEFT) y + 1 else y
                    // If it's time to move onto the next direction
                    if ((nextX < (-mGridWidth / 2)) || (nextX == (mGridWidth / 2)) || (nextY < (-mGridHeight / 2)) || (nextY == (mGridHeight / 2)) ||
                        pixelQueue.contains(Pair(nextX, nextY))
                    ) {
                        edge = when (edge) {
                            Edge.LEFT -> Edge.TOP
                            Edge.TOP -> Edge.RIGHT
                            Edge.RIGHT -> Edge.BOTTOM
                            Edge.BOTTOM -> Edge.LEFT
                        }
                        // Determine the next X and Y given the new edge
                        x = if (edge == Edge.TOP) x + 1 else if (edge == Edge.BOTTOM) x - 1 else x
                        y = if (edge == Edge.RIGHT) y - 1 else if (edge == Edge.LEFT) y + 1 else y
                    } else {
                        // Continue on without changing direction
                        x = nextX
                        y = nextY
                    }
                }
                // The pixels are pushed onto the queue with the inward spiralling order so we can get the outward order just by reversing it
                if (type == Type.OUTWARD_SPIRAL)
                    pixelQueue.reverse()
                mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                    // If all the pixels are being drawn
                    if (pixelQueue.isEmpty()) {
                        mTasks.clear()
                        mTasks.schedulePeriodic(100L, mOnDone) // Let the screen stay black for a moment
                    } else {
                        for (i in 0 until pixelsPerCallback) {
                            val pixel = pixelQueue.remove()
                            // Note: the pixel must be created here or the primitives list mutex lock and renderer mutex lock could cause a deadlock
                            val pixelQuad = pixelAt(pixel.first, pixel.second)
                            mPrimitivesLock.withLock { mPrimitives.add(pixelQuad) }
                        }
                    }
                }
            }
            Type.SHRINK -> {
                var t = 0f
                mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                    if (t > duration) {
                        mTasks.clear()
                        mTasks.schedulePeriodic(100L, mOnDone) // Let the screen stay black for a moment
                    } else {
                        val widthInPixels = (max(mGridWidth, mGridHeight).toFloat() * (t / duration) / 2f).roundToInt()
                        t += (1f / frequency)
                        // Left edge
                        mPrimitives[0].move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
                        mPrimitives[0].size(widthInPixels * mGridPixelWidthNDC, gGZ.Camera.Height)
                        // Bottom edge
                        mPrimitives[1].move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
                        mPrimitives[1].size(gGZ.Camera.Width, widthInPixels * mGridPixelHeightNDC)
                        // Right edge
                        mPrimitives[2].move(gGZ.Camera.RightEdge - widthInPixels * mGridPixelWidthNDC, gGZ.Camera.BottomEdge)
                        mPrimitives[2].size(widthInPixels * mGridPixelWidthNDC, gGZ.Camera.Height)
                        // Top edge
                        mPrimitives[3].move(gGZ.Camera.LeftEdge, gGZ.Camera.TopEdge - widthInPixels * mGridPixelHeightNDC)
                        mPrimitives[3].size(gGZ.Camera.Width, widthInPixels * mGridPixelHeightNDC)
                    }
                }
            }
            Type.SPLIT -> {
                var t = 0f
                mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                    if (t > duration) {
                        mTasks.clear()
                        mTasks.schedulePeriodic(100L, mOnDone) // Let the screen stay black for a moment
                    } else {
                        val widthInPixels = (max(mGridWidth, mGridHeight).toFloat() * (t / duration)).roundToInt()
                        t += (1f / frequency)
                        // Vertical component
                        mPrimitives[0].move(-(widthInPixels * mGridPixelWidthNDC / 2f), gGZ.Camera.BottomEdge)
                        mPrimitives[0].size(widthInPixels * mGridPixelWidthNDC, gGZ.Camera.Height)
                        // Horizontal component
                        mPrimitives[1].move(gGZ.Camera.LeftEdge, -(widthInPixels * mGridPixelHeightNDC / 2f))
                        mPrimitives[1].size(gGZ.Camera.Width, widthInPixels * mGridPixelHeightNDC)
                    }
                }
            }
        }
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized)
            return
        mPrimitivesLock.withLock {
            for (primitive in mPrimitives)
                primitive.draw(dt)
        }
    }

    override fun step(dt: Long) = mTasks.step(dt)

    private data class Point constructor(var X: Int, var Y: Int)

    @Suppress("unused")
    private enum class Edge { LEFT, BOTTOM, RIGHT, TOP }

    private fun pointsBetween(theta1: Double, theta2: Double): List<Point> {
        val path: LinkedList<Point> = LinkedList()
        val maxTheta = max(theta1, theta2)
        val minTheta = if (maxTheta == theta1) theta2 else theta1
        val maxLine = linePath(maxTheta)
        val minLine = linePath(minTheta)
        val dx: Int
        val dy: Int
        if ((maxTheta > (Math.PI / 4f)) && (maxTheta <= (3f * Math.PI / 4f))) {
            dx = 1
            dy = 0
        } else if ((maxTheta > (3f * Math.PI / 4f)) && (maxTheta <= (5f * Math.PI / 4f))) {
            dx = 0
            dy = 1
        } else if ((maxTheta > (5f * Math.PI / 4f)) && (maxTheta <= (7f * Math.PI / 4f))) {
            dx = -1
            dy = 0
        } else {
            dx = 0
            dy = -1
        }
        for (point in maxLine) {
            var newPoint = Point(point.X, point.Y)
            while ((newPoint.X >= -mGridWidth / 2) && (newPoint.Y >= -mGridHeight / 2) && (newPoint.X <= mGridWidth / 2) && (newPoint.Y <= mGridHeight / 2)) {
                if (!path.contains(newPoint))
                    path.add(newPoint)
                if (minLine.contains(newPoint))
                    break
                newPoint = Point(newPoint.X + dx, newPoint.Y + dy)
            }
        }
        return path
    }

    private fun linePath(theta: Double): List<Point> {
        // This is basically Bresenham's line algorithm
        val path: LinkedList<Point> = LinkedList()
        var x = 0
        var y = 0
        val dx = abs(cos(theta) * mGridWidth).roundToInt()
        val dy = abs(sin(theta) * mGridHeight).roundToInt()
        val sx = if (cos(theta) < 0f) -1 else 1
        val sy = if (sin(theta) < 0f) -1 else 1
        var err = 0
        while ((x >= -mGridWidth / 2) && (y >= -mGridHeight / 2) && (x <= mGridWidth / 2) && (y <= mGridHeight / 2)) {
            path.add(Point(x, y))
            val derr = err
            if (derr > -dx) {
                err -= dy
                x += sx
            }
            if (derr < dy) {
                err += dx
                y += sy
            }
        }
        return path
    }

    private fun pixelAt(x: Int, y: Int): GLPrimitive {
        val quad = GLPrimitive()
        quad.init(GLPrimitive.Shape.QUAD)
        quad.Color = floatArrayOf(0f, 0f, 0f)
        quad.IsStatic = true
        quad.size(mGridPixelWidthNDC, mGridPixelHeightNDC)
        val quadRect = quad.Rect
        quad.move(quadRect.Width * x, quadRect.Height * y)
        return quad
    }
}