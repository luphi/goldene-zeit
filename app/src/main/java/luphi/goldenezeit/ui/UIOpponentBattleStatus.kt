package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLText

@Suppress("PropertyName")
class UIOpponentBattleStatus : UIBattleStatus() {
    override val Rect: Rect
        get() {
            val arrowRect = mArrow.Rect
            val nameRect = mNameText.Rect
            return Rect(arrowRect.X, arrowRect.Y, arrowRect.Width, nameRect.Y + nameRect.Height - arrowRect.Y)
        }

    private val mNameText = GLText()
    private val mAttackDefenseStyleText = GLText()

    override var HP = 0
        set(value) {
            field = if (value < 0) 0 else value
            mHPBar.Percentage = field.toFloat() / MaxHP.toFloat()
        }
    override var MaxHP = 1
        set(value) {
            field = if (value < 1) 1 else value
            mHPBar.Percentage = HP.toFloat() / field.toFloat()
        }
    override var Level = 1
        set(value) {
            field = if (value < 1) 1 else value
            mLevelText.Text = ":L$field" // e.g. :L1 or :L99
            sizeByWidth(mArrow.Rect.Width)
        }
    var Name = "MISSINGNO."
        set(value) {
            field = value
            mNameText.Text = value
            sizeByWidth(mArrow.Rect.Width)
        }
    var AttackStyle = ""
        set(value) {
            field = value
            mAttackDefenseStyleText.Text = "$value/$DefenseStyle"
            sizeByWidth(mArrow.Rect.Width)
        }
    var DefenseStyle = ""
        set(value) {
            field = value
            mAttackDefenseStyleText.Text = "$AttackStyle/$value"
            sizeByWidth(mArrow.Rect.Width)
        }

    override fun init() {
        super.init()
        mArrow.init(gGZ.Resources.loadTexture("sprites/ui/battle_opponent_status_liner.png"))
        mNameText.init("MISSINGNO.")
        mAttackDefenseStyleText.init("/")
    }

    override fun draw(dt: Long) {
        if (!IsVisible)
            return
        super.draw(dt)
        mNameText.draw(dt)
        mAttackDefenseStyleText.draw(dt)
    }

    fun move(x: Float, y: Float) {
        mArrow.move(x, y)
        val rect = mArrow.Rect
        sizeByWidth(rect.Width) // Repositions the inner drawables (health bar, experience bar, text, etc.)
    }

    fun sizeByWidth(w: Float) {
        mArrow.sizeByWidth(w)
        val arrowRect = mArrow.Rect
        mHPBar.sizeByWidth(w * 0.7f)
        var hpBarRect = mHPBar.Rect
        mHPBar.move(arrowRect.X + arrowRect.Width - hpBarRect.Width, arrowRect.Y + (arrowRect.Height * (4f / 21f)) + (gGZ.Camera.Margin * 2f))
        hpBarRect = mHPBar.Rect
        val hpLabelRect = mHPLabelText.Rect
        mHPLabelText.move(hpBarRect.X - hpLabelRect.Width - gGZ.Camera.Margin, hpBarRect.Y + (hpBarRect.Height / 2f) - (hpLabelRect.Height / 2f))
        mLevelText.move(hpBarRect.X, hpBarRect.Y + hpBarRect.Height + gGZ.Camera.Margin)
        val levelTextRect = mLevelText.Rect
        mAttackDefenseStyleText.move(arrowRect.X, levelTextRect.Y + levelTextRect.Height + gGZ.Camera.Margin)
        val attackDefenseRect = mAttackDefenseStyleText.Rect
        mNameText.move(attackDefenseRect.X, attackDefenseRect.Y + attackDefenseRect.Height + gGZ.Camera.Margin)
    }

}