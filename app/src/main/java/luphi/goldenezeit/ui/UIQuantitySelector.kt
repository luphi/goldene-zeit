package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.gl.GLText

class UIQuantitySelector(max: Int, onSelect: (Int) -> Unit, onCancel: () -> Unit) : IDrawable, IInteractable {
    // IDrawable property
    override val Rect: Rect
        get() = mBubble.Rect

    private val mBubble = UIBubble()
    private val mText = GLText()
    private val mMaxQuantity = max
    private var mQuantity = 1 // The current quantity
    private val mOnSelect: (Int) -> Unit = onSelect
    private val mOnCancel: () -> Unit = onCancel

    fun init() {
        mBubble.init()
        mText.init("x" + "$mQuantity".padStart(2, '0'))
    }

    override fun draw(dt: Long) {
        mBubble.draw(dt)
        mText.draw(dt)
    }

    override fun handleInput(input: Input) {
        if (input.IsPress) {
            when (input.Opcode) {
                Input.Button.A -> mOnSelect.invoke(mQuantity)
                Input.Button.B -> mOnCancel.invoke()
                Input.Button.DPAD_UP -> {
                    if (mQuantity < mMaxQuantity)
                        mQuantity += 1 // Increment
                    else
                        mQuantity = 1 // Wrap around to 1
                    mText.Text = "x" + "$mQuantity".padStart(2, '0')
                }
                Input.Button.DPAD_DOWN -> {
                    if (mQuantity > 1)
                        mQuantity -= 1 // Decrement
                    else
                        mQuantity = mMaxQuantity // Wrap around to the max
                    mText.Text = "x" + "$mQuantity".padStart(2, '0')
                }
                else -> {
                }
            }
        }
    }

    fun autoSize() {
        val textRect = mText.Rect
        mBubble.sizeForUsableWidthAndHeight(textRect.Width, textRect.Height)
    }

    fun move(x: Float, y: Float) {
        mBubble.move(x, y)
        val usableSurface = mBubble.UsableSurface
        mText.move(usableSurface.X, usableSurface.Y)
    }
}