package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.Tasks
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.gl.GLText
import java.util.Locale

class UITitleScreen(private val mOnDone: () -> Unit) : IDrawable,
    IInteractable {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    private val mBackground = GLPrimitive()
    private val mMask = GLPrimitive()
    private val mChapterText = GLText()
    private val mTitleText = GLText()
    private val mSubtitleText = GLText()
    private val mTasks = Tasks() // Owned by the draw thread
    private var mPhase = 0 // 0 = fade in for chapter #, 1 = fade in for title, 2 = fade in for subtitle, 3 = everything's visible
    private var mTicks = 0L // Tracks how long the current phase has been running, moving onto the next one after 1000 ticks (milliseconds)
    private var mIsInitialized = false

    init {
        mBackground.IsStatic = true
        mBackground.Color = floatArrayOf(1f, 1f, 1f) // White
        mMask.IsStatic = true
        mMask.Color = floatArrayOf(1f, 1f, 1f) // White
        mTitleText.IsVisible = false
        mSubtitleText.IsVisible = false
    }

    fun init() {
        val titles = gGZ.Resources.ChapterCatalog["${gGZ.Chapter}"].split(";")
        val title = titles.getOrNull(0) ?: ""
        val subtitle = titles.getOrNull(1) ?: ""
        mBackground.init(GLPrimitive.Shape.QUAD)
        mBackground.size(gGZ.Camera.Width, gGZ.Camera.Height)
        mBackground.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mChapterText.init("CHAPTER ${gGZ.Chapter}")
        mTitleText.init(title.toUpperCase(Locale.getDefault()))
        mSubtitleText.init(subtitle.toUpperCase(Locale.getDefault()))
        // We'll position the title text first because it will appear in the dead center making it easier to position others relative to it
        var titleRect = mTitleText.Rect
        mTitleText.move(-(titleRect.Width / 2f), -(titleRect.Height / 2f))
        titleRect = mTitleText.Rect
        mChapterText.move(-(mChapterText.Rect.Width / 2f), titleRect.Y + (gGZ.FontSize * 3f))
        mSubtitleText.move(-(mSubtitleText.Rect.Width / 2f), titleRect.Y - (gGZ.FontSize * 3f))
        mMask.init(GLPrimitive.Shape.QUAD)
        mMask.size(mChapterText.Rect)
        mMask.move(mChapterText.Rect)
        var phase = mPhase // Tracks the phase to determine if the phase was manually incremented by user input
        val frequency = 30f // 30 Hz
        val dt = (1000f / frequency).toLong()
        // Schedule a task to repeating make the mask more opaque over one of the texts and make text visible depending on phase
        mTasks.schedulePeriodic(dt) {
            if ((mTicks >= 1000L) || (phase != mPhase)) {
                // If the phase should change due to time passing
                if (mTicks >= 1000L) {
                    mPhase += 1
                    mTicks -= 1000L
                }
                // If the phase was manually incremented by a button press
                else
                    mTicks = 0L
                phase = mPhase
                mMask.Alpha = 1f
                when (mPhase) {
                    1 -> {
                        mMask.size(mTitleText.Rect)
                        mMask.move(mTitleText.Rect)
                        mTitleText.IsVisible = true
                    }
                    2 -> {
                        mMask.size(mSubtitleText.Rect)
                        mMask.move(mSubtitleText.Rect)
                        mSubtitleText.IsVisible = true
                    }
                    3 -> {
                        mMask.IsVisible = false
                        mTasks.clear() // Seppuku
                        mTasks.scheduleOneOff(2000L, mOnDone) // Invoke the "on done" callback after 3000 milliseconds
                    }
                }
            }
            mTicks += dt
            mMask.Alpha = 1f - (mTicks.toFloat() / 1000f) // Will range from 1f to 0f with 0f being completely invisible
        }
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized)
            return
        mTasks.step(dt)
        mBackground.draw(dt)
        mChapterText.draw(dt)
        mTitleText.draw(dt) // Initially set with IsVisible = false
        mSubtitleText.draw(dt) // Initially set with IsVisible = false
        mMask.draw(dt)
    }

    override fun handleInput(input: Input) {
        // If the input was a press/down event anywhere within the app's surface
        if (input.IsPress) {
            if (mPhase >= 3)
                mOnDone.invoke()
            else
                mPhase += 1
        }
    }
}