package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.gl.GLText

@Suppress("PropertyName")
class UIPlayerBattleStatus : UIBattleStatus() {
    // IDrawable property
    override val Rect: Rect
        get() {
            val arrowRect = mArrow.Rect
            val levelTextRect = mLevelText.Rect
            return Rect(arrowRect.X, arrowRect.Y, arrowRect.Width, levelTextRect.Y + levelTextRect.Height - arrowRect.Y)
        }

    private val mExperienceBar = GLPrimitive()
    private val mCurrentHPText = GLText()
    private val mMaximumHPText = GLText()

    override var HP = 0
        set(value) {
            field = if (value < 0) 0 else value
            mCurrentHPText.Text = "$field/"
            val hpBarRect = mHPBar.Rect
            mCurrentHPText.move(hpBarRect.X, hpBarRect.Y - gGZ.Camera.Margin - mCurrentHPText.Rect.Height)
            mHPBar.Percentage = field.toFloat() / MaxHP.toFloat()
        }
    override var MaxHP = 1
        set(value) {
            field = if (value < 1) 1 else value
            mMaximumHPText.Text = field.toString()
            val hpBarRect = mHPBar.Rect
            val maximumHPTextRect = mMaximumHPText.Rect
            mMaximumHPText.move(hpBarRect.X + hpBarRect.Width - maximumHPTextRect.Width, hpBarRect.Y - gGZ.Camera.Margin - maximumHPTextRect.Height)
            mHPBar.Percentage = HP.toFloat() / field.toFloat()
        }
    override var Level = 1
        set(value) {
            field = if (value < 1) 1 else value
            mLevelText.Text = ":L$field" // e.g. :L1 or :L99
            val hpLabelTextRect = mHPLabelText.Rect
            val hpBarRect = mHPBar.Rect
            mLevelText.move(hpBarRect.X + (hpBarRect.Width / 2f) - (hpLabelTextRect.Width / 2f), hpBarRect.Y + hpBarRect.Height + gGZ.Camera.Margin)
        }
    var ExperiencePercentage = 1f
        set(value) {
            field = if (value > 1f) 1f else if (value < 0f) 0f else value
            val arrowRect = mArrow.Rect
            // The dimensions of the background/arrow sprite are 77 x 21 (pixels). The experience bar is placed parallel to the arrow between the arrow head and
            // the right border/edge (64 x 2 pixels). The bar expands from the right as experience increases. A 100% (1f) value would result in the whole gap
            // being filled.
            mExperienceBar.size(arrowRect.Width * (64f / 77f) * field, arrowRect.Height * (1.5f / 21f)) // 1.5 "pixels" tall, leaving a gap above the edge
            // The bar is placed in the gap described above, with a Y offset of 0.5 pixels above the edge (with the edge being 2 "pixels" tall). The 5 "pixel"
            // X offset is due to the 5 pixel width of the right border/edge where the bar is to grow from.
            mExperienceBar.move(
                arrowRect.X + (arrowRect.Width * ((77 - 5).toFloat() / 77f)) - mExperienceBar.Rect.Width,
                arrowRect.Y + (arrowRect.Height * (2.5f / 21f))
            )
        }

    init {
        mExperienceBar.IsStatic = true
        mExperienceBar.Color = floatArrayOf(0.04f, 0.471f, 0.973f)
    }

    override fun init() {
        super.init()
        mArrow.init(gGZ.Resources.loadTexture("sprites/ui/battle_player_status_liner.png"))
        mExperienceBar.init(GLPrimitive.Shape.QUAD)
        mCurrentHPText.init("0/")
        mMaximumHPText.init("1")
    }

    override fun draw(dt: Long) {
        if (!IsVisible)
            return
        super.draw(dt)
        mExperienceBar.draw(dt)
        mCurrentHPText.draw(dt)
        mMaximumHPText.draw(dt)
    }

    fun move(x: Float, y: Float) {
        mArrow.move(x, y)
        val rect = mArrow.Rect
        size(rect.Width, rect.Height) // Repositions the inner drawables (health bar, experience bar, text, etc.)
    }

    fun size(w: Float, h: Float) {
        mArrow.size(w, h)
        val arrowRect = mArrow.Rect
        mHPBar.sizeByWidth(w * 0.7f)
        var hpBarRect = mHPBar.Rect
        mHPBar.move(
            arrowRect.X + arrowRect.Width - gGZ.Camera.Margin - hpBarRect.Width - (arrowRect.Width * (2f / 77f)),
            arrowRect.Y + (arrowRect.Height * (4f / 21f)) + (gGZ.Camera.Margin * 2f) + gGZ.FontSize
        )
        hpBarRect = mHPBar.Rect
        val hpLabelRect = mHPLabelText.Rect
        mHPLabelText.move(hpBarRect.X - hpLabelRect.Width - gGZ.Camera.Margin, hpBarRect.Y + (hpBarRect.Height / 2f) - (hpLabelRect.Height / 2f))
        // The following assignments aren't pointless. Their setter methods will set the underlying graphical text and properly position them.
        HP = HP
        MaxHP = MaxHP
        Level = Level
        ExperiencePercentage = ExperiencePercentage
    }

    fun sizeByWidth(w: Float) {
        mArrow.sizeByWidth(w)
        val arrowRect = mArrow.Rect
        size(arrowRect.Width, arrowRect.Height)
    }
}