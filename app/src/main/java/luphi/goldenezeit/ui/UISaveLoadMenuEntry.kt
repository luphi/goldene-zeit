package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLText
import kotlin.math.roundToInt

class UISaveLoadMenuEntry(private val mSlot: Int, onSelect: () -> Unit) : UIMenuEntry(mSlot.toString(), onSelect) {
    // IDrawable property
    override val Rect: Rect
        get() = mBubble.Rect

    private val mBubble = UIBubble()
    private var mSceneName = ""

    // The text will be laid out like:
    // ------------------------------------------------------------------------------
    // |  slot number                                                "Manual save"  |
    // |  chapter                                                     player level  |
    // |  scene name, which may be very long and, if so, will end with "..."        |
    // ------------------------------------------------------------------------------
    //private val mSlotNumberText = GLText() // Rather than create a new variable, mLabelText will be used for the slot number
    private val mChapterText = GLText()
    private val mPlayerLevelText = GLText()
    private val mSceneNameText = GLText()
    private val mSaveTypeText = GLText()

    override val NativeWidth: Float
        get() = mBubble.Rect.Width
    override val NativeHeight: Float
        get() = (gGZ.Camera.WidthWithMargin - (gGZ.Camera.Margin * 2f)) / 3f // Fits three entries evenly within the visible screen (assuming a 1:1 ratio)

    override fun init() {
        mLabelText.init(if (mSlot < 10) "0$mSlot" else mSlot.toString()) // The slot number
        mChapterText.init("Chapter ${gGZ.Record.getDirect("slot${mSlot}_chapter", "?")}")
        mPlayerLevelText.init("Level ${gGZ.Record.getDirect("slot${mSlot}_player_level", "?")}")
        mSceneName = gGZ.Record.getDirect("slot${mSlot}_scene_name", "?")
        mSceneNameText.init(mSceneName)
        val sortedSlotsOnDisk = gGZ.Record.SlotsOnDisk.sorted() // SlotsOnDisk is a HashSet and isn't necessarily in ascending order
        mSaveTypeText.init(
            when (mSlot) {
                // Slot 0 is dedicated to the autosave feature
                0 -> "Autosave"
                // Slot N + 1 is added as an entry when saving as a way to create a new save slot and should be labeled as such
                (sortedSlotsOnDisk.getOrNull(sortedSlotsOnDisk.size - 1) ?: 0 + 1) -> "New save"
                // Any other slot number between 1 and N, inclusive, is a game saved by the user manually
                else -> "Manual save"
            }
        )
        mBubble.init()
        // Although this entry will be sized to fit the contents, it will be resized (or, at least, the width will be changed) to fill out the screen
        mBubble.sizeForUsableWidthAndHeight(
            gGZ.FontSize + maxOf(
                mLabelText.Rect.Width + mSaveTypeText.Rect.Width,
                mChapterText.Rect.Width + mPlayerLevelText.Rect.Width,
                mSceneNameText.Rect.Width
            ), // The maximum of the three rows
            NativeHeight
        )
    }

    override fun draw(dt: Long) {
        mBubble.draw(dt)
        super.draw(dt) // The slot number
        mChapterText.draw(dt)
        mPlayerLevelText.draw(dt)
        mSceneNameText.draw(dt)
        mSaveTypeText.draw(dt)
    }

    override fun size(w: Float, h: Float) {
        mBubble.size(w, h)
        // mWidth and mHeight are not used by this entry type
        move(mX, mY)
    }

    override fun sizeByWidth(w: Float) = size(w, NativeHeight)

    override fun sizeByHeight(h: Float) = size(NativeWidth, h)

    override fun translate(dx: Float, dy: Float) {
        super.translate(dx, dy)
        mBubble.translate(dx, dy)
        mChapterText.translate(dx, dy)
        mPlayerLevelText.translate(dx, dy)
        mSceneNameText.translate(dx, dy)
        mSaveTypeText.translate(dx, dy)
    }

    override fun move(x: Float, y: Float) {
        mX = x
        mY = y
        mBubble.move(mX, mY)
        // ------------------------------------------------------------------------------
        // |  slot number                                                "Manual save"  |
        // |  chapter                                                     player level  |
        // |  scene name, which may be very long and truncated with "..."               |
        // ------------------------------------------------------------------------------
        val usableSurface = mBubble.UsableSurface
        // Move the slot number to the top left corner of the usable surface
        mLabelText.move(usableSurface.X, usableSurface.Y + usableSurface.Height - mPlayerLevelText.Rect.Height) // mLabelText is used for the slot number
        // Move the chapter to the left edge of the usable surface, centered vertically
        mChapterText.move(usableSurface.X, usableSurface.Y + (usableSurface.Height / 2f) - (mChapterText.Rect.Height / 2f))
        // Move the scene's name to the bottom left corner of the usable surface
        if (mSceneName.length * gGZ.FontSize > usableSurface.Width)
            mSceneNameText.Text = mSceneName.substring(0, (usableSurface.Width / gGZ.FontSize).roundToInt() - 3) + "..."
        else
            mSceneNameText.Text = mSceneName
        mSceneNameText.move(usableSurface.X, usableSurface.Y)
        // Move the save type to the top right corner of the usable surface
        val saveTypeRect = mSaveTypeText.Rect
        mSaveTypeText.move(usableSurface.X + usableSurface.Width - saveTypeRect.Width, usableSurface.Y + usableSurface.Height - saveTypeRect.Height)
        // Move the player's level to the right edge of the usable surface, centered vertically
        val playerLevelRect = mPlayerLevelText.Rect
        mPlayerLevelText.move(
            usableSurface.X + usableSurface.Width - playerLevelRect.Width,
            usableSurface.Y + (usableSurface.Height / 2f) - (playerLevelRect.Height / 2f)
        )
    }
}