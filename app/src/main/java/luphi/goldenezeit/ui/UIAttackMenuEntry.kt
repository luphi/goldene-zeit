package luphi.goldenezeit.ui

import luphi.goldenezeit.Addition
import luphi.goldenezeit.gGZ
import java.util.Locale

class UIAttackMenuEntry(val Addition: Addition?, onSelect: () -> Unit, onHover: () -> Unit = {}) :
    UIMenuEntry(Addition?.Name?.toUpperCase(Locale.getDefault()) ?: "-", onSelect, onHover) {
    override val NativeHeight: Float
        get() = gGZ.FontSize
}