package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.gl.GLSprite
import luphi.goldenezeit.gl.GLText
import java.util.LinkedList

class UIPlayerStatAllotment(onDone: () -> Unit) : IDrawable, IInteractable {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    private val mOnDone = onDone
    private val mDrawables = LinkedList<IDrawable>()
    private var mPointsAllottedToStrength = 0
    private var mPointsAllottedToVitality = 0
    private var mPointsAllottedToAgility = 0
    private var mPointsAllottedToSense = 0
    private var mAllottedPoints = 0 // Number of currently allotted (before being applied) total points
    private val mAllottedPointsText = GLText() // Displays the number of total points that have been allotted (before being applied)
    private var mOverviewUsableSurface = Rect(0f, 0f, 0f, 0f)
    private val mAllotmentMenu = UIStatAllotmentMenu(
        { /* onCancel */ mOnDone.invoke() },
        { /* onApply */
            gGZ.Player.StrengthStatAllotted += mPointsAllottedToStrength
            gGZ.Player.VitalityStatAllotted += mPointsAllottedToVitality
            gGZ.Player.AgilityStatAllotted += mPointsAllottedToAgility
            gGZ.Player.SenseStatAllotted += mPointsAllottedToSense
            mOnDone.invoke()
        }
    )

    fun init() {
        // Create a white background to fill the screen.
        // This UI is launched from the player status UI (itself launched from the overworld mode which uses a black clear color making this necessary).
        val background = GLPrimitive()
        background.IsStatic = true
        background.Color = floatArrayOf(1f, 1f, 1f)
        background.init(GLPrimitive.Shape.QUAD)
        background.size(gGZ.Camera.Width, gGZ.Camera.Height)
        background.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mDrawables.add(background)
        // The following areas divide the available surface into this general layout:
        // -------------------------------------------------------------------------
        // |                        | |                                            |
        // |                        | |                                            |
        // |                        | |                                            |
        // |                        | |                                            |
        // |           A            | |                   B                        |
        // |                        | |                                            |
        // |                        | |                                            |
        // |                        | |                                            |
        // |                        | |                                            |
        // |-----------------------------------------------------------------------|
        // |-----------------------------------------------------------------------|
        // |                                                                       |
        // |                                                                       |
        // |                                                                       |
        // |                                 C                                     |
        // |                                                                       |
        // |                                                                       |
        // |                                                                       |
        // |                                                                       |
        // --------------------------------------------------------------------------
        // with A being a sprite depicting the player character, B being a stats overview, and C being the stat allotment menu. The margins between the four are
        // just for aesthetics.
        val usableSurface = Rect(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.BottomEdgeWithMargin, gGZ.Camera.WidthWithMargin, gGZ.Camera.HeightWithMargin)
        val spriteArea = Rect(
            usableSurface.X, // X
            usableSurface.Y + ((usableSurface.Height - gGZ.Camera.Margin) * 0.5f) + gGZ.Camera.Margin, // Y
            ((usableSurface.Width - gGZ.Camera.Margin) * 0.4f), // Width
            ((usableSurface.Height - gGZ.Camera.Margin) * 0.5f) // Height
        )
        val overviewArea = Rect(
            spriteArea.X + spriteArea.Width + gGZ.Camera.Margin, // X
            spriteArea.Y, // Y
            ((usableSurface.Width - gGZ.Camera.Margin) * 0.6f), // Width
            spriteArea.Height // Height
        )
        val allotmentArea = Rect(
            usableSurface.X, // X
            usableSurface.Y, // Y
            usableSurface.Width, // Width
            (usableSurface.Height - gGZ.Camera.Margin) * 0.5f // Height
        )
        // Load an image of the player to display in the top left of the surface and size it based on the aspect ratio of the image versus that of the
        // available space.
        val sprite = GLSprite()
        sprite.IsStatic = true
        sprite.init(gGZ.Resources.loadTexture("sprites/card/card_player_low.png"))
        var spriteRect = sprite.Rect
        if ((spriteRect.Width / spriteRect.Height) < (spriteArea.Width / spriteArea.Height))
            sprite.sizeByHeight(spriteArea.Height * 0.75f)
        else
            sprite.sizeByWidth(spriteArea.Width * 0.75f)
        spriteRect = sprite.Rect
        sprite.move(spriteArea.X + (spriteArea.Width / 2f) - (spriteRect.Width / 2f), spriteArea.Y + (spriteArea.Height / 2f) - (spriteRect.Height / 2f))
        mDrawables.add(sprite)
        // Create a border for the stat overview area
        val overviewBubble = UIInfoBubble()
        overviewBubble.init()
        overviewBubble.size(overviewArea.Width, overviewArea.Height)
        overviewBubble.move(overviewArea.X, overviewArea.Y)
        mDrawables.add(overviewBubble)
        // Create the allotment menu that will handle input events if there are available stat points
        mAllotmentMenu.ArePointsAvailable = gGZ.Player.AvailableStatPoints > 0
        mAllotmentMenu.init()
        mAllotmentMenu.add(
            UIStatAllotmentMenuEntry("STRENGTH", /* initialValue */ gGZ.Player.Strength,
                { /* onChanged */ diff -> onEntryChanged("STRENGTH", diff) })
        )
        mAllotmentMenu.add(
            UIStatAllotmentMenuEntry("VITALITY", /* initialValue */ gGZ.Player.Strength,
                { /* onChanged */ diff -> onEntryChanged("VITALITY", diff) })
        )
        mAllotmentMenu.add(
            UIStatAllotmentMenuEntry("AGILITY", /* initialValue */ gGZ.Player.Strength,
                { /* onChanged */ diff -> onEntryChanged("AGILITY", diff) })
        )
        mAllotmentMenu.add(UIStatAllotmentMenuEntry("SENSE", /* initialValue */ gGZ.Player.Strength, { /* onChanged */ diff -> onEntryChanged("SENSE", diff) }))
        mAllotmentMenu.size(allotmentArea.Width, allotmentArea.Height)
        mAllotmentMenu.move(allotmentArea.X, allotmentArea.Y)
        mDrawables.add(mAllotmentMenu)
        // Fill the overview bubble to display the total available stat points and the number of currently allotted points (before applying)
        mOverviewUsableSurface = overviewBubble.UsableSurface
        val availableText = GLText()
        availableText.init("AVAILABLE")
        availableText.move(mOverviewUsableSurface.X, mOverviewUsableSurface.Y + mOverviewUsableSurface.Height - availableText.Rect.Height)
        mDrawables.add(availableText)
        var statPointsText = GLText()
        statPointsText.init(" STAT POINTS")
        statPointsText.move(mOverviewUsableSurface.X, availableText.Rect.Y - statPointsText.Rect.Height)
        mDrawables.add(statPointsText)
        val availableValueText = GLText()
        availableValueText.init("${gGZ.Player.AvailableStatPoints}")
        val availableValueTextRect = availableValueText.Rect
        availableValueText.move(
            mOverviewUsableSurface.X + mOverviewUsableSurface.Width - availableValueTextRect.Width,
            statPointsText.Rect.Y - availableValueTextRect.Height
        )
        mDrawables.add(availableValueText)
        val allottedText = GLText()
        allottedText.init("ALLOTTED")
        allottedText.move(mOverviewUsableSurface.X, mOverviewUsableSurface.Y + (mOverviewUsableSurface.Height / 2f) - allottedText.Rect.Height)
        mDrawables.add(allottedText)
        statPointsText = GLText()
        statPointsText.init(" STAT POINTS")
        statPointsText.move(mOverviewUsableSurface.X, allottedText.Rect.Y - statPointsText.Rect.Height)
        mDrawables.add(statPointsText)
        mAllottedPointsText.init("0")
        val allottedValueTextRect = availableValueText.Rect
        mAllottedPointsText.move(
            mOverviewUsableSurface.X + mOverviewUsableSurface.Width - allottedValueTextRect.Width,
            statPointsText.Rect.Y - allottedValueTextRect.Height
        )
        mDrawables.add(mAllottedPointsText)
    }

    override fun draw(dt: Long) {
        for (drawable in mDrawables)
            drawable.draw(dt)
    }

    override fun handleInput(input: Input) {
        if (gGZ.Player.AvailableStatPoints > 0)
            mAllotmentMenu.handleInput(input)
        else if (input.IsPress && (input.Opcode == Input.Button.A || input.Opcode == Input.Button.B))
            mOnDone.invoke()
    }

    private fun onEntryChanged(label: String, difference: Int) {
        when (label) {
            "STRENGTH" -> mPointsAllottedToStrength += difference
            "VITALITY" -> mPointsAllottedToVitality += difference
            "AGILITY" -> mPointsAllottedToAgility += difference
            "SENSE" -> mPointsAllottedToSense += difference
        }
        mAllottedPoints += difference // Either +1 or -1
        mAllottedPointsText.Text = mAllottedPoints.toString()
        val allottedPointsTextRect = mAllottedPointsText.Rect
        // If the points changed in number of digits (e.g. 9 -> 10 or 100 -> 99), it will need to be translated so it lines up with the right edge of the bubble
        mAllottedPointsText.move(mOverviewUsableSurface.X + mOverviewUsableSurface.Width - allottedPointsTextRect.Width, allottedPointsTextRect.Y)
        mAllotmentMenu.ArePointsAvailable = mAllottedPoints < gGZ.Player.AvailableStatPoints
    }
}