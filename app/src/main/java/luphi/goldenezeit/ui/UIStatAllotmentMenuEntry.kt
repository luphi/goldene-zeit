package luphi.goldenezeit.ui

import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLSprite
import luphi.goldenezeit.gl.GLText

@Suppress("PropertyName")
class UIStatAllotmentMenuEntry(label: String, initialValue: Int, onChanged: (Int) -> Unit, onHover: () -> Unit = {}) :
    UIMenuEntry(label, /* onSelect */ {}, onHover) {
    private val mValueText = GLText() // Graphical representation of the current value
    private val mDecrementArrow = GLSprite() // Appears to the left of the current value; indicates the value can be lowered
    private val mIncrementArrow = GLSprite() // Appears to the right of the current value
    private val mInitialValue = initialValue // The current value of the stat, before allotting any new points to it
    private var mCurrentValue = initialValue
    private val mOnChanged: ((Int) -> Unit) = onChanged

    internal var IsHovered = false // Set by the parent menu to indicate this entry is the one the user is currently hovering on
    internal var ArePointsAvailable = false // Set by the parent menu to indicate if there are still stat points to be allotted

    init {
        mDecrementArrow.IsStatic = true
        mIncrementArrow.IsStatic = true
    }

    override fun init() {
        super.init()
        mValueText.init(mCurrentValue.toString())
        mDecrementArrow.init(gGZ.Resources.loadTexture("sprites/ui/arrow_left.png"))
        mDecrementArrow.size(gGZ.FontSize, gGZ.FontSize)
        mIncrementArrow.init(gGZ.Resources.loadTexture("sprites/ui/arrow_right.png"))
        mIncrementArrow.size(mDecrementArrow.Rect)
    }

    override fun draw(dt: Long) {
        super.draw(dt)
        mValueText.draw(dt)
        if (IsHovered) {
            if (mCurrentValue > mInitialValue)
                mDecrementArrow.draw(dt)
            if (ArePointsAvailable)
                mIncrementArrow.draw(dt)
        }
    }

    override fun move(x: Float, y: Float) {
        super.move(x, y)
        val labelY = mLabelText.Rect.Y
        val arrowWidth = mIncrementArrow.Rect.Width
        mIncrementArrow.move(mX + mWidth - arrowWidth, labelY)
        mValueText.move(mIncrementArrow.Rect.X - mValueText.Rect.Width - gGZ.FontSize, labelY)
        mDecrementArrow.move(mValueText.Rect.X - (gGZ.FontSize * 2f), mIncrementArrow.Rect.Y)
    }

    override fun translate(dx: Float, dy: Float) {
        super.translate(dx, dy)
        mValueText.translate(dx, dy)
        mDecrementArrow.translate(dx, dy)
        mIncrementArrow.translate(dx, dy)
    }

    fun decrement() {
        if (mCurrentValue <= mInitialValue)
            return
        mCurrentValue -= 1
        mValueText.Text = mCurrentValue.toString()
        mOnChanged.invoke(-1)
    }

    fun increment() {
        if (!ArePointsAvailable)
            return
        mCurrentValue += 1
        mValueText.Text = mCurrentValue.toString()
        mOnChanged.invoke(1)
    }
}