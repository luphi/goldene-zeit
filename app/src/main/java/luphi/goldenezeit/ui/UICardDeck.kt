package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.card.Card
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLSprite

@Suppress("PropertyName")
class UICardDeck(private val mIsPlayer: Boolean, private val mOnSelect: (Int) -> Unit) : IDrawable, IInteractable {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(mX, mY, mWidth, mHeight)

    private var mX = 0f
    private var mY = 0f
    private var mWidth = 0f
    private var mHeight = 0f
    private var mCardWidth = 0f
    private var mCardHeight = 0f
    private val mCursor = GLSprite()
    private var mCursorMargin = 0f // Margin provided for the cursor
    private var mHoveredIndex = -1

    val Cards = arrayOfNulls<Card>(5)
    var Count = 0
    val HoveredIndex
        get() = mHoveredIndex
    var IsFocused = false
        set(value) {
            field = value
            mCursor.IsVisible = value
        }

    init {
        mCursor.IsStatic = true
        mCursor.IsMirrored = !mIsPlayer // The opponent's card are placed on the screen's left edge with the cursor to the right, so point the cursor left
    }

    fun init() = mCursor.init(gGZ.Resources.loadTexture("sprites/ui/arrow_right.png"))

    override fun draw(dt: Long) {
        for (card in Cards)
            card?.draw(dt)
        if (Cards.getOrNull(mHoveredIndex) != null)
            mCursor.draw(dt)
    }

    override fun handleInput(input: Input) {
        if (input.IsPress) {
            when (input.Opcode) {
                Input.Button.DPAD_UP -> {
                    var toHoverOn: Int? = null
                    var i = if (mHoveredIndex >= Cards.size) Cards.size - 1 else mHoveredIndex - 1
                    while (i >= 0) {
                        if (Cards.getOrNull(i) != null) {
                            toHoverOn = i
                            break
                        }
                        i -= 1
                    }
                    if (toHoverOn != null)
                        hoverOnIndex(toHoverOn)
                }
                Input.Button.DPAD_DOWN -> {
                    var toHoverOn: Int? = null
                    var i = if (mHoveredIndex < 0) 0 else mHoveredIndex + 1
                    while (i < Cards.size) {
                        if (Cards.getOrNull(i) != null) {
                            toHoverOn = i
                            break
                        }
                        i += 1
                    }
                    if (toHoverOn != null)
                        hoverOnIndex(toHoverOn)
                }
                Input.Button.A -> mOnSelect(mHoveredIndex)
                else -> {
                }
            }
        }
    }

    fun move(x: Float, y: Float) {
        mX = x
        mY = y
        val verticalSpacing = (mHeight - mCardHeight) / 4f // A fifth (due to five cards) of the effective vertical height
        for ((i, card) in Cards.withIndex())
            card?.move(if (mIsPlayer) mX + mWidth - mCardWidth else mX, mY + mHeight - mCardHeight - (i * verticalSpacing))
        hoverOnIndex(mHoveredIndex) // Positions the cursor appropriately
    }

    fun size(w: Float, h: Float) {
        mWidth = w
        mHeight = h
        mCursorMargin = mWidth - mCardWidth // Horizontal space remaining after accounting for the card and margin on both sides
        mCursor.sizeByWidth(mCursorMargin * 0.45f)
        move(mX, mY) // Positions the cards and cursor
    }

    fun add(card: Card) {
        if (Count >= 5)
            return
        Cards[Count] = card
        Count += 1
        // If this was the first card)
        if (Count == 1) {
            // Use its dimensions to calculate a few things
            mCardWidth = Cards.getOrNull(0)?.Rect?.Width ?: gGZ.Camera.Width / 6f
            mCardHeight = Cards.getOrNull(0)?.Rect?.Height ?: mCardWidth
            size(mWidth, mHeight) // Also calls on move()
        }
        else
            move(mX, mY) // Positions the cards
    }

    fun remove(): Card {
        if (Count == 0)
            return Card(-1, 1, 1, 1, 1, 1) // Dummy
        val card = Cards[Count - 1]!!
        Cards[Count - 1] = null
        Count -= 1
        return card
    }

    fun remove(index: Int): Card {
        if ((Count == 0) || (Cards.getOrNull(index) == null))
            return Card(-1, 1, 1, 1, 1, 1) // Dummy
        val card = Cards[index]!!
        Cards[index] = null
        Count -= 1
        return card
    }

    fun hoverOnIndex(index: Int) {
        val previousIndex = mHoveredIndex
        mHoveredIndex = index
        var card = Cards.getOrNull(previousIndex)
        card?.move(if (mIsPlayer) mX + mWidth - mCardWidth else mX, card.Rect.Y) // Move the card (if not null) to its home position
        card = Cards.getOrNull(mHoveredIndex)
        card?.translate(if (mIsPlayer) -mCursorMargin / 2f else mCursorMargin / 2f, 0f) // Protrude the card (if not null) by half the cursor margin
        if (card != null)
            mCursor.move(if (mIsPlayer) mX else mX + mWidth - mCursor.Rect.Width, card.Rect.Y + (mCardHeight / 2f) - (mCursor.Rect.Height / 2f))
    }

    fun hoverOnFirstOrDefault() {
        for ((i, card) in Cards.withIndex()) {
            if (card != null) {
                hoverOnIndex(i)
                return
            }
        }
        hoverOnIndex(-1)
    }
}