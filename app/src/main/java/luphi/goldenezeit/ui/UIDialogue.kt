package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLSprite

class UIDialogue(private var mOnDone: (() -> Unit)? = null) : UIReusableDialogue(), IInteractable {
    private var mIsInitialized = false
    private val mMoreIndicator = GLSprite()
    private var mShouldDrawMoreIndicator = false

    init {
        mMoreIndicator.IsStatic = true
    }

    fun init(text: String) {
        super.init()
        // Create a "has more lines" indicator that will, when there are more lines, appear in the bottom right of the dialogue
        mMoreIndicator.init(gGZ.Resources.loadTexture("sprites/ui/arrow_down.png"))
        mMoreIndicator.size(gGZ.FontSize * 0.75f, gGZ.FontSize * 0.75f)
        val usableArea = mBubble.UsableSurface
        mMoreIndicator.move(usableArea.X + usableArea.Width - mMoreIndicator.Rect.Width, mLine2Rect.Y)
        Text = text // Generate the graphical text, align it, and so on
        mTasks.clear() // The parent init() may have scheduled a periodic task to cycle through lines but that's left to user input in this class
        mTasks.schedulePeriodic(650) { mShouldDrawMoreIndicator = !mShouldDrawMoreIndicator }
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized)
            return
        super.draw(dt)
        if (mShouldDrawMoreIndicator)
            mMoreIndicator.draw(dt)
    }

    override fun handleInput(input: Input) {
        // If the input was a press/down event anywhere within the app's surface
        if (input.IsPress) {
            if (mGLTexts.size - mLineIndex > 2)
                nextLines()
            else
                mOnDone?.invoke()
        }
    }
}