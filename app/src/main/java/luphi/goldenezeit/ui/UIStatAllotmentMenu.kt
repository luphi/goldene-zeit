package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.gl.GLText
import kotlin.concurrent.withLock

@Suppress("PropertyName")
class UIStatAllotmentMenu(onCancel: () -> Unit, private val mOnApply: () -> Unit) : UIMenu(onCancel) {
    private val mCancelText = GLText()
    private val mApplyText = GLText()

    override val CurrentlySelected: String
        get() = when (mHoveredIndex) {
            mEntries.size -> "CANCEL"
            mEntries.size + 1 -> "APPLY"
            else -> mEntries.getOrNull(mHoveredIndex)?.Label ?: ""
        }
    var ArePointsAvailable = false // Indicates if there are still stat points to be allotted
        set(value) {
            field = value
            for (entry in mEntries)
                (entry as UIStatAllotmentMenuEntry).ArePointsAvailable = field
        }

    override fun init() {
        super.init()
        mCancelText.init("CANCEL")
        mApplyText.init("APPLY")
        // If there are available points, then this menu has a use and we'll default to hovering on the first entry
        if (ArePointsAvailable)
            hoverOnIndex(0)
    }

    override fun draw(dt: Long) {
        if (!mIsMoved || !mIsSized)
            return
        super.draw(dt)
        mCancelText.draw(dt)
        mApplyText.draw(dt)
    }

    override fun handleInput(input: Input) {
        if (input.IsPress) {
            when (input.Opcode) {
                Input.Button.A -> {
                    when (mHoveredIndex) {
                        mEntries.size -> mOnCancel.invoke() // If "CANCEL" is selected
                        mEntries.size + 1 -> mOnApply.invoke() // If "APPLY" is selected
                        else -> (mEntries.getOrNull(mHoveredIndex) as UIStatAllotmentMenuEntry?)?.increment()
                    }
                    input.Opcode = Input.Button.NO_OP // Neuter the event before handing it to super.handleUIEvent()
                }
                Input.Button.DPAD_LEFT -> {
                    // If "APPLY" is selected
                    if (mHoveredIndex == mEntries.size + 1)
                        hoverOnIndex(mHoveredIndex - 1) // Hover on "CANCEL"
                    else
                        (mEntries.getOrNull(mHoveredIndex) as UIStatAllotmentMenuEntry?)?.decrement()
                }
                Input.Button.DPAD_RIGHT -> {
                    // If "CANCEL" is selected
                    if (mHoveredIndex == mEntries.size)
                        hoverOnIndex(mHoveredIndex + 1) // Hover on "APPLY"
                    else
                        (mEntries.getOrNull(mHoveredIndex) as UIStatAllotmentMenuEntry?)?.increment()
                }
                else -> {
                }
            }
        }
        super.handleInput(input)
    }

    override fun translate(dx: Float, dy: Float) {
        super.translate(dx, dy)
        mCancelText.translate(dx, dy)
        mApplyText.translate(dx, dy)
    }

    override fun size(w: Float, h: Float) {
        mBubble.size(w, h)
        val usableSurface = mBubble.UsableSurface
        mCancelText.move(usableSurface.X + mCursorPadding, usableSurface.Y)
        mApplyText.move(usableSurface.X + usableSurface.Width - mApplyText.Rect.Width, usableSurface.Y)
        val entryHeight = (usableSurface.Height - (mCancelText.Rect.Height * 2f)) / mEntries.size
        for (i in mEntries.indices) {
            mEntries[i].size(usableSurface.Width - mCursorPadding, entryHeight)
            if (i == 0)
                mEntries[i].move(usableSurface.X + mCursorPadding, usableSurface.Y + usableSurface.Height - entryHeight)
            else
                mEntries[i].move(usableSurface.X + mCursorPadding, mEntries[i - 1].Rect.Y - entryHeight)
        }
        hoverOnIndex(mHoveredIndex) // Used here to move the cursor and any other related graphical changes
        mIsSized = true
    }

    internal fun add(entry: UIStatAllotmentMenuEntry) {
        if (mIsInitialized)
            entry.init()
        entry.ArePointsAvailable = ArePointsAvailable
        mEntriesLock.withLock { mEntries.add(entry) }
        // If there are available points and this is the first entry, then this menu has a use and we'll default to hovering on the first entry
        if (ArePointsAvailable && (mEntries.size == 1))
            hoverOnIndex(0)
    }

    override fun hoverOnIndex(index: Int) {
        if ((index < 0) || (index > mEntries.size + 1))
            return
        // If an actual entry was/is currently hovered on, flag is as "not hovered" (so it doesn't display its indicators)
        (mEntries.getOrNull(mHoveredIndex) as UIStatAllotmentMenuEntry?)?.IsHovered = false
        mHoveredIndex = index
        val selectedRect: Rect
        when (mHoveredIndex) {
            mEntries.size -> selectedRect = mCancelText.Rect // If "CANCEL" is being selected
            mEntries.size + 1 -> selectedRect = mApplyText.Rect // If "APPLY" is being selected
            else -> { // If an actual entry is being selected
                (mEntries[mHoveredIndex] as UIStatAllotmentMenuEntry).IsHovered = true
                selectedRect = mEntries[mHoveredIndex].Rect
            }
        }
        val cursorRect = mCursor.Rect
        mCursor.move(selectedRect.X - (mCursorPadding / 2f) - (cursorRect.Width / 2f), selectedRect.Y + (selectedRect.Height / 2f) - (cursorRect.Height / 2f))
        (mEntries.getOrNull(mHoveredIndex) as UIStatAllotmentMenuEntry?)?.OnHover?.invoke()
    }
}