package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.gGZ
import kotlin.concurrent.withLock
import kotlin.math.max

// The attack menu is exclusively used by the battle mode, appears at the bottom of the screen, uses the full screen width but only enough height to display the
// available options, has up to four entries listing the player's currently-configured additions, and is launched from and overlaps the battle menu
class UIAttackMenu(onCancel: () -> Unit) : UIMenu(onCancel) {
    private val mBlurb = UIBlurb()

    init {
        // This menu intentionally sizes the bubble to perfectly fit all entries so, while this menu theoretically uses this flag, it doesn't in practice
        IsEvenlySpaced = false
    }

    override fun init() {
        super.init()
        mBlurb.init("TYPE/\n ")
        autoSize()
        move(gGZ.Camera.RightEdge - mBubble.Rect.Width, gGZ.Camera.BottomEdge) // Place this in the bottom right of the screen
        hoverOnIndex(0)
    }

    override fun draw(dt: Long) {
        super.draw(dt)
        mBlurb.draw(dt)
    }

    override fun translate(dx: Float, dy: Float) {
        super.translate(dx, dy)
        mBlurb.translate(0f, dy)
    }

    override fun autoSize() {
        mIsAutosized = true
        if (!mIsInitialized || mEntries.isEmpty()) {
            if (mEntries.isEmpty())
                mBubble.sizeForUsableWidthAndHeight(0f, 0f)
            return
        }
        var maxEntryWidth = 0f
        for (entry in mEntries)
            maxEntryWidth = max(entry.NativeWidth, maxEntryWidth)
        // Size the bubble to fit the widest entry (and padding for the cursor) with enough height to fit four entries
        mBubble.sizeForUsableWidthAndHeight(maxEntryWidth + mCursorPadding, gGZ.FontSize * 4f)
        doEntries()
        val bubbleUsableSurface = mBubble.UsableSurface
        // Move the "TYPE" blurb to the left edge of the screen and slightly overlap the bubble
        mBlurb.move(gGZ.Camera.LeftEdge, bubbleUsableSurface.Y + bubbleUsableSurface.Height + gGZ.Camera.Margin)
        mIsSized = true
    }

    override fun handleInput(input: Input) {
        if (input.IsPress)
            when (input.Opcode) {
                Input.Button.A -> mEntries.getOrNull(mHoveredIndex)?.OnSelect?.invoke() // Select the hovered entry (if one exists)
                Input.Button.B -> mOnCancel.invoke()
                Input.Button.DPAD_UP -> hoverOnIndex(if (mHoveredIndex > 0) mHoveredIndex - 1 else mEntries.size - 1)
                Input.Button.DPAD_DOWN -> hoverOnIndex(if (mHoveredIndex < mEntries.size - 1) mHoveredIndex + 1 else 0)
                else -> {
                }
            }
    }

    fun add(entry: UIAttackMenuEntry) {
        if (mEntries.size < 4) {
            if (mIsInitialized)
                entry.init()
            mEntriesLock.withLock { mEntries.add(entry) }
        }
    }

    override fun hoverOnIndex(index: Int) {
        super.hoverOnIndex(index)
        val line1 = "TYPE/"
        val line2 = " ${(mEntries[mHoveredIndex] as UIAttackMenuEntry).Addition?.Style ?: "NONE"}"
        val line3 = ((mEntries[mHoveredIndex] as UIAttackMenuEntry).Addition?.Adds?.size?.toString() ?: "0").padStart(max(line1.length, line2.length))
        // The blurb will look something like:
        //    -------
        //   |TYPE/ |
        //   | SLASH|
        //   |     2|
        //   -------
        mBlurb.Text = "$line1\n$line2\n$line3"
    }
}