package luphi.goldenezeit.ui

import luphi.goldenezeit.*
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.gl.GLSprite
import luphi.goldenezeit.gl.GLText
import java.util.LinkedList

@Suppress("PrivatePropertyName")
class UITownMap(onCancel: () -> Unit, onSelected: (GZ.Town) -> Unit) : IDrawable, IInteractable {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    // These constants correspond to measurements of/within the town map sprite(s) and must match it for the cursor's size and position to be calculated
    private val SPRITE_WIDTH_IN_PIXELS = 160f
    private val SPRITE_HEIGHT_IN_PIXELS = 136f
    private val TOWN_WIDTH_IN_PIXELS = 8f
    private val TOWN_HEIGHT_IN_PIXELS = 8f
    private val CURSOR_WIDTH_IN_PIXELS = TOWN_WIDTH_IN_PIXELS * 2f
    private val CURSOR_HEIGHT_IN_PIXELS = TOWN_HEIGHT_IN_PIXELS * 2f

    // These constants are used as representations of specific towns and also correspond to indices in mTownRects and mTownNames
    private val ERSTEBURG_INDEX = 0
    private val ZWEITEBURG_INDEX = 1
    private val DRITTEBURG_INDEX = 2
    private val KRONE_INDEX = 3
    private val SCHICKSALBURG_INDEX = 4
    private val SCHLACHFELT_INDEX = 5
    private val ENDBURG_INDEX = 6
    private val ROTERSEE_INDEX = 7
    private val EINFACHESLEBEN_INDEX = 8
    private val BERGDORF_INDEX = 9
    private val HULLE_GRANZ_INDEX = 10

    private var mIsInitialized = false
    private val mOnCancel = onCancel
    private val mOnSelected = onSelected
    private val mTasks = Tasks()
    private val mMapBackground1 = GLSprite()
    private val mMapBackground2 = GLSprite()
    private val mMapForeground = GLSprite()
    private val mTextBackground = GLPrimitive()
    private val mText = GLText()
    private val mCursor = GLSprite()
    private val mTownRects = LinkedList<Rect>()
    private val mTownNames = LinkedList<String>()
    private var mShouldDrawBackground1 = true
    private var mShouldDrawCursor = true
    private var mCurrentIndex = ERSTEBURG_INDEX

    init {
        mMapBackground1.IsStatic = true
        mMapBackground2.IsStatic = true
        mMapForeground.IsStatic = true
        mTextBackground.IsStatic = true
        mTextBackground.Color = floatArrayOf(1f, 1f, 1f) // White
        mCursor.IsStatic = true
        mTownRects.add(Rect(32f, 40f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Ersteburg
        mTownRects.add(Rect(32f, 64f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Zweiteburg
        mTownRects.add(Rect(32f, 104f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Dritteburg
        mTownRects.add(Rect(96f, 112f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Krone
        mTownRects.add(Rect(96f, 88f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Schicksalburg
        mTownRects.add(Rect(72f, 88f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Schlachtfeld
        mTownRects.add(Rect(96f, 56f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Endburg
        mTownRects.add(Rect(80f, 24f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Rotersee
        mTownRects.add(Rect(128f, 88f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Einfachesleben
        mTownRects.add(Rect(32f, 8f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Bergdorf
        mTownRects.add(Rect(16f, 112f, TOWN_WIDTH_IN_PIXELS, TOWN_HEIGHT_IN_PIXELS)) // Hülle Granz
        mTownNames.add("ERSTEBURG")
        mTownNames.add("ZWEITEBURG")
        mTownNames.add("DRITTEBURG")
        mTownNames.add("KRONE")
        mTownNames.add("SCHICKSALBURG")
        mTownNames.add("SCHALCHTFELD")
        mTownNames.add("ENDBURG")
        mTownNames.add("ROTERSEE")
        mTownNames.add("EINFACHESLEBEN")
        mTownNames.add("BERGDORF")
        mTownNames.add("HÜLLE GRANZ")
    }

    fun init() {
        mMapBackground1.init(gGZ.Resources.loadTexture("sprites/ui/town_map_background_1.png"))
        mMapBackground1.sizeByWidth(gGZ.Camera.Width)
        mMapBackground1.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mMapBackground2.init(gGZ.Resources.loadTexture("sprites/ui/town_map_background_2.png"))
        mMapBackground2.sizeByWidth(gGZ.Camera.Width)
        mMapBackground2.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mMapForeground.init(gGZ.Resources.loadTexture("sprites/ui/town_map_foreground.png"))
        mMapForeground.sizeByWidth(gGZ.Camera.Width)
        mMapForeground.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mTextBackground.init(GLPrimitive.Shape.QUAD)
        mTextBackground.size(gGZ.Camera.Width, gGZ.Camera.Height - mMapForeground.Rect.Height)
        mTextBackground.move(gGZ.Camera.LeftEdge, gGZ.Camera.TopEdge - mTextBackground.Rect.Height)
        mText.init(mTownNames[ERSTEBURG_INDEX])
        mText.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdge - (mTextBackground.Rect.Height / 2f) - (gGZ.FontSize / 2f))
        mCursor.init(gGZ.Resources.loadTexture("sprites/ui/town_map_cursor.png"))
        val foregroundRect = mMapForeground.Rect
        mCursor.size(
            (CURSOR_WIDTH_IN_PIXELS / SPRITE_WIDTH_IN_PIXELS) * foregroundRect.Width,
            (CURSOR_HEIGHT_IN_PIXELS / SPRITE_HEIGHT_IN_PIXELS) * foregroundRect.Height
        )
        hoverOnIndex(ERSTEBURG_INDEX)
        // Schedule a task to switch between the two water backgrounds
        mTasks.schedulePeriodic(500L) { mShouldDrawBackground1 = !mShouldDrawBackground1 }
        // Schedule two tasks with the same period, but out of sync, that will flash the cursor with a 75% duty cycle
        mTasks.schedulePeriodic(750L) { mShouldDrawCursor = true }
        mTasks.scheduleOneOff((750f * 0.75f).toLong()) {
            mShouldDrawCursor = false
            mTasks.schedulePeriodic(750L) { mShouldDrawCursor = false }
        }
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized)
            return
        if (mShouldDrawBackground1)
            mMapBackground1.draw(dt)
        else
            mMapBackground2.draw(dt)
        mMapForeground.draw(dt)
        if (mShouldDrawCursor)
            mCursor.draw(dt)
        mTextBackground.draw(dt)
        mText.draw(dt)
        mTasks.step(dt)
    }

    override fun handleInput(input: Input) {
        if (input.IsPress) {
            when (input.Opcode) {
                Input.Button.A -> mOnSelected.invoke(when (mCurrentIndex) {
                    ZWEITEBURG_INDEX -> GZ.Town.ZWEITEBURG
                    DRITTEBURG_INDEX -> GZ.Town.DRITTEBURG
                    KRONE_INDEX -> GZ.Town.KRONE
                    SCHICKSALBURG_INDEX -> GZ.Town.SCHICKSALBURG
                    SCHLACHFELT_INDEX -> GZ.Town.SCHLACHTFELD
                    ENDBURG_INDEX -> GZ.Town.ENDBURG
                    ROTERSEE_INDEX -> GZ.Town.ROTERSEE
                    EINFACHESLEBEN_INDEX -> GZ.Town.EINFACHESLEBEN
                    BERGDORF_INDEX -> GZ.Town.BERGDORF
                    HULLE_GRANZ_INDEX -> GZ.Town.HULLE_GRANZ
                    else -> GZ.Town.ERSTEBURG
                })
                Input.Button.B -> mOnCancel.invoke()
                Input.Button.DPAD_UP -> {
                    when (mCurrentIndex) {
                        ERSTEBURG_INDEX -> hoverOnIndex(ZWEITEBURG_INDEX)
                        ZWEITEBURG_INDEX -> hoverOnIndex(DRITTEBURG_INDEX)
                        SCHICKSALBURG_INDEX -> hoverOnIndex(KRONE_INDEX)
                        SCHLACHFELT_INDEX -> hoverOnIndex(KRONE_INDEX)
                        ENDBURG_INDEX -> hoverOnIndex(SCHICKSALBURG_INDEX)
                        ROTERSEE_INDEX -> hoverOnIndex(ENDBURG_INDEX)
                        EINFACHESLEBEN_INDEX -> hoverOnIndex(KRONE_INDEX)
                        BERGDORF_INDEX -> hoverOnIndex(ERSTEBURG_INDEX)
                    }
                }
                Input.Button.DPAD_RIGHT -> {
                    when (mCurrentIndex) {
                        ERSTEBURG_INDEX -> hoverOnIndex(ROTERSEE_INDEX)
                        ZWEITEBURG_INDEX -> hoverOnIndex(ENDBURG_INDEX)
                        DRITTEBURG_INDEX -> hoverOnIndex(KRONE_INDEX)
                        KRONE_INDEX -> hoverOnIndex(EINFACHESLEBEN_INDEX)
                        SCHICKSALBURG_INDEX -> hoverOnIndex(EINFACHESLEBEN_INDEX)
                        SCHLACHFELT_INDEX -> hoverOnIndex(SCHICKSALBURG_INDEX)
                        ENDBURG_INDEX -> hoverOnIndex(EINFACHESLEBEN_INDEX)
                        ROTERSEE_INDEX -> hoverOnIndex(ENDBURG_INDEX)
                        BERGDORF_INDEX -> hoverOnIndex(ROTERSEE_INDEX)
                        HULLE_GRANZ_INDEX -> hoverOnIndex(DRITTEBURG_INDEX)
                    }
                }
                Input.Button.DPAD_DOWN -> {
                    when (mCurrentIndex) {
                        ERSTEBURG_INDEX -> hoverOnIndex(BERGDORF_INDEX)
                        ZWEITEBURG_INDEX -> hoverOnIndex(ERSTEBURG_INDEX)
                        DRITTEBURG_INDEX -> hoverOnIndex(ZWEITEBURG_INDEX)
                        KRONE_INDEX -> hoverOnIndex(SCHICKSALBURG_INDEX)
                        SCHICKSALBURG_INDEX -> hoverOnIndex(ENDBURG_INDEX)
                        SCHLACHFELT_INDEX -> hoverOnIndex(ROTERSEE_INDEX)
                        ENDBURG_INDEX -> hoverOnIndex(ROTERSEE_INDEX)
                        ROTERSEE_INDEX -> hoverOnIndex(BERGDORF_INDEX)
                        EINFACHESLEBEN_INDEX -> hoverOnIndex(ENDBURG_INDEX)
                    }
                }
                Input.Button.DPAD_LEFT -> {
                    when (mCurrentIndex) {
                        DRITTEBURG_INDEX -> hoverOnIndex(HULLE_GRANZ_INDEX)
                        KRONE_INDEX -> hoverOnIndex(DRITTEBURG_INDEX)
                        SCHICKSALBURG_INDEX -> hoverOnIndex(SCHLACHFELT_INDEX)
                        SCHLACHFELT_INDEX -> hoverOnIndex(ZWEITEBURG_INDEX)
                        ENDBURG_INDEX -> hoverOnIndex(ZWEITEBURG_INDEX)
                        ROTERSEE_INDEX -> hoverOnIndex(BERGDORF_INDEX)
                        EINFACHESLEBEN_INDEX -> hoverOnIndex(SCHICKSALBURG_INDEX)
                    }
                }
                else -> {
                }
            }
        }
    }

    private fun hoverOnIndex(index: Int) {
        if ((index < ERSTEBURG_INDEX) || (index > HULLE_GRANZ_INDEX))
            return
        mCurrentIndex = index
        // Change the text to the town's name
        mText.Text = mTownNames[mCurrentIndex]
        // Center the text in its area
        val textRect = mText.Rect
        val textBackgroundRect = mTextBackground.Rect
        mText.move(
            textBackgroundRect.X + (textBackgroundRect.Width / 2f) - (textRect.Width / 2f),
            textBackgroundRect.Y + (textBackgroundRect.Height / 2f) - (textRect.Height / 2f)
        )
        // Center the cursor on top of the town
        val townRect = mTownRects[mCurrentIndex]
        val foregroundRect = mMapForeground.Rect
        val xPixel = townRect.X + (townRect.Width / 2f) - (CURSOR_WIDTH_IN_PIXELS / 2f)
        val yPixel = townRect.Y + (townRect.Height / 2f) - (CURSOR_HEIGHT_IN_PIXELS / 2f)
        mCursor.move(
            gGZ.Camera.LeftEdge + ((xPixel / SPRITE_WIDTH_IN_PIXELS) * foregroundRect.Width),
            gGZ.Camera.BottomEdge + ((yPixel / SPRITE_HEIGHT_IN_PIXELS) * foregroundRect.Height)
        )
    }
}