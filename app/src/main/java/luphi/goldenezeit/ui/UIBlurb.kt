package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLText
import java.util.LinkedList

// This class creates a non-interactable blurb or sign which displays unformatted text
@Suppress("PropertyName")
class UIBlurb : IDrawable {
    override val Rect: Rect
        get() = mBubble.Rect

    private var mIsInitialized = false
    private var mIsMoved = false
    private var mIsSized = false
    private var mSpacing = 0f
    private val mBubble = UIBubble()
    private val mGLTexts = LinkedList<GLText>()

    var Text: String = ""
        set(value) {
            for (glText in mGLTexts)
                glText.free()
            mGLTexts.clear()
            field = value
            for (line in field.lines())
                addLine(line)
            autoSize()
        }
    val UsableSurface
        get() = mBubble.UsableSurface

    fun init(text: String) {
        mSpacing = gGZ.Camera.Margin * 2f
        mBubble.init()
        mBubble.move(0f, 0f)
        Text = text
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized || !mIsMoved || !mIsSized)
            return
        mBubble.draw(dt)
        for (text in mGLTexts)
            text.draw(dt)
    }

    fun move(x: Float, y: Float) {
        val rect = Rect
        translate(x - rect.X, y - rect.Y)
        mIsMoved = true
    }

    fun translate(dx: Float, dy: Float) {
        mBubble.translate(dx, dy)
        for (text in mGLTexts)
            text.translate(dx, dy)
    }

    private fun addLine(text: String) {
        val glText = GLText()
        glText.init(text)
        addGLText(glText)
    }

    private fun addGLText(glText: GLText) {
        mGLTexts.add(glText)
        val usableSurface = mBubble.UsableSurface
        if (mGLTexts.size == 1)
            glText.move(usableSurface.X, usableSurface.Y + usableSurface.Height - glText.Rect.Height)
        else
            glText.move(usableSurface.X, mGLTexts[mGLTexts.size - 2].Rect.Y - glText.Rect.Height - mSpacing)
    }

    private fun autoSize() {
        var maxWidthNDC = 0f
        var textHeight = 0f
        for (text in mGLTexts) {
            val textRect = text.Rect
            textHeight = textRect.Height
            if (textRect.Width > maxWidthNDC)
                maxWidthNDC = textRect.Width
        }
        val glTexts = LinkedList(mGLTexts)
        mGLTexts.clear()
        mBubble.sizeForUsableWidthAndHeight(maxWidthNDC, textHeight * glTexts.size + (mSpacing * (glTexts.size - 1)))
        for (glText in glTexts)
            addGLText(glText)
        mIsSized = true
    }
}