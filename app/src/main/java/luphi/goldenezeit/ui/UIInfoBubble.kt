package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive

class UIInfoBubble : UIBubble() {
    private val mVerticalBar = GLPrimitive()
    private val mHorizontalBar = GLPrimitive()

    override val UsableSurface: Rect
        get() = Rect(mX + (mBorderThickness), mY + (mBorderThickness * 3f), mWidth - (mBorderThickness * 3f), mHeight - (mBorderThickness * 4f))

    init {
        mVerticalBar.IsStatic = true
        mVerticalBar.Color = floatArrayOf(0f, 0f, 0f)
        mHorizontalBar.IsStatic = true
        mHorizontalBar.Color = floatArrayOf(0f, 0f, 0f)
    }

    override fun init() {
        mBorderThickness = gGZ.Camera.Margin * 2f
        mCornerBottomRight.init(gGZ.Resources.loadTexture("sprites/ui/player_status_border_corner.png"))
        mCornerBottomRight.size(mBorderThickness, mBorderThickness)
        mVerticalBar.init(GLPrimitive.Shape.QUAD)
        mCornerBottomLeft.init(gGZ.Resources.loadTexture("sprites/ui/player_status_border_arrow.png"))
        mCornerBottomLeft.sizeByHeight(mBorderThickness * 2f)
        mHorizontalBar.init(GLPrimitive.Shape.QUAD)
        mBackground.init(GLPrimitive.Shape.QUAD)
        mUsableSurfaceQuad.init(GLPrimitive.Shape.QUAD)
        size(1f, 1f)
    }

    override fun draw(dt: Long) {
        if (!mIsMoved || !mIsSized)
            return
        mBackground.draw(dt)
        mCornerBottomRight.draw(dt)
        mCornerBottomLeft.draw(dt)
        mVerticalBar.draw(dt)
        mHorizontalBar.draw(dt)
    }

    override fun translate(dx: Float, dy: Float) {
        mX += dx
        mY += dy
        mCornerBottomRight.translate(dx, dy)
        mCornerBottomLeft.translate(dx, dy)
        mVerticalBar.translate(dx, dy)
        mHorizontalBar.translate(dx, dy)
        mBackground.translate(dx, dy)
        mUsableSurfaceQuad.translate(dx, dy)
        mIsMoved = true
    }

    override fun size(w: Float, h: Float) {
        mWidth = w
        mHeight = h
        mBackground.size(w, h)
        mBackground.move(mX, mY)
        mCornerBottomRight.move(mX + w - mCornerBottomRight.Rect.Width, mY)
        val cornerBottomRightRect = mCornerBottomRight.Rect
        mVerticalBar.size(cornerBottomRightRect.Width, h - cornerBottomRightRect.Height)
        mVerticalBar.move(cornerBottomRightRect.X, cornerBottomRightRect.Y + mBorderThickness)
        mCornerBottomLeft.move(mX, mY)
        val cornerBottomLeftRect = mCornerBottomLeft.Rect
        mHorizontalBar.size(cornerBottomRightRect.X - (cornerBottomLeftRect.X + cornerBottomLeftRect.Width), mBorderThickness)
        mHorizontalBar.move(cornerBottomLeftRect.X + cornerBottomLeftRect.Width, mY)
        val usableSurface = UsableSurface
        mUsableSurfaceQuad.size(usableSurface.Width, usableSurface.Height)
        mUsableSurfaceQuad.move(usableSurface.X, usableSurface.Y)
        mIsSized = true
    }

    override fun sizeForUsableWidthAndHeight(w: Float, h: Float) = size(w + (mBorderThickness * 3f), h + (mBorderThickness * 3f))

    override fun sizeForUsableWidth(w: Float, h: Float) = size(w + (mBorderThickness * 3f), h)

    override fun sizeForUsableHeight(w: Float, h: Float) = size(w, h + (mBorderThickness * 3f))
}