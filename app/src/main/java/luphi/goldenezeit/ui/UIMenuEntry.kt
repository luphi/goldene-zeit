package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLText
import kotlin.math.abs

@Suppress("PropertyName")
open class UIMenuEntry(internal val Label: String, internal val OnSelect: () -> Unit, internal val OnHover: () -> Unit = {}) : IDrawable {
    override val Rect: Rect
        get() = Rect(mX, mY, mWidth, mHeight)

    protected val mLabelText = GLText()
    protected var mX = 0f
    protected var mY = 0f
    protected var mWidth = 0f
    protected var mHeight = 0f
    protected var mIsInitialized = false

    var IsVisible = true // Indicates if this entry should be drawn by the parent menu
    open val NativeWidth // The width of the entry before it is resized, used for autosizing
        get() = mLabelText.Rect.Width
    open val NativeHeight // The height of the entry before it is resized
        get() = gGZ.FontSize + (gGZ.Camera.Margin * 3f)

    open fun init() {
        mLabelText.init(Label)
        mWidth = mLabelText.Rect.Width
        mHeight = NativeHeight
        mIsInitialized = true
    }

    override fun draw(dt: Long) = mLabelText.draw(dt)

    open fun size(w: Float, h: Float) {
        mWidth = w
        mHeight = h
        move(mX, mY)
    }

    open fun sizeByWidth(w: Float) {
        mWidth = w
        mHeight = NativeHeight
        move(mX, mY)
    }

    open fun sizeByHeight(h: Float) {
        mHeight = h
        mWidth = NativeWidth
        move(mX, mY)
    }

    open fun translate(dx: Float, dy: Float) {
        mX += dx
        mY += dy
        mLabelText.translate(dx, dy)
    }

    open fun move(x: Float, y: Float) {
        mX = x
        mY = y
        mLabelText.move(mX, mY + (mHeight / 2f) - (mLabelText.Rect.Height / 2f)) // Vertically center the label at mX
    }

    fun determineVisibilityForSurface(surface: Rect) {
        val rect = Rect
        // IsVisible = true if the top and bottom edges are within the surface (allowing for some margin of error due to float precision), otherwise false
        IsVisible = if ((rect.Y < surface.Y) && (abs(rect.Y - surface.Y) > 0.001))
            false
        else !((rect.Y + rect.Height > surface.Y + surface.Height) && (abs((rect.Y + rect.Height) - (surface.Y + surface.Height)) > 0.001))
    }

    override fun toString(): String {
        val rect = Rect
        return "{ position (${rect.X}, ${rect.Y}), dimensions [${rect.Width} x ${rect.Height}], label = $Label, is visible = $IsVisible }"
    }
}