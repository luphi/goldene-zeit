package luphi.goldenezeit.ui

import luphi.goldenezeit.Addition
import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import java.util.Locale
import kotlin.collections.LinkedHashMap

// The addition menu uses the whole screen, displays the available and chosen additions to be used in battle, and is launched from the overworld's root menu
class UIAdditionMenu(onCancel: () -> Unit) : IDrawable, IInteractable {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    private val mBackground = GLPrimitive()
    private val mRootMenu = UIMenu { /* onCancel */ onCancel.invoke() }
    private val mOptionsMenu = UIMenu { /* onCancel */ mCurrentMenu = mRootMenu }
    private var mCurrentMenu: UIMenu? = mRootMenu
    private val mValuesBlurb = UIValuesBlurb()

    init {
        mBackground.IsStatic = true
        mBackground.Color = floatArrayOf(1f, 1f, 1f) // White
    }

    fun init() {
        mBackground.init(GLPrimitive.Shape.QUAD)
        mBackground.size(gGZ.Camera.Width, gGZ.Camera.Height)
        mBackground.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mRootMenu.init()
        buildRootMenu()
        mOptionsMenu.init()
        mValuesBlurb.init()
        mValuesBlurb.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.BottomEdgeWithMargin)
        previewAddition(gGZ.Player.Addition1)
    }

    override fun draw(dt: Long) {
        mBackground.draw(dt)
        mCurrentMenu?.draw(dt)
        mValuesBlurb.draw(dt)
    }

    override fun handleInput(input: Input) {
        mCurrentMenu?.handleInput(input)
    }

    private fun buildRootMenu() {
        mRootMenu.clear()
        mRootMenu.add(
            UIMenuEntry(gGZ.Player.Addition1?.Name?.toUpperCase(Locale.getDefault()) ?: "-", {
                // onSelect
                buildOptionsMenu(1)
                mCurrentMenu = mOptionsMenu
            }, { /* onHover*/ previewAddition(gGZ.Player.Addition1) })
        )
        mRootMenu.add(
            UIMenuEntry(gGZ.Player.Addition2?.Name?.toUpperCase(Locale.getDefault()) ?: "-", {
                // onSelect
                buildOptionsMenu(2)
                mCurrentMenu = mOptionsMenu
            }, { /* onHover*/ previewAddition(gGZ.Player.Addition2) })
        )
        mRootMenu.add(
            UIMenuEntry(gGZ.Player.Addition3?.Name?.toUpperCase(Locale.getDefault()) ?: "-", {
                // onSelect
                buildOptionsMenu(3)
                mCurrentMenu = mOptionsMenu
            }, { /* onHover*/ previewAddition(gGZ.Player.Addition3) })
        )
        mRootMenu.add(
            UIMenuEntry(gGZ.Player.Addition4?.Name?.toUpperCase(Locale.getDefault()) ?: "-", {
                // onSelect
                buildOptionsMenu(4)
                mCurrentMenu = mOptionsMenu
            }, { /* onHover*/ previewAddition(gGZ.Player.Addition4) })
        )
        mRootMenu.autoSize()
        val menuRect = mRootMenu.Rect
        mRootMenu.size(gGZ.Camera.WidthWithMargin, menuRect.Height)
        mRootMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - menuRect.Height)
    }

    private fun buildOptionsMenu(slot: Int) {
        mOptionsMenu.clear()
        val additions = gGZ.Resources.loadAdditions()
        for (addition in additions) {
            val isAlreadySlotted = (addition.ID == gGZ.Player.Addition1?.ID ?: -1) || (addition.ID == gGZ.Player.Addition2?.ID ?: -1) ||
                    (addition.ID == gGZ.Player.Addition3?.ID ?: -1) || (addition.ID == gGZ.Player.Addition4?.ID ?: -1)
            if (!isAlreadySlotted)
                mOptionsMenu.add(
                    UIMenuEntry(addition.Name.toUpperCase(Locale.getDefault()), {
                        // onSelect
                        conditionallySetAddition(slot, addition)
                    }, { /* onHover */ previewAddition(addition) })
                )
        }
        mOptionsMenu.add(UIMenuEntry("NONE", { /* onSelect */ conditionallySetAddition(slot, null) }, { /* onHover */ previewAddition(null) }))
        val rootMenuRect = mRootMenu.Rect
        mOptionsMenu.size(rootMenuRect.Width, rootMenuRect.Height)
        mOptionsMenu.move(rootMenuRect.X, rootMenuRect.Y)
        mOptionsMenu.IsFocused = true
    }

    private fun conditionallySetAddition(slot: Int, addition: Addition?) {
        // If the player may be trying to set the very last used slot to "NONE"
        if (addition == null) {
            val slottedCount = (if (gGZ.Player.Addition1 == null) 0 else 1) + (if (gGZ.Player.Addition2 == null) 0 else 1) +
                    (if (gGZ.Player.Addition3 == null) 0 else 1) + (if (gGZ.Player.Addition4 == null) 0 else 1)
            if (slottedCount <= 1) {
                // Compare the slot being set against it's current value
                when {
                    (slot == 1) && (gGZ.Player.Addition1 != null) -> return // Stupid player, you can't go into battle without any attacks
                    (slot == 2) && (gGZ.Player.Addition2 != null) -> return
                    (slot == 3) && (gGZ.Player.Addition3 != null) -> return
                    (slot == 4) && (gGZ.Player.Addition4 != null) -> return
                }
            }
        }
        // If the player's level is greater than or equal to the addition's availability level, or if the addition is null
        if (addition?.Level ?: 0 <= gGZ.Player.Level) {
            when (slot) {
                1 -> gGZ.Player.Addition1 = addition
                2 -> gGZ.Player.Addition2 = addition
                3 -> gGZ.Player.Addition3 = addition
                4 -> gGZ.Player.Addition4 = addition
            }
            buildRootMenu()
            mCurrentMenu = mRootMenu
        }
    }

    private fun previewAddition(addition: Addition?) {
        val map = LinkedHashMap<String, Array<String>>()
        if (addition == null) {
            map["AVAILABLE AT LEVEL"] = arrayOf("-")
            map["STYLE"] = arrayOf("-")
            map["ADDS"] = arrayOf("-")
            map["DAMAGE"] = arrayOf("-")
        } else {
            map["AVAILABLE AT LEVEL"] = arrayOf("${addition.Level}")
            map["STYLE"] = arrayOf("${addition.Style}")
            map["ADDS"] = arrayOf("${addition.Adds.size}")
            map["DAMAGE"] = arrayOf("${(addition.Modifier * 100f).toInt()}%")
        }
        mValuesBlurb.Map = map
        mValuesBlurb.size(gGZ.Camera.WidthWithMargin, gGZ.Camera.HeightWithMargin - mRootMenu.Rect.Height - gGZ.Camera.Margin)
    }
}