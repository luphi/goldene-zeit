package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import kotlin.concurrent.withLock

// This class serves as both the "save the game" and "load a game" menu depending on the behavior passed to the constructor. It uses the full screen and
// contains N entries when acting as a load menu and N + 1 when acting as a save menu, where N is the number of save slots. Entry addition and saving/loading
// are handled by this class unlike most other menus.
class UISaveLoadMenu(behavior: Behavior, onCancel: () -> Unit, private val mOnSelect: (Int) -> Unit) : UIMenu(onCancel) {
    // IDrawable properties
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    enum class Behavior { SAVE, LOAD } // This *could* be a boolean but this is improves readability

    private val mBehavior = behavior
    private val mBackground = GLPrimitive()
    private var mSelectMenu: UIMenu? = null // Appears over the entry with "CANCEL" and either "SAVE" or "LOAD" as options

    override val UsableSurface
        get() = Rect

    init {
        IsEvenlySpaced = false
        mBackground.IsStatic = true
        mBackground.Color = floatArrayOf(1f, 1f, 1f) // White
    }

    override fun init() {
        if (mIsInitialized)
            return
        mBackground.init(GLPrimitive.Shape.QUAD)
        mBackground.size(gGZ.Camera.Width, gGZ.Camera.Height)
        mBackground.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mCursorPadding = gGZ.FontSize * 1.5f
        mCursor.init(gGZ.Resources.loadTexture("sprites/ui/arrow_right.png"))
        mCursor.size(gGZ.FontSize, gGZ.FontSize)
        mHollowCursor.init(gGZ.Resources.loadTexture("sprites/ui/arrow_right_hollow.png"))
        val cursorRect = mCursor.Rect
        mHollowCursor.size(cursorRect)
        mMoreAboveIndicator.init(gGZ.Resources.loadTexture("sprites/ui/arrow_up.png"))
        mMoreAboveIndicator.size(cursorRect)
        mMoreBelowIndicator.init(gGZ.Resources.loadTexture("sprites/ui/arrow_down.png"))
        mMoreBelowIndicator.size(cursorRect)
        val sortedSlotsOnDisk = gGZ.Record.SlotsOnDisk.sorted() // SlotsOnDisk is a HashSet and isn't necessarily in ascending order
        for (i in sortedSlotsOnDisk) {
            // If saving, don't show the autosave slot (slot 0 is dedicated as autosave) as an option
            if ((mBehavior == Behavior.SAVE) && (i == 0))
                continue
            val onSelect = {
                mSelectMenu = UIMenu {
                    // onCancel
                    mSelectMenu = null
                    IsFocused = true
                }
                mSelectMenu?.add(UIMenuEntry(if (mBehavior == Behavior.SAVE) "SAVE" else "LOAD", { /* onSelect */ mOnSelect.invoke(i) }))
                mSelectMenu?.add(UIMenuEntry("CANCEL", {
                    // onSelect
                    mSelectMenu = null
                    IsFocused = true
                }))
                mSelectMenu?.init()
                mSelectMenu?.autoSize()
                val entryRect = mEntries[mHoveredIndex].Rect
                mSelectMenu?.move(entryRect.X + entryRect.Width - mSelectMenu!!.Rect.Width, entryRect.Y)
                IsFocused = false
            }
            mEntriesLock.withLock { mEntries.add(UISaveLoadMenuEntry(i) { /* onSelect */ onSelect.invoke() }) }
        }
        // When creating a save menu, we need to make an entry for an unused slot, available for a new file
        if (mBehavior == Behavior.SAVE) {
            mEntriesLock.withLock {
                mEntries.add(UISaveLoadMenuEntry(sortedSlotsOnDisk.getOrNull(sortedSlotsOnDisk.size - 1) ?: 0 + 1) {
                    // onSelect
                    mSelectMenu = UIMenu {
                        mSelectMenu = null
                        IsFocused = true
                    }
                    mSelectMenu?.add(UIMenuEntry("SAVE", { mOnSelect.invoke(sortedSlotsOnDisk[sortedSlotsOnDisk.size - 1] + 1) }))
                    mSelectMenu?.add(UIMenuEntry("CANCEL", {
                        mSelectMenu = null
                        IsFocused = true
                    }))
                    mSelectMenu?.init()
                    mSelectMenu?.autoSize()
                    val entryRect = mEntries[mHoveredIndex].Rect
                    mSelectMenu?.move(entryRect.X + entryRect.Width - mSelectMenu!!.Rect.Width, entryRect.Y)
                    IsFocused = false
                })
            }
        }
        for (entry in mEntries)
            entry.init()
        autoSize()
        mIsMoved = true
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized)
            return
        mBackground.draw(dt)
        super.draw(dt)
        mSelectMenu?.draw(dt)
    }

    override fun handleInput(input: Input) {
        if (mSelectMenu != null) {
            mSelectMenu?.handleInput(input)
            return
        }
        super.handleInput(input)
    }

    override fun size(w: Float, h: Float) = autoSize()

    override fun autoSize() {
        mIsOverflow = mEntries.size > 3
        doIndicators()
        doEntries()
        hoverOnIndex(mHoveredIndex)
        mIsAutosized = true
        mIsSized = true
    }

    override fun remove(label: String) = super.remove(if (label.isEmpty()) "00" else if (label.length == 1) "0$label" else label) // e.g. 00 or 09 or 90

    override fun doEntries() {
        if (mEntries.isEmpty())
            return
        val usableSurface = UsableSurface
        // This menu fits up to three entries on screen at once. Therefore, each entry's height is equal to 1/3 the available height where the available height
        // is the height of the (visible) screen minus some margin above, below, and between entries.
        val entryHeight = mEntries[0].NativeHeight
        for (i in mEntries.indices) {
            // In an overflow case, room needs to be made for the indicators (on the right edge of the screen)
            mEntries[i].sizeByWidth(usableSurface.Width - (if (mIsOverflow) mCursorPadding * 2f else mCursorPadding) - gGZ.Camera.Margin)
            mEntries[i].move(usableSurface.X + mCursorPadding, usableSurface.Y + usableSurface.Height - ((entryHeight + gGZ.Camera.Margin) * (i + 1)))
            mEntries[i].determineVisibilityForSurface(usableSurface)
        }
    }
}