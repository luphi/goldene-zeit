package luphi.goldenezeit.ui

import luphi.goldenezeit.gGZ
import kotlin.math.max

@Suppress("PropertyName")
class UIInventoryMenuEntry(label: String, quantity: Int, onUse: () -> Unit, onToss: (Int) -> Unit = {}, onHover: () -> Unit = {}) :
    UIJustifiedMenuEntry(label, /* right label */ if (quantity == 0) "" else quantity.toString(), /* onSelect */ onUse, onHover, /* useTwoLines */ true) {
    override val NativeWidth
        get() = if (Quantity == 0) super.NativeWidth else max(mLabelText.Rect.Width, gGZ.FontSize * 3f) // FontSize * 3 for the 3-character string e.g. "x10"

    // Variables related to callbacks
    var OnUse = onUse // Callback run when "USE" is selected
    var OnToss = onToss // Callback run when "TOSS" is selected

    var Quantity = quantity // Quantity of the item within this inventory
        set(value) {
            field = if (value < 0) 0 else value
            // If not a unique item
            if (value > 0) {
                mRightLabelText.Text = "x${if (field < 10) " " else ""}$field" // Include a space if less than 10 (e.g. "x 9" compard to "x99")
                move(mX, mY)
            }
        }

    override fun init() {
        super.init()
        Quantity = Quantity // This isn't redundant, we want to execute the setter
    }

    override fun move(x: Float, y: Float) {
        super.move(x, y)
        if (Quantity != 0)
            mRightLabelText.move(mX + mWidth - (gGZ.FontSize * 3f), mY)
    }
}