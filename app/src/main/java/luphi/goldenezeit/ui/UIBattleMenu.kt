package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLSprite
import kotlin.concurrent.withLock
import kotlin.math.max

// The battle menu is exclusively used by the battle mode, appears at the bottom of the screen, uses the full screen width but only enough height to display the
// available options, and has four hard-coded entries with two optional submenus. The optional submenus are a menu for limits or an armed menu (UIArmedMenu).
@Suppress("PropertyName")
class UIBattleMenu(
    onAttack: () -> Unit,
    onAttackHover: () -> Unit,
    onGuard: () -> Unit,
    onGuardHover: () -> Unit,
    onItem: () -> Unit,
    onItemHover: () -> Unit,
    onRun: () -> Unit,
    onRunHover: () -> Unit
) : UIMenu(/* onCancel */ {}) {
    private val mAttackSubmenuIndicator = GLSprite()
    private val mItemSubmenuIndicator = GLSprite()
    private var mSubmenu: UIMenu? = null

    var AttackSubmenu: UIMenu? = null
        set(submenu) {
            field = submenu
            if (submenu != null) {
                submenu.autoSize()
                var submenuRect = submenu.Rect
                val bubbleRect = mBubble.Rect
                val entryRect = mEntries[0].Rect
                submenu.move(bubbleRect.X - submenuRect.Width, entryRect.Y + (entryRect.Height / 2f) - (submenuRect.Height / 2f))
                submenuRect = submenu.Rect
                if (submenuRect.X < gGZ.Camera.LeftEdge)
                    submenu.move(gGZ.Camera.LeftEdge, submenuRect.Y)
                submenuRect = submenu.Rect
                // Snap the submenu's top edge to the battle menu's top edge
                submenu.move(submenuRect.X, bubbleRect.Y + bubbleRect.Height - submenuRect.Height)
                submenuRect = submenu.Rect
                // If the above caused the submenu to extend beyond the surface
                if (submenuRect.Y < gGZ.Camera.BottomEdge)
                    submenu.move(submenuRect.X, gGZ.Camera.BottomEdge)
            }
        }
    override val CurrentlySelected: String
        get() = when (mHoveredIndex) {
            0 -> "Attack"
            1 -> "Item"
            2 -> "Guard"
            3 -> "Run"
            4 -> "Attack submenu"
            5 -> "Item submenu"
            else -> ""
        }
    var ItemSubmenu: UIMenu? = null
        set(submenu) {
            field = submenu
            if (submenu != null) {
                submenu.autoSize()
                var submenuRect = submenu.Rect
                val bubbleRect = mBubble.Rect
                val entryRect = mEntries[1].Rect
                submenu.move(bubbleRect.X - submenuRect.Width, entryRect.Y + (entryRect.Height / 2f) - (submenuRect.Height / 2f))
                submenuRect = submenu.Rect
                if (submenuRect.X < gGZ.Camera.LeftEdge)
                    submenu.move(gGZ.Camera.LeftEdge, submenuRect.Y)
                if (submenuRect.Y < bubbleRect.Y)
                    submenu.move(submenuRect.X, bubbleRect.Y)
            }
        }

    init {
        // This flag (from UIMenu) doesn't affect this class because it overrides entry positioning but said entries are not evenly spaced so this should match
        IsEvenlySpaced = false
        mAttackSubmenuIndicator.IsStatic = true
        mItemSubmenuIndicator.IsStatic = true
        mEntriesLock.withLock {
            mEntries.add(UIBattleMenuEntry("ATTACK", /* onSelect */ onAttack, /* onHover */ onAttackHover)) // index 0
            mEntries.add(UIBattleMenuEntry("ITEM", /* onSelect */ onItem, /* onHover */ onItemHover)) // index 1
            mEntries.add(UIBattleMenuEntry("GUARD", /* onSelect */ onGuard, /* onHover */ onGuardHover)) // index 2
            mEntries.add(UIBattleMenuEntry("RUN", /* onSelect */onRun, /* onHover */ onRunHover)) // index 3
        }
    }

    override fun init() {
        super.init()
        mAttackSubmenuIndicator.init(gGZ.Resources.loadTexture("sprites/ui/arrow_left.png"))
        val cursorRect = mCursor.Rect
        mAttackSubmenuIndicator.size(cursorRect.Width, cursorRect.Height)
        mItemSubmenuIndicator.init(gGZ.Resources.loadTexture("sprites/ui/arrow_left.png"))
        mItemSubmenuIndicator.size(cursorRect.Width, cursorRect.Height)
        for (entry in mEntries)
            entry.init()
        autoSize()
        move(gGZ.Camera.RightEdge - mBubble.Rect.Width, gGZ.Camera.BottomEdge) // Place this in the bottom right of the screen
    }

    override fun draw(dt: Long) {
        super.draw(dt)
        // If there is an attack submenu and the cursor is currently hovering on "ATTACK"
        if ((AttackSubmenu != null) && (mHoveredIndex == 0))
            mAttackSubmenuIndicator.draw(dt)
        // If there is an item submenu and the cursor is currently hovering on "ITEM"
        else if ((ItemSubmenu != null) && (mHoveredIndex == 1))
            mItemSubmenuIndicator.draw(dt)
        // If the cursor is currently hovering on/in either submenu
        if ((mHoveredIndex == 4) || (mHoveredIndex == 5))
            mSubmenu?.draw(dt)
    }

    override fun autoSize() {
        // Note: this class' init block (not the init() function) adds entries so we know for a fact that mEntries isn't empty
        mIsAutosized = true
        if (!mIsInitialized || mEntries.isEmpty()) {
            mBubble.sizeForUsableWidthAndHeight(0f, 0f)
            return
        }
        // Determine the appropriate widths for the left and right columns
        val leftColumnWidth = max(mEntries[0].NativeWidth, mEntries[1].NativeWidth)
        val rightColumnWidth = max(mEntries[2].NativeWidth, mEntries[3].NativeWidth)
        // Apply the widths to the entries
        mEntries[0].sizeByWidth(leftColumnWidth)
        mEntries[1].sizeByWidth(leftColumnWidth)
        mEntries[2].sizeByWidth(rightColumnWidth)
        mEntries[3].sizeByWidth(rightColumnWidth)
        mBubble.sizeForUsableWidthAndHeight(
            (2f * mCursorPadding) + leftColumnWidth + rightColumnWidth, // Width of both columns with room for the cursor(s)
            gGZ.FontSize * 4f // Height of four texts in order to equal the height of the subsequent attack menu
        )
        doEntries()
        hoverOnIndex(mHoveredIndex)
        mIsSized = true
    }

    override fun translate(dx: Float, dy: Float) {
        super.translate(dx, dy)
        mAttackSubmenuIndicator.translate(dx, dy)
        mItemSubmenuIndicator.translate(dx, dy)
        AttackSubmenu?.translate(dx, dy)
        ItemSubmenu?.translate(dx, dy)
    }

    fun default() = hoverOnIndex(0)

    override fun handleInput(input: Input) {
        if (input.IsPress)
            when (input.Opcode) {
                Input.Button.A -> {
                    mEntries.getOrNull(mHoveredIndex)?.OnSelect?.invoke() // Won't do anything when hovering on indices 4 or 5 (the submenus)
                    mSubmenu?.handleInput(input) // Won't do anything due to mSubmenu being null if not hovering over a submenu
                }
                Input.Button.B -> {
                    // If either secondary option is currently selected
                    if (mHoveredIndex > 3)
                        hoverOnIndex(mHoveredIndex - 4)
                }
                Input.Button.DPAD_UP -> {
                    // If either "ITEM" or "RUN" is currently selected
                    if ((mHoveredIndex % 2 == 1) && (mHoveredIndex < 4))
                        hoverOnIndex(mHoveredIndex - 1)
                    else if (mSubmenu != null)
                        mSubmenu?.handleInput(input)
                }
                Input.Button.DPAD_RIGHT -> {
                    // If either "ATTACK" or "ITEM" is currently selected
                    if (mHoveredIndex < 2)
                        hoverOnIndex(mHoveredIndex + 2)
                    // If either secondary option is currently selected
                    else if (mHoveredIndex > 3)
                        hoverOnIndex(mHoveredIndex - 4)
                }
                Input.Button.DPAD_DOWN -> {
                    // If either "ATTACK" or "GUARD" are currently selected
                    if ((mHoveredIndex % 2 == 0) && (mHoveredIndex < 4))
                        hoverOnIndex(mHoveredIndex + 1)
                    else if (mSubmenu != null)
                        mSubmenu?.handleInput(input)
                }
                Input.Button.DPAD_LEFT -> {
                    // If either "GUARD" or "RUN" are currently selected
                    if ((mHoveredIndex > 1) && (mHoveredIndex < 4))
                        hoverOnIndex(mHoveredIndex - 2)
                    // If "ATTACK" is currently selected and a secondary attack option exists
                    else if ((mHoveredIndex == 0) && (AttackSubmenu != null))
                        hoverOnIndex(4)
                    // IF "ITEM" is currently selected and a secondary item option exists
                    else if ((mHoveredIndex == 1) && (ItemSubmenu != null))
                        hoverOnIndex(5)
                }
                else -> {
                }
            }
    }

    override fun hoverOnIndex(index: Int) {
        if ((index < 0) || (index >= mEntries.size + 2))
            return
        mHoveredIndex = index
        when {
            // "ATTACK," "ITEM," "GUARD," or "RUN"
            (index < mEntries.size) -> {
                val entryRect = mEntries[mHoveredIndex].Rect
                mCursor.move(entryRect.X - (mCursorPadding * 0.75f), entryRect.Y + (gGZ.FontSize / 2f) - (mCursor.Rect.Height / 2f))
                mSubmenu = null
                mEntries[mHoveredIndex].OnHover.invoke()
            }
            // Attack submenu option
            (index == 4) -> {
                mSubmenu = AttackSubmenu
                val submenuRect = mSubmenu!!.Rect
                mCursor.move(submenuRect.X, submenuRect.Y)
            }
            // Item submenu option
            else /* if (index == 5) */ -> {
                mSubmenu = ItemSubmenu
                val submenuRect = mSubmenu!!.Rect
                mCursor.move(submenuRect.X, submenuRect.Y)
            }
        }
    }

    override fun doEntries() {
        // Note: this class' init {} block adds entries so we know for a fact that mEntries isn't empty
        val usableSurface = mBubble.UsableSurface
        val entryRect = mEntries[2].Rect // Needs to be index 2 or 3 for the proper width
        mEntries[0].move(usableSurface.X + mCursorPadding, usableSurface.Y + entryRect.Height) // "ATTACK" - top left
        mEntries[1].move(usableSurface.X + mCursorPadding, usableSurface.Y) // "ITEM" - bottom left
        mEntries[2].move(usableSurface.X + usableSurface.Width - entryRect.Width, usableSurface.Y + entryRect.Height) // "GUARD" - top right
        mEntries[3].move(usableSurface.X + usableSurface.Width - entryRect.Width, usableSurface.Y) // "RUN" - bottom right
        val bubbleRect = mBubble.Rect
        mAttackSubmenuIndicator.move(/* Place it to the left of the bubble */ bubbleRect.X - mCursorPadding, mEntries[0].Rect.Y)
        mItemSubmenuIndicator.move(/* Place it to the left of the bubble */ bubbleRect.X - mCursorPadding, mEntries[1].Rect.Y)
        AttackSubmenu = AttackSubmenu // The setter will position the menu
        ItemSubmenu = ItemSubmenu
    }
}