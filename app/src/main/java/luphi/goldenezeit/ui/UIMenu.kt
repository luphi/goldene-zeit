package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.Tasks
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLSprite
import java.util.LinkedList
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.math.max

@Suppress("PropertyName", "PrivatePropertyName")
open class UIMenu(protected val mOnCancel: () -> Unit) : IDrawable, IInteractable, IProgressive {
    // IDrawable property
    override val Rect: Rect
        get() = mBubble.Rect

    // Constant(s)
    private val SCROLL_FREQUENCY = 10L // Hz, represents the number of times (per second) that a new entry is hovered over when scrolling

    // Drawables
    protected val mBubble = UIBubble() // The background and border drawn behind entries; determines this menus ultimate position and dimensions
    protected val mCursor = GLSprite() // The east-pointing cursor placed to the left of the entry that is currently hovered on
    protected val mHollowCursor = GLSprite() // An outline-only version of the above to be drawn in the above's place when this menu is out of focus
    protected val mMoreAboveIndicator = GLSprite() // A north-pointing arrow that appears/flashes to indicate there are more entries above
    protected val mMoreBelowIndicator = GLSprite() // A south-pointing arrow that appears/flashes to indicate there are more entries below

    // Tasks
    private val mIndicatorTasks = Tasks() // Used by the draw thread
    private val mScrollTasks = Tasks() // Used by the main thread

    // Various flags
    protected var mIsInitialized = false // Indicates whether the menu has been initialized or not, must be true to draw
    protected var mIsMoved = false // Indicates whether the menu has been given a position or not, must be true to draw
    protected var mIsSized = false // Indicates whether the menu has been given dimensions or not, must be true to draw
    protected var mIsAutosized = false // Indicates whether the menu has been sized using the autosize() method or not
    protected var mIsOverflow = false // Indicates whether the menu's entries require more height than is available in the usable space or not
    private var mShouldDrawMoreIndicators = false // Frequently-changed flag which controls the drawing/flashing of the "has more" indicators
    private var mIsMoreAbove = false // When true, indicates that there are menu entries hidden above the usable surface that may be scrolled to
    private var mIsMoreBelow = false // When true, indicates that there are menu entries hidden below the usable surface that may be scrolled to
    protected var mShouldCenterCursor = true // When true, this menu will vertically center the cursor next to the hovered entry or snap to the top if false

    // Other
    protected val mEntries = LinkedList<UIMenuEntry>()
    protected val mEntriesLock = ReentrantLock()
    protected var mHoveredIndex = 0
    protected var mCursorPadding = 0f

    open val CurrentlySelected: String
        get() = mEntries.getOrNull(mHoveredIndex)?.Label ?: ""
    open val UsableSurface
        get() = mBubble.UsableSurface
    var IsEvenlySpaced = true
        set(value) {
            if (field != value) {
                field = value
                // Now, resize with the new setting to apply it (if initialized)
                if (mIsInitialized) {
                    if (mIsAutosized)
                        autoSize()
                    else
                        size(Rect)
                }
            }
        }
    var IsFocused = true
        set(value) {
            field = value
            if (value)
                mEntries.getOrNull(mHoveredIndex)?.OnHover?.invoke()
        }

    init {
        mCursor.IsStatic = true
        mHollowCursor.IsStatic = true
        mMoreAboveIndicator.IsStatic = true
        mMoreBelowIndicator.IsStatic = true
    }

    open fun init() {
        if (mIsInitialized)
            return
        mBubble.init()
        mCursorPadding = gGZ.FontSize * 1.5f
        mCursor.init(gGZ.Resources.loadTexture("sprites/ui/arrow_right.png"))
        mCursor.size(gGZ.FontSize, gGZ.FontSize)
        mHollowCursor.init(gGZ.Resources.loadTexture("sprites/ui/arrow_right_hollow.png"))
        val cursorRect = mCursor.Rect
        mHollowCursor.size(cursorRect)
        mMoreAboveIndicator.init(gGZ.Resources.loadTexture("sprites/ui/arrow_up.png"))
        mMoreAboveIndicator.size(cursorRect)
        mMoreBelowIndicator.init(gGZ.Resources.loadTexture("sprites/ui/arrow_down.png"))
        mMoreBelowIndicator.size(cursorRect)
        for (entry in mEntries)
            entry.init()
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized || !mIsMoved || !mIsSized)
            return
        mIndicatorTasks.step(dt)
        mBubble.draw(dt)
        mEntriesLock.withLock {
            if (mEntries.isNotEmpty()) {
                for (entry in mEntries) {
                    if (entry.IsVisible)
                        entry.draw(dt)
                }
                if (IsFocused)
                    mCursor.draw(dt)
                else
                    mHollowCursor.draw(dt)
                if (mShouldDrawMoreIndicators) {
                    if (mIsMoreAbove)
                        mMoreAboveIndicator.draw(dt)
                    if (mIsMoreBelow)
                        mMoreBelowIndicator.draw(dt)
                }
            }
        }
    }

    override fun handleInput(input: Input) {
        // If the input is a button release, stop the scrolling task
        if (!input.IsPress)
            mScrollTasks.clear()
        else {
            when (input.Opcode) {
                Input.Button.A -> mEntries.getOrNull(mHoveredIndex)?.OnSelect?.invoke() // Select the hovered entry (if one exists)
                Input.Button.B -> mOnCancel.invoke()
                Input.Button.DPAD_UP -> {
                    hoverOnIndex(mHoveredIndex - 1) // Immediately hover on the above entry (or do nothing if already on the top entry)
                    // Schedule a task to kick in later (500 ms) that will scroll up at a rate of SCROLL_FREQUENCY indices per second
                    mScrollTasks.scheduleOneOff(500) { mScrollTasks.schedulePeriodic(1000L / SCROLL_FREQUENCY) { hoverOnIndex(mHoveredIndex - 1) } }
                }
                Input.Button.DPAD_DOWN -> {
                    hoverOnIndex(mHoveredIndex + 1) // Immediately hover on the below entry (or do nothing if already on the bottom entry)
                    // Schedule a task to kick in later (500 ms) that will scroll down at a rate of SCROLL_FREQUENCY indices per second
                    mScrollTasks.scheduleOneOff(500) { mScrollTasks.schedulePeriodic(1000L / SCROLL_FREQUENCY) { hoverOnIndex(mHoveredIndex + 1) } }
                }
                Input.Button.DPAD_LEFT -> { // Page up
                    hoverOnIndex(if (mHoveredIndex - 10 < 0) 0 else mHoveredIndex - 10)
                    // Schedule a task to kick in later (500 ms) that will scroll up at a rate of SCROLL_FREQUENCY * 10 indices per second
                    mScrollTasks.scheduleOneOff(500) {
                        mScrollTasks.schedulePeriodic(1000L / SCROLL_FREQUENCY) { hoverOnIndex(if (mHoveredIndex - 10 < 0) 0 else mHoveredIndex - 10) }
                    }
                }
                Input.Button.DPAD_RIGHT -> { // Page down
                    hoverOnIndex(if (mHoveredIndex + 10 >= mEntries.size) mEntries.size - 1 else mHoveredIndex + 10)
                    // Schedule a task to kick in later (500 ms) that will scroll down at a rate of SCROLL_FREQUENCY * 10 indices per second
                    mScrollTasks.scheduleOneOff(500) {
                        mScrollTasks.schedulePeriodic(1000L / SCROLL_FREQUENCY) {
                            hoverOnIndex(if (mHoveredIndex + 10 >= mEntries.size) mEntries.size - 1 else mHoveredIndex + 10)
                        }
                    }
                }
                else -> {
                }
            }
        }
    }

    override fun step(dt: Long) = mScrollTasks.step(dt)

    fun size(rect: Rect) = size(rect.Width, rect.Height)

    open fun size(w: Float, h: Float) {
        mIsAutosized = false
        if (!mIsInitialized || mEntries.isEmpty()) {
            if (mEntries.isEmpty())
                mBubble.sizeForUsableWidthAndHeight(0f, 0f)
            return
        }
        mBubble.size(w, h)
        doEntries()
        mIsOverflow = !mEntries[mEntries.size - 1].IsVisible
        doIndicators()
        val originalHoveredIndex = mHoveredIndex
        mHoveredIndex = 0
        hoverOnIndex(originalHoveredIndex)
        mIsSized = true
    }

    fun sizeByWidth(w: Float) {
        autoSize()
        size(w, Rect.Height)
    }

    fun sizeByHeight(h: Float) {
        autoSize()
        size(Rect.Width, h)
    }

    open fun autoSize() {
        mIsAutosized = true
        if (!mIsInitialized || mEntries.isEmpty()) {
            if (mEntries.isEmpty())
                mBubble.sizeForUsableWidthAndHeight(0f, 0f)
            return
        }
        var maxEntryWidth = 0f
        for (entry in mEntries)
            maxEntryWidth = max(entry.NativeWidth, maxEntryWidth)
        mBubble.sizeForUsableWidthAndHeight(maxEntryWidth + mCursorPadding, mEntries[0].Rect.Height * mEntries.size)
        val bubbleRect = mBubble.Rect
        mIsOverflow = bubbleRect.Height > gGZ.Camera.HeightWithMargin // If the bubble is taller than the screen (i.e. at least one entry won't fit)
        if (mIsOverflow)
            mBubble.size(bubbleRect.Width + mCursorPadding, gGZ.Camera.HeightWithMargin) // Resize to fit within the screen, with padding for the indicators
        doIndicators()
        doEntries()
        val originalHoveredIndex = mHoveredIndex
        mHoveredIndex = 0
        hoverOnIndex(originalHoveredIndex)
        mIsSized = true
    }

    open fun translate(dx: Float, dy: Float) {
        mBubble.translate(dx, dy)
        mCursor.translate(dx, dy)
        mHollowCursor.translate(dx, dy)
        mMoreAboveIndicator.translate(dx, dy)
        mMoreBelowIndicator.translate(dx, dy)
        for (entry in mEntries)
            entry.translate(dx, dy)
    }

    fun move(rect: Rect) = move(rect.X, rect.Y)

    fun move(x: Float, y: Float) {
        val rect = mBubble.Rect
        translate(x - rect.X, y - rect.Y)
        mIsMoved = true
    }

    fun add(entry: UIMenuEntry, index: Int? = null) {
        mEntriesLock.withLock { mEntries.add(index ?: mEntries.size, entry) }
        if (mIsInitialized) {
            entry.init()
            if (mIsAutosized)
                autoSize()
            else
                size(Rect)
        }
    }

    open fun remove(label: String) {
        val indicesToRemove = LinkedList<Int>()
        mEntries.forEachIndexed { index, entry ->
            if (entry.Label == label)
                indicesToRemove.add(index)
        }
        for (index in indicesToRemove)
            remove(index)
    }

    open fun remove(index: Int) {
        if ((index < 0) || (index >= mEntries.size))
            return
        mEntriesLock.withLock { mEntries.removeAt(index) }
        // Hover on the same index, or the now-last index if the previously-last index was just removed
        mHoveredIndex = if (index == mEntries.size) mEntries.size - 1 else index
        // If the menu should be autosized again without the entry
        if (mIsAutosized)
            autoSize()
        // If the menu should retain its current dimensions, just adjust the entries
        else
            size(Rect)
    }

    open fun clear() {
        mEntriesLock.withLock { mEntries.clear() }
        mHoveredIndex = 0
    }

    override fun toString(): String {
        var entries = ""
        for (entry in mEntries) {
            if (entries.isNotEmpty())
                entries += ", "
            entries += entry.Label
        }
        val rect = Rect
        return "{ position (${rect.X}, ${rect.Y}), dimensions [${rect.Width} x ${rect.Height}], entries = $entries }"
    }

    protected open fun hoverOnIndex(index: Int) {
        if ((index < 0) || (index >= mEntries.size))
            return
        val delta = index - mHoveredIndex
        mHoveredIndex = index
        val hoveredEntry = mEntries[mHoveredIndex]
        val usableSurface = UsableSurface
        // If one or more entries will be made invisible due to lacking space
        if (mIsOverflow) {
            // If the hovered entry isn't visible, it's currently offscreen meaning we need to shift all the entries by one position
            while (!hoveredEntry.IsVisible) {
                val yDiff = mEntries[0].Rect.Y - mEntries[1].Rect.Y // We can assume that an overflow case has at least two entries
                for (entry in mEntries) {
                    entry.translate(0f, yDiff * (if (delta > 0) 1f else -1f)) // Move the entry up or down by the height of one entry
                    entry.determineVisibilityForSurface(usableSurface) // Draw the the entry only if it is within the usable surface
                }
            }
            mIsMoreAbove = !mEntries[0].IsVisible // If the very top entry is invisible due to being offscreen
            mIsMoreBelow = !mEntries[mEntries.size - 1].IsVisible // Likewise, if the very bottom entry is invisible
        }
        val entryRect = hoveredEntry.Rect
        val cursorRect = mCursor.Rect
        // Move the cursor a little left of the entry
        if (mShouldCenterCursor)
            mCursor.move(usableSurface.X + ((mCursorPadding - cursorRect.Width) / 2f), entryRect.Y + (entryRect.Height / 2f) - (cursorRect.Height / 2f))
        else
            mCursor.move(usableSurface.X + ((mCursorPadding - cursorRect.Width) / 2f), entryRect.Y + entryRect.Height - cursorRect.Height)
        mHollowCursor.move(mCursor.Rect)
        hoveredEntry.OnHover.invoke()
    }

    protected open fun doEntries() {
        if (mEntries.isEmpty())
            return
        val usableSurface = UsableSurface
        val entryHeight = mEntries[0].Rect.Height
        var spacing = 0f
        // If there would be unused vertical space and this menu should evenly space entries
        if (((mEntries.size * entryHeight) < usableSurface.Height) && IsEvenlySpaced)
            spacing = (usableSurface.Height - (mEntries.size * entryHeight)) / (mEntries.size + 1)
        for (i in mEntries.indices) {
            mEntries[i].move(usableSurface.X + mCursorPadding, usableSurface.Y + usableSurface.Height - ((entryHeight + spacing) * (i + 1)))
            mEntries[i].determineVisibilityForSurface(usableSurface)
        }
    }

    protected open fun doIndicators() {
        mIndicatorTasks.clear()
        if (mIsOverflow) {
            val usableSurface = UsableSurface
            val indicatorRect = mMoreAboveIndicator.Rect
            // Move the more above and below indicators to the top right and bottom right of the usable surface, respectively
            mMoreAboveIndicator.move(usableSurface.X + usableSurface.Width - indicatorRect.Width, usableSurface.Y + usableSurface.Height - indicatorRect.Height)
            mMoreBelowIndicator.move(usableSurface.X + usableSurface.Width - indicatorRect.Width, usableSurface.Y)
            // Schedule a task that turns the indicators on and off every 650 milliseconds
            mIndicatorTasks.schedulePeriodic(650) { mShouldDrawMoreIndicators = !mShouldDrawMoreIndicators }
        }
    }
}