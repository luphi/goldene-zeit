package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.Tasks
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLText
import java.util.*

@Suppress("PropertyName", "PrivatePropertyName")
open class UIReusableDialogue : IDrawable {
    // IDrawable property
    override val Rect: Rect
        get() = mBubble.Rect

    private var mIsInitialized = false
    private val DELAY_BETWEEN_LINES_IN_MILLISECONDS = 2000L
    protected val mBubble = UIBubble()
    protected val mGLTexts = LinkedList<GLText>()
    protected val mTasks = Tasks()
    private var mLine1: GLText? = null
    private var mLine2: GLText? = null
    private var mLine1Rect = Rect(0f, 0f, 0f, 0f)
    protected var mLine2Rect = Rect(0f, 0f, 0f, 0f)
    protected var mLineIndex = 0

    var IsBottomAligned = true
        set(value) {
            field = value
            if (value)
                mBubble.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
            else
                mBubble.move(gGZ.Camera.LeftEdge, gGZ.Camera.TopEdge - mBubble.Rect.Height)
            val usableSurface = mBubble.UsableSurface
            // Calculate and store the positions text will be placed at when it's applied
            mLine1Rect = Rect(usableSurface.X, usableSurface.Y + (gGZ.FontSize * 2f), usableSurface.Width, gGZ.FontSize)
            mLine2Rect = Rect(usableSurface.X, usableSurface.Y, usableSurface.Width, gGZ.FontSize)
            mLine1?.move(mLine1Rect)
            mLine2?.move(mLine2Rect)
        }
    var Text = ""
        set(text) {
            field = text
            for (glText in mGLTexts)
                glText.free()
            mGLTexts.clear()
            mLineIndex = 0
            val usableWidth = mBubble.UsableSurface.Width
            for (line in field.lines()) {
                var workingLine = ""
                val split = line.trim().split("\\s".toRegex())
                for (i in split.indices) {
                    val word = split[i]
                    val wouldBeLine = "$workingLine $word".trim()
                    val isTooLong = (wouldBeLine.length.toFloat() * gGZ.FontSize > usableWidth)
                    val isFinalWord = i == split.size - 1
                    // If appending the next word would make the line too long OR if there are still more words following this one (thus requiring the more
                    // indicator) and this word would overlap the more indicator OR if this is simply the last word
                    if (isTooLong || isFinalWord) {
                        // If this is the last word and it can be included on this line, include it
                        if (isFinalWord && !isTooLong)
                            workingLine = wouldBeLine
                        // Create a GLText from the line and add it to the list
                        var glLine = GLText()
                        glLine.init(workingLine)
                        mGLTexts.add(glLine)
                        workingLine = word
                        // If this is the final word and it was skipped above due to not fitting on the line, put it on one more line
                        if (isFinalWord && isTooLong) {
                            glLine = GLText()
                            glLine.init(workingLine)
                            mGLTexts.add(glLine)
                        }
                    } else
                        workingLine = wouldBeLine
                }
            }
            if (mGLTexts.isNotEmpty()) {
                mLine1 = mGLTexts[0]
                mLine1?.move(mLine1Rect)
            } else mLine1?.Text = ""
            if (mGLTexts.size > 1) {
                mLine2 = mGLTexts[1]
                mLine2?.move(mLine2Rect)
            } else mLine2?.Text = ""
            mTasks.clear()
            if (mGLTexts.size > 2)
                mTasks.schedulePeriodic(DELAY_BETWEEN_LINES_IN_MILLISECONDS) { nextLines() }
        }
    val UsableSurface
        get() = mBubble.UsableSurface

    fun init() {
        // Size the dialogue to fit the width of the view and two lines of text with some space between them (the space being equal to the height of text)
        mBubble.init()
        mBubble.sizeForUsableHeight(gGZ.Camera.Width, gGZ.FontSize * 4f)
        // Use the alignment setter to position the dialogue to the bottom of the screen. It can be repositioned to the top at any time by using the same setter
        // with a 'false' value, although the majority of cases will retain the default.
        IsBottomAligned = true
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized)
            return
        mTasks.step(dt)
        mBubble.draw(dt)
        mLine1?.draw(dt)
        mLine2?.draw(dt)
    }

    protected fun nextLines() {
        // If the lines currently being displayed are the last in the text, wrap back around to the first line(s)
        mLineIndex = if (mGLTexts.size - mLineIndex <= 2) 0 else mLineIndex + 2
        mLine1 = mGLTexts[mLineIndex]
        mLine1?.move(mLine1Rect)
        if (mLineIndex + 1 < mGLTexts.size) {
            mLine2 = mGLTexts[mLineIndex + 1]
            mLine2?.move(mLine2Rect)
        } else
            mLine2 = null
    }
}