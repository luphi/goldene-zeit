package luphi.goldenezeit.ui

import kotlin.concurrent.withLock

open class UIJustifiedMenu(onCancel: () -> Unit) : UIMenu(onCancel) {
    init {
        mShouldCenterCursor = false
    }

    override fun size(w: Float, h: Float) {
        super.size(w, h)
        val usableSurface = mBubble.UsableSurface
        for (entry in mEntries)
            entry.sizeByWidth(usableSurface.Width - (mCursorPadding * if (mIsOverflow) 2f else 1f))
    }

    override fun autoSize() {
        super.autoSize()
        val usableSurface = mBubble.UsableSurface
        for (entry in mEntries)
            entry.sizeByWidth(usableSurface.Width - (mCursorPadding * if (mIsOverflow) 2f else 1f))
    }

    fun add(entry: UIJustifiedMenuEntry) {
        mEntriesLock.withLock { mEntries.add(entry) }
        if (mIsInitialized) {
            entry.init()
            if (mIsAutosized)
                autoSize()
            else
                size(Rect)
        }
    }
}