package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import kotlin.concurrent.withLock
import kotlin.math.max

class UIOptionMenu(onCancel: () -> Unit, onCancelHover: () -> Unit = {}) : UIJustifiedMenu(onCancel) {
    private val mOnCancelHover = onCancelHover

    init {
        IsEvenlySpaced = false
        mEntries.add(UIOptionMenuEntry("CANCEL", /* options */ arrayOf(), /* index */ 0, /* onChanged */ {}, /* onHover */ mOnCancelHover))
    }

    override fun handleInput(input: Input) {
        if (input.IsPress) {
            when (input.Opcode) {
                Input.Button.A -> {
                    // If the last index (i.e. "CANCEL") is selected
                    if (mHoveredIndex == mEntries.size - 1)
                        mOnCancel.invoke()
                    else {
                        (mEntries.getOrNull(mHoveredIndex) as UIOptionMenuEntry?)?.next()
                        input.Opcode = Input.Button.NO_OP // Neuter the event before handing it to super.handleUIEvent()
                    }
                }
                Input.Button.DPAD_LEFT -> (mEntries.getOrNull(mHoveredIndex) as UIOptionMenuEntry?)?.previous()
                Input.Button.DPAD_RIGHT -> (mEntries.getOrNull(mHoveredIndex) as UIOptionMenuEntry?)?.next()
                else -> {
                }
            }
            if ((input.Opcode == Input.Button.DPAD_LEFT) || (input.Opcode == Input.Button.DPAD_RIGHT))
                return // The parent handleInput() will do something completely different with these two and we want to avoid that
        }
        super.handleInput(input)
    }

    override fun size(w: Float, h: Float) {
        super.size(w, h)
        doOptionWidths()
    }

    override fun autoSize() {
        super.autoSize()
        doOptionWidths()
    }

    fun add(entry: UIOptionMenuEntry) {
        // Note: this class' init {} block already added a "CANCEL" entry and it should remain the last entry
        mEntriesLock.withLock { mEntries.add(mEntries.size - 1, entry) }
        if (mIsInitialized) {
            entry.init()
            if (mIsAutosized)
                autoSize()
            else
                size(Rect)
        }
    }

    override fun remove(index: Int) {
        // If the index is for a valid entry that isn't "CANCEL"
        if ((index >= 0) && (index < mEntries.size - 1))
            super.remove(index)
    }

    override fun clear() {
        super.clear() // Remove everything and zero the hovered index
        mEntries.add(UIOptionMenuEntry("CANCEL", /* options */ arrayOf(), /* index */ 0, /* onChanged */ {}, /* onHover */ mOnCancelHover))
    }

    private fun doOptionWidths() {
        var maxOptionWidth = 0f
        for (entry in mEntries)
            maxOptionWidth = max(maxOptionWidth, (entry as UIOptionMenuEntry).NativeOptionWidth)
        for (entry in mEntries)
            (entry as UIOptionMenuEntry).OptionWidth = maxOptionWidth
    }
}