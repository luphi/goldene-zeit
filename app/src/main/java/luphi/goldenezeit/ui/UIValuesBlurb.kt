package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLText
import java.util.LinkedList
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.LinkedHashMap
import kotlin.concurrent.withLock

// Similar to the more generic UIBlurb, this class creates a non-interactable blurb or sign. Unlike the other, this displays a sort of key -> value list of
// entries passed in with a map.
@Suppress("PropertyName")
class UIValuesBlurb : IDrawable {
    override val Rect: Rect
        get() = mBubble?.Rect ?: Rect(0f, 0f, 0f, 0f)

    private var mBubble: UIBubble? = null
    private val mMap = LinkedHashMap<String, LinkedList<String>>()
    private val mGLTexts = LinkedList<GLText>()
    private val mGLTextsLock = ReentrantLock()
    private var mIsInitialized = false
    private var mIsMoved = false
    private var mIsSized = false

    var Map: Map<String, Array<String>> = LinkedHashMap()
        set(value) {
            mMap.clear()
            for (glText in mGLTexts)
                glText.free()
            mGLTextsLock.withLock { mGLTexts.clear() }
            field = value
            for (pair in field) {
                mMap[pair.key] = LinkedList()
                var text = GLText()
                text.init(pair.key)
                mGLTextsLock.withLock { mGLTexts.add(text) }
                for (mappedValue in pair.value) {
                    mMap[pair.key]?.add(mappedValue)
                    text = GLText()
                    text.init(mappedValue)
                    mGLTextsLock.withLock { mGLTexts.add(text) }
                }
            }
            autoSize()
        }

    fun init(useInfoBubble: Boolean = true) {
        mBubble = if (useInfoBubble) UIInfoBubble() else UIBubble()
        mBubble?.init()
        mBubble?.move(0f, 0f)
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized || !mIsMoved || !mIsSized)
            return
        mBubble?.draw(dt)
        mGLTextsLock.withLock {
            for (text in mGLTexts)
                text.draw(dt)
        }
    }

    fun move(x: Float, y: Float) {
        val rect = Rect
        translate(x - rect.X, y - rect.Y)
        mIsMoved = true
    }

    fun translate(dx: Float, dy: Float) {
        mBubble?.translate(dx, dy)
        for (text in mGLTexts)
            text.translate(dx, dy)
    }

    fun size(w: Float, h: Float) {
        mBubble?.size(w, h)
        val verticalSpacing = if (mBubble != null)
            ((mBubble!!.UsableSurface.Height - (gGZ.FontSize * mGLTexts.size)) / (mGLTexts.size + 1))
        else
            gGZ.FontSize * mGLTexts.size
        justify(verticalSpacing)
        mIsSized = true
    }

    fun sizeByWidth(w: Float) {
        mBubble?.sizeForUsableHeight(w, mGLTexts.size * gGZ.FontSize)
        justify(0f)
        mIsSized = true
    }

    fun sizeByHeight(h: Float) {
        var maxWidth = 0f
        for (glText in mGLTexts) {
            val glTextRect = glText.Rect
            if (glTextRect.Width > maxWidth)
                maxWidth = glTextRect.Width
        }
        mBubble?.sizeForUsableWidth(maxWidth, h)
        val verticalSpacing = if (mBubble != null)
            ((mBubble!!.UsableSurface.Height - (gGZ.FontSize * mGLTexts.size)) / (mGLTexts.size + 1))
        else
            gGZ.FontSize * mGLTexts.size
        justify(verticalSpacing)
        mIsSized = true
    }

    private fun autoSize() {
        var maxWidth = 0f
        for (glText in mGLTexts) {
            val glTextRect = glText.Rect
            if (glTextRect.Width > maxWidth)
                maxWidth = glTextRect.Width
        }
        mBubble?.sizeForUsableWidthAndHeight(maxWidth, mGLTexts.size * gGZ.FontSize)
        justify(0f)
        mIsSized = true
    }

    private fun justify(verticalSpacing: Float) {
        val usableSurface = mBubble?.UsableSurface ?: return
        var i = 0
        for (pair in mMap) {
            var text = mGLTexts[i]
            if (i == 0)
                text.move(usableSurface.X, usableSurface.Y + usableSurface.Height - gGZ.FontSize - verticalSpacing)
            else
                text.move(usableSurface.X, mGLTexts[i - 1].Rect.Y - gGZ.FontSize - verticalSpacing)
            for (value in pair.value) {
                i += 1
                text = mGLTexts[i]
                text.move(usableSurface.X + usableSurface.Width - text.Rect.Width, mGLTexts[i - 1].Rect.Y - gGZ.FontSize - verticalSpacing)
            }
            i += 1
        }
    }
}