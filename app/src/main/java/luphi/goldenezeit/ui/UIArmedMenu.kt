package luphi.goldenezeit.ui

import luphi.goldenezeit.Player
import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.item.ITAccessory
import luphi.goldenezeit.item.ITArmor
import luphi.goldenezeit.item.ITWeapon
import luphi.goldenezeit.item.Item
import java.util.LinkedList
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap

// The armed (referring to equipment) menu uses the whole screen, displays the available and chosen equipment (weapon, armor, and accessories) to be used in
// battle, and is launched from the overworld's root menu or the battle menu's item submenu
class UIArmedMenu(onCancel: () -> Unit) : IDrawable, IInteractable {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    private val mBackground = GLPrimitive()
    private var mActiveBlurb: UIValuesBlurb? = null
    private var mActiveMenu: UIMenu? = null
    private var mActiveCategory = Categories.NONE
    private val mCategoryMenu = UIJustifiedMenu { /* onCancel */ onCancel.invoke() } // Item category menu
    private val mItemsMenu = UIMenu {  // Lists equipable weapons, armors, or accessories
        // onCancel
        mActiveBlurb = null
        mActiveMenu = mCategoryMenu
        mActiveCategory = Categories.NONE
        setOverviewBlurb(null)
    }
    private val mOverviewBlurb = UIValuesBlurb()
    private val mWeaponArmorBlurb = UIValuesBlurb()
    private val mAccessoryBlurb = UIValuesBlurb()

    init {
        mBackground.IsStatic = true
        mBackground.Color = floatArrayOf(1f, 1f, 1f)
    }

    fun init() {
        // Create a white background to fill the screen.
        // This UI is launched from the overworld mode which uses a black clear color making this necessary.
        mBackground.init(GLPrimitive.Shape.QUAD)
        mBackground.size(gGZ.Camera.Width, gGZ.Camera.Height)
        mBackground.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mOverviewBlurb.init()
        mWeaponArmorBlurb.init()
        mAccessoryBlurb.init()
        mCategoryMenu.init()
        mItemsMenu.init()
        setCategoryMenu()
        setOverviewBlurb(null)
        setWeaponArmorBlurb(gGZ.Player.EquippedWeapon)
        setAccessoryBlurb(null)
        mActiveMenu = mCategoryMenu
    }

    override fun draw(dt: Long) {
        mBackground.draw(dt)
        mOverviewBlurb.draw(dt)
        mActiveBlurb?.draw(dt)
        mActiveMenu?.draw(dt)
    }

    override fun handleInput(input: Input) {
        mActiveMenu?.handleInput(input)
    }

    private fun setCategoryMenu() {
        mCategoryMenu.clear()
        mCategoryMenu.add(UIJustifiedMenuEntry("WEAPON", gGZ.Player.EquippedWeapon?.Name ?: "NONE", {
            // onSelected
            mActiveCategory = Categories.WEAPON
            mActiveBlurb = mWeaponArmorBlurb
            setItemsMenu(Item.ItemCategory.WEAPON)
            setWeaponArmorBlurb(gGZ.Player.EquippedWeapon)
            mActiveMenu = mItemsMenu
        }))
        mCategoryMenu.add(UIJustifiedMenuEntry("ARMOR", gGZ.Player.EquippedArmor?.Name ?: "NONE", {
            // onSelected
            mActiveCategory = Categories.ARMOR
            mActiveBlurb = mWeaponArmorBlurb
            setItemsMenu(Item.ItemCategory.ARMOR)
            setWeaponArmorBlurb(gGZ.Player.EquippedArmor)
            mActiveMenu = mItemsMenu
        }))
        mCategoryMenu.add(UIJustifiedMenuEntry("ACC. 1", gGZ.Player.EquippedAccessory1?.Name ?: "NONE", {
            // onSelected
            mActiveCategory = Categories.ACCESSORY1
            mActiveBlurb = mAccessoryBlurb
            setItemsMenu(Item.ItemCategory.ACCESSORY)
            setAccessoryBlurb(gGZ.Player.EquippedAccessory1)
            mActiveMenu = mItemsMenu
        }))
        mCategoryMenu.add(UIJustifiedMenuEntry("ACC. 2", gGZ.Player.EquippedAccessory2?.Name ?: "NONE", {
            // onSelected
            mActiveCategory = Categories.ACCESSORY2
            mActiveBlurb = mAccessoryBlurb
            setItemsMenu(Item.ItemCategory.ACCESSORY)
            setAccessoryBlurb(gGZ.Player.EquippedAccessory2)
            mActiveMenu = mItemsMenu
        }))
        mCategoryMenu.autoSize()
        mCategoryMenu.size(gGZ.Camera.WidthWithMargin, mCategoryMenu.Rect.Height)
        mCategoryMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - mCategoryMenu.Rect.Height)
    }

    private fun setItemsMenu(category: Item.ItemCategory) {
        val options = LinkedList<Item>()
        for (pair in gGZ.Player.Inventory) {
            val item = gGZ.Resources.loadItem(pair.key)
            if (item.Category == category)
                options.add(item)
        }
        options.add(Item(-1, Item.ItemCategory.SPECIAL, "NONE", "", 0))
        mItemsMenu.clear()
        for (option in options) {
            mItemsMenu.add(UIMenuEntry(option.Name, {
                // onSelect
                val returnToInventory: Item?
                when (mActiveCategory) {
                    Categories.NONE -> returnToInventory = null
                    Categories.WEAPON -> {
                        returnToInventory = gGZ.Player.EquippedWeapon
                        gGZ.Player.EquippedWeapon = if (option.Name != "NONE") option as ITWeapon else null
                    }
                    Categories.ARMOR -> {
                        returnToInventory = gGZ.Player.EquippedArmor
                        gGZ.Player.EquippedArmor = if (option.Name != "NONE") option as ITArmor else null
                    }
                    Categories.ACCESSORY1 -> {
                        returnToInventory = gGZ.Player.EquippedAccessory1
                        gGZ.Player.EquippedAccessory1 = if (option.Name != "NONE") option as ITAccessory else null
                    }
                    Categories.ACCESSORY2 -> {
                        returnToInventory = gGZ.Player.EquippedAccessory2
                        gGZ.Player.EquippedAccessory2 = if (option.Name != "NONE") option as ITAccessory else null
                    }
                }
                if (option.Name != "NONE") {
                    gGZ.Player.Inventory[option.ID] = gGZ.Player.Inventory[option.ID]!! - 1
                    if (gGZ.Player.Inventory[option.ID] == 0)
                        gGZ.Player.Inventory.remove(option.ID)
                }
                if (returnToInventory != null) {
                    if (gGZ.Player.Inventory.containsKey(returnToInventory.ID))
                        gGZ.Player.Inventory[returnToInventory.ID] = gGZ.Player.Inventory[returnToInventory.ID]!! + 1
                    else
                        gGZ.Player.Inventory[returnToInventory.ID] = 1
                }
                mActiveBlurb = null
                mActiveCategory = Categories.NONE
                setCategoryMenu()
                mActiveMenu = mCategoryMenu
                setOverviewBlurb(null)
            }, {
                // onHover
                setOverviewBlurb(if (option.Name != "NONE") option else null)
                when (mActiveCategory) {
                    Categories.NONE -> {
                    }
                    Categories.WEAPON, Categories.ARMOR -> setWeaponArmorBlurb(if (option.Name != "NONE") option else null)
                    Categories.ACCESSORY1, Categories.ACCESSORY2 -> setAccessoryBlurb(if (option.Name != "NONE") option else null)
                }
            }))
        }
        val categoryMenuRect = mCategoryMenu.Rect
        mItemsMenu.size(categoryMenuRect.Width, categoryMenuRect.Height)
        mItemsMenu.move(categoryMenuRect.X, categoryMenuRect.Y)
        mItemsMenu.IsFocused = true
    }

    private fun setOverviewBlurb(item: Item?) {
        var wouldBePlayer: Player? = gGZ.Player.clone()
        when (mActiveCategory) {
            Categories.NONE -> wouldBePlayer = null
            Categories.WEAPON -> wouldBePlayer?.EquippedWeapon = if (item != null) (item as ITWeapon) else null
            Categories.ARMOR -> wouldBePlayer?.EquippedArmor = if (item != null) (item as ITArmor) else null
            Categories.ACCESSORY1 -> wouldBePlayer?.EquippedAccessory1 = if (item != null) (item as ITAccessory) else null
            Categories.ACCESSORY2 -> wouldBePlayer?.EquippedAccessory2 = if (item != null) (item as ITAccessory) else null
        }
        val stats = LinkedHashMap<String, Array<String>>()
        if (wouldBePlayer != null) {
            val deltas = HashMap<String, Int>()
            deltas["STR"] = wouldBePlayer.StrengthBonus - gGZ.Player.StrengthBonus
            deltas["VIT"] = wouldBePlayer.VitalityBonus - gGZ.Player.VitalityBonus
            deltas["AGT"] = wouldBePlayer.AgilityBonus - gGZ.Player.AgilityBonus
            deltas["SNS"] = wouldBePlayer.SenseBonus - gGZ.Player.SenseBonus
            stats["STR BONUS"] = arrayOf(gGZ.Player.StrengthBonus.toString() + if (deltas["STR"] != 0) " -> " + wouldBePlayer.StrengthBonus.toString() else "")
            stats["VIT BONUS"] = arrayOf(gGZ.Player.VitalityBonus.toString() + if (deltas["VIT"] != 0) " -> " + wouldBePlayer.VitalityBonus.toString() else "")
            stats["AGT BONUS"] = arrayOf(gGZ.Player.AgilityBonus.toString() + if (deltas["AGT"] != 0) " -> " + wouldBePlayer.AgilityBonus.toString() else "")
            stats["SNS BONUS"] = arrayOf(gGZ.Player.SenseBonus.toString() + if (deltas["SNS"] != 0) " -> " + wouldBePlayer.SenseBonus.toString() else "")
        } else {
            stats["STR BONUS"] = arrayOf(gGZ.Player.StrengthBonus.toString())
            stats["VIT BONUS"] = arrayOf(gGZ.Player.VitalityBonus.toString())
            stats["AGT BONUS"] = arrayOf(gGZ.Player.AgilityBonus.toString())
            stats["SNS BONUS"] = arrayOf(gGZ.Player.SenseBonus.toString())
        }
        mOverviewBlurb.Map = stats
        mOverviewBlurb.size(
            (gGZ.Camera.WidthWithMargin / 2f) - (gGZ.Camera.Margin / 2f),
            gGZ.Camera.HeightWithMargin - mCategoryMenu.Rect.Height - gGZ.Camera.Margin
        )
        mOverviewBlurb.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.BottomEdgeWithMargin)
    }

    private fun setWeaponArmorBlurb(item: Item?) {
        val stats = LinkedHashMap<String, Array<String>>()
        if (item == null) {
            stats["BONUS"] = arrayOf("0")
            stats["STYLE"] = arrayOf("")
            stats["EFFECT"] = arrayOf("")
            if (mActiveCategory == Categories.WEAPON) {
                stats["BLEED"] = arrayOf("NO")
                stats["BLIND"] = arrayOf("NO")
                stats["STUN"] = arrayOf("NO")
                stats["FEAR"] = arrayOf("NO")
            } else if (mActiveCategory == Categories.ARMOR) {
                stats["BLOCK BLEED"] = arrayOf("NO")
                stats["BLOCK BLIND"] = arrayOf("NO")
                stats["BLOCK STUN"] = arrayOf("NO")
                stats["BLOCK FEAR"] = arrayOf("NO")
            }
        } else if (mActiveCategory == Categories.WEAPON) {
            val weapon = item as ITWeapon
            stats["BONUS"] = arrayOf(weapon.Bonus.toString())
            stats["EFFECT"] = arrayOf(weapon.Effect.toString())
            stats["BLEED"] = arrayOf(if (weapon.CanCauseBleed) "YES" else "NO")
            stats["BLIND"] = arrayOf(if (weapon.CanBlind) "YES" else "NO")
            stats["STUN"] = arrayOf(if (weapon.CanStun) "YES" else "NO")
            stats["FEAR"] = arrayOf(if (weapon.CanCauseFear) "YES" else "NO")
        } else if (mActiveCategory == Categories.ARMOR) {
            val armor = item as ITArmor
            stats["BONUS"] = arrayOf(armor.Bonus.toString())
            stats["STYLE"] = arrayOf(armor.Style.toString())
            stats["EFFECT"] = arrayOf(armor.Effect.toString())
            stats["BLOCK BLEED"] = arrayOf(if (armor.PreventsBleed) "YES" else "NO")
            stats["BLOCK BLIND"] = arrayOf(if (armor.PreventsBlind) "YES" else "NO")
            stats["BLOCK STUN"] = arrayOf(if (armor.PreventsStun) "YES" else "NO")
            stats["BLOCK FEAR"] = arrayOf(if (armor.PreventsFear) "YES" else "NO")
        }
        mWeaponArmorBlurb.Map = stats
        mWeaponArmorBlurb.size(
            (gGZ.Camera.WidthWithMargin / 2f) - (gGZ.Camera.Margin / 2f),
            gGZ.Camera.HeightWithMargin - mCategoryMenu.Rect.Height - gGZ.Camera.Margin
        )
        mWeaponArmorBlurb.move(gGZ.Camera.RightEdgeWithMargin - mWeaponArmorBlurb.Rect.Width, gGZ.Camera.BottomEdgeWithMargin)
    }

    private fun setAccessoryBlurb(item: Item?) {
        val stats = LinkedHashMap<String, Array<String>>()
        if (item == null) {
            stats["EFFECT 1"] = arrayOf("")
            stats["EFFECT 2"] = arrayOf("")
            stats["EFFECT 3"] = arrayOf("")
            stats["EFFECT 4"] = arrayOf("")
        } else {
            val accessory = (item as ITAccessory)
            stats["EFFECT 1"] = arrayOf(if (accessory.Effect1 != ITAccessory.AccessoryEffect.NONE) accessory.Effect1.toString() else "")
            stats["EFFECT 2"] = arrayOf(if (accessory.Effect2 != ITAccessory.AccessoryEffect.NONE) accessory.Effect2.toString() else "")
            stats["EFFECT 3"] = arrayOf(if (accessory.Effect3 != ITAccessory.AccessoryEffect.NONE) accessory.Effect3.toString() else "")
            stats["EFFECT 4"] = arrayOf(if (accessory.Effect4 != ITAccessory.AccessoryEffect.NONE) accessory.Effect4.toString() else "")
        }
        mAccessoryBlurb.Map = stats
        mAccessoryBlurb.size(
            (gGZ.Camera.WidthWithMargin / 2f) - (gGZ.Camera.Margin / 2f),
            gGZ.Camera.HeightWithMargin - mCategoryMenu.Rect.Height - gGZ.Camera.Margin
        )
        mAccessoryBlurb.move(gGZ.Camera.RightEdgeWithMargin - mAccessoryBlurb.Rect.Width, gGZ.Camera.BottomEdgeWithMargin)
    }

    private enum class Categories { NONE, WEAPON, ARMOR, ACCESSORY1, ACCESSORY2 }
}