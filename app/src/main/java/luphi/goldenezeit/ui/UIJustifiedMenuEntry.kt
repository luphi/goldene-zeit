package luphi.goldenezeit.ui

import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLText
import kotlin.math.max

open class UIJustifiedMenuEntry(
    leftLabel: String,
    private val mRightLabel: String,
    onSelect: () -> Unit,
    onHover: () -> Unit = {},
    private val mUseTwoLines: Boolean = false
) :
    UIMenuEntry(leftLabel, onSelect, onHover) {
    protected val mRightLabelText = GLText()

    override val NativeWidth: Float
        get() = if (mUseTwoLines) max(mLabelText.Rect.Width, mRightLabelText.Rect.Width) else mLabelText.Rect.Width + gGZ.FontSize + mRightLabelText.Rect.Width
    override val NativeHeight: Float
        get() = if (mUseTwoLines) gGZ.FontSize * 2f else gGZ.FontSize + (gGZ.Camera.Margin * 2f)

    override fun init() {
        super.init()
        mRightLabelText.init(mRightLabel)
        mWidth = NativeWidth
        mHeight = NativeHeight
    }

    override fun draw(dt: Long) {
        super.draw(dt)
        mRightLabelText.draw(dt)
    }

    override fun translate(dx: Float, dy: Float) {
        super.translate(dx, dy)
        mRightLabelText.translate(dx, dy)
    }

    override fun move(x: Float, y: Float) {
        super.move(x, y)
        if (mUseTwoLines) {
            mLabelText.move(mX, mY + (mHeight / 2f))
            mRightLabelText.move(mX + mWidth - mRightLabelText.Rect.Width, mY)
        } else
            mRightLabelText.move(mX + mWidth - mRightLabelText.Rect.Width, mLabelText.Rect.Y)
    }
}