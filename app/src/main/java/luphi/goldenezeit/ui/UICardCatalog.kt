package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.card.Card
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive

class UICardCatalog(onCancel: () -> Unit) : IDrawable, IInteractable, IProgressive {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    private val mInventoryMenu =
        UIInventoryMenu( /* behavior */ UIInventoryMenu.Behavior.TOSS_ONLY,
            { /* onCancel */ onCancel.invoke() },
            { /* onCancelHover */ mDisplayCard = null })
    private val mNumbersBlurb = UIValuesBlurb()
    private val mBackground = GLPrimitive()
    private var mDisplayCard: Card? = null

    fun init() {
        // Create a white background to fill the screen.
        // This UI is launched from the overworld mode which uses a black clear color making this necessary.
        mBackground.IsStatic = true
        mBackground.Color = floatArrayOf(1f, 1f, 1f)
        mBackground.init(GLPrimitive.Shape.QUAD)
        mBackground.size(gGZ.Camera.Width, gGZ.Camera.Height)
        mBackground.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        for (idQuantityPair in gGZ.Player.Cards.toSortedMap()) {
            mInventoryMenu.add(
                UIInventoryMenuEntry(gGZ.Resources.loadCard(idQuantityPair.key).Shorthand, idQuantityPair.value,
                    { /* onUse */ },
                    { /* onToss */ quantityToRemove ->
                        // If removing the chosen quantity was enough to remove the ID from the deck entirely
                        if (gGZ.Player.removeCard(idQuantityPair.key, quantityToRemove))
                            mInventoryMenu.remove(mInventoryMenu.CurrentlySelected)
                        else
                            mInventoryMenu.setEntryQuantity(mInventoryMenu.CurrentlySelected, (gGZ.Player.Cards[idQuantityPair.key] ?: 0) - quantityToRemove)
                    },
                    { /* onHover */ setDisplayCard(idQuantityPair.key) }
                )
            )
        }
        mInventoryMenu.init()
        mInventoryMenu.autoSize()
        mInventoryMenu.move(gGZ.Camera.LeftEdge + gGZ.Camera.Margin, gGZ.Camera.TopEdge - mInventoryMenu.Rect.Height - gGZ.Camera.Margin)
        var numUnique = 0
        var numTotal = 0
        for (idQuantityPair in gGZ.Player.Cards) {
            numUnique += 1
            numTotal += idQuantityPair.value
        }
        val numbers = LinkedHashMap<String, Array<String>>()
        numbers["UNIQUE"] = arrayOf(numUnique.toString())
        numbers["TOTAL"] = arrayOf(numTotal.toString())
        mNumbersBlurb.init()
        mNumbersBlurb.Map = numbers
        mNumbersBlurb.sizeByWidth(gGZ.Camera.WidthWithMargin - mInventoryMenu.Rect.Width - gGZ.Camera.Margin)
        mNumbersBlurb.move(gGZ.Camera.RightEdge - mNumbersBlurb.Rect.Width - gGZ.Camera.Margin, gGZ.Camera.BottomEdge + gGZ.Camera.Margin)
        // Other than initialization, the display card will be chosen, sized, and positioned as an onHover callback via the inventory menu but that requires
        // both drawables involved (inventory menu and numbers blurb) be defined whereas they cannot be for the initial hover callback simply due to the
        // order in which both drawables are initialized. So, it must (kind of redundantly) be done here.
        if (gGZ.Player.Cards.size > 0)
            setDisplayCard(gGZ.Player.Cards.entries.first().key)
    }

    override fun draw(dt: Long) {
        mBackground.draw(dt)
        mNumbersBlurb.draw(dt)
        mDisplayCard?.draw(dt)
        mInventoryMenu.draw(dt)
    }

    override fun handleInput(input: Input) = mInventoryMenu.handleInput(input)

    override fun step(dt: Long) = mInventoryMenu.step(dt)

    private fun setDisplayCard(id: Int) {
        mDisplayCard = gGZ.Resources.loadCard(id)
        val inventoryMenuRect = mInventoryMenu.Rect
        val numbersBlurbRect = mNumbersBlurb.Rect
        // The surface formed by the remaining space to the right of the inventory menu and above the numbers blurb
        val surface = Rect(
            inventoryMenuRect.X + inventoryMenuRect.Width + gGZ.Camera.Margin,
            numbersBlurbRect.Y + numbersBlurbRect.Height + gGZ.Camera.Margin,
            gGZ.Camera.WidthWithMargin - inventoryMenuRect.Width - gGZ.Camera.Margin,
            gGZ.Camera.HeightWithMargin - numbersBlurbRect.Height - gGZ.Camera.Margin
        )
        var displayCardRect = mDisplayCard!!.Rect
        if ((surface.Width / surface.Height) > (displayCardRect.Width / displayCardRect.Height))
            mDisplayCard?.sizeByHeight(surface.Height * 0.8f)
        else
            mDisplayCard?.sizeByWidth(surface.Width * 0.8f)
        displayCardRect = mDisplayCard!!.Rect
        mDisplayCard?.move(surface.X + (surface.Width / 2f) - (displayCardRect.Width / 2f), surface.Y + (surface.Height / 2f) - (displayCardRect.Height / 2f))
    }
}
