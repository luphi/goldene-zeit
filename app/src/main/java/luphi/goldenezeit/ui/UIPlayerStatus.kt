package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.gl.GLSprite
import luphi.goldenezeit.gl.GLText
import java.util.LinkedList

class UIPlayerStatus(onDone: () -> Unit) : IDrawable, IInteractable {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    private val mOnDone = onDone
    private val mDrawables = LinkedList<IDrawable>()

    fun init() {
        // Create a white background to fill the screen.
        // This UI is launched from the overworld mode which uses a black clear color making this necessary.
        val background = GLPrimitive()
        background.IsStatic = true
        background.Color = floatArrayOf(1f, 1f, 1f) // White
        background.init(GLPrimitive.Shape.QUAD)
        background.size(gGZ.Camera.Width, gGZ.Camera.Height)
        background.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mDrawables.add(background)
        // The following areas divide the available surface into this general layout:
        // -------------------------------------------------------------------------
        // |                        | |                                            |
        // |                        | |                                            |
        // |                        | |                                            |
        // |                        | |                                            |
        // |           A            | |                   B                        |
        // |                        | |                                            |
        // |                        | |                                            |
        // |                        | |                                            |
        // |                        | |                                            |
        // |-----------------------------------------------------------------------|
        // |-----------------------------------------------------------------------|
        // |                                 | |                                   |
        // |                                 | |                                   |
        // |                                 | |                                   |
        // |                C                | |               D                   |
        // |                                 | |                                   |
        // |                                 | |                                   |
        // |                                 | |                                   |
        // |                                 | |                                   |
        // -------------------------------------------------------------------------
        // with A being a sprite depicting the player character, B being a general overview, C being battle-related stats (level, strength, etc.), and D being
        // an overview of experience points. The margins between the four are just for aesthetics.
        val usableSurface = Rect(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.BottomEdgeWithMargin, gGZ.Camera.WidthWithMargin, gGZ.Camera.HeightWithMargin)
        val spriteArea = Rect(
            usableSurface.X, // X
            usableSurface.Y + ((usableSurface.Height - gGZ.Camera.Margin) * 0.5f) + gGZ.Camera.Margin, // Y
            ((usableSurface.Width - gGZ.Camera.Margin) * 0.4f), // Width
            ((usableSurface.Height - gGZ.Camera.Margin) * 0.5f) // Height
        )
        val overviewArea = Rect(
            spriteArea.X + spriteArea.Width + gGZ.Camera.Margin, // X
            spriteArea.Y, // Y
            ((usableSurface.Width - gGZ.Camera.Margin) * 0.6f), // Width
            spriteArea.Height // Height
        )
        val statsArea = Rect(
            usableSurface.X, // X
            usableSurface.Y, // Y
            (usableSurface.Width - gGZ.Camera.Margin) * 0.5f, // Width
            (usableSurface.Height - gGZ.Camera.Margin) * 0.5f // Height
        )
        val experienceArea = Rect(
            statsArea.X + statsArea.Width + gGZ.Camera.Margin, // X
            statsArea.Y, // Y
            statsArea.Width, // Width
            statsArea.Height // Height
        )
        // Create a bubble for the player's stats
        val stats = LinkedHashMap<String, Array<String>>()
        stats["LEVEL"] = arrayOf("${gGZ.Player.Level}")
        stats["STRENGTH"] = arrayOf("${gGZ.Player.Strength}")
        stats["VITALITY"] = arrayOf("${gGZ.Player.Vitality}")
        stats["AGILITY"] = arrayOf("${gGZ.Player.Agility}")
        stats["SENSE"] = arrayOf("${gGZ.Player.Sense}")
        val statsBlurb = UIValuesBlurb()
        statsBlurb.init(false)
        statsBlurb.Map = stats
        statsBlurb.size(statsArea.Width, statsArea.Height)
        statsBlurb.move(statsArea.X, statsArea.Y)
        mDrawables.add(statsBlurb)
        // Create a border for the general status area
        val overviewBubble = UIInfoBubble()
        overviewBubble.init()
        overviewBubble.size(overviewArea.Width, overviewArea.Height)
        overviewBubble.move(overviewArea.X, overviewArea.Y)
        mDrawables.add(overviewBubble)
        // Load an image of the player to display in the top left of the surface and size it based on the aspect ratio of the image versus that of the
        // available space.
        val sprite = GLSprite()
        sprite.IsStatic = true
        sprite.init(gGZ.Resources.loadTexture("sprites/card/card_player_low.png")) // TODO use an actual sprite for the player
        var spriteRect = sprite.Rect
        if ((spriteRect.Width / spriteRect.Height) < (spriteArea.Width / spriteArea.Height))
            sprite.sizeByHeight(spriteArea.Height * 0.75f)
        else
            sprite.sizeByWidth(spriteArea.Width * 0.75f)
        spriteRect = sprite.Rect
        sprite.move(spriteArea.X + (spriteArea.Width / 2f) - (spriteRect.Width / 2f), spriteArea.Y + (spriteArea.Height / 2f) - (spriteRect.Height / 2f))
        mDrawables.add(sprite)
        // Add HP and remaining stat points information to the general status area.
        // First, the HP section.
        val overviewUsableSurface = overviewBubble.UsableSurface
        val hpLabelText = GLText()
        hpLabelText.init("HP")
        hpLabelText.move(overviewUsableSurface.X, overviewUsableSurface.Y + overviewUsableSurface.Height - hpLabelText.Rect.Height)
        mDrawables.add(hpLabelText)
        val healthBar = UIHealthBar()
        healthBar.init()
        healthBar.sizeByWidth(overviewUsableSurface.Width)
        var healthBarRect = healthBar.Rect
        healthBar.move(overviewUsableSurface.X, hpLabelText.Rect.Y - healthBarRect.Height - gGZ.Camera.Margin)
        healthBar.Percentage = gGZ.Player.HP.toFloat() / gGZ.Player.MaxHP.toFloat()
        mDrawables.add(healthBar)
        val currentHPText = GLText()
        currentHPText.init("${gGZ.Player.HP}/")
        healthBarRect = healthBar.Rect
        currentHPText.move(healthBarRect.X, healthBarRect.Y - currentHPText.Rect.Height - gGZ.Camera.Margin)
        mDrawables.add(currentHPText)
        val maxHPText = GLText()
        maxHPText.init("${gGZ.Player.MaxHP}")
        val maxHPTextRect = maxHPText.Rect
        maxHPText.move(healthBarRect.X + healthBarRect.Width - maxHPTextRect.Width, healthBarRect.Y - maxHPTextRect.Height - gGZ.Camera.Margin)
        mDrawables.add(maxHPText)
        val hpLabelTextRect = hpLabelText.Rect
        val currentHPTextRect = currentHPText.Rect
        val hpSectionHeight = hpLabelTextRect.Y + hpLabelTextRect.Height - currentHPTextRect.Y
        // Second, the available stat points section
        val availableText = GLText()
        availableText.init("AVAILABLE")
        mDrawables.add(availableText)
        val statPointsText = GLText()
        statPointsText.init(" STAT POINTS")
        mDrawables.add(statPointsText)
        val statPointsValueText = GLText()
        statPointsValueText.init("${gGZ.Player.AvailableStatPoints}")
        mDrawables.add(statPointsValueText)
        val statPointsSectionHeight = hpLabelTextRect.Height + healthBarRect.Height + statPointsValueText.Rect.Height
        // Third, and final, the money section
        val moneyText = GLText()
        moneyText.init("MONEY")
        mDrawables.add(moneyText)
        val moneyValueText = GLText()
        moneyValueText.init("$${gGZ.Player.Cash}")
        mDrawables.add(moneyValueText)
        var moneyTextRect = moneyText.Rect
        val moneySectionHeight = moneyTextRect.Height + moneyValueText.Rect.Height
        // Evenly space the above three sections. (The HP drawables have already been positioned leaving just the stat points and money sections.)
        val spacing = (overviewUsableSurface.Height - (hpSectionHeight + statPointsSectionHeight + moneySectionHeight)) / 2f // Spacing between the sections
        moneyValueText.move(overviewUsableSurface.X + overviewUsableSurface.Width - moneyValueText.Rect.Width, overviewUsableSurface.Y)
        val moneyValueTextRect = moneyValueText.Rect
        moneyText.move(overviewUsableSurface.X, moneyValueTextRect.Y + moneyValueTextRect.Height)
        moneyTextRect = moneyText.Rect
        statPointsValueText.move(
            overviewUsableSurface.X + overviewUsableSurface.Width - statPointsValueText.Rect.Width,
            moneyTextRect.Y + moneyTextRect.Height + spacing
        )
        val statPointsValueTextRect = statPointsValueText.Rect
        statPointsText.move(overviewUsableSurface.X, statPointsValueTextRect.Y + statPointsValueTextRect.Height)
        val statPointsTextRect = statPointsText.Rect
        availableText.move(overviewUsableSurface.X, statPointsTextRect.Y + statPointsTextRect.Height)
        // Create a border for the experience information area sized to fit the remaining space in the bottom right of the surface, then place it in the
        // bottom right.
        val experienceBubble = UIInfoBubble()
        experienceBubble.init()
        experienceBubble.size(experienceArea.Width, experienceArea.Height)
        experienceBubble.move(experienceArea.X, experienceArea.Y)
        mDrawables.add(experienceBubble)
        // Fill the experience information area with the player's experience with the labels being aligned to the left and the values to the right. Also add
        // an experience bar to the bottom of this area.
        val experienceUsableSurface = experienceBubble.UsableSurface
        for (index in 1..6) {
            // Skip index 3 and 4 to provide some space between indices 2 and 5
            if ((index == 3) || (index == 4))
                continue
            val text = GLText()
            when (index) {
                1 -> text.init("EXP POINTS")
                2 -> text.init("${gGZ.Player.Experience}")
                5 -> text.init("LEVEL UP")
                6 -> text.init("${gGZ.Utils.experienceForLevel(gGZ.Player.Level + 1) - gGZ.Player.Experience}")
            }
            val textRect = text.Rect
            text.move(
                if (index % 2 == 1) experienceUsableSurface.X else experienceUsableSurface.X + experienceUsableSurface.Width - textRect.Width,
                experienceUsableSurface.Y + experienceUsableSurface.Height - (textRect.Height * (index + 1))
            )
            mDrawables.add(text)
        }
        val experienceBar = UIExperienceBar()
        experienceBar.init()
        experienceBar.sizeByWidth(experienceUsableSurface.Width)
        experienceBar.move(experienceUsableSurface.X, experienceUsableSurface.Y + gGZ.FontSize)
        experienceBar.Percentage = (gGZ.Player.Experience - gGZ.Utils.experienceForLevel(gGZ.Player.Level)).toFloat() /
                (gGZ.Utils.experienceForLevel(gGZ.Player.Level + 1) - gGZ.Utils.experienceForLevel(gGZ.Player.Level)).toFloat()
        mDrawables.add(experienceBar)
    }

    override fun draw(dt: Long) {
        for (drawable in mDrawables)
            drawable.draw(dt)
    }

    override fun handleInput(input: Input) {
        if (input.IsPress && (input.Opcode == Input.Button.A || input.Opcode == Input.Button.B))
            mOnDone.invoke()
    }
}