package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.gGZ
import kotlin.concurrent.withLock

class UIInventoryMenu(private val mBehavior: Behavior, onCancel: () -> Unit, private val mOnCancelHover: () -> Unit = {}) : UIJustifiedMenu(onCancel) {
    enum class Behavior { USE_AND_TOSS, TOSS_ONLY, NEITHER_USE_NOR_TOSS, NO_SELECTOR }

    // Use/toss menu-related variable(s)
    private var mUseMenu: UIMenu? = null // Either "USE" and "TOSS" options, or just "USE"

    // "How many?" menu-related variable(s)
    private var mHowManyDialogue: UIDialogue? = null // Literally just asks "How many?"
    private var mHowManySelector: UIQuantitySelector? = null // Tiny popup for selecting a number

    init {
        mShouldCenterCursor = false
        IsEvenlySpaced = false
        mEntries.add(UIInventoryMenuEntry("CANCEL", 0, /* onUse/onSelect */ onCancel))
    }

    override fun draw(dt: Long) {
        super.draw(dt)
        mUseMenu?.draw(dt) // If not null, draw the use/toss menu
        mHowManyDialogue?.draw(dt) // If not null, draw the "How many?" dialogue...
        mHowManySelector?.draw(dt) // ...likewise for the quantity selector
    }

    override fun handleInput(input: Input) {
        // If an item was selected, pass the events to the use/toss menu instead
        if (mUseMenu != null) {
            mUseMenu?.handleInput(input)
            return
        }
        if (mHowManySelector != null) {
            mHowManySelector?.handleInput(input)
            return
        }
        if ((input.IsPress) && (input.Opcode == Input.Button.A)) {
            // If the hovered index isn't "CANCEL" (we'll just let that one pass through to super.handleUIEvent())
            if ((mHoveredIndex >= 0) && (mHoveredIndex < mEntries.size - 1)) {
                val selectedEntry = mEntries[mHoveredIndex] as UIInventoryMenuEntry
                // If neither a "USE" nor "TOSS" option should be shown and we should skip straight to the "How many?" step
                if (mBehavior == Behavior.NEITHER_USE_NOR_TOSS) {
                    // ...unless the item is one-of-a-kind because there's no point in asking "How many?" when there's only one
                    if (selectedEntry.Quantity == 0)
                        return
                    mHowManyDialogue = UIDialogue()
                    mHowManyDialogue?.init("How many?")
                    mHowManySelector = UIQuantitySelector(selectedEntry.Quantity, {
                        /* onSelect */ quantity ->
                        mHowManyDialogue = null
                        mHowManySelector = null
                        selectedEntry.OnToss.invoke(quantity)
                    }, {
                        // onCancel
                        mHowManyDialogue = null
                        mHowManySelector = null
                    })
                    mHowManySelector?.init()
                    mHowManySelector?.autoSize()
                    val howManyDialogueRect = mHowManyDialogue!!.Rect
                    val howManySelectorRect = mHowManySelector!!.Rect
                    mHowManySelector?.move(
                        howManyDialogueRect.X + howManyDialogueRect.Width - howManySelectorRect.Width,
                        howManyDialogueRect.Y + howManyDialogueRect.Height + (howManySelectorRect.Height / 4f)
                    )
                }
                // If a quantity of one should be assumed leading straight into invoking OnUse
                else if (mBehavior == Behavior.NO_SELECTOR)
                    selectedEntry.OnUse.invoke()
                // If this is an inventory menu from which items are used or tossed
                else {
                    mUseMenu = UIMenu { /* onCancel */ mUseMenu = null }
                    mUseMenu?.init()
                    if (mBehavior == Behavior.USE_AND_TOSS)
                        mUseMenu?.add(UIMenuEntry("USE", {
                            // onSelect
                            selectedEntry.OnUse.invoke()
                            mUseMenu = null
                        }))
                    mUseMenu?.add(UIMenuEntry("TOSS", {
                        // onSelect
                        if (selectedEntry.Quantity > 0) {
                            mHowManyDialogue = UIDialogue()
                            mHowManyDialogue?.init("How many?")
                            mHowManySelector = UIQuantitySelector(selectedEntry.Quantity, {
                                /* onSelect */ quantity ->
                                mHowManyDialogue = null
                                mHowManySelector = null
                                selectedEntry.OnToss.invoke(quantity)
                            }, {
                                // onCancel
                                mHowManyDialogue = null
                                mHowManySelector = null
                            })
                            mHowManySelector?.init()
                            mHowManySelector?.autoSize()
                            val howManyDialogueRect = mHowManyDialogue!!.Rect
                            val howManySelectorRect = mHowManySelector!!.Rect
                            mHowManySelector?.move(
                                howManyDialogueRect.X + howManyDialogueRect.Width - howManySelectorRect.Width,
                                howManyDialogueRect.Y + howManyDialogueRect.Height + (howManySelectorRect.Height / 4f)
                            )
                        } else
                            selectedEntry.OnToss.invoke(1)
                        mUseMenu = null
                    }))
                    mUseMenu?.autoSize()
                    val bubbleRect = mBubble.Rect
                    var useMenuRect = mUseMenu!!.Rect
                    mUseMenu?.move(bubbleRect.X + bubbleRect.Width - useMenuRect.Width, bubbleRect.Y - (useMenuRect.Height / 2f))
                    useMenuRect = mUseMenu!!.Rect
                    // If the use menu would be displayed outsize the visible surface, accounting for margin, reposition the menu
                    if (useMenuRect.Y < (gGZ.Camera.BottomEdge + gGZ.Camera.Margin))
                        mUseMenu?.move(useMenuRect.X, gGZ.Camera.BottomEdge + gGZ.Camera.Margin)
                }
                input.Opcode = Input.Button.NO_OP // Neuter the event before handing it to super.handleUIEvent()
            }
        }
        super.handleInput(input)
    }

    fun add(entry: UIInventoryMenuEntry) {
        // Note: this class' init {} block already added a "CANCEL" entry and it should remain the last entry
        mEntriesLock.withLock { mEntries.add(mEntries.size - 1, entry) }
        if (mIsInitialized) {
            entry.init()
            if (mIsAutosized)
                autoSize()
            else
                size(Rect)
        }
    }

    override fun remove(index: Int) {
        // If the index is for a valid entry that isn't "CANCEL"
        if ((index >= 0) && (index < mEntries.size - 1))
            super.remove(index)
    }

    override fun clear() {
        super.clear() // Remove everything and zero the hovered index
        mEntries.add(UIInventoryMenuEntry("CANCEL", 0, /* onUse/onSelect */ mOnCancel)) // Re-add the "CANCEL" entry
    }

    fun setEntryQuantity(label: String, quantity: Int) {
        var indexToModify = -1
        mEntries.forEachIndexed { index, entry ->
            if (entry.Label == label)
                indexToModify = index
        }
        if (indexToModify != -1)
            (mEntries[indexToModify] as UIInventoryMenuEntry).Quantity = quantity
    }

    override fun hoverOnIndex(index: Int) {
        super.hoverOnIndex(index)
        // If hovering on the "CANCEL" index
        if (index == mEntries.size - 1)
            mOnCancelHover.invoke()
    }
}