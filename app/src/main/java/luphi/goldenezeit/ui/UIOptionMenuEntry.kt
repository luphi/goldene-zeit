package luphi.goldenezeit.ui

import luphi.goldenezeit.gGZ
import kotlin.math.max

@Suppress("PropertyName")
class UIOptionMenuEntry(label: String, options: Array<String>, index: Int, onChanged: (String) -> Unit, onHover: () -> Unit = {}) :
    UIJustifiedMenuEntry(label, /* rightLabel */ "", /* onSelect */ {}, /* onHover */ onHover, /* useTwoLines */ false) {
    private val mOptions: Array<String> = options // List of options (e.g. "ON" or "OFF")
    private var mIndex = index // Index of the currently-selected option, or "CANCEL"
    private val mOnChanged: ((String) -> Unit) = onChanged

    override val NativeWidth: Float
        get() = (Label.length * gGZ.FontSize) + NativeOptionWidth
    val NativeOptionWidth: Float
        get() {
            var maxWidth = 0f
            for (option in mOptions)
                maxWidth = max(maxWidth, (option.length + 1) * gGZ.FontSize) // length + 1 to account for the ":" prepended to options
            return maxWidth
        }
    var OptionWidth = 0f
        set(value) {
            field = value
            mRightLabelText.move(mX + mWidth - value, mLabelText.Rect.Y)
            // Menus will use NativeWidth to calculate appropriate positions and sizes which is based on the combined length of the label and longest option.
            // As a result, entries with long labels can sometimes their options overlap their labels.
            val leftLabelRect = mLabelText.Rect
            // If the option overlaps the label
            if (mRightLabelText.Rect.X <= leftLabelRect.X + leftLabelRect.Width)
            // Move it immediately right of the label
                mRightLabelText.move(leftLabelRect.X + leftLabelRect.Width, leftLabelRect.Y)
        }

    override fun init() {
        super.init()
        OptionWidth = NativeOptionWidth
        doOptionLabel()
    }

    override fun move(x: Float, y: Float) {
        super.move(x, y)
        mRightLabelText.move(mX + mWidth - OptionWidth, mLabelText.Rect.Y)
    }

    fun previous() {
        mIndex -= 1
        if (mIndex < 0)
            mIndex = mOptions.size - 1 // Wrap around to the last index
        onPreviousOrNext()
    }

    fun next() {
        mIndex += 1
        if (mIndex >= mOptions.size)
            mIndex = 0 // Wrap around to the first index
        onPreviousOrNext()
    }

    private fun doOptionLabel() {
        if ((mIndex >= 0) && (mIndex < mOptions.size)) {
            mRightLabelText.Text = ":" + mOptions[mIndex]
            mRightLabelText.move(mX + mWidth - OptionWidth, mLabelText.Rect.Y)
            val leftLabelRect = mLabelText.Rect
            if (mRightLabelText.Rect.X <= leftLabelRect.X + leftLabelRect.Width)
                mRightLabelText.move(leftLabelRect.X + leftLabelRect.Width, leftLabelRect.Y)
        }
    }

    private fun onPreviousOrNext() {
        doOptionLabel()
        if ((mIndex >= 0) && (mIndex < mOptions.size)) {
            mOnChanged.invoke(mOptions[mIndex])
        }
    }
}