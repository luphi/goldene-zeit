package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.item.Item
import java.util.LinkedList
import java.util.Locale

class UIShop(items: LinkedList<Item>, onBuy: (Item) -> Unit, onSell: (Item) -> Unit, onCancel: () -> Unit) : IDrawable, IInteractable, IProgressive {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    private val mItems = items
    private val mOnBuy = onBuy
    private val mOnSell = onSell
    private val mOnCancel = onCancel
    private val mCashBlurb = UIBlurb()
    private val mRootMenu = UIMenu { /* onCancel */ mOnCancel.invoke() }
    private val mDialogue = UIReusableDialogue()
    private var mDrawable: IDrawable? = null
    private var mInteractable: IInteractable? = mRootMenu
    private var mProgressive: IProgressive? = mRootMenu
    private val mSubmenuOnCancel = {
        mDrawable = null
        mInteractable = mRootMenu
        mProgressive = mRootMenu
        mDialogue.Text = "Is there anything else I can do?"
        mRootMenu.IsFocused = true
    }
    private val mBuyMenu = UIJustifiedMenu { /* onCancel */ mSubmenuOnCancel.invoke() }
    private val mSellMenu = UIJustifiedMenu { /* onCancel */ mSubmenuOnCancel.invoke() }
    private var mIsInitialized = false

    fun init() {
        mCashBlurb.init("$289578")
        val fundsRect = mCashBlurb.Rect
        mCashBlurb.move(gGZ.Camera.RightEdgeWithMargin - fundsRect.Width, gGZ.Camera.TopEdgeWithMargin - fundsRect.Height)
        mRootMenu.init()
        mRootMenu.add(UIMenuEntry("BUY    ", {
            // onSelect
            mDrawable = mBuyMenu
            mInteractable = mBuyMenu
            mProgressive = mBuyMenu
            mBuyMenu.IsFocused = true
            mRootMenu.IsFocused = false
        }))
        mRootMenu.add(UIMenuEntry("SELL   ", {
            // onSelect
            mDrawable = mSellMenu
            mInteractable = mSellMenu
            mProgressive = mSellMenu
            mSellMenu.IsFocused = true
            mRootMenu.IsFocused = false
        }))
        mRootMenu.add(UIMenuEntry("QUIT   ", { /* onSelect */ mOnCancel.invoke() }))
        mRootMenu.autoSize()
        mRootMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - mRootMenu.Rect.Height)
        mDialogue.init()
        mBuyMenu.init()
        for (item in mItems) {
            mBuyMenu.add(
                UIJustifiedMenuEntry(item.Name.toUpperCase(Locale.getDefault()), "$${item.Cost}",
                    { /* onSelected */ mOnBuy.invoke(item) }, { /* onHover */ mDialogue.Text = item.Description }, /* useTwoLines */ true
                )
            )
        }
        mBuyMenu.add(
            UIJustifiedMenuEntry("CANCEL", "", { /* onSelect */ mSubmenuOnCancel.invoke() }, { /* onHover */ mDialogue.Text = "" }, /* useTwoLines */ true)
        )
        mBuyMenu.autoSize() // Let the menu determine the best width
        var buyMenuRect = mBuyMenu.Rect
        val cashBlurbUsableSurface = mCashBlurb.UsableSurface
        val dialogueUsableSurface = mDialogue.UsableSurface
        // Resize the menu to use the best fit width and greatest possible space between the cash blurb and dialogue
        mBuyMenu.size(buyMenuRect.Width, cashBlurbUsableSurface.Y - dialogueUsableSurface.Y - dialogueUsableSurface.Height)
        buyMenuRect = mBuyMenu.Rect
        mBuyMenu.move(gGZ.Camera.RightEdgeWithMargin - buyMenuRect.Width, cashBlurbUsableSurface.Y - buyMenuRect.Height)
        mSellMenu.init()
        for (itemID in gGZ.Player.Inventory.keys) {
            val item = gGZ.Resources.loadItem(itemID)
            mSellMenu.add(
                UIJustifiedMenuEntry(item.Name.toUpperCase(Locale.getDefault()), "$${item.Cost}",
                    { /* onSelected */ mOnSell.invoke(item) }, { /* onHover */ mDialogue.Text = item.Description }, /* useTwoLines */ true
                )
            )
        }
        mSellMenu.add(
            UIJustifiedMenuEntry("CANCEL", "", { /* onSelect */ mSubmenuOnCancel.invoke() }, { /* onHover */ mDialogue.Text = "" }, /* useTwoLines */true)
        )
        mSellMenu.autoSize() // Let the menu determine the best width
        var sellMenuRect = mSellMenu.Rect
        // Resize the menu to use the best fit width and greatest possible space between the cash blurb and dialogue
        mSellMenu.size(sellMenuRect.Width, cashBlurbUsableSurface.Y - dialogueUsableSurface.Y - dialogueUsableSurface.Height)
        sellMenuRect = mSellMenu.Rect
        mSellMenu.move(gGZ.Camera.RightEdgeWithMargin - sellMenuRect.Width, cashBlurbUsableSurface.Y - sellMenuRect.Height)
        // The dialogue's initial text must be set last. The menus modify it when a new entry is hovered over, including on initialization, so it will be with
        // some item's description unless set to something else.
        mDialogue.Text = "Hi there!\nMay I help you?"
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized)
            return
        mCashBlurb.draw(dt)
        mRootMenu.draw(dt)
        mDialogue.draw(dt)
        mDrawable?.draw(dt)
    }

    override fun handleInput(input: Input) {
        mInteractable?.handleInput(input)
    }

    override fun step(dt: Long) {
        mProgressive?.step(dt)
    }
}