package luphi.goldenezeit.ui

import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.Tasks
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.card.Card
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLSprite
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

class UICardSelector(
    private val mPlayerDeck: List<Card>,
    private val mOpponentDeck: List<Card>,
    private val mIsWin: Boolean,
    private val mOnSelect: (Card) -> Unit
) : IDrawable, IInteractable, IProgressive {
    // IDrawable property
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    private val mTasks = Tasks()
    private val mCursor = GLSprite()
    private var mHoveredIndex = 0
    private var mInstructionBlurb = UIBlurb()
    private var mOwnedBlurb = UIBlurb()
    private var mAnimationState = AnimationState.NOT_ANIMATING
    private var mChosenCard: Card? = null

    init {
        mCursor.IsStatic = true
    }

    fun init() {
        mCursor.init(gGZ.Resources.loadTexture("sprites/ui/arrow_down.png"))
        mCursor.sizeByHeight(gGZ.FontSize)
        // The blurbs will only show if the player won but they'll be used for positioning either way so initialize them anyway
        mInstructionBlurb.init("Select 1 card you want")
        mOwnedBlurb.init("") // Will have its text set when hovering over a card
        val cardHeight = mPlayerDeck[0].Rect.Height
        // Calculate the spacing between the various drawables (equal to one fifth of the height remaining after considering both blurbs and two rows of cards)
        val spacing = (gGZ.Camera.Height - (mInstructionBlurb.Rect.Height + mOwnedBlurb.Rect.Height + (2f * cardHeight))) / 5f
        val instructionBlurbRect = mInstructionBlurb.Rect
        mInstructionBlurb.move(-instructionBlurbRect.Width / 2f, gGZ.Camera.TopEdge - instructionBlurbRect.Height - spacing)
        val ownedBlurbRect = mOwnedBlurb.Rect
        mOwnedBlurb.move(-ownedBlurbRect.Width / 2f, gGZ.Camera.BottomEdge + spacing)
        val cardWidth = mPlayerDeck[0].Rect.Width
        val cardLeftmostX = -(2f * cardWidth) - (2f * gGZ.Camera.Margin) - (cardWidth / 2f) // X coordinate of the leftmost card
        for ((i, card) in mOpponentDeck.withIndex())
            card.move(cardLeftmostX + (i * (cardWidth + gGZ.Camera.Margin)), mInstructionBlurb.Rect.Y - spacing - cardHeight)
        for ((i, card) in mPlayerDeck.withIndex())
            card.move(cardLeftmostX + (i * (cardWidth + gGZ.Camera.Margin)), mOpponentDeck[0].Rect.Y - spacing - cardHeight)
        if (mIsWin)
            hoverOnIndex(0)
        else
            opponentChoose()
    }

    override fun draw(dt: Long) {
        for (card in mOpponentDeck)
            card.draw(dt)
        for (card in mPlayerDeck)
            card.draw(dt)
        mCursor.draw(dt)
        if (mIsWin) {
            mInstructionBlurb.draw(dt)
            mOwnedBlurb.draw(dt)
        }
        mChosenCard?.draw(dt) // Removes the possibility of the card being drawn under any others although it is drawn in the loops above as well
    }

    override fun handleInput(input: Input) {
        if (input.IsPress) {
            if (mAnimationState == AnimationState.POST_FLIP_PAUSE) {
                // If the player is choosing a card, chose one by mistake, and want to choose a different one
                if (mIsWin && (input.Opcode == Input.Button.B))
                    flipCard(mChosenCard!!, AnimationState.NOT_ANIMATING) // Flip the card back to its original owner
                else // Any press within the app's surface
                    glideAndEnlargeCard(mChosenCard!!)
                return
            }
            if (mAnimationState == AnimationState.POST_GLIDE_PAUSE) {
                mOnSelect.invoke(mChosenCard!!)
                return
            }
            // If the player is selecting a card
            if (mIsWin && (mAnimationState == AnimationState.NOT_ANIMATING)) {
                when (input.Opcode) {
                    Input.Button.DPAD_LEFT -> hoverOnIndex(mHoveredIndex - 1)
                    Input.Button.DPAD_RIGHT -> hoverOnIndex(mHoveredIndex + 1)
                    Input.Button.A -> {
                        mChosenCard = mOpponentDeck[mHoveredIndex]
                        flipCard(mChosenCard!!, AnimationState.POST_FLIP_PAUSE)
                    }
                    else -> {
                    }
                }
            }
        }
    }

    override fun step(dt: Long) = mTasks.step(dt)

    private fun hoverOnIndex(index: Int) {
        if ((index < 0) || (index > 4))
            return
        mHoveredIndex = index
        val card = if (mIsWin) mOpponentDeck[index] else mPlayerDeck[index]
        val cardRect = card.Rect
        mCursor.move(cardRect.X + (cardRect.Width / 2f) - (mCursor.Rect.Width / 2f), cardRect.Y + cardRect.Height + gGZ.Camera.Margin)
        mOwnedBlurb.Text = "You have ${gGZ.Player.Cards[card.ID] ?: 0} of this card"
        val ownedBlurbRect = mOwnedBlurb.Rect
        mOwnedBlurb.move(-ownedBlurbRect.Width / 2f, ownedBlurbRect.Y)
    }

    private fun opponentChoose() {
        mChosenCard = mPlayerDeck.maxBy { it.ID } // The IDs are conveniently in order of worst to best so the highest ID number is the (player's) best card
        flipCard(mChosenCard!!, AnimationState.POST_FLIP_PAUSE)
    }

    private fun flipCard(card: Card, nextState: AnimationState) {
        if (gGZ.Record.get("animations", "on") == "on") {
            val origin = card.copy() // Make a copy of the card as it exists before the flip
            val originRect = origin.Rect
            var theta = 0.0
            val duration = 0.5 // In seconds, the duration of a single card flip
            val frequency = 75f // Hz
            mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                val riseValue = sin(theta / 4f)
                if (theta < Math.PI) {
                    card.IsPlayerOwned = origin.IsPlayerOwned
                    card.IsFaceDown = false
                } else if ((theta >= Math.PI) && (theta <= 3f * Math.PI))
                    card.IsFaceDown = true
                else {
                    card.IsPlayerOwned = !origin.IsPlayerOwned
                    card.IsFaceDown = false
                }
                val riseWidthDiff = originRect.Width * riseValue / 20f
                val riseHeightDiff = originRect.Height * riseValue / 20f
                val flipWidthFactor = (cos(theta) + 1f) / 2f // cos() is restricted to [0f, 1f] with cos(0) == 1f and cos(pi) = 0
                card.size(((originRect.Width - riseWidthDiff) * flipWidthFactor).toFloat(), (originRect.Height - riseHeightDiff).toFloat())
                card.move(
                    originRect.X + (originRect.Width / 2f) - (card.Rect.Width / 2f),
                    (originRect.Y + (riseValue * (originRect.Height / 3f)) + (riseHeightDiff / 2f)).toFloat()
                )
                theta += (1000 / frequency) / (1000f * duration) * 4f * Math.PI // Advance theta relative to the change in time
                // If this is the last frame of this layer's animation
                if (theta >= (4f * Math.PI)) {
                    // Adjust the card such that it match the origin slot exactly, otherwise there will be small but visible offsets
                    card.size(origin.Rect)
                    card.move(origin.Rect)
                    mAnimationState = nextState
                    mTasks.clear()
                }
            }
            mAnimationState = AnimationState.FLIP
        } else {
            mChosenCard!!.IsPlayerOwned = !mChosenCard!!.IsPlayerOwned
            mAnimationState = nextState
        }
    }

    private fun glideAndEnlargeCard(card: Card) {
        var cardRect = card.Rect
        val goalRect = if (gGZ.Camera.AspectRatio > (cardRect.Width / cardRect.Height)) // If the screen is wider than the card, relatively
            Rect(0f, 0f, (cardRect.Width / cardRect.Height) * (gGZ.Camera.Height * 0.75f), gGZ.Camera.Height * 0.75f) // height = 75% of the screen's height
        else
            Rect(0f, 0f, gGZ.Camera.Width / 2f, (cardRect.Height / cardRect.Width) * (gGZ.Camera.Width / 2f))
        goalRect.X = -goalRect.Width / 2f
        goalRect.Y = -goalRect.Height / 2f
        if (gGZ.Record.get("animations", "on") == "on") {
            val duration = 1f // Duration of the glide in seconds
            val frequency = 75f // Frequency of the periodic routine in hertz
            val dt = (1000f / frequency).toLong()
            val dxdt = (goalRect.X - cardRect.X) / (frequency * duration) // Change in X (position) per callback
            val dydt = (goalRect.Y - cardRect.Y) / (frequency * duration) // Change in Y per callback
            val dwdt = (goalRect.Width - cardRect.Width) / (frequency * duration) // Change in width (dimensions) per callback
            val dhdt = (goalRect.Height - cardRect.Height) / (frequency * duration) // Change in height per callback
            var t = 0L // Total time counter
            var lx = cardRect.X // X coordinate if moved in a straight line
            var ly = cardRect.Y // Y coordinate if moved in a straight line
            mTasks.schedulePeriodic(dt) {
                cardRect = card.Rect
                // Increment lx and ly as if the card were moving in a perfectly linear path from its origin to the goal
                lx += dxdt
                ly += dydt
                val yOffset = sin(PI.toFloat() * t / (duration * 1000f)) * (gGZ.Camera.Height * 0.75f) // Y coordinate offset from the linear path
                // Add (or subtract, if the opponent is choosing) the Y offset to the linear path to effectively create a curved path
                card.move(lx, ly + if (mIsWin) yOffset else -yOffset)
                card.size(cardRect.Width + dwdt, cardRect.Height + dhdt) // Increase the cards dimensions at a linear rate
                t += dt
                if (t >= (duration * 1000L)) {
                    card.size(goalRect)
                    card.move(goalRect)
                    mAnimationState = AnimationState.POST_GLIDE_PAUSE
                    mTasks.clear()
                }
            }
            mAnimationState = AnimationState.GLIDE
        } else {
            card.size(goalRect)
            card.move(goalRect)
            mAnimationState = AnimationState.POST_GLIDE_PAUSE
        }
    }

    private enum class AnimationState { NOT_ANIMATING, FLIP, POST_FLIP_PAUSE, GLIDE, POST_GLIDE_PAUSE }
}