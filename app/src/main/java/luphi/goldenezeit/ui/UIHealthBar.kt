package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.gl.GLSprite

@Suppress("PropertyName")
open class UIHealthBar : IDrawable {
    override val Rect: Rect
        get() = mBorder.Rect

    private val mBorder = GLSprite()
    protected var mBar = GLPrimitive()

    var Percentage = 1f
        set(value) {
            field = if (value > 1f) 1f else if (value < 0f) 0f else value
            val borderRect = mBorder.Rect
            // The dimensions of the background sprite are 50 x 4 (pixels with a border width of one pixel). Therefore, <width of background> * (48 / 50) is the
            // width of the actual health bar inside that border. Because the border is drawn second, the bar's height can be set to that of the border and the
            // overlap will simply be drawn over.
            mBar.size(borderRect.Width * (48f / 50f) * field, borderRect.Height)
            when {
                field <= 0.25f -> mBar.Color = floatArrayOf(0.973f, 0f, 0f) // Red
                field <= 0.5f -> mBar.Color = floatArrayOf(0.973f, 0.659f, 0f) // Orange
                else -> mBar.Color = floatArrayOf(0.027f, 0.675f, 0.027f) // Green
            }
        }

    init {
        mBorder.IsStatic = true
        mBar.IsStatic = true
        mBar.Color = floatArrayOf(0.027f, 0.675f, 0.027f) // Green
    }

    fun init() {
        mBar.init(GLPrimitive.Shape.QUAD)
        mBorder.init(gGZ.Resources.loadTexture("sprites/ui/battle_health_bar.png"))
    }

    override fun draw(dt: Long) {
        mBar.draw(dt)
        mBorder.draw(dt)
    }

    fun move(x: Float, y: Float) {
        mBorder.move(x, y)
        mBar.move(x + (mBorder.Rect.Width / 50f), y)
    }

    fun translate(dx: Float, dy: Float) {
        mBorder.translate(dx, dy)
        mBar.translate(dx, dy)
    }

    fun size(w: Float, h: Float) {
        mBorder.size(w, h)
        mBar.size(w * (48f / 50f), h)
    }

    fun sizeByWidth(w: Float) {
        mBorder.sizeByWidth(w)
        val rect = mBorder.Rect
        mBar.size(rect.Width * (48f / 50f), rect.Height)
    }

    fun sizeByHeight(h: Float) {
        mBorder.sizeByHeight(h)
        val rect = mBorder.Rect
        mBar.size(rect.Width * (48f / 50f), rect.Height)
    }
}