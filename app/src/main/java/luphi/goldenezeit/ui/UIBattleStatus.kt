package luphi.goldenezeit.ui

import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gl.GLSprite
import luphi.goldenezeit.gl.GLText

@Suppress("PropertyName")
open class UIBattleStatus : IDrawable {
    // IDrawable property
    override val Rect
        get() = Rect(0f, 0f, 0f, 0f)

    protected val mArrow = GLSprite()
    protected val mHPBar = UIHealthBar()
    protected val mHPLabelText = GLText()
    protected val mLevelText = GLText()

    open var HP = 0
    open var MaxHP = 0
    open var Level = 0
    var IsVisible = true

    init {
        mArrow.IsStatic = true
    }

    open fun init() {
        mHPBar.init()
        mHPLabelText.init("HP:")
        mLevelText.init(":L1")
    }

    override fun draw(dt: Long) {
        if (!IsVisible)
            return
        mArrow.draw(dt)
        mHPBar.draw(dt)
        mHPLabelText.draw(dt)
        mLevelText.draw(dt)
    }
}