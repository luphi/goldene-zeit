package luphi.goldenezeit.ui

import android.util.Log
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.gl.GLSprite

@Suppress("PropertyName")
open class UIBubble : IDrawable {
    override val Rect: Rect
        get() = Rect(mX, mY, mWidth, mHeight)

    protected var mIsMoved = false
    protected var mIsSized = false
    protected var mX = 0f
    protected var mY = 0f
    protected var mWidth = 0f
    protected var mHeight = 0f
    private val mCornerTopLeft = GLSprite()
    private val mCornerTopRight = GLSprite()
    protected val mCornerBottomRight = GLSprite()
    protected val mCornerBottomLeft = GLSprite()
    private val mEdgeHorizontal = GLSprite()
    private val mEdgeVertical = GLSprite()
    protected var mBorderThickness = 0.01f
    protected val mBackground = GLPrimitive()
    protected val mUsableSurfaceQuad = GLPrimitive()

    open val UsableSurface: Rect
        get() = Rect(mX + (mBorderThickness * 2f), mY + (mBorderThickness * 2f), mWidth - (mBorderThickness * 4f), mHeight - (mBorderThickness * 4f))

    init {
        mCornerTopLeft.IsStatic = true
        mCornerTopRight.IsStatic = true
        mCornerBottomRight.IsStatic = true
        mCornerBottomLeft.IsStatic = true
        mEdgeHorizontal.IsStatic = true
        mEdgeVertical.IsStatic = true
        mBackground.IsStatic = true
        mBackground.Color = floatArrayOf(1f, 1f, 1f)
        mUsableSurfaceQuad.IsStatic = true
        mUsableSurfaceQuad.Color = floatArrayOf(1f, 0f, 0f)
        mUsableSurfaceQuad.Alpha = 0.25f
    }

    open fun init() {
        mBorderThickness = gGZ.Camera.Margin * 2f
        mCornerTopLeft.init(gGZ.Resources.loadTexture("sprites/ui/dialogue_background_corner_top_left.png"))
        mCornerTopRight.init(gGZ.Resources.loadTexture("sprites/ui/dialogue_background_corner_top_right.png"))
        mCornerBottomRight.init(gGZ.Resources.loadTexture("sprites/ui/dialogue_background_corner_bottom_right.png"))
        mCornerBottomLeft.init(gGZ.Resources.loadTexture("sprites/ui/dialogue_background_corner_bottom_left.png"))
        mEdgeHorizontal.init(gGZ.Resources.loadTexture("sprites/ui/dialogue_background_edge_horizontal.png"))
        mEdgeVertical.init(gGZ.Resources.loadTexture("sprites/ui/dialogue_background_edge_vertical.png"))
        mCornerTopLeft.size(mBorderThickness, mBorderThickness)
        mCornerTopRight.size(mBorderThickness, mBorderThickness)
        mCornerBottomRight.size(mBorderThickness, mBorderThickness)
        mCornerBottomLeft.size(mBorderThickness, mBorderThickness)
        mBackground.init(GLPrimitive.Shape.QUAD)
        mUsableSurfaceQuad.init(GLPrimitive.Shape.QUAD)
        size(1f, 1f)
    }

    override fun draw(dt: Long) {
        Log.d("UIBubble", "mIsMoved = $mIsMoved, mIsSized = $mIsSized")
        if (!mIsMoved || !mIsSized)
            return
        mBackground.draw(dt)
        mCornerTopLeft.draw(dt)
        mCornerTopRight.draw(dt)
        mCornerBottomRight.draw(dt)
        mCornerBottomLeft.draw(dt)
        // Move the horizontal edge to the top and draw
        val cornerTopLeftRect = mCornerTopLeft.Rect
        mEdgeHorizontal.move(cornerTopLeftRect.X + cornerTopLeftRect.Width, cornerTopLeftRect.Y)
        mEdgeHorizontal.draw(dt)
        // To the bottom and draw
        val cornerBottomLeftRect = mCornerBottomLeft.Rect
        mEdgeHorizontal.move(mEdgeHorizontal.Rect.X, cornerBottomLeftRect.Y)
        mEdgeHorizontal.draw(dt)
        // Move the vertical edge to the left and draw
        mEdgeVertical.move(cornerBottomLeftRect.X, cornerBottomLeftRect.Y + cornerBottomLeftRect.Height)
        mEdgeVertical.draw(dt)
        // To the right and draw
        mEdgeVertical.move(mCornerBottomRight.Rect.X, mEdgeVertical.Rect.Y)
        mEdgeVertical.draw(dt)
    }

    // For development purposes, will eventually be removed
    fun drawUsableSurface() = mUsableSurfaceQuad.draw(0)

    fun move(x: Float, y: Float) = translate(x - mX, y - mY)

    open fun translate(dx: Float, dy: Float) {
        mX += dx
        mY += dy
        mCornerTopLeft.translate(dx, dy)
        mCornerTopRight.translate(dx, dy)
        mCornerBottomRight.translate(dx, dy)
        mCornerBottomLeft.translate(dx, dy)
        mBackground.translate(dx, dy)
        mUsableSurfaceQuad.translate(dx, dy)
        mIsMoved = true
    }

    open fun size(w: Float, h: Float) {
        mWidth = w
        mHeight = h
        mCornerTopLeft.move(mX, mY + mHeight - mBorderThickness)
        mCornerTopRight.move(mX + mWidth - mBorderThickness, mY + mHeight - mBorderThickness)
        mCornerBottomRight.move(mX + mWidth - mBorderThickness, mY)
        mCornerBottomLeft.move(mX, mY)
        val cornerTopLeftRect = mCornerTopLeft.Rect
        mEdgeHorizontal.size(mCornerTopRight.Rect.X - (cornerTopLeftRect.X + mBorderThickness), mBorderThickness)
        mEdgeVertical.size(mBorderThickness, cornerTopLeftRect.Y - (mCornerBottomLeft.Rect.Y + mBorderThickness))
        mBackground.size(mWidth - (mBorderThickness * 2f), mHeight - (mBorderThickness * 2f))
        mBackground.move(mX + mBorderThickness, mY + mBorderThickness)
        val usableSurface = UsableSurface
        mUsableSurfaceQuad.size(usableSurface.Width, usableSurface.Height)
        mUsableSurfaceQuad.move(usableSurface.X, usableSurface.Y)
        mIsSized = true
    }

    open fun sizeForUsableWidthAndHeight(w: Float, h: Float) = size(w + (mBorderThickness * 4f), h + (mBorderThickness * 4f))

    open fun sizeForUsableWidth(w: Float, h: Float) = size(w + (mBorderThickness * 4f), h)

    open fun sizeForUsableHeight(w: Float, h: Float) = size(w, h + (mBorderThickness * 4f))
}