package luphi.goldenezeit.ui

import luphi.goldenezeit.gGZ

class UIBattleMenuEntry(label: String, onSelect: () -> Unit, onHover: () -> Unit = {}) : UIMenuEntry(label, onSelect, onHover) {
    override val NativeHeight: Float
        get() = gGZ.FontSize * 2f

    override fun move(x: Float, y: Float) {
        mX = x
        mY = y
        mLabelText.move(mX, mY)
    }
}