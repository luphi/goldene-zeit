package luphi.goldenezeit.card

import android.util.SparseArray
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLPrimitive

@Suppress("PropertyName")
class CDIdenticon internal constructor() : IDrawable {
    override val Rect: Rect
        get() = Rect(mX, mY, mWidth, mHeight)

    private var mX = 0f
    private var mY = 0f
    private var mWidth = 0f
    private var mHeight = 0f
    private val mColumns = 8
    private val mRows = 8
    private var mQuad = GLPrimitive()
    private var mGrid: Array<BooleanArray>
    private var mColors = SparseArray<FloatArray>() // Should probably be moved to a static variable

    internal var Level: Int = 1
        set(value) {
            field = value
            mQuad.Color = mColors.get(if (value % 10 == 0) 10 else value % 10)
        }

    init {
        mQuad.IsStatic = true
        mGrid = Array(mColumns) { BooleanArray(mRows) }
        // The following colors are arbitrarily chosen with the only criteria being sufficient contrast with the gradients
        mColors.append(1, gGZ.Utils.hexColorToFloatArray("#FFFFD1AD"))
        mColors.append(2, gGZ.Utils.hexColorToFloatArray("#FFFFEFAD"))
        mColors.append(3, gGZ.Utils.hexColorToFloatArray("#FFE0FFAD"))
        mColors.append(4, gGZ.Utils.hexColorToFloatArray("#FFAEFFAD"))
        mColors.append(5, gGZ.Utils.hexColorToFloatArray("#FFADFFEC"))
        mColors.append(6, gGZ.Utils.hexColorToFloatArray("#FFADDDFF"))
        mColors.append(7, gGZ.Utils.hexColorToFloatArray("#FFCAADFF"))
        mColors.append(8, gGZ.Utils.hexColorToFloatArray("#FFFFADFF"))
        mColors.append(9, gGZ.Utils.hexColorToFloatArray("#FFFFADAD"))
        mColors.append(10, gGZ.Utils.hexColorToFloatArray("#FFFFFFFF"))
    }

    fun init(seed: String, level: Int) {
        Level = level
        val hash = seed.hashCode()
        val halfColumns = mColumns / 2 + mColumns % 2
        val cells = mRows * halfColumns
        for (i in 0 until cells) {
            // If the bit at offset i is 1
            if (hash shr i and 1 == 1) {
                val column = i / mColumns
                val row = i % mRows
                mGrid[column][row] = true
                mGrid[mColumns - column - 1][row] = true
            }
        }
        mQuad.init(GLPrimitive.Shape.QUAD)
        mQuad.Color = mColors.get(if (level % 10 == 0) 10 else level % 10)
    }

    override fun draw(dt: Long) {
        val quadRect = mQuad.Rect
        for (column in 0 until mColumns) {
            for (row in 0 until mRows) {
                if (mGrid[column][row]) {
                    mQuad.move(mX + quadRect.Width * column.toFloat(), mY + quadRect.Height * row.toFloat())
                    mQuad.draw(dt)
                }
            }
        }
    }

    fun move(x: Float, y: Float) {
        mX = x
        mY = y
    }

    fun translate(dx: Float, dy: Float) {
        mX += dx
        mY += dy
    }

    fun size(w: Float, h: Float) {
        mWidth = w
        mHeight = h
        mQuad.size(w / mColumns.toFloat(), h / mRows.toFloat())
    }
}