package luphi.goldenezeit.card

import android.util.Log
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gl.GLCharacter

// Cards look like
//
// --------------------------------
// |                        +   + |
// |    7                     +   |
// |  6   8                     + |
// |    9                         |
// |                              |
// |  --------------------------  |
// |  |                        |  |
// |  |                        |  |
// |  |                        |  |
// |  |                        |  |
// |  |                        |  |
// |  |       identicon        |  |
// |  |                        |  |
// |  |                        |  |
// |  |                        |  |
// |  |                        |  |
// |  --------------------------  |
// |                              |
// | +                            |
// |   +                          |
// | +   +                        |
// --------------------------------
//
// where 6 is the left value, 7 the top, 8 the right, and 9 the bottom. The sum of these values
// indicate it's level, ranging 1 to 10, for which level 1 has the lowest sum values and level 10
// has the highest. The identicon is a grid/pixel visual representation of the card's values with
// color indicating the card's level.  The algorithm is described in the identicon class.
// Cards have a background to indicate ownership.  Blue indicates the card is owned by the player,
// and red for the opponent.

@Suppress("PropertyName")
class Card(
    val ID: Int, // Unique identifier, mostly used  by the card catalog
    level: Int, // Indicates a sort of league for the card, higher numbers are better cards
    val Left: Int,  // Left- or west-facing value
    val Top: Int, // Top- or north-facing value
    val Right: Int, // Right- or east-facing value
    val Bottom: Int // Bottom- or south-facing value
) : IDrawable {
    override val Rect: Rect
        get() = mBackground.Rect

    private var mBackground = CDBackground() // Background, ownership-based color and border
    private var mIdenticon = CDIdenticon() // Unique visualization of values, level-based color
    private var mValueCharacters = Array(4) { GLCharacter() } // The four graphical values

    var Level = level
        set(value) {
            if (field != value) {
                field = value
                mBackground.Level = value
                mIdenticon.Level = value
            }
        }
    val Shorthand // A very short string identifier, human-readable(-ish)
        get() = "L$Level C${if ((ID + 1) % 11 == 0) 11 else (ID + 1) % 11}" // e.g. "L3 C1" for card 1 of level 3
    var IsPlayerOwned = true // True if owned by the player, false if it's the opponent's (red)
        set(value) {
            if (field != value) {
                field = value
                mBackground.IsPlayerOwned = value
            }
        }
    var IsFaceDown = false // True if the card is turned over and the back side is visible
        set(value) {
            if (field != value) {
                field = value
                mBackground.IsFaceDown = value
            }
        }

    fun init() {
        // The background will determine the aspect ratio based on the sprite(s) used and this object should match that ratio
        mBackground.init(Level, IsPlayerOwned)
        mIdenticon.init("" + Left + "" + Top + "" + Right + "" + Bottom, Level)
        mValueCharacters[0].init(if (Left == 10) 'A' else Character.forDigit(Left, 10))
        mValueCharacters[1].init(if (Top == 10) 'A' else Character.forDigit(Top, 10))
        mValueCharacters[2].init(if (Right == 10) 'A' else Character.forDigit(Right, 10))
        mValueCharacters[3].init(if (Bottom == 10) 'A' else Character.forDigit(Bottom, 10))
    }

    fun copy(): Card {
        val card = Card(ID, Level, Left, Top, Right, Bottom)
        card.init()
        card.move(Rect)
        card.size(Rect)
        card.IsPlayerOwned = IsPlayerOwned
        card.IsFaceDown = IsFaceDown
        Log.d("Card", "copy() = $card")
        return card
    }

    override fun draw(dt: Long) {
        mBackground.draw(dt)
        if (!IsFaceDown) {
            for (character in mValueCharacters)
                character.draw(dt)
            mIdenticon.draw(dt)
        }
    }

    fun move(x: Float, y: Float) {
        mBackground.move(x, y)
        val rect = Rect
        mIdenticon.move(x + (rect.Width / 2f) - (mIdenticon.Rect.Width / 2f), y + rect.Height * 0.15f)
        // Center in the top-left corner
        val centerX = x + rect.Width * 0.35f
        val centerY = y + rect.Height * 0.8f
        mValueCharacters[1].move(centerX - (mValueCharacters[1].Rect.Width / 2f), centerY) // Top value
        val rect1 = mValueCharacters[1].Rect
        val rect2 = mValueCharacters[2].Rect
        mValueCharacters[3].move(rect1.X, centerY - rect2.Height) // Bottom value
        mValueCharacters[2].move(rect1.X + rect1.Width, centerY - (rect2.Height / 2f)) // Right value
        val rect0 = mValueCharacters[0].Rect
        mValueCharacters[0].move(rect1.X - rect0.Width, centerY - (rect0.Height / 2f)) // Left value
    }

    fun move(rect: Rect) = move(rect.X, rect.Y)

    fun translate(dx: Float, dy: Float) {
        mBackground.translate(dx, dy)
        mIdenticon.translate(dx, dy)
        for (character in mValueCharacters)
            character.translate(dx, dy)
    }

    fun size(w: Float, h: Float) {
        mBackground.size(w, h)
        mIdenticon.size(w * 0.5f, h * 0.5f)
        val spriteAspectRatio = 16f / 20f
        val actualAspectRatio = w / h // Card sprites are 16 x 20 pixels
        val characterHeight = h / 8f // 8 was arbitrarily chosen because, visually, it looked right
        // Character width shrinks as card width shrinks
        val characterWidth = characterHeight * (actualAspectRatio / spriteAspectRatio)
        for (character in mValueCharacters)
            character.size(characterWidth, characterHeight)
        // Reposition the values and identicon with the new dimensions by "moving" to the current location
        move(Rect)
    }

    fun size(rect: Rect) = size(rect.Width, rect.Height)

    fun sizeByWidth(w: Float) {
        val rect = Rect
        size(w, rect.Height / rect.Width * w)
    }

    fun sizeByHeight(h: Float) {
        val rect = Rect
        size(rect.Width / rect.Height * h, h)
    }

    override fun toString(): String = "{ ID = \"$ID\" (Left, Top, Right, Bottom) = ($Left, $Top, $Right, $Bottom), mBackground = $mBackground }"
}