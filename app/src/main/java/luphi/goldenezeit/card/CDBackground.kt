package luphi.goldenezeit.card

import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLSprite

@Suppress("PropertyName")
class CDBackground : IDrawable {
    override val Rect: Rect
        get() = mSprite.Rect

    private var mSprite = GLSprite()

    internal var Level = 1
        set(value) {
            field = value
            assignTexture()
        }
    internal var IsPlayerOwned = true
        set(value) {
            field = value
            assignTexture()
        }
    internal var IsFaceDown = false
        set(value) {
            field = value
            assignTexture()
        }

    init {
        mSprite.IsStatic = true
    }

    fun init(level: Int, isPlayerOwned: Boolean) {
        mSprite.init(
            if (level < 8 && isPlayerOwned)
                gGZ.Resources.loadTexture("sprites/card/card_player_low.png")
            else if (level < 8)
                gGZ.Resources.loadTexture("sprites/card/card_opponent_low.png")
            else if (isPlayerOwned)
                gGZ.Resources.loadTexture("sprites/card/card_player_high.png")
            else
                gGZ.Resources.loadTexture("sprites/card/card_opponent_high.png")
        )
        Level = level
        IsPlayerOwned = isPlayerOwned
    }

    override fun draw(dt: Long) = mSprite.draw(dt)

    fun move(x: Float, y: Float) = mSprite.move(x, y)

    fun move(rect: Rect) = move(rect.X, rect.Y)

    fun translate(dx: Float, dy: Float) = mSprite.translate(dx, dy)

    fun size(w: Float, h: Float) = mSprite.size(w, h)

    fun size(rect: Rect) = size(rect.Width, rect.Height)

    fun sizeByWidth(w: Float) {
        val rect = Rect
        size(w, rect.Height / rect.Width * w)
    }

    fun sizeByHeight(h: Float) {
        val rect = Rect
        size(rect.Width / rect.Height * h, h)
    }

    override fun toString(): String = "{ Level $Level, IsPlayerOwned = $IsPlayerOwned, IsFaceDown = $IsFaceDown, sprite = $mSprite }"

    private fun assignTexture() {
        mSprite.Texture = if (IsFaceDown)
            gGZ.Resources.loadTexture("sprites/card/card_back.png")
        else {
            if (Level < 8 && IsPlayerOwned)
                gGZ.Resources.loadTexture("sprites/card/card_player_low.png")
            else if (Level < 8)
                gGZ.Resources.loadTexture("sprites/card/card_opponent_low.png")
            else if (IsPlayerOwned)
                gGZ.Resources.loadTexture("sprites/card/card_player_high.png")
            else
                gGZ.Resources.loadTexture("sprites/card/card_opponent_high.png")
        }
    }
}