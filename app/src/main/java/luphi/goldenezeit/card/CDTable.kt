package luphi.goldenezeit.card

import android.util.SparseArray
import android.util.SparseBooleanArray
import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.Tasks
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLSprite
import java.util.LinkedList
import kotlin.collections.HashSet
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin

@Suppress("PropertyName")
class CDTable(private val mOnSelect: () -> Unit, private val mOnCancel: () -> Unit) : IDrawable, IInteractable, IProgressive {
    // IDrawable property
    override val Rect: Rect
        get() = mSprite.Rect

    private val mSprite = GLSprite()
    private val mCursor = GLSprite()
    private val mTasks = Tasks()
    private var mCards = Array(3) { arrayOfNulls<Card>(3) }
    private var mCount = 0 // Total count of cards, of other player, on the table
    private val mRules = SparseBooleanArray()
    private var mHoveredColumn = 1
    private var mHoveredRow = 1

    // Animation variable(s)
    private val mAnimationLayers = LinkedHashMap<Int, Pair<HashSet<Card>, HashSet<Card>>>()
    private val mAnimationDrawables = LinkedList<Card>()

    val SAME_RULE = 1
    val PLUS_RULE = 2
    val SAME_WALL_RULE = 4
    val PLUS_WALL_RULE = 8
    val COMBO_RULE = 16

    val HoveredColumn
        get() = mHoveredColumn
    val HoveredRow
        get() = mHoveredRow
    var IsFocused = false
        set(value) {
            field = value
            hoverOnSlot(mHoveredColumn, mHoveredRow)
            mCursor.IsVisible = value
        }
    val IsFull: Boolean
        get() = mCount == 9
    var Rules: Int = 0
        set(flags) {
            mRules.clear()
            // Cycle through the available rule flags
            var i = 1
            while (i <= 16) {
                // For example, put(PLUS_RULE, true) if the flag for the plus rule is set such that get(PLUS_RULE) returns true
                mRules.put(i, flags and i == i)
                i *= 2
            }
            field = flags
        }

    init {
        mSprite.IsStatic = true
        mCursor.IsStatic = true
    }

    fun init() {
        mSprite.init(gGZ.Resources.loadTexture("sprites/card/card_table.png"))
        mCursor.init(gGZ.Resources.loadTexture("sprites/ui/arrow_right.png"))
        mCursor.sizeByWidth(gGZ.FontSize)
    }

    override fun draw(dt: Long) {
        mSprite.draw(dt) // Draw the texture (the table grid)
        for (row in mCards) {
            for (card in row)
                card?.draw(dt)
        }
        // Cards which are animating (flipping over) need to visually appear above the other cards requiring them to be drawn (again) after those sitting on the
        // table.  This list will be populated during animation.
        val animationDrawables = mAnimationDrawables.toTypedArray()
        for (card in animationDrawables)
            card.draw(dt)
        mCursor.draw(dt)
    }

    override fun handleInput(input: Input) {
        if (input.IsPress)
            when (input.Opcode) {
                Input.Button.DPAD_UP -> hoverOnSlot(mHoveredColumn, mHoveredRow + 1)
                Input.Button.DPAD_RIGHT -> hoverOnSlot(mHoveredColumn + 1, mHoveredRow)
                Input.Button.DPAD_DOWN -> hoverOnSlot(mHoveredColumn, mHoveredRow - 1)
                Input.Button.DPAD_LEFT -> hoverOnSlot(mHoveredColumn - 1, mHoveredRow)
                Input.Button.A -> {
                    if (!isOccupied(mHoveredColumn, mHoveredRow)) {
                        mOnSelect.invoke()
                        // Prime the cursor for the next move
                        mHoveredColumn = 1
                        mHoveredRow = 1
                    }
                }
                Input.Button.B -> mOnCancel.invoke()
                else -> {
                }
            }
    }

    override fun step(dt: Long) = mTasks.step(dt)

    fun move(x: Float, y: Float) = mSprite.move(x, y)

    fun size(w: Float, h: Float) = mSprite.size(w, h)

    fun getRectForSlot(column: Int, row: Int): Rect? {
        if ((column < 0) || (column > 2) || (row < 0) || (row > 2))
            return null
        val rect = Rect
        return Rect(rect.X + ((rect.Width / 3f) * column.toFloat()), rect.Y + ((rect.Height / 3f) * row.toFloat()), rect.Width / 3f, rect.Height / 3f)
    }

    fun playCard(card: Card?, column: Int, row: Int, onDone: (Result) -> Unit) {
        if ((card == null) || (column < 0) || (column > 2) || (row < 0) || (row > 2))
            return
        val shouldAnimate = gGZ.Record.get("animations", "on") == "on" // True if animations are on or unset
        val result = playCardImmediate(card, column, row, shouldAnimate)
        // If animations are on and at least one card should be (visually) flipped
        if (shouldAnimate && ((result.PlayerScoreDiff != 0) || (result.OpponentScoreDiff != 0)))
            animatePlay { onDone.invoke(result) }
        else
            onDone.invoke(result)
    }

    fun playCardDryRun(card: Card?, column: Int, row: Int): Result? {
        if ((card == null) || (column < 0) || (column > 2) || (row < 0) || (row > 2) || (mCards[row][column] != null))
            return null
        // Make copies of the states of a few things so the changes made by playCardImmediate() can be undone
        val originalCardRect = card.Rect
        val originalCards = Array(3) { arrayOfNulls<Card>(3) }
        for (i in mCards.indices) {
            for (j in mCards[i].indices)
                originalCards[i][j] = if (mCards[i][j] == null) null else mCards[i][j]!!.copy()
        }
        val originalCount = mCount
        val result = playCardImmediate(card, column, row, false)
        // Return all cards to their original states, including placing the card back in the deck, and clear the animation-related lists if needed
        card.move(originalCardRect.X, originalCardRect.Y)
        // Cards' backgrounds (and identicons) are references meaning that the copies made above will have had their background's colors changed as a result
        // if ownership changed.  In order to reverse that, the IsPlayerOwned setter must be called on again to change the background's color/gradient.
        for (i in originalCards.indices) {
            for (j in originalCards[i].indices) {
                if (originalCards[i][j] != null)
                    mCards[i][j]?.IsPlayerOwned = originalCards[i][j]!!.IsPlayerOwned
                else
                    mCards[i][j] = null
            }
        }
        mCount = originalCount
        return result
    }

    override fun toString(): String {
        var array = ""
        for (row in mCards.reversed()) {
            for (card in row)
                array += if (card == null) "[   ]" else "[ ${if (card.IsPlayerOwned) "P" else "O"} ]"
            array += "\n"
        }
        return array
    }

    private fun isOccupied(column: Int, row: Int): Boolean = mCards[row][column] != null

    private fun hoverOnSlot(column: Int, row: Int) {
        val slotRect = getRectForSlot(column, row) ?: return // Performs a bounds check so this method doesn't need to
        mHoveredColumn = column
        mHoveredRow = row
        val cursorRect = mCursor.Rect
        mCursor.move(slotRect.X + (slotRect.Width / 2f) - cursorRect.Width, slotRect.Y + (slotRect.Height / 2f) - (cursorRect.Height / 2f))
    }

    private fun animatePlay(onDone: () -> Unit) {
        val duration = 0.5 // Seconds, the duration of a single card flip
        val frequency = 75f // Hz
        var theta = 4.0 * Math.PI // Spans from 0 to 4*pi, reset with each layer, used by the trig functions to determine rotations and such
        var minLayer = Int.MAX_VALUE // The lowest layer (distance from the card being played) of cards to be animated
        var maxLayer = Int.MIN_VALUE // The highest layer of cards t obe animated
        for (key in mAnimationLayers.keys) {
            if (key < minLayer)
                minLayer = key
            if (key > maxLayer)
                maxLayer = key
        }
        var layer = minLayer // The current layer, spans from minLayer to maxLayer
        var cardSet: HashSet<Card> = HashSet() // A set (list without duplicates) of cards in the current layer
        var originSet: HashSet<Card> = HashSet() // A set of copies of the above prior to the animation, with their original graphical locations and ownerships
        mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            when {
                // If the current layer hasn't finished flipping yet
                theta < (4f * Math.PI) -> {
                    for (index in cardSet.indices) {
                        val riseValue = sin(theta / 4f)
                        val card = cardSet.elementAt(index)
                        val origin = originSet.elementAt(index)
                        if (theta < Math.PI) {
                            card.IsPlayerOwned = !origin.IsPlayerOwned
                            card.IsFaceDown = false
                        } else if ((theta >= Math.PI) && (theta <= 3f * Math.PI))
                            card.IsFaceDown = true
                        else {
                            card.IsPlayerOwned = origin.IsPlayerOwned
                            card.IsFaceDown = false
                        }
                        val originRect = origin.Rect
                        val riseWidthDiff = originRect.Width * riseValue.toFloat() / 20f
                        val riseHeightDiff = originRect.Height * riseValue.toFloat() / 20f
                        // cos() is restricted to [0f, 1f] with cos(0) == 1f and cos(pi) = 0
                        val flipWidthFactor = (cos(theta).toFloat() + 1f) / 2f
                        card.size((originRect.Width - riseWidthDiff) * flipWidthFactor, originRect.Height - riseHeightDiff)
                        card.move(
                            originRect.X + (originRect.Width / 2f) - (card.Rect.Width / 2f),
                            originRect.Y + (riseValue.toFloat() * (originRect.Height / 3f)) + (riseHeightDiff / 2f)
                        )
                    }
                    // Advance theta relative to the change in time
                    theta += (1000 / frequency) / (1000f * duration) * 4f * Math.PI
                    // If this is the last frame of this layer's animation
                    if (theta >= (4f * Math.PI)) {
                        // Adjust the layer's card such that they match the origin slot exactly, otherwise there will be small but visible offsets
                        for (index in cardSet.indices) {
                            val card = cardSet.elementAt(index)
                            val origin = originSet.elementAt(index)
                            card.size(origin.Rect)
                            card.move(origin.Rect)
                        }
                    }
                }
                // If it's time for the next layer
                layer <= maxLayer -> {
                    mAnimationDrawables.clear()
                    for (index in cardSet.indices) {
                        val card = cardSet.elementAt(index)
                        val origin = originSet.elementAt(index)
                        // Return the cards to their original positions and dimensions
                        val originRect = origin.Rect
                        card.move(originRect)
                        card.size(originRect)
                    }
                    cardSet = mAnimationLayers[layer]!!.first
                    originSet = mAnimationLayers[layer]!!.second
                    mAnimationDrawables.addAll(cardSet)
                    layer += 1
                    theta = 0.0
                }
                // If the animation is complete
                else -> {
                    mAnimationDrawables.clear()
                    mAnimationLayers.clear()
                    mTasks.clear()
                    onDone.invoke()
                }
            }
        }
    }

    private fun playCardImmediate(card: Card, column: Int, row: Int, shouldAnimate: Boolean): Result {
        // If no card exists at the given location
        if (!isOccupied(column, row)) {
            mCards[row][column] = card
            mCount += 1
        }
        val flippedCards = HashSet<Coordinate>() // Cards flipped as a result of this move
        val eligibleCards = LinkedList<Coordinate>() // Cards eligible for the combo rule's effect
        eligibleCards.add(Coordinate(column, row)) // The given card is treated as eligible
        do {
            val coordinate = eligibleCards.remove()
            val card2 = mCards[coordinate.Row][coordinate.Column]
            if (mRules[SAME_WALL_RULE])
                eligibleCards.addAll(applySameWallRule(card2, coordinate.Column, coordinate.Row))
            else if (mRules[SAME_RULE])
                eligibleCards.addAll(applySameRule(card2, coordinate.Column, coordinate.Row))
            if (mRules[PLUS_WALL_RULE])
                eligibleCards.addAll(applyPlusWallRule(card2, coordinate.Column, coordinate.Row))
            else if (mRules[PLUS_RULE])
                eligibleCards.addAll(applyPlusRule(card2, coordinate.Column, coordinate.Row))
            flippedCards.addAll(eligibleCards)
            flippedCards.addAll(applyBase(card2, coordinate.Column, coordinate.Row))
        } while (!eligibleCards.isEmpty() && mRules[COMBO_RULE])
        val result = Result()
        for (coordinate in flippedCards) {
            val card2 = mCards[coordinate.Row][coordinate.Column]
            if (card2!!.IsPlayerOwned) {
                result.PlayerScoreDiff += 1
                result.OpponentScoreDiff -= 1
            } else {
                result.OpponentScoreDiff += 1
                result.PlayerScoreDiff -= 1
            }
            if (shouldAnimate) {
                val distance = abs(coordinate.Column - column) + abs(coordinate.Row - row)
                if (!mAnimationLayers.containsKey(distance))
                    mAnimationLayers[distance] = Pair(HashSet(), HashSet())
                mAnimationLayers[distance]!!.first.add(card2)
                mAnimationLayers[distance]!!.second.add(card2.copy())
                // The animation will invert the card's ownership partway through so they should be returned to their previous states for now
                card2.IsPlayerOwned = card.IsPlayerOwned
            }
        }
        return result
    }

    private fun applyBase(card: Card?, column: Int, row: Int): Set<Coordinate> {
        val flippedCoordinates = HashSet<Coordinate>()
        var adjacent = if (column < 2) mCards[row][column + 1] else null
        if ((adjacent != null) && (adjacent.IsPlayerOwned != card!!.IsPlayerOwned) && (card.Right > adjacent.Left))
            flippedCoordinates.add(Coordinate(column + 1, row))
        adjacent = if (column > 0) mCards[row][column - 1] else null
        if ((adjacent != null) && (adjacent.IsPlayerOwned != card!!.IsPlayerOwned) && (card.Left > adjacent.Right))
            flippedCoordinates.add(Coordinate(column - 1, row))
        adjacent = if (row < 2) mCards[row + 1][column] else null
        if ((adjacent != null) && (adjacent.IsPlayerOwned != card!!.IsPlayerOwned) && (card.Top > adjacent.Bottom))
            flippedCoordinates.add(Coordinate(column, row + 1))
        adjacent = if (row > 0) mCards[row - 1][column] else null
        if ((adjacent != null) && (adjacent.IsPlayerOwned != card!!.IsPlayerOwned) && (card.Bottom > adjacent.Top))
            flippedCoordinates.add(Coordinate(column, row - 1))
        for (coordinate in flippedCoordinates)
            mCards[coordinate.Row][coordinate.Column]!!.IsPlayerOwned = card!!.IsPlayerOwned
        return flippedCoordinates
    }

    private fun applySameRule(card: Card?, column: Int, row: Int): Set<Coordinate> {
        val flippedCoordinates = HashSet<Coordinate>()
        val sames = LinkedList<Coordinate>()
        var adjacent = if (column < 2) mCards[row][column + 1] else null
        if ((adjacent != null) && (card!!.Right == adjacent.Left))
            sames.add(Coordinate(column + 1, row))
        adjacent = if (column > 0) mCards[row][column - 1] else null
        if ((adjacent != null) && (card!!.Left == adjacent.Right))
            sames.add(Coordinate(column - 1, row))
        adjacent = if (row < 2) mCards[row + 1][column] else null
        if ((adjacent != null) && (card!!.Top == adjacent.Bottom))
            sames.add(Coordinate(column, row + 1))
        adjacent = if (row > 0) mCards[row - 1][column] else null
        if ((adjacent != null) && (card!!.Bottom == adjacent.Top))
            sames.add(Coordinate(column, row - 1))
        if (sames.size > 1) {
            for (coordinate in sames) {
                adjacent = mCards[coordinate.Row][coordinate.Column]
                if (adjacent!!.IsPlayerOwned != card!!.IsPlayerOwned) {
                    adjacent.IsPlayerOwned = card.IsPlayerOwned
                    flippedCoordinates.add(coordinate)
                }
            }
        }
        return flippedCoordinates
    }

    private fun applyPlusRule(card: Card?, column: Int, row: Int): Set<Coordinate> {
        val flippedCoordinates = HashSet<Coordinate>()
        val possibleFlips = HashSet<Coordinate>() // coordinates with cards
        val sums = SparseArray<Coordinate>() // e.g. card.Right + adjacent.Left -> Coordinate()
        var adjacent = if (column < 2) mCards[row][column + 1] else null
        if (adjacent != null)
            sums.put(card!!.Right + adjacent.Left, Coordinate(column + 1, row))
        adjacent = if (column > 0) mCards[row][column - 1] else null
        if (adjacent != null) {
            if (sums[card!!.Left + adjacent.Right] != null) {
                possibleFlips.add(Coordinate(column - 1, row))
                possibleFlips.add(sums[card.Left + adjacent.Right])
            } else
                sums.put(card.Left + adjacent.Right, Coordinate(column - 1, row))
        }
        adjacent = if (row < 2) mCards[row + 1][column] else null
        if (adjacent != null) {
            if (sums[card!!.Top + adjacent.Bottom] != null) {
                possibleFlips.add(Coordinate(column, row + 1))
                possibleFlips.add(sums[card.Top + adjacent.Bottom])
            } else
                sums.put(card.Top + adjacent.Bottom, Coordinate(column, row + 1))
        }
        adjacent = if (row > 0) mCards[row - 1][column] else null
        if (adjacent != null) {
            if (sums[card!!.Bottom + adjacent.Top] != null) {
                possibleFlips.add(Coordinate(column, row - 1))
                possibleFlips.add(sums[card.Bottom + adjacent.Top])
            }
        }
        for (coordinate in possibleFlips) {
            adjacent = mCards[coordinate.Row][coordinate.Column]
            if (adjacent!!.IsPlayerOwned != card!!.IsPlayerOwned) {
                adjacent.IsPlayerOwned = card.IsPlayerOwned
                flippedCoordinates.add(coordinate)
            }
        }
        return flippedCoordinates
    }

    private fun applySameWallRule(card: Card?, column: Int, row: Int): Set<Coordinate> {
        val flippedCoordinates = HashSet<Coordinate>()
        val sames = LinkedList<Coordinate>()
        val dummy = Card(-1, 1, 10, 10, 10, 10)
        var adjacent = if (column < 2) mCards[row][column + 1] else dummy
        if ((adjacent != null) && (card!!.Right == adjacent.Left))
            sames.add(Coordinate(column + 1, row))
        adjacent = if (column > 0) mCards[row][column - 1] else dummy
        if ((adjacent != null) && (card!!.Left == adjacent.Right))
            sames.add(Coordinate(column - 1, row))
        adjacent = if (row < 2) mCards[row + 1][column] else dummy
        if ((adjacent != null) && (card!!.Top == adjacent.Bottom))
            sames.add(Coordinate(column, row + 1))
        adjacent = if (row > 0) mCards[row - 1][column] else dummy
        if ((adjacent != null) && (card!!.Bottom == adjacent.Top))
            sames.add(Coordinate(column, row - 1))
        if (sames.size > 1) {
            for (coordinate in sames) {
                if ((coordinate.Column < 0) || (coordinate.Column > 2) || (coordinate.Row < 0) || (coordinate.Row > 2))
                    continue
                adjacent = mCards[coordinate.Row][coordinate.Column]
                if (adjacent!!.IsPlayerOwned != card!!.IsPlayerOwned) {
                    adjacent.IsPlayerOwned = card.IsPlayerOwned
                    flippedCoordinates.add(coordinate)
                }
            }
        }
        return flippedCoordinates
    }

    private fun applyPlusWallRule(card: Card?, column: Int, row: Int): Set<Coordinate> {
        val flippedCoordinates = HashSet<Coordinate>()
        val possibleFlips = HashSet<Coordinate>() // coordinates with cards
        val sums = SparseArray<Coordinate>() // e.g. card.Right + adjacent.Left -> Coordinate()
        val dummy = Card(-1, 1, 10, 10, 10, 10)
        var adjacent = if (column < 2) mCards[row][column + 1] else dummy
        if (adjacent != null)
            sums.put(card!!.Right + adjacent.Left, Coordinate(column + 1, row))
        adjacent = if (column > 0) mCards[row][column - 1] else dummy
        if (adjacent != null) {
            if (sums[card!!.Left + adjacent.Right] != null) {
                possibleFlips.add(Coordinate(column - 1, row))
                possibleFlips.add(sums[card.Left + adjacent.Right])
            } else
                sums.put(card.Left + adjacent.Right, Coordinate(column - 1, row))
        }
        adjacent = if (row < 2) mCards[row + 1][column] else dummy
        if (adjacent != null) {
            if (sums[card!!.Top + adjacent.Bottom] != null) {
                possibleFlips.add(Coordinate(column, row + 1))
                possibleFlips.add(sums[card.Top + adjacent.Bottom])
            } else
                sums.put(card.Top + adjacent.Bottom, Coordinate(column, row + 1))
        }
        adjacent = if (row > 0) mCards[row - 1][column] else dummy
        if (adjacent != null) {
            if (sums[card!!.Bottom + adjacent.Top] != null) {
                possibleFlips.add(Coordinate(column, row - 1))
                possibleFlips.add(sums[card.Bottom + adjacent.Top])
            }
        }
        for (coordinate in possibleFlips) {
            if ((coordinate.Column < 0) || (coordinate.Column > 2) || (coordinate.Row < 0) || (coordinate.Row > 2))
                continue
            adjacent = mCards[coordinate.Row][coordinate.Column]
            if (adjacent!!.IsPlayerOwned != card!!.IsPlayerOwned) {
                adjacent.IsPlayerOwned = card.IsPlayerOwned
                flippedCoordinates.add(coordinate)
            }
        }
        return flippedCoordinates
    }

    inner class Result {
        var PlayerScoreDiff = 0
        var OpponentScoreDiff = 0

        override fun equals(other: Any?): Boolean {
            if (other == null)
                return false
            if (other === this)
                return true
            if (other !is Result)
                return false
            return (PlayerScoreDiff == other.PlayerScoreDiff) && (OpponentScoreDiff == other.OpponentScoreDiff)
        }

        override fun hashCode(): Int {
            var result = PlayerScoreDiff
            result = 31 * result + OpponentScoreDiff
            return result
        }

        override fun toString(): String {
            return "{ PlayerScoreDiff = $PlayerScoreDiff, OpponentScoreDiff = $OpponentScoreDiff }"
        }
    }

    @Suppress("unused")
    private inner class Coordinate(var Column: Int, var Row: Int) {
        override fun equals(other: Any?): Boolean {
            if (other === this)
                return true
            if (other !is Coordinate)
                return false
            val coordinate: Coordinate? = other
            return (Column == coordinate?.Column) && (Row == coordinate.Row)
        }

        override fun hashCode(): Int {
            var result = Column
            result = 31 * result + Row
            return result
        }
    }
}