package luphi.goldenezeit.scene

import android.util.Log
import luphi.goldenezeit.GZ
import luphi.goldenezeit.gGZ
import org.xml.sax.Attributes
import org.xml.sax.Locator
import org.xml.sax.helpers.DefaultHandler
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.util.LinkedList

@Suppress("PropertyName")
class SCCutsceneParser(fileOrContent: String, isFile: Boolean) : DefaultHandler() {
    private val mFileOrContent = fileOrContent
    private val mIsFile = isFile
    private val mCutscene = SCCutscene()
    private var mOpenTag = ""
    private var mEvent: SCEvent? = null
    private var mList: MutableList<SCEvent>? = null
    private var mInSync = false
    private var mInAsync = false

    // Variables for creating/parsing branch cutscenes
    private var mInBranch = false
    private var mBranchDepth = 0 // Tracks the current layer (e.g. <branch><branch><branch></branch></branch></branch> would have a max depth of 3)
    private var mLocator: Locator? = null // Provides the line and/or column number within the content being parsed as XML
    private var mBranchLabel = ""
    private var mBranchStartLocation = 0 // Either a line or column number indicating where the branch's text (XML) begins

    val Cutscene: SCCutscene
        get() = mCutscene

    override fun setDocumentLocator(locator: Locator?) {
        mLocator = locator
    }

    override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
        mOpenTag = localName
        when (localName) {
            "sync" -> {
                mInSync = true
                mInAsync = false
            }
            "async" -> {
                mInSync = false
                mInAsync = true
                mList = LinkedList()
            }
        }
        if ((!mInSync && !mInAsync) || mInBranch)
            return
        when (localName) {
            "animate" -> {
                mEvent = SCEvent(SCEvent.Opcode.ANIMATE)
                mEvent!!.ID = atts.getValue("npc") // May be null
                mEvent!!.Bool = atts.getValue("value")?.equals("false") ?: true
            }
            "branch" -> {
                if (mBranchDepth == 0) {
                    // Either get the line number of the <branch> tag (in a file) or column/index of the tag's ending > character within a single-line string
                    mBranchStartLocation = if (mIsFile) mLocator?.lineNumber ?: 0 else mFileOrContent.indexOf(">", mLocator?.columnNumber ?: 0) + 1
                    // If a speech/thought branch
                    if ((atts.getValue("label") != null) && ((mEvent?.Op == SCEvent.Opcode.SPEECH) || (mEvent?.Op == SCEvent.Opcode.THOUGHT))) {
                        mBranchLabel = atts.getValue("label")
                        mEvent?.Branches?.add(mBranchLabel)
                    }
                    // If a conditional, invisible branch
                    else if ((atts.getValue("conditions") != null) && (atts.getValue("values") != null)) {
                        mEvent = SCEvent(SCEvent.Opcode.BRANCH)
                        // Compress the conditions and values into a single string separated by a newline. The conditions and values strings themselves are also
                        // separated using commas. This compressed string will also act as the key in the branch map as it's sufficiently unique.
                        mBranchLabel = "${atts.getValue("conditions")}\n${atts.getValue("values")}"
                        mEvent?.String = mBranchLabel
                    }
                    // else {
                    // Although nothing can be done without more information, the following lines will still cause this parser to appropriately skip over
                    // this unusable <branch> block and should, therefore, still go ahead
                    // }
                    mInBranch = true
                    mBranchDepth = 1
                } else
                    mBranchDepth += 1
            }
            "exclamation", "question", "ellipsis" -> {
                when (localName) {
                    "exclamation" -> mEvent = SCEvent(SCEvent.Opcode.EXCLAMATION)
                    "question" -> mEvent = SCEvent(SCEvent.Opcode.QUESTION)
                    "ellipsis" -> mEvent = SCEvent(SCEvent.Opcode.ELLIPSIS)
                }
                mEvent!!.ID = atts.getValue("npc") // May be null
                mEvent!!.Int = 1000 // 1000 millisecond delay
            }
            "heal" -> mEvent = SCEvent(SCEvent.Opcode.HEAL)
            "incrementchapter" -> mEvent = SCEvent(SCEvent.Opcode.INC_CHAPTER)
            "incrementverse" -> mEvent = SCEvent(SCEvent.Opcode.INC_VERSE)
            "look" -> {
                mEvent = SCEvent(SCEvent.Opcode.LOOK)
                mEvent!!.X = atts.getValue("x")?.toInt() // May be null
                mEvent!!.Y = atts.getValue("y")?.toInt() // May be null
                mEvent!!.ID = atts.getValue("npc") // May be null
            }
            "pan" -> {
                mEvent = SCEvent(SCEvent.Opcode.PAN)
                mEvent!!.X = atts.getValue("x")?.toInt() // May be null
                mEvent!!.Y = atts.getValue("y")?.toInt() // May be null
                mEvent!!.ID = atts.getValue("npc") // May be null
                mEvent!!.Int = atts.getValue("ms")?.toInt() ?: 1000 // 1000 milliseconds
            }
            "place" -> {
                mEvent = SCEvent(SCEvent.Opcode.PLACE)
                mEvent!!.ID = atts.getValue("npc") // May be null
                mEvent!!.X = atts.getValue("x")?.toInt()  // May be null
                mEvent!!.Y = atts.getValue("y")?.toInt() // May be null
                mEvent!!.Direction = when (atts.getValue("direction")) {
                    "east" -> GZ.Cardinal.EAST
                    "south" -> GZ.Cardinal.SOUTH
                    "west" -> GZ.Cardinal.WEST
                    "north" -> GZ.Cardinal.NORTH
                    else -> null
                }
            }
            "remove" -> {
                mEvent = SCEvent(SCEvent.Opcode.REMOVE)
                mEvent!!.ID = atts.getValue("npc") // May be null
            }
            "scene" -> {
                mEvent = SCEvent(SCEvent.Opcode.SCENE)
                mEvent!!.String = atts.getValue("id")
            }
            "sleep" -> {
                mEvent = SCEvent(SCEvent.Opcode.SLEEP)
                mEvent!!.Int = atts.getValue("ms")?.toInt() ?: 1000 // 1000 milliseconds
            }
            "speech", "thought" -> {
                mEvent = SCEvent(if (mOpenTag == "speech") SCEvent.Opcode.SPEECH else SCEvent.Opcode.THOUGHT)
                mEvent!!.ID = atts.getValue("npc") ?: ""
            }
            "title" -> mEvent = SCEvent(SCEvent.Opcode.TITLE)
            "turn" -> {
                mEvent = SCEvent(SCEvent.Opcode.TURN)
                mEvent!!.ID = atts.getValue("npc") // May be null
                mEvent!!.Direction = when (atts.getValue("direction")) {
                    "east" -> GZ.Cardinal.EAST
                    "south" -> GZ.Cardinal.SOUTH
                    "west" -> GZ.Cardinal.WEST
                    "north" -> GZ.Cardinal.NORTH
                    else -> null
                }
            }
            "walk" -> {
                mEvent = SCEvent(SCEvent.Opcode.WALK)
                mEvent!!.ID = atts.getValue("npc") // May be null
                mEvent!!.X = atts.getValue("x")?.toInt() // May be null
                mEvent!!.Y = atts.getValue("y")?.toInt() // May be null
                mEvent!!.Bool = atts.getValue("follow")?.toBoolean() // May be null
            }
        }
    }

    override fun endElement(namespaceURI: String, localName: String, qName: String) {
        mOpenTag = ""
        if ((!mInSync && !mInAsync))
            return
        if (!mInBranch && (mEvent != null) && (localName != "text") && (localName != "branch") && (localName != "sync") && (localName != "async")) {
            if (mInSync)
            // Add the event to the cutscene as a sequential event
                mCutscene.Acts.add(listOf(mEvent!!))
            else if (mInAsync)
            // Add the event to list of parallel events to be added to the cutscene at the end of the <async> block
                mList!!.add(mEvent!!)
            mEvent = null
        }
        when (localName) {
            "sync" -> mInSync = false
            "async" -> {
                mInAsync = false
                mCutscene.Acts.add(mList!!)
                mList = null
            }
            "branch" -> {
                mBranchDepth -= 1
                // If this marks the end of the top-level branch block (rather than a nested one)
                if (mBranchDepth == 0) {
                    val branchEndLocation = if (mIsFile) mLocator?.lineNumber ?: 0 else mLocator?.columnNumber ?: 0 // Line number for files, column for content
                    var content = ""
                    if ((mBranchStartLocation > 0) && (branchEndLocation > mBranchStartLocation)) {
                        var excerpt = ""
                        if (mIsFile) {
                            val assetManager = gGZ.Context?.assets
                            if (assetManager != null) {
                                try {
                                    val reader = BufferedReader(InputStreamReader(assetManager.open(mFileOrContent)))
                                    // Read the lines starting with mCutsceneStartLine (inclusive) and ending with cutsceneEndLine (inclusive)
                                    // Note: subList()'s indices begin at 0 but the parser reports line numbers beginning at 1
                                    // Note: subList()'s end index (the second parameter) is exclusive
                                    val lines = reader.readLines().subList(mBranchStartLocation, branchEndLocation - 1)
                                    excerpt = lines.joinToString(separator = "", transform = { l -> l.trim() }).trim()
                                } catch (e: Exception) {
                                    Log.e("SCCutsceneParser", "Cutscene parsing error", e)
                                }
                            }
                        } else /* if (!mIsFile) */
                            excerpt = mFileOrContent.substring(mBranchStartLocation, branchEndLocation)
                        // Branches are expected to be written as if they are part of the cutscene - they don't have separate <cutscene> blocks so we add them.
                        // It's also assumed that they are synchronous although this could be changed later if a need arises.
                        content = "<cutscene><sync>$excerpt</sync></cutscene>"
                    }
                    if (mEvent?.Op == SCEvent.Opcode.BRANCH) {
                        if (mInSync)
                            mCutscene.Acts.add(listOf(mEvent!!))
                        else if (mInAsync)
                            mList!!.add(mEvent!!)
                        mEvent = null
                    }
                    mCutscene.Branches[mBranchLabel] = gGZ.Resources.loadCutscene(content, /* isFile */ false)
                    mCutscene.Branches[mBranchLabel]?.ID = mBranchLabel // Has no practical effect but can help with debugging
                    mBranchLabel = ""
                    mBranchStartLocation = 0
                    mInBranch = false
                }
            }
        }
    }

    override fun characters(ch: CharArray, start: Int, length: Int) {
        val string = String(ch, start, length).trim { it <= ' ' }
        if (string.isEmpty() || mInBranch)
            return
        when (mOpenTag) {
            "id" -> mCutscene.ID = string
            "minverse" -> mCutscene.MinVerse = string.toInt()
            "maxverse" -> mCutscene.MaxVerse = string.toInt()
            "once" -> mCutscene.Once = string.toBoolean()
            "text" -> {
                if (mEvent != null) {
                    // If there was already an existing string, prepend a newline before this new (second) string.
                    if ((mEvent!!.String)?.isNotEmpty() == true)
                        mEvent!!.String += "\n"
                    // Append the string to the event (which initializes as a null)
                    mEvent!!.String = (mEvent!!.String ?: "") + string
                }
            }
        }
    }
}