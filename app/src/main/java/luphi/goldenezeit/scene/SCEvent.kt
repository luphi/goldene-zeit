package luphi.goldenezeit.scene

import luphi.goldenezeit.GZ
import java.util.LinkedList

@Suppress("PropertyName")
data class SCEvent(val Op: Opcode) {
    enum class Opcode {
        ANIMATE, BRANCH, ELLIPSIS, EXCLAMATION, HEAL, INC_CHAPTER, INC_VERSE, LOOK, PAN, PLACE, REMOVE, SCENE, SLEEP, SPEECH, THOUGHT, TITLE, TURN, QUESTION,
        WALK
    }

    var ID: String? = null
    var X: Int? = null
    var Y: Int? = null
    var Direction: GZ.Cardinal? = null
    var Bool: Boolean? = null
    var Int: Int? = null
    var Float: Float? = null
    var String: String? = null
    val Branches = LinkedList<String>()

    override fun toString(): String = "{ ID = \"$ID\", Tile = ($X, $Y), Direction = $Direction, Int = $Int, Float = $Float, String = \"$String\" }"
}