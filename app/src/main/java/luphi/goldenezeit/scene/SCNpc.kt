package luphi.goldenezeit.scene

import luphi.goldenezeit.GZ
import luphi.goldenezeit.Tasks
import luphi.goldenezeit.gGZ
import java.util.LinkedList
import java.util.Random

@Suppress("PropertyName")
class SCNpc(val ID: String?, name: String?, private val mCutscenes: LinkedList<SCCutscene>, val Cards: HashSet<Int>) : SCCharacter() {
    private val mTasks = Tasks()

    var CardMessage = "Wanna play cards?"
    val Cutscene: SCCutscene?
        get() {
            for (cutscene in mCutscenes) {
                if ((cutscene.MinVerse <= gGZ.Verse) && (cutscene.MaxVerse >= gGZ.Verse))
                    return cutscene
            }
            return null
        }
    var IsPaused = false
    var Origin: Pair<Int, Int> = Pair(0, 0)
        set(value) {
            field = value
            placeOnTile(value.first, value.second)
        }
    var Roaming: Int = -1 // Distance to roam/pace/walk around. 1+ means the NPC will change tiles, 0 changes direction, -1 means no movement at all.
        set(value) {
            field = value
            if (value >= 0)
                walkAround()
        }

    init {
        Name = name
    }

    override fun step(dt: Long) {
        super.step(dt) // super.step() handles gliding and has a separate Tasks object
        if (!IsPaused)
            mTasks.step(dt) // handles roaming
    }

    override fun toString(): String {
        return "{ ID \"$ID\", origin = (${Origin.first}, ${Origin.second}), roaming distance = ${if (Roaming >= 0) Roaming.toString() else "none"}, " +
                "sprite = ${super.toString()} }"
    }

    private fun walkAround() {
        val random = Random()
        val standDelay: Long = Random().nextInt(5001).toLong() + 3000L // Choose a stand duration between three and eight seconds
        mTasks.scheduleOneOff(standDelay) { walkAround() } // Prepare more roaming with a delay, effectively standing still for a bit
        // If there's no scene in which to walk (yet)
        if (Scene == null)
            return // Another call to this method was prepared above (with a delay) so we'll try again soon
        // If the NPC should walk to a new tile
        if (Roaming > 0) {
            var walkToX = X
            var walkToY = Y
            // Select a random tile within the travel distance to walk to
            do {
                if (random.nextBoolean()) // Choose a random X
                // Walk to an X (tile) coordinate +/- `Travel` tiles away
                    walkToX = Origin.first + (random.nextInt(2 * Roaming) - Roaming)
                else // Choose a random Y
                    walkToY = Origin.second + (random.nextInt(2 * Roaming) - Roaming)
                // while (the coordinate is not a door and not one tile below a door) { ... }
            } while ((Scene?.getDoorAt(walkToX, walkToY) != null) || (Scene?.getDoorAt(walkToX, walkToY - 1) != null))
            // In the event that the coordinate is not walkable or the current coordinate was chosen, the NPC will remain still until the next walkAround()
            walkToTile(walkToX, walkToY, false) { /* onDone */ _, _ -> }
        }
        // If the NPC should stay on its tile but look in different directions
        else
        // Select a random direction
            Direction = if (random.nextBoolean())
                if (random.nextBoolean()) GZ.Cardinal.EAST else GZ.Cardinal.WEST
            else
                if (random.nextBoolean()) GZ.Cardinal.SOUTH else GZ.Cardinal.NORTH
    }
}