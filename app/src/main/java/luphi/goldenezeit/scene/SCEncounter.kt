package luphi.goldenezeit.scene

class SCEncounter(val ID: String, val Weight: Int, val CenterLevel: Int, val LevelMargin: Int) {
    override fun toString(): String = "{ ID \"$ID\", Weight $Weight, CenterLevel $CenterLevel, LevelMargin $LevelMargin }"
}