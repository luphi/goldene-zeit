package luphi.goldenezeit.scene

import luphi.goldenezeit.gl.GLTexture
import java.util.LinkedList

@Suppress("PropertyName")
class SCTileset {
    var Texture: GLTexture? = null
    var FirstGid = 0
    var LastGid = 0
    var NumberOfTiles = 0
    var WidthInTiles = 0
    var HeightInTiles = 0
    var WidthInPixels = 0
    var HeightInPixels = 0
    var WidthOfTileInPixels = 0
    var HeightOfTileInPixels = 0
    var Name = ""
    var Animations = LinkedList<SCAnimation>()

    override fun toString(): String {
        return "{ Texture = $Texture, FirstGid = $FirstGid, LastGid = $LastGid, NumberOfTiles = $NumberOfTiles, WidthInTiles = $WidthInTiles, " +
                "HeightInTiles = $HeightInTiles, WidthInPixels = $WidthInPixels, HeightInPixels = $HeightInPixels, WidthOfTile = $WidthOfTileInPixels" +
                ", HeightOfTile = $HeightOfTileInPixels, Name = \"$Name\", Animations = ${Animations.joinToString()} }"
    }
}