package luphi.goldenezeit.scene

import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gGZ
import java.util.LinkedList
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.math.hypot

// Tiles coordinates are laid out like:
// (0, 0)   (1, 0)   (2, 0)   (3, 0)   (4, 0)
// (0, 1)   (1, 1)   (2, 1)   (3, 1)   (4, 1)
// (0, 2)   (1, 2)   (2, 2)   (3, 2)   (4, 2)
// ...

@Suppress("PropertyName", "PrivatePropertyName")
class Scene(
    val File: String,
    val WidthInTiles: Int,
    val HeightInTiles: Int,
    val WidthOfTile: Float,
    val HeightOfTile: Float,
    private val mWidthOfTileInPixels: Int,
    private val mHeightOfTileInPixels: Int,
    private val mRenderOrder: String,
    private val mLayers: LinkedList<SCLayer>,
    private val mTilesets: List<SCTileset>,
    private val mNPCs: HashMap<String, SCNpc>, // ID string -> non-player character
    private val mIsWalkable: Array<BooleanArray>,
    private val mTraps: Array<Array<String?>>,
    private val mDoors: Array<Array<SCDoor?>>,
    private val mSigns: Array<Array<String?>>,
    private val mItems: Array<Array<String?>>,
    private val mDoorsMap: Map<Int, SCDoor>,
    private val mLanding: Pair<Int, Int>?,
    private val mBackgroundLayerIndex: Int,
    private val mForegroundLayerIndex: Int,
    val PrimaryColor: FloatArray,
    val SecondaryColor: FloatArray,
    val LightColor: FloatArray,
    val DarkColor: FloatArray,
    val EncounterRate: Float,
    val Encounters: List<SCEncounter>,
    val ID: String,
    val Name: String
) : IProgressive {
    private val ITEM_GID = 48 // The image used for items is actually contained in the tileset. This is its GID.

    private val mNpcsLock = ReentrantLock()

    val BackgroundLayer: SCLayer?
        get() = mLayers.getOrNull(mBackgroundLayerIndex)
    val ForegroundLayer: SCLayer?
        get() = mLayers.getOrNull(mForegroundLayerIndex)
    val ItemLayer: SCLayer?
        get() = mLayers.getOrNull(mLayers.size - 1) // The last index in the layers will always be item because it's appended in init {}
    val Landing: SCTile?
        get() = if (mLanding != null) getDummyTileAt(mLanding.first, mLanding.second) else null
    val NPCs
        get() = mNPCs
    val NpcsDrawable: IDrawable = object : IDrawable {
        override val Rect: Rect
            get() = Rect(0f, 0f, 0f, 0f)
        override fun draw(dt: Long) {
            mNpcsLock.withLock {
                for (npc in mNPCs.values)
                    npc.draw(dt)
            }
        }
    }

    init {
        for (npc in mNPCs.values) {
            npc.Scene = this
            npc.placeOnTile(npc.Origin)
        }
        val itemLayer = SCLayer()
        itemLayer.Name = "Items"
        itemLayer.WidthInTiles = WidthInTiles
        itemLayer.HeightInTiles = HeightInTiles
        for (x in mItems.indices) {
            for (y in mItems[x].indices) {
                val itemID = mItems.getOrNull(x)?.getOrNull(y)
                if (itemID != null) {
                    // If records indicate the player has already picked up this item in this scene
                    if (gGZ.Record.get("picked_up $ID $itemID", "false") == "true") {
                        mItems[x][y] = null // Remove it
                        continue // Don't bother adding it to the layer either
                    }
                    // If the item is hidden (the parser will have appended the ID with an asterisk)
                    if (itemID.endsWith("*"))
                        mItems[x][y] = itemID.substring(0, itemID.length - 1)
                    else
                        itemLayer.Tiles.add(
                            SCTile(
                                gGZ.Utils.getTilesetByGID(mTilesets, ITEM_GID), x, y, ITEM_GID, WidthInTiles, HeightInTiles, WidthOfTile, HeightOfTile,
                                mRenderOrder
                            )
                        )
                }
            }
        }
        mLayers.add(itemLayer)
    }

    override fun step(dt: Long) {
        for (layer in mLayers)
            layer.step(dt)
        for (npc in mNPCs.values)
            npc.step(dt)
    }

    fun free() {
        // TODO?
    }

    fun isTileWalkableAt(x: Int, y: Int): Boolean = (mIsWalkable.getOrNull(x)?.getOrNull(y) == true) && (mItems.getOrNull(x)?.getOrNull(y) == null)

    fun setTileWalkableAt(x: Int, y: Int, isWalkable: Boolean) {
        if ((x < 0) || (y < 0) || (x >= WidthInTiles) || (y >= HeightInTiles))
            return
        mIsWalkable[x][y] = isWalkable
    }

    fun getDummyTileAt(x: Int, y: Int): SCTile = SCTile(x, y, WidthInTiles, HeightInTiles, WidthOfTile, HeightOfTile, mRenderOrder)

    fun getPath(fromX: Int, fromY: Int, toX: Int, toY: Int): List<SCTile>? {
        val nodes: Array<Array<SCTile?>> = Array(WidthInTiles) { arrayOfNulls<SCTile?>(HeightInTiles) } // Dummy tiles representing the background tiles
        val openNodes = LinkedList<SCTile?>() // "Open" nodes, those which can be walked on
        val closedNodes = LinkedList<SCTile?>() // "Closed" nodes, those which are not walkable or have been tried
        val fromMap = HashMap<SCTile?, SCTile?>() // Web of nodes pointing to other nodes based on the shortest path
        val gScore = HashMap<SCTile?, Float>() // For a given node, the cost to reach that node from the start
        val fScore = HashMap<SCTile?, Float>() // For a given node, the estimated cost to the goal
        var current: SCTile? // Pointer/iterator for traversing the graph
        // Error checks
        if ((fromX < 0) || (fromX >= WidthInTiles) || (fromY < 0) || (fromY >= HeightInTiles))
            return null
        // Initialize the dummy graph
        for (i in 0 until WidthInTiles) {
            for (j in 0 until HeightInTiles)
                nodes[i][j] = getDummyTileAt(i, j)
        }
        // Initialize fScore and gScore
        for (i in 0 until WidthInTiles) {
            for (j in 0 until HeightInTiles) {
                fScore[nodes[i][j]] = Float.POSITIVE_INFINITY
                gScore[nodes[i][j]] = Float.POSITIVE_INFINITY
            }
        }
        // This is an unweighted implementation of A*
        current = nodes[fromX][fromY]
        gScore[current] = 0f
        fScore[current] = hypot(toX - fromX.toDouble(), toY - fromY.toDouble()).toFloat()
        openNodes.add(current)
        // Get the node with the lowest estimated cost, consider it a closed node, and remove it from the open node list
        while (!openNodes.isEmpty()) {
            var lowest = Float.POSITIVE_INFINITY
            for (adjacent in openNodes) {
                if (fScore[adjacent]!! < lowest) {
                    lowest = fScore[adjacent]!!
                    current = adjacent
                }
            }
            closedNodes.add(current)
            openNodes.remove(current)
            // If the goal tile was reached
            if (current?.X == toX && current.Y == toY) {
                val path = LinkedList<SCTile>()
                while (current != null) {
                    path.add(0, current)
                    current = fromMap[current]
                }
                return path
            }
            val adjacents = LinkedList<SCTile?>()
            if (current!!.X - 1 >= 0)
                adjacents.add(nodes[current.X - 1][current.Y])
            if (current.X + 1 < WidthInTiles)
                adjacents.add(nodes[current.X + 1][current.Y])
            if (current.Y - 1 >= 0)
                adjacents.add(nodes[current.X][current.Y - 1])
            if (current.Y + 1 < HeightInTiles)
                adjacents.add(nodes[current.X][current.Y + 1])
            for (adjacent in adjacents) {
                if (!mIsWalkable[adjacent!!.X][adjacent.Y] && adjacent != nodes[fromX][fromY]) {
                    closedNodes.add(adjacent)
                    continue
                }
                if (closedNodes.contains(adjacent))
                    continue
                if (!openNodes.contains(adjacent))
                    openNodes.add(adjacent)
                val gTentative = gScore[current]!! + hypot((current.X - adjacent.X).toDouble(), (current.Y - adjacent.Y).toDouble()).toFloat()
                if (gTentative >= gScore[adjacent]!!)
                    continue
                fromMap[adjacent] = current
                gScore[adjacent] = gTentative
                fScore[adjacent] = gTentative + hypot((adjacent.X - toX).toDouble(), (adjacent.Y - toY).toDouble()).toFloat()
            }
        }
        return null // The goal is unreachable
    }

    fun getTrapAt(x: Int, y: Int): String? = mTraps.getOrNull(x)?.getOrNull(y) // can be seen as return mTraps[x]?[y] but those operators don't exist

    fun getDoorAt(x: Int, y: Int): SCDoor? = mDoors.getOrNull(x)?.getOrNull(y) // can be seen as return mDoors[x]?[y] but those operators don't exist

    fun getNpcAt(x: Int, y: Int): SCNpc? {
        for (npc in mNPCs.values) {
            if ((npc.X == x) && (npc.Y == y))
                return npc
        }
        return null
    }

    fun getNpcByID(id: String): SCNpc? = mNPCs[id]

    fun removeNpcByID(id: String) {
        val npc = mNPCs[id] ?: return
        setTileWalkableAt(npc.X, npc.Y, true)
        mNpcsLock.withLock { mNPCs.remove(id) }
    }

    fun getSignAt(x: Int, y: Int): String? = mSigns.getOrNull(x)?.getOrNull(y) // can be seen as return mSigns[x]?[y] but those operators don't exist

    fun pickUpItemAt(x: Int, y: Int): String? {
        val item = mItems.getOrNull(x)?.getOrNull(y)
        if (item != null) {
            mItems[x][y] = null
            ItemLayer?.remoteTileAt(x, y)
        }
        return item
    }

    fun getDoorByID(id: Int): SCDoor? = mDoorsMap[id]

    override fun toString(): String = "{ $File [$WidthInTiles x $HeightInTiles] ${mLayers.size} layer(s) }"
}