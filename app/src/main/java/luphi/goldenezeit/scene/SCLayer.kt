package luphi.goldenezeit.scene

import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IProgressive
import java.util.LinkedList
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

@Suppress("PropertyName")
class SCLayer : IDrawable, IProgressive {
    override val Rect: Rect
        get() = Rect(0f, 0f, 0f, 0f) // Just a dummy, these values are never used

    private val mTilesLock = ReentrantLock()

    var Name = ""
    var OffsetXInTiles = 0 // X coordinate in tiles
    var OffsetYInTiles = 0 // Y coordinate in tiles
    var WidthInTiles = 0
    var HeightInTiles = 0
    var IsVisible = true
    var Tiles = LinkedList<SCTile>()
    var Alpha: Float = 1f
        set(value) {
            field = value
            for (tile in Tiles)
                tile.Alpha = value
        }

    override fun draw(dt: Long) {
        if (!IsVisible)
            return
        mTilesLock.withLock {
            for (tile in Tiles)
                tile.draw(dt)
        }
    }

    override fun step(dt: Long) {
        for (tile in Tiles)
            tile.step(dt)
    }

    // TODO remove this when no longer needed (exists for visual debugging)
    fun setTileHighlightColorAt(x: Int, y: Int, color: FloatArray?) {
        getTileAt(x, y)?.setHighlightColor(color)
    }

    fun remoteTileAt(x: Int, y: Int) {
        val tile = getTileAt(x, y)
        if (tile != null)
            mTilesLock.withLock { Tiles.remove(tile) }
    }

    private fun getTileAt(x: Int, y: Int): SCTile? {
        for (tile in Tiles) {
            if ((tile.X == x) && (tile.Y == y))
                return tile
        }
        return null
    }

    override fun toString(): String {
        return "{ Name = \"$Name\", TileOffsetX = $OffsetXInTiles, TileOffsetY = $OffsetYInTiles, WidthInTiles = $WidthInTiles, HeightInTiles = " +
                "$HeightInTiles, Alpha = $Alpha, IsVisible = $IsVisible, Tiles.size = ${Tiles.size} }"
    }
}