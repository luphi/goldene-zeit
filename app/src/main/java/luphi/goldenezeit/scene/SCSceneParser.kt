package luphi.goldenezeit.scene

import android.util.Base64
import android.util.Log
import luphi.goldenezeit.GZ
import luphi.goldenezeit.gGZ
import org.xml.sax.Attributes
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler
import java.io.BufferedInputStream
import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.util.*
import java.util.zip.GZIPInputStream
import java.util.zip.InflaterInputStream
import kotlin.collections.HashMap


@Suppress("PropertyName", "PrivatePropertyName")
class SCSceneParser(private val mID: String, private val mFile: String) : DefaultHandler() {
    private class Area(var X: Int, var Y: Int, var W: Int, var H: Int)
    private class Item(var X: Int, var Y: Int, var ID: String = "", var IsHidden: Boolean = false)

    private val VISIBLE_ROWS = 9

    // State trackers
    private var mInTileset = false
    private var mInLayer = false
    private var mInData = false
    private var mInAnimation = false
    private var mInDoor = false
    private var mInItem = false
    private var mInSign = false
    private var mInTrap = false
    private var mInNpc = false
    private var mInEncounters = false

    // Parsed properties
    private var mRenderOrder = ""
    private var mDataEncoding: String? = null // "csv" or "base64" or null if using the (deprecated) <tile> elements
    private var mDataCompression: String? = null // "gzip" or "zlib" or null (no compression) are acceptable; may also be "zstd" but that's not supported
    private var mCsv = "" // Comma-separated GIDs, if using the "csv" encoding
    private val mLayers = LinkedList<SCLayer>()
    private val mTilesets = LinkedList<SCTileset>()
    private val mNPCs = HashMap<String, SCNpc>()
    private var mIsWalkable = Array(0) { BooleanArray(0) }
    private var mTraps = Array(0) { arrayOfNulls<String?>(0) }
    private var mDoors = Array(0) { arrayOfNulls<SCDoor?>(0) }
    private var mSigns = Array(0) { arrayOfNulls<String?>(0) }
    private var mItems = Array(0) { arrayOfNulls<String?>(0) }
    private val mDoorsMap = HashMap<Int, SCDoor>()
    private var mLanding: Pair<Int, Int>? = null
    private var mBackgroundLayerIndex = -1
    private var mForegroundLayerIndex = -1
    private var mPrimaryColor = floatArrayOf(0.721568627f, 0.533333333f, 0.97254902f) // Purple-ish
    private var mSecondaryColor = floatArrayOf(0.345098039f, 0.721568627f, 0.97254902f) // Blue-ish
    private var mLightColor = floatArrayOf(0.97254902f, 0.97254902f, 0.97254902f) // White-ish
    private var mDarkColor = floatArrayOf(0.094117647f, 0.094117647f, 0.094117647f) // Black-ish
    private var mEncounterRate = 0f
    private var mEncounters = LinkedList<SCEncounter>()
    private var mName = "Unknown"

    // Temporary references
    private var mCurrentTileset: SCTileset? = null
    private var mCurrentNpc: SCNpc? = null
    private var mCurrentLayer: SCLayer? = null
    private var mCurrentAnimation: SCAnimation? = null
    private var mCurrentItem: Item? = null
    private var mCurrentSign: Pair<Int, Int>? = null // Coordinate of the sign-to-be
    private var mNpcDirection = GZ.Cardinal.SOUTH
    private var mNpcOrigin = Pair(0, 0)
    private var mNpcRoaming = -1
    private var mArea: Area? = null
    private var mEncountersString = ""

    // Dimension properties
    private var mNumberOfTiles = 0
    private var mWidthInTiles = 0
    private var mHeightInTiles = 0
    private var mWidthOfTile = 0f
    private var mHeightOfTile = 0f
    private var mWidthOfTileInPixels = 0
    private var mHeightOfTileInPixels = 0

    val Scene: Scene
        get() {
            return Scene(
                mFile, mWidthInTiles, mHeightInTiles, mWidthOfTile, mHeightOfTile, mWidthOfTileInPixels, mHeightOfTileInPixels, "right-down", mLayers,
                mTilesets, mNPCs, mIsWalkable, mTraps, mDoors, mSigns, mItems, mDoorsMap, mLanding, mBackgroundLayerIndex, mForegroundLayerIndex, mPrimaryColor,
                mSecondaryColor, mLightColor, mDarkColor, mEncounterRate, mEncounters, mID, mName
            )
        }

    @Throws(SAXException::class)
    override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
        var source: String
        when {
            localName == "map" -> {
                if (atts.getValue("orientation") != "orthogonal")
                    throw SAXException("Unsupported orientation.")
                mWidthInTiles = atts.getValue("width").toInt()
                mHeightInTiles = atts.getValue("height").toInt()
                mWidthOfTileInPixels = atts.getValue("tilewidth").toInt()
                mHeightOfTileInPixels = atts.getValue("tileheight").toInt()
                mHeightOfTile = 1f / VISIBLE_ROWS
                mWidthOfTile = mHeightOfTile * (mWidthOfTileInPixels.toFloat() / mHeightOfTileInPixels.toFloat())
                mRenderOrder = atts.getValue("renderorder")
                mIsWalkable = Array(mWidthInTiles + 1) { BooleanArray(mHeightInTiles + 1) }
                mTraps = Array(mWidthInTiles + 1) { arrayOfNulls<String?>(mHeightInTiles + 1) }
                mDoors = Array(mWidthInTiles + 1) { arrayOfNulls<SCDoor?>(mHeightInTiles + 1) }
                mSigns = Array(mWidthInTiles + 1) { arrayOfNulls<String?>(mHeightInTiles + 1) }
                mItems = Array(mWidthInTiles + 1) { arrayOfNulls<String?>(mHeightInTiles + 1) }
                for (i in mIsWalkable.indices)
                    for (j in mIsWalkable[i].indices) {
                        mIsWalkable[i][j] = true
                        mTraps[i][j] = null
                        mDoors[i][j] = null
                        mSigns[i][j] = null
                        mItems[i][j] = null
                    }
            }
            localName == "tileset" -> {
                mInTileset = true
                mCurrentTileset = SCTileset()
                mCurrentTileset?.FirstGid = atts.getValue("firstgid").toInt()
                mCurrentTileset?.WidthOfTileInPixels = atts.getValue("tilewidth").toInt()
                mCurrentTileset?.HeightOfTileInPixels = atts.getValue("tileheight").toInt()
                mCurrentTileset?.Name = atts.getValue("name") ?: ""
                mCurrentTileset?.WidthInTiles = atts.getValue("columns")?.toInt() ?: 0
                mCurrentTileset?.NumberOfTiles = atts.getValue("tilecount")?.toInt() ?: 0
                if (((mCurrentTileset?.NumberOfTiles ?: 0) != 0) && (mCurrentTileset?.WidthInTiles ?: 0 != 0))
                    mCurrentTileset?.HeightInTiles = mCurrentTileset!!.NumberOfTiles / mCurrentTileset!!.WidthInTiles
                mCurrentTileset?.LastGid = mCurrentTileset!!.FirstGid + mCurrentTileset!!.WidthInTiles * mCurrentTileset!!.HeightInTiles - 1
                mTilesets.add(mCurrentTileset!!)
            }
            (localName == "image") && mInTileset -> {
                source = atts.getValue("source")
                // It's assumed that all tileset images will be in assets/tilesets so any path leading up to the source basename will be removed.  This also
                // means scenes can be created on a desktop using whatever directory structure a developer wishes.
                if (source.contains("/"))
                    source = source.substring(source.lastIndexOf("/") + 1)
                mCurrentTileset?.Texture = gGZ.Resources.loadTexture("tilesets/$source")
                mCurrentTileset?.WidthInPixels = atts.getValue("width").toInt()
                mCurrentTileset?.HeightInPixels = atts.getValue("height").toInt()
                if (mCurrentTileset?.WidthInTiles == 0)
                    mCurrentTileset?.WidthInTiles = mCurrentTileset!!.WidthOfTileInPixels / mCurrentTileset!!.WidthInPixels
                if (mCurrentTileset?.HeightInTiles == 0)
                    mCurrentTileset?.HeightInTiles = mCurrentTileset!!.HeightOfTileInPixels / mCurrentTileset!!.HeightInPixels
            }
            (localName == "tile") && mInTileset -> {
                mCurrentAnimation = SCAnimation(atts.getValue("id").toInt())
                mCurrentTileset?.Animations?.add(mCurrentAnimation!!)
            }
            (localName == "animation") && mInTileset -> mInAnimation = true
            (localName == "frame") && mInAnimation -> mCurrentAnimation?.addFrame(
                atts.getValue("tileid").toInt() + (mCurrentTileset?.FirstGid ?: 0),
                atts.getValue("duration").toLong()
            )
            localName == "layer" -> {
                mInLayer = true
                val layerW = atts.getValue("width").toInt()
                val layerH = atts.getValue("height").toInt()
                mCurrentLayer = SCLayer()
                mCurrentLayer?.Name = atts.getValue("name")
                if (atts.getValue("x") != null)
                    mCurrentLayer?.OffsetXInTiles = atts.getValue("x").toInt()
                if (atts.getValue("y") != null)
                    mCurrentLayer?.OffsetYInTiles = atts.getValue("y").toInt()
                mCurrentLayer?.WidthInTiles = layerW
                mCurrentLayer?.HeightInTiles = layerH
                if (atts.getValue("visible") != null)
                    mCurrentLayer?.IsVisible = atts.getValue("visible") == "1"
                if (atts.getValue("opacity") != null)
                    mCurrentLayer?.Alpha = atts.getValue("opacity").toFloat()
                mLayers.add(mCurrentLayer!!)
            }
            (localName == "data") && mInLayer -> {
                mInData = true
                mNumberOfTiles = 0
                mDataEncoding = atts.getValue("encoding") // May be null
                mDataCompression = atts.getValue("compression") // May be null
            }
            (localName == "tile") && mInLayer && mInData && (mDataEncoding == null) -> {
                // <tile> tags may exist without any information - these are essentially unused tiles within the setWidthNDC and setHeightNDC of the scene but
                // have nothing placed on them
                val gid = atts.getValue("gid")?.toIntOrNull() ?: 0
                if (gid != 0)
                    mCurrentLayer?.Tiles?.add(
                        SCTile(
                            gGZ.Utils.getTilesetByGID(mTilesets, gid), mNumberOfTiles % mWidthInTiles, mNumberOfTiles / mWidthInTiles, gid, mWidthInTiles,
                            mHeightInTiles, mWidthOfTile, mHeightOfTile, mRenderOrder
                        )
                    )
                mNumberOfTiles += 1
            }
            localName == "object" -> {
                var type = ""
                if (atts.getValue("type") != null)
                    type = atts.getValue("type")
                val objX: Int = atts.getValue("x")?.toInt() ?: 0
                val objY: Int = atts.getValue("y")?.toInt() ?: 0
                val objW: Int = atts.getValue("width")?.toInt() ?: mWidthOfTileInPixels
                val objH: Int = atts.getValue("height")?.toInt() ?: mHeightOfTileInPixels
                // An <object> is actually defined as an area of pixels so, in order to know which tiles to make non-walkable, we need to identify which tiles
                // are within that area and loop through all of them
                val tileX = objX / mWidthOfTileInPixels
                val tileY = objY / mHeightOfTileInPixels
                val tileW = objW / mWidthOfTileInPixels
                val tileH = objH / mHeightOfTileInPixels
                for (i in tileX until tileX + tileW) {
                    for (j in tileY until tileY + tileH) {
                        var x: Int
                        var y: Int
                        when (mRenderOrder) {
                            "right-down" -> {
                                x = i
                                y = j
                            }
                            "right-up" -> {
                                x = i
                                y = mHeightInTiles - j - 1
                            }
                            "left-down" -> {
                                x = mWidthInTiles - i - 1
                                y = j
                            }
                            "left-up" -> {
                                x = mWidthInTiles - i - 1
                                y = mHeightInTiles - j - 1
                            }
                            else -> {
                                x = 0
                                y = 0
                            }
                        }
                        if ((x < 0) || (y < 0) || (x > mWidthInTiles) || (y > mHeightInTiles)) {
                            // Collisions may exist within the scene for the sake of understanding boundaries when using a map editor but do not need to be
                            // included in mIsWalkable because all tiles outside the scene's boundaries are considered non-walkable
                            continue
                        }
                        when (type) {
                            "Wall" -> mIsWalkable[x][y] = false
                            "NPC" -> {
                                mNpcOrigin = Pair(x, y)
                                mInNpc = true
                            }
                            "Door" -> mDoors[x][y] = SCDoor(x, y)
                            "Sign" -> {
                                mCurrentSign = Pair(x, y)
                                mInSign = true
                            }
                            "Item" -> {
                                mCurrentItem = Item(x, y)
                                mInItem = true
                            }
                            "Landing" -> mLanding = Pair(x, y)
                        }
                    }
                }
                if ((type == "Trap") || (type == "Door")) {
                    // Traps and doors, similar to collisions, often span multiple tiles.  However, unlike collisions, traps and doors (as well NPCs) contain
                    // child tags with custom properties.  As a result, those properties are not yet known so the current trap's/door's area will be defined
                    // here and used later to fill out their properties.
                    mArea = Area(tileX, tileY, tileW, tileH)
                    if (type == "Trap")
                        mInTrap = true
                    else
                        mInDoor = true
                }
            }
            localName == "property" -> {
                var name = ""
                var value = ""
                if (atts.getValue("name") != null)
                    name = atts.getValue("name")
                if (atts.getValue("value") != null)
                    value = atts.getValue("value")
                if (mInDoor) {
                    for (x in mArea!!.X until mArea!!.X + mArea!!.W) {
                        for (y in mArea!!.Y until mArea!!.Y + mArea!!.H) {
                            val door = mDoors.getOrNull(x)?.getOrNull(y)
                            when (name) {
                                "Direction" -> when (value.toLowerCase(Locale.getDefault())) {
                                    "north" -> door?.Direction = GZ.Cardinal.NORTH
                                    "east" -> door?.Direction = GZ.Cardinal.EAST
                                    "south" -> door?.Direction = GZ.Cardinal.SOUTH
                                    "west" -> door?.Direction = GZ.Cardinal.WEST
                                }
                                "ID" -> {
                                    door?.ID = value.toInt()
                                    if (door != null)
                                        mDoorsMap[value.toInt()] = door
                                }
                                "ToDoor" -> door?.ToDoor = value.toInt()
                                "ToScene" -> door?.ToScene = value
                            }
                        }
                    }
                } else if (mInSign) {
                    // If setting the sign's text by ID and it has not already been set by value
                    if ((name == "ID") && (mSigns.getOrNull(mCurrentSign!!.first)?.getOrNull(mCurrentSign!!.second) == null))
                        mSigns[mCurrentSign!!.first][mCurrentSign!!.second] = gGZ.Resources.SignCatalog[value]
                    else if (name == "Value")
                        mSigns[mCurrentSign!!.first][mCurrentSign!!.second] = value
                } else if (mInItem) {
                    if (name == "ID")
                        mCurrentItem?.ID = value
                    else if (name == "Hidden")
                        mCurrentItem?.IsHidden = value.toBoolean()
                } else if (mInNpc) {
                    if (name == "ID") {
                        mCurrentNpc = gGZ.Resources.loadNpc(gGZ.Resources.NpcCatalog.get(value, gGZ.Verse))
                        if (mCurrentNpc != null) {
                            mCurrentNpc!!.Direction = mNpcDirection
                            mCurrentNpc!!.Origin = mNpcOrigin
                            mCurrentNpc!!.Roaming = mNpcRoaming
                            mNPCs[value.toLowerCase(Locale.getDefault())] = mCurrentNpc!!
                        }
                    } else if (name == "Direction") {
                        val directionAsCardinal = when (value.toLowerCase(Locale.getDefault())) {
                            "north" -> GZ.Cardinal.NORTH
                            "east" -> GZ.Cardinal.EAST
                            "west" -> GZ.Cardinal.WEST
                            else -> GZ.Cardinal.SOUTH
                        }
                        if (mCurrentNpc != null)
                            mCurrentNpc!!.Direction = directionAsCardinal
                        else
                            mNpcDirection = directionAsCardinal
                    } else if (name == "Roaming") {
                        if (mCurrentNpc != null)
                            mCurrentNpc!!.Roaming = value.toInt()
                        else
                            mNpcRoaming = value.toInt()
                    }
                } else if (mInTrap && (name == "ID")) {
                    for (x in mArea!!.X until mArea!!.X + mArea!!.W) {
                        for (y in mArea!!.Y until mArea!!.Y + mArea!!.H)
                            mTraps[x][y] = value
                    }
                } else if (name == "ColorPrimary")
                    mPrimaryColor = gGZ.Utils.hexColorToFloatArray(value)
                else if (name == "ColorSecondary")
                    mSecondaryColor = gGZ.Utils.hexColorToFloatArray(value)
                else if (name == "ColorLight")
                    mLightColor = gGZ.Utils.hexColorToFloatArray(value)
                else if (name == "ColorDark")
                    mDarkColor = gGZ.Utils.hexColorToFloatArray(value)
                else if (name == "EncounterRate")
                    mEncounterRate = value.toFloat()
                else if (name == "Encounters")
                    mInEncounters = true
                else if (name == "Name")
                    mName = value
            }
        }
    }

    override fun endElement(namespaceURI: String, localName: String, qName: String) {
        when (localName) {
            "tileset" -> {
                mCurrentTileset = null
                mInTileset = false
            }
            "layer" -> {
                mCurrentLayer = null
                mInLayer = false
            }
            "data" -> {
                mInData = false
                if (mDataEncoding == "csv")
                    decodeCsv()
            }
            "animation" -> {
                mCurrentAnimation = null
                mInAnimation = false
            }
            "object" -> {
                if (mInItem && (mCurrentItem != null))
                // Appending an asterisk to the ID indicates, to the Scene, that the item should not be included in the visible layer
                    mItems[mCurrentItem!!.X][mCurrentItem!!.Y] = mCurrentItem!!.ID + (if (mCurrentItem?.IsHidden == true) "*" else "")
                mCurrentNpc = null
                mCurrentSign = null
                mArea = null
                mInDoor = false
                mInSign = false
                mInItem = false
                mInNpc = false
                mInTrap = false
                mNpcDirection = GZ.Cardinal.SOUTH
                mNpcOrigin = Pair(0, 0)
                mNpcRoaming = -1
            }
            "property" -> {
                if (mInEncounters) {
                    mInEncounters = false
                    for (line in mEncountersString.lines()) {
                        val split = line.trim().split(":")
                        if (split.size != 4)
                            continue
                        mEncounters.add(SCEncounter(split[0], split[1].toInt(), split[2].toInt(), split[3].toInt()))
                    }
                }
            }
        }
    }

    override fun characters(ch: CharArray, start: Int, length: Int) {
        val string = String(ch, start, length).trim()
        if (string.isEmpty())
            return
        // If in a <data> block and the data is encoded using comma-separated values (CSV)
        if (mInData && (mDataEncoding == "csv"))
        // characters() is called on a per-line basis so we must append the line(s) to a variable in order to collect the full data
            mCsv += (if (mCsv.isNotEmpty()) "\n" else "") + string
        // If in a <data> block and the data is encoded using compressed or uncompressed Base64 encoding
        else if (mInData && (mDataEncoding == "base64"))
        // Unlike CSV data, base64 data is contained on a single line so it can be decoded immediately
            decodeBase64(string)
        else if (mInEncounters)
            mEncountersString += (if (mEncountersString.isNotEmpty()) "\n" else "") + string
    }

    override fun endDocument() {
        for (i in mLayers.indices) {
            if (mLayers[i].Name == "Background")
                mBackgroundLayerIndex = i
            else if (mLayers[i].Name == "Foreground")
                mForegroundLayerIndex = i
        }
    }

    private fun decodeCsv() {
        // mCsv will have been filled out by characters() using the typical format: rows separated by newlines and columns within them separated by commas
        // Iterate through the rows
        var y = 0
        for (line in mCsv.lines()) {
            // Iterate through the columns in the row
            var x = 0
            for (entry in line.split(",")) {
                val gid = entry.toIntOrNull() ?: continue // Will probably continue at the end of the row due to Tiled using trailing commas
                mCurrentLayer?.Tiles?.add(
                    SCTile(gGZ.Utils.getTilesetByGID(mTilesets, gid), x, y, gid, mWidthInTiles, mHeightInTiles, mWidthOfTile, mHeightOfTile, mRenderOrder)
                )
                x += 1 // Next column
            }
            y += 1 // Next row
        }
    }

    private fun decodeBase64(base64: String) {
        // Decode the base64 string to an array of bytes (ByteArray). Both compressed and uncompressed data, are encapsulated in Base64 encoding.
        val byteStream = ByteArrayInputStream(Base64.decode(base64, Base64.DEFAULT))
        // Feed the byte array into a DataInputStream in order to make use of its very helpful readInt() method
        val dataStream = when (mDataCompression) {
            null -> DataInputStream(byteStream)
            "gzip" -> DataInputStream(BufferedInputStream(GZIPInputStream(byteStream)))
            "zlib" -> DataInputStream(BufferedInputStream(InflaterInputStream(byteStream)))
            else -> {
                Log.e("SCSceneParser", "Data compression \"$mDataCompression\" is not supported")
                return
            }
        }
        var numberOfTiles = 0
        // While the stream, of any type, reports at least one byte available
        // Note: available() is not guaranteed to be accurate in regards to how many bytes remain (but may be accurate when fed a buffered input stream as it is
        // here). That said, it's used here to determine whether we've reached the EOF or not. To that end, it does appear reliable.
        while (dataStream.available() > 0) {
            // Read the next 32 bits as an integer and reverse the bytes (the TMX documentation states base64 data uses little-endian ordering)
            val gid = Integer.reverseBytes(dataStream.readInt())
            mCurrentLayer?.Tiles?.add(
                SCTile(
                    gGZ.Utils.getTilesetByGID(mTilesets, gid), numberOfTiles % mWidthInTiles, numberOfTiles / mWidthInTiles, gid, mWidthInTiles,
                    mHeightInTiles, mWidthOfTile, mHeightOfTile, mRenderOrder
                )
            )
            numberOfTiles += 1
        }
    }
}