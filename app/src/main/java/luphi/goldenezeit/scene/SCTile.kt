package luphi.goldenezeit.scene

import luphi.goldenezeit.Rect
import luphi.goldenezeit.Tasks
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.gl.GLSprite

@Suppress("PropertyName")
class SCTile : IDrawable, IProgressive {
    override val Rect: Rect
        get() = mSprite.Rect

    private val mSprite = GLSprite()
    private var mGid = 0
    private var mSceneIndex = 0
    private var mTileset: SCTileset? = null
    private var mWidthOfTile = 0f
    private var mHeightOfTile = 0f
    private var mWidthInTiles = 0
    private var mHeightInTiles = 0
    private var mRenderOrder = "right-down"

    // Highlight quad-related variables
    private var mIsHighlighted = false
    private var mHighlightQuad: GLPrimitive? = null

    // Animation-related variables
    private var mAnimation: SCAnimation? = null
    private val mTasks = Tasks()
    private var mFrame = 0

    var X = 0
    var Y = 0

    var Alpha = 1f
        set(value) {
            field = value
            mSprite.Alpha = value
        }

    // Intended for dummy tiles (those that indicate a tile's position and dimensions [used in various ways] but are not drawn)
    constructor(x: Int, y: Int, widthInTiles: Int, heightInTiles: Int, widthOfTile: Float, heightOfTile: Float, renderOrder: String) :
            this(null, x, y, 0, widthInTiles, heightInTiles, widthOfTile, heightOfTile, renderOrder)

    constructor(
        tileset: SCTileset?,
        x: Int,
        y: Int,
        gid: Int,
        widthInTiles: Int,
        heightInTiles: Int,
        widthOfTile: Float,
        heightOfTile: Float,
        renderOrder: String
    ) {
        if ((x < 0) || (y < 0) || (x >= widthInTiles) || (y >= heightInTiles))
            return
        mTileset = tileset
        mSprite.init(mTileset?.Texture)
        X = x
        Y = y
        mWidthInTiles = widthInTiles
        mHeightInTiles = heightInTiles
        mWidthOfTile = widthOfTile
        mHeightOfTile = heightOfTile
        mRenderOrder = renderOrder
        mSceneIndex = y * mWidthInTiles + x
        mGid = gid
        calculateSizeAndPosition()
        if (mTileset != null) {
            for (animation in mTileset!!.Animations) {
                if (animation.Gid == gid - mTileset!!.FirstGid) {
                    mAnimation = animation
                    setTextureCoordinates(mAnimation!!.Frames[0])
                    mTasks.scheduleOneOff(mAnimation!!.Durations[mFrame]) { nextAnimationFrame() }
                    return
                }
            }
            setTextureCoordinates(mGid)
        }
    }

    override fun draw(dt: Long) {
        if (mTileset == null)
            return
        mSprite.draw(dt)
        if (mIsHighlighted)
            mHighlightQuad?.draw(dt)
    }

    override fun step(dt: Long) = mTasks.step(dt)

    fun setHighlightColor(color: FloatArray?) {
        if (color == null) {
            mIsHighlighted = false
            mHighlightQuad = null
        } else {
            mIsHighlighted = true
            if (mHighlightQuad == null) {
                mHighlightQuad = GLPrimitive()
                mHighlightQuad?.init(GLPrimitive.Shape.QUAD)
                val rect = Rect
                mHighlightQuad?.move(rect)
                mHighlightQuad?.size(rect)
                mHighlightQuad?.Alpha = 0.3f
            }
            mHighlightQuad?.Color = color
        }
    }

    override fun toString(): String = "{ position ($X, $Y), GID $mGid, tileset \"${mTileset?.Name}\" }"

    private fun nextAnimationFrame() {
        mFrame += 1
        if (mFrame == mAnimation?.Frames?.size)
            mFrame = 0
        setTextureCoordinates(mAnimation?.Frames!![mFrame])
        mTasks.scheduleOneOff(mAnimation?.Durations!![mFrame]) { nextAnimationFrame() }
    }

    private fun setTextureCoordinates(gid: Int) {
        if (mTileset == null)
            return
        val id = gid - mTileset!!.FirstGid
        val cols = mTileset?.WidthInTiles
        val rows = mTileset?.HeightInTiles
        val x = (id % cols!!).toFloat() / cols.toFloat()
        val y = (id / cols).toFloat() / rows!!.toFloat()
        val w = mTileset?.WidthOfTileInPixels!!.toFloat() / mTileset?.WidthInPixels!!.toFloat()
        val h = mTileset?.HeightOfTileInPixels!!.toFloat() / mTileset?.HeightInPixels!!.toFloat()
        mSprite.setTextureCoordinates(x, y, w, h)
    }

    private fun calculateSizeAndPosition() {
        val w = mWidthOfTile
        val h = mHeightOfTile
        var x = 0f
        var y = 0f
        if (mRenderOrder == "right-down") {
            x = w * (mSceneIndex % mWidthInTiles)
            y = h * (mHeightInTiles - (mSceneIndex / mWidthInTiles) - 1)
        }
        mSprite.move(x, y)
        mSprite.size(w, h)
        mHighlightQuad?.move(x, y)
        mHighlightQuad?.size(w, h)
    }
}