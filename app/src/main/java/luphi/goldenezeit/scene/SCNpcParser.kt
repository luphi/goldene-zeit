package luphi.goldenezeit.scene

import android.util.Log
import luphi.goldenezeit.gGZ
import org.xml.sax.Attributes
import org.xml.sax.Locator
import org.xml.sax.helpers.DefaultHandler
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.util.LinkedList

@Suppress("PropertyName")
class SCNpcParser(file: String) : DefaultHandler() {
    private val mFile = file
    private var mLocator: Locator? = null
    private var mCurrentTag = ""
    private var mID: String? = null
    private var mName: String? = null
    private var mTexture: String? = null
    private val mCutscenes = LinkedList<SCCutscene>()
    private var mCardMessage: String? = null
    private val mCards = HashSet<Int>()
    private var mCutsceneStartLine = 0

    val NPC: SCNpc
        get() {
            val npc = SCNpc(mID, mName, mCutscenes, mCards)
            npc.init(mTexture ?: "")
            if (mCardMessage != null)
                npc.CardMessage = mCardMessage!!
            return npc
        }

    override fun setDocumentLocator(locator: Locator?) {
        mLocator = locator
    }

    override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
        mCurrentTag = localName
        if (localName == "cutscene")
            mCutsceneStartLine = mLocator?.lineNumber ?: 0
    }

    override fun endElement(namespaceURI: String, localName: String, qName: String) {
        mCurrentTag = ""
        if (localName == "cutscene") {
            val cutsceneEndLine = mLocator?.lineNumber ?: 0
            if ((mCutsceneStartLine > 0) && (cutsceneEndLine > mCutsceneStartLine)) {
                val assetManager = gGZ.Context?.assets
                if (assetManager != null) {
                    try {
                        val reader = BufferedReader(InputStreamReader(assetManager.open(mFile)))
                        // Read the lines starting with mCutsceneStartLine (inclusive) and ending with cutsceneEndLine (inclusive)
                        // Note: subList()'s indices begin at 0 but the parser reports line numbers beginning at 1
                        // Note: subList()'s end index (the second parameter) is exclusive
                        val lines = reader.readLines().subList(mCutsceneStartLine - 1, cutsceneEndLine)
                        // Parse the cutscene from a single string containing the XML content (the whole <cutscene> node)
                        val cutscene = gGZ.Resources.loadCutscene(lines.joinToString(separator = "", transform = { l -> l.trim()}).trim(), /* isFile */ false)
                        if (cutscene != null) {
                            for (act in cutscene.Acts) {
                                for (event in act) {
                                    if (event.Op == SCEvent.Opcode.SPEECH)
                                        event.ID = mID
                                }
                            }
                            mCutscenes.add(cutscene)
                        }
                    }
                    catch (e: Exception) {
                        Log.e("SCNpcParser", "Cutscene parsing error", e)
                    }
                }
            }
            mCutsceneStartLine = 0
        }
    }

    override fun characters(ch: CharArray, start: Int, length: Int) {
        if (mCurrentTag.isEmpty())
            return
        val string = String(ch, start, length)
        when (mCurrentTag) {
            "cardmessage" -> mCardMessage = string
            "cards" -> mCards.addAll(string.split(",").map { i -> i.toInt() }) // e.g. "1,2,3" (string) -> {"1", "2", "3"} (strings) -> {1, 2, 3} (ints)
            "id" -> mID = string
            "name" -> mName = string
            "spritesheet" -> mTexture = string
        }
    }
}