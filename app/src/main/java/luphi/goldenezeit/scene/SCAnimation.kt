package luphi.goldenezeit.scene

import java.util.LinkedList

@Suppress("PropertyName")
class SCAnimation(var Gid: Int) {
    var Frames = LinkedList<Int>()
    var Durations = LinkedList<Long>()

    fun addFrame(gid: Int, duration: Long) {
        Frames.add(gid)
        Durations.add(duration)
    }

    override fun toString(): String = "{ Frames = (${Frames.joinToString()}), Durations = (${Durations.joinToString()}) }"
}