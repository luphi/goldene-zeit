package luphi.goldenezeit.scene

import android.annotation.SuppressLint
import android.util.Log
import luphi.goldenezeit.*
import luphi.goldenezeit.scene.SCEvent.Opcode
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.gl.GLSprite
import luphi.goldenezeit.ui.UIDialogue
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.ui.UIMenu
import luphi.goldenezeit.ui.UIMenuEntry
import luphi.goldenezeit.ui.UITitleScreen
import java.util.LinkedList
import java.util.Locale
import java.util.concurrent.locks.ReentrantLock
import kotlin.collections.HashMap
import kotlin.concurrent.withLock

@Suppress("PropertyName", "SimplifyNegatedBinaryExpression")
class SCCutscene : IDrawable, IProgressive, IInteractable {
    override val Rect: Rect
        get() = Rect(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge, gGZ.Camera.Width, gGZ.Camera.Height)

    @SuppressLint("UseSparseArrays")
    private val mActMap = HashMap<Int, Boolean>()
    private val mScta = LinkedList<List<SCEvent>>() // Queue of past acts ("Acts" reversed = "Stca")
    private val mTasks = Tasks()
    private val mNPCs = HashMap<String, SCNpc>() // ID -> NPC
    private val mNPCLock = ReentrantLock()
    private var mCallback: ((String?) -> Unit)? = null // To be called when the cutscene is complete
    private val mDrawables = LinkedList<IDrawable>() // Frequently-modified list of things to be drawn
    private val mDrawablesLock = ReentrantLock()
    private var mInteractable: IInteractable? = null
    private var mBranch: SCCutscene? = null // A branch/child cutscene that pauses the playing of this one until it completes
    private var mShouldAutosave = false // A basic flag indicating, when true, that this cutscene was important enough to autosave
    private var mNewSceneID: String? = null // When set, indicates the new scene (ID) to be loaded by the overworld mode

    var ID: String? = null
    var MinVerse = Int.MIN_VALUE // The lowest verse in which this cutscene can play
    var MaxVerse = Int.MAX_VALUE // The highest verse in which this cutscene can play
    var Once = false // True if the cutscene should play only once
    var Acts = LinkedList<List<SCEvent>>()
    var Scene: Scene? = null // The scene in which this cutscene happens
    var PC: SCCharacter? = null // The player character
    val Branches = HashMap<String, SCCutscene?>()

    override fun draw(dt: Long) {
        mNPCLock.withLock {
            for (pair in mNPCs) {
                // If the NPC isn't also held by the scene (i.e. if the scene isn't already drawing this NPC)
                if (Scene?.getNpcByID(pair.key) == null)
                    pair.value.draw(dt)
            }
        }
        mDrawablesLock.withLock {
            for (drawable in mDrawables)
                drawable.draw(dt)
        }
        mBranch?.draw(dt)
    }

    override fun step(dt: Long) {
        if (mBranch != null)
            mBranch?.step(dt)
        else {
            mTasks.step(dt)
            // The values are cloned for iteration due the possibility of an NPC's step() modifying the list (via walkToTile()'s onDone event)
            for (npc in mNPCs.values.toMutableList())
                npc.step(dt)
            PC?.step(dt)
        }
    }

    override fun handleInput(input: Input) {
        if (mBranch != null)
            mBranch?.handleInput(input)
        else
            mInteractable?.handleInput(input)
    }

    fun play(callback: (String?) -> Unit) {
        mCallback = callback
        onActComplete()
    }

    override fun toString(): String = "{ ID \"$ID\", MinVerse $MinVerse, MaxVerse $MaxVerse, Once $Once, # Acts ${Acts.size}, # Branches ${Branches.size} }"

    private fun onEventComplete(id: Int) {
        mActMap[id] = true
        var areAllDone = true
        for (isDone in mActMap.values) {
            if (!isDone) {
                areAllDone = false
                break
            }
        }
        if (areAllDone)
            onActComplete()
    }

    private fun onActComplete() {
        mActMap.clear()
        // If there are no more acts (meaning the cutscene is done)
        if (Acts.isEmpty()) {
            // Rewind
            while (!mScta.isEmpty())
                Acts.add(mScta.remove())
            // Make some local copies of a couple things before resetting them in preparation for a possible replay
            val shouldAutoSave = mShouldAutosave
            val newSceneID = mNewSceneID
            mShouldAutosave = false
            mNewSceneID = null
            mTasks.clear()
            // Mark any tile with an NPC as walkable
            // Clean up after the NPCs
            for (pair in mNPCs) {
                val sceneNpc = Scene?.getNpcByID(pair.key)
                // If this NPC was already in the scene, unpause it
                if (sceneNpc != null)
                    sceneNpc.IsPaused = false
                // If this NPC was loaded as part of the cutscene and will be removed, set its tile as walkable
                else
                    Scene?.setTileWalkableAt(pair.value.X, pair.value.Y, true)
            }
            // Clear the NPC map. Any NPCs originally in the scene will still be referenced by the scene's map.
            mNPCLock.withLock { mNPCs.clear() }
            // If this cutscene was an important one
            if (shouldAutoSave)
                gGZ.Record.autosave()
            mCallback?.invoke(newSceneID)
            mCallback = null
        } else {
            val act = Acts.remove()
            // Add the act to the sctA stack so it can be rewound later, if needed
            mScta.add(LinkedList(act))
            // Launch a task for each event in the act along with an ID for the event
            for ((id, event) in act.withIndex()) {
                mActMap[id] = false
                launchEvent(event, id)
            }
        }
    }

    private fun launchEvent(event: SCEvent, id: Int) {
        when (event.Op) {
            Opcode.ANIMATE -> {
                if (event.ID != null)
                    getNpc(event.ID!!)?.IsAnimating = event.Bool ?: true
                else
                    PC?.IsAnimating = event.Bool ?: true
                onEventComplete(id)
            }
            Opcode.BRANCH -> {
                val lines = (event.String ?: "").lines() // The two components of conditions are compressed into a single string by joining them with a newline
                if ((lines.size == 2) && branchCondition(lines[0], lines[1]) && Branches.containsKey(event.String ?: "")) {
                    mBranch = Branches[event.String ?: ""]
                    mBranch?.Scene = Scene
                    mBranch?.PC = PC
                    mBranch?.play {
                        newSceneID -> // onDone
                        mBranch = null
                        onEventComplete(id)
                        mNewSceneID = newSceneID
                    }
                } else
                    onEventComplete(id)
            }
            Opcode.EXCLAMATION, Opcode.QUESTION, Opcode.ELLIPSIS -> {
                val bubble = GLSprite()
                when (event.Op) {
                    Opcode.EXCLAMATION -> bubble.init(gGZ.Resources.loadTexture("sprites/cutscene/exclamation.png"))
                    Opcode.QUESTION -> bubble.init(gGZ.Resources.loadTexture("sprites/cutscene/question.png"))
                    Opcode.ELLIPSIS -> bubble.init(gGZ.Resources.loadTexture("sprites/cutscene/ellipsis.png"))
                    else -> {
                    }
                }
                val character = if (event.ID != null) getNpc(event.ID!!) else PC!! // Select an NPC or the player character if no ID was given
                val tileRect = Scene!!.getDummyTileAt(character!!.X, character.Y - 1).Rect // Get the NDC position and dimensions of the tile above
                bubble.move(tileRect.X, tileRect.Y + (tileRect.Height / 4f)) // Place the bubble in the tile with a quarter offset to match the character
                bubble.size(tileRect) // Copy the dimensions of the tile
                mDrawablesLock.withLock { mDrawables.add(bubble) }
                mTasks.scheduleOneOff(event.Int?.toLong() ?: 0L) {
                    // After event.Int milliseconds, remove the bubble and complete the event
                    mDrawablesLock.withLock { mDrawables.remove(bubble) }
                    onEventComplete(id)
                }
            }
            Opcode.PLACE -> {
                if (event.ID != null) {
                    val character = getNpc(event.ID!!)
                    if (character != null) {
                        character.Scene = Scene
                        character.placeOnTile(event.X ?: -1, event.Y ?: -1)
                    }
                } else
                    PC?.placeOnTile(event.X ?: -1, event.Y ?: -1)
                onEventComplete(id)
            }
            Opcode.HEAL -> {
                gGZ.Player.HP = gGZ.Player.MaxHP
                onEventComplete(id)
            }
            Opcode.INC_CHAPTER -> {
                gGZ.Chapter += 1
                gGZ.Verse = (gGZ.Chapter - 1) * 1000 // e.g. Chapter 2 -> Verse 1000
                mShouldAutosave = true
                onEventComplete(id)
            }
            Opcode.INC_VERSE -> {
                gGZ.Verse += 1
                mShouldAutosave = true
                onEventComplete(id)
            }
            Opcode.LOOK -> {
                if (event.ID != null) {
                    onEventComplete(id)
                    val character = getNpc(event.ID!!)
                    if (character != null)
                        gGZ.Camera.lookAt(character.Rect)
                } else if ((event.X != null) && (event.Y != null) && (Scene != null))
                    gGZ.Camera.lookAt(Scene!!.getDummyTileAt(event.X!!, event.Y!!).Rect)
                else if (PC != null)
                    gGZ.Camera.lookAt(PC!!.Rect)
                onEventComplete(id)
            }
            Opcode.PAN -> {
                var panToX = gGZ.Camera.LookAtX
                var panToY = gGZ.Camera.LookAtY
                // If panning to an NPC
                if (event.ID != null) {
                    val characterRect = getNpc(event.ID!!)?.Rect
                    if (characterRect != null) {
                        panToX = characterRect.X + (characterRect.Width / 2f)
                        panToY = characterRect.Y + (characterRect.Height / 2f)
                    }
                }
                // If panning to a tile
                else if ((event.X != null) && (event.Y != null)) {
                    val tileRect = Scene?.getDummyTileAt(event.X!!, event.Y!!)?.Rect
                    if (tileRect != null) {
                        panToX = tileRect.X + tileRect.Width / 2f
                        panToY = tileRect.Y + tileRect.Height / 2f
                    }
                }
                // If panning to the player character
                else {
                    panToX = (PC?.Rect?.X ?: 0f) + ((PC?.Rect?.Width ?: 0f) / 2f)
                    panToY = (PC?.Rect?.Y ?: 0f) + ((PC?.Rect?.Height ?: 0f) / 2f)
                }
                // If something went wrong and panning would result in an infinite loop
                if (((gGZ.Camera.LookAtX - panToX) == 0f) && (gGZ.Camera.LookAtY - panToY == 0f))
                    onEventComplete(id)
                // If panning is a go
                else {
                    gGZ.Camera.panTo(panToX, panToY, event.Int!!) // The integer value is the duration in milliseconds and is always set by the parser
                    mTasks.scheduleOneOff(event.Int!!.toLong()) { onEventComplete(id) }
                }
            }
            Opcode.REMOVE -> {
                if (event.ID != null) {
                    val npc = mNPCs[event.ID!!]
                    Scene?.setTileWalkableAt(npc?.X ?: -1, npc?.Y ?: -1, true) // The tile must become walkable whether the NPC was present in the scene or not
                    Scene?.removeNpcByID(event.ID!!) // If the NPC was not originally present in the scene, this will return immediately
                    mNPCLock.withLock { mNPCs.remove(event.ID!!) }
                }
                onEventComplete(id)
            }
            Opcode.SCENE -> {
                mNewSceneID = event.String
                onEventComplete(id)
            }
            Opcode.SLEEP -> mTasks.scheduleOneOff(event.Int?.toLong() ?: 1000L) { onEventComplete(id) }
            Opcode.SPEECH, Opcode.THOUGHT -> {
                var dialogue: UIDialogue? = null
                dialogue = UIDialogue {
                    // onDone
                    if (event.Branches.isNotEmpty()) {
                        val menu = UIMenu { }
                        menu.init()
                        for (branch in event.Branches)
                            menu.add(UIMenuEntry(branch.toUpperCase(Locale.getDefault()), {
                                // onSelect
                                mDrawablesLock.withLock {
                                    mDrawables.remove(dialogue as UIDialogue)
                                    mDrawables.remove(menu)
                                }
                                mInteractable = dialogue
                                mBranch = Branches[branch]
                                mBranch?.Scene = Scene
                                mBranch?.PC = PC
                                mBranch?.play {
                                    // onDone
                                    mBranch = null
                                    onEventComplete(id)
                                }
                            }))
                        menu.autoSize()
                        val dialogueRect = dialogue!!.Rect
                        menu.move(gGZ.Camera.RightEdgeWithMargin - menu.Rect.Width, dialogueRect.Y + dialogueRect.Height + gGZ.Camera.Margin)
                        mDrawablesLock.withLock { mDrawables.add(menu) }
                        mInteractable = menu
                    } else {
                        mDrawablesLock.withLock { mDrawables.remove(dialogue as UIDialogue) }
                        mInteractable = null
                        onEventComplete(id)
                    }
                }
                // A non-empty ID means an NPC (of the given ID) is speaking, an empty but non-null ID means the PC is speaking, and a null ID means that no
                // name should be shown
                val name = if (event.ID?.isNotEmpty() == true) getNpc(event.ID!!)?.Name else PC?.Name
                val message = if (event.Op == Opcode.SPEECH) "\"${event.String ?: ""}\"" else /* event.Op == Event.Opcode.THOUGHT */ "(${event.String ?: ""})"
                val text = if (name != null) "$name\n$message" else message
                dialogue.init(text)
                mDrawablesLock.withLock { mDrawables.add(dialogue) }
                mInteractable = dialogue
            }
            Opcode.TITLE -> {
                var titleScreen: UITitleScreen? = null
                titleScreen = UITitleScreen {
                    // onDone
                    mDrawablesLock.withLock { mDrawables.remove(titleScreen as UITitleScreen) }
                    mInteractable = null
                    onEventComplete(id)
                }
                titleScreen.init()
                mDrawablesLock.withLock { mDrawables.add(titleScreen) }
                mInteractable = titleScreen
            }
            Opcode.TURN -> {
                if (event.ID != null)
                    getNpc(event.ID!!)?.Direction = event.Direction ?: GZ.Cardinal.SOUTH
                else
                    PC?.Direction = event.Direction ?: GZ.Cardinal.SOUTH
                onEventComplete(id)
            }
            Opcode.WALK -> {
                val character = if (event.ID != null) getNpc(event.ID!!) else PC
                if (character != null)
                    character.walkToTile(event.X ?: -1, event.Y ?: -1, /* shouldCameraFollow*/ event.Bool ?: character === PC) {
                        /* onDone */ _, _ ->
                        if (character === PC) {
                            gGZ.Player.X = PC?.X ?: 0
                            gGZ.Player.Y = PC?.Y ?: 0
                        }
                        onEventComplete(id)
                    }
                else
                    onEventComplete(id)
            }
        }
    }

    private fun getNpc(id: String): SCNpc? {
        var npc: SCNpc?
        val lowercaseID = id.toLowerCase(Locale.getDefault())
        // First, check the list of already loaded NPCs
        if (mNPCs.containsKey(lowercaseID))
            return mNPCs[lowercaseID]
        // Second, check the scene for the NPC
        npc = Scene?.getNpcByID(lowercaseID)
        // Finally, try to load the NPC (it may even be cached)
        if (npc == null)
            npc = gGZ.Resources.loadNpc(gGZ.Resources.NpcCatalog.get(id, gGZ.Verse))
        if (npc != null)
            mNPCLock.withLock { mNPCs[lowercaseID] = npc }
        npc?.IsPaused = true // Prevent the NPC from acting in any way that is not related to this cutscene (until the cutscene is done)
        return npc // Could theoretically be null
    }

    // Parses the list of conditions and values returning false if any are unmet. If true is returned, all conditions are met and the cutscene can proceed.
    private fun branchCondition(compressedConditions: String, compressedValues: String): Boolean {
        val conditions = compressedConditions.split(",")
        val values = compressedValues.split(",")
        if (conditions.size != values.size)
            return false
        for (i in conditions.indices) {
            when (conditions[i]) {
                // expected value: a non-negative integer
                "verseGreaterThan" -> if (!(gGZ.Verse > values[i].toInt())) return false
                // expected value: a non-negative integer
                "verseLessThan" -> if (!(gGZ.Verse < values[i].toInt())) return false
                // expected value: a non-negative integer
                "levelGreaterThan" -> if (!(gGZ.Player.Level > values[i].toInt())) return false
                // expected value: a non-negative integer
                "levelLessThan" -> if (!(gGZ.Player.Level < values[i].toInt())) return false
                // expected value: a non-negative integer
                "cashGreaterThan" -> if (!(gGZ.Player.Cash > values[i].toInt())) return false
                // expected value: a non-negative integer
                "cashLessThan" -> if (!(gGZ.Player.Cash < values[i].toInt())) return false
                // expected value: an item ID as an integer
                "hasItem" -> if (!gGZ.Player.Inventory.containsKey(values[i].toInt())) return false
                // expected value: "true" or "false"
                "hasNobleCrest" -> if (gGZ.Player.Inventory.containsKey(41) != values[i].toBoolean()) return false
                // expected value: "true" or "false"
                "hasLockPick" -> if (gGZ.Player.Inventory.containsKey(42) != values[i].toBoolean()) return false
                // expected value: "a card ID as an integer
                "hasCard" -> if (!gGZ.Player.Cards.containsKey(values[i].toInt())) return false
                "canSwim" -> {
                    // TODO
                }
                else -> {
                    Log.w("SCCutscene", "The unknown branch condition \"${conditions[i]}\" cannot be evaluated")
                    return false
                }
            }
        }
        return true
    }
}