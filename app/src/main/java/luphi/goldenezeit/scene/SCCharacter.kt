package luphi.goldenezeit.scene

import android.util.Log
import luphi.goldenezeit.*
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gl.GLPerson
import luphi.goldenezeit.gl.GLTexture
import java.util.LinkedList
import kotlin.math.atan2
import kotlin.math.hypot
import kotlin.math.sign

@Suppress("PropertyName", "PrivatePropertyName")
open class SCCharacter : IDrawable, IProgressive {
    override val Rect: Rect
        get() = mSprite.Rect

    private val TILES_PER_SECOND = 3f // Glide velocity
    private val mSprite = GLPerson()
    private val mTasks = Tasks()

    var X = -1 // Current (tile) X coordinate
    var Y = -1 // Current (tile) Y coordinate
    var Scene: Scene? = null
    var Spritesheet = ""
        set(value) {
            if (field != value) {
                field = value
                mSprite.Texture = gGZ.Resources.loadTexture(value)
            }
        }
    var Name: String? = null
    var IsGliding = false
        private set

    // The north, east, south, west direction the person is facing
    var Direction = GZ.Cardinal.SOUTH
        set(direction) {
            if (field == direction)
                return
            field = direction
            mSprite.Direction = direction
        }
    var IsAnimating = false
        set(value) {
            field = value
            mSprite.walk(value)
        }

    init {
        Direction = GZ.Cardinal.SOUTH
    }

    fun init(file: String) = mSprite.init(gGZ.Resources.loadTexture(file))

    override fun draw(dt: Long) = mSprite.draw(dt)

    override fun step(dt: Long) = mTasks.step(dt)

    fun move(x: Float, y: Float) = mSprite.move(x, y)

    fun size(w: Float, h: Float) = mSprite.size(w, h)

    fun placeOnTile(tile: Pair<Int, Int>) = placeOnTile(tile.first, tile.second)

    fun placeOnTile(x: Int, y: Int) {
        if (Scene == null)
            return
        val dummyTile: SCTile = Scene!!.getDummyTileAt(x, y)
        mSprite.move(dummyTile.Rect.X, dummyTile.Rect.Y + dummyTile.Rect.Height / 4f)
        mSprite.size(dummyTile.Rect.Width, dummyTile.Rect.Height)
        Scene?.setTileWalkableAt(X, Y, true) // Make the old tile walkable
        Scene?.setTileWalkableAt(x, y, false) // Make the new tile non-walkable
        X = x
        Y = y
    }

    fun stand() {
        if (!IsGliding)
            return
        mTasks.clear()
        IsGliding = false
        placeOnTile(X, Y) // In case the character is off-center, reposition them to the current coordinates
    }

    fun glide(direction: GZ.Cardinal, shouldCameraFollow: Boolean, onBlocked: () -> Unit, onNewTile: (Int, Int) -> Unit) {
        if (IsGliding)
            return
        val dX = if (direction == GZ.Cardinal.WEST) -1 else if (direction == GZ.Cardinal.EAST) 1 else 0
        val dY = if (direction == GZ.Cardinal.NORTH) -1 else if (direction == GZ.Cardinal.SOUTH) 1 else 0
        var toX = X + dX
        var toY = Y + dY
        // If the scene is unknown or the tile in the given direction isn't walkable
        if ((Scene == null) || !Scene!!.isTileWalkableAt(toX, toY)) {
            stand() // Stop any current gliding
            onBlocked.invoke()
            return
        }
        Scene!!.setTileWalkableAt(toX, toY, false) // Mark the new tile as non-walkable
        Direction = direction // Also changes to the appropriate frame in the spritesheet
        IsGliding = true
        // Schedule a periodic callback to perform the gliding
        val frequency = 75f // Hz
        var prevSpriteRect = mSprite.Rect
        mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            val fromX = X
            val fromY = Y
            val toTile = Scene!!.getDummyTileAt(toX, toY) // The tile the character is gliding to
            val toTileRect = toTile.Rect // Position (and dimensions) of the next tile
            mSprite.translate(
                Scene!!.WidthOfTile * TILES_PER_SECOND / frequency * sign(dX.toDouble()).toFloat(),
                Scene!!.HeightOfTile * TILES_PER_SECOND / frequency * sign(-dY.toDouble()).toFloat()
            )
            val newSpriteRect = mSprite.Rect
            if (shouldCameraFollow)
                gGZ.Camera.lookAt(this)
            // If the character has now completely slid onto a new tile
            // (For example, if gliding east and the sprite's X coordinate is larger than the next tile's, it's on a new tile)
            // (The "Height / 4" additions are due to characters being offset from their tiles by a quarter of the tile's height)
            if ((sign(toTileRect.X - prevSpriteRect.X) != sign(toTileRect.X - newSpriteRect.X)) ||
                (sign(toTileRect.Y + (toTileRect.Height / 4f) - prevSpriteRect.Y) != sign(toTileRect.Y + (toTileRect.Height / 4f) - newSpriteRect.Y))
            ) {
                Scene?.setTileWalkableAt(fromX, fromY, true) // Mark the previous tile as walkable
                X = toX
                Y = toY
                // Determine the next tile
                toX += dX
                toY += dY
                // If the next tile is blocked, a wall, etc. and gliding must stop
                if (!Scene!!.isTileWalkableAt(toX, toY))
                    stand() // Stop gliding
                onNewTile.invoke(X, Y) // Let the caller decide what to do; continue, or stop here by calling stand()
            }
            prevSpriteRect = mSprite.Rect
        }
    }

    fun walkToTile(toX: Int, toY: Int, shouldCameraFollow: Boolean, onDone: (Int, Int) -> Unit) {
        if ((Scene == null) || ((toX == X) && (toY == Y))) {
            onDone.invoke(X, Y)
            return
        }
        stand() // Stop any current gliding
        val tilesToGlide = LinkedList<SCTile>() // Queue of 1+ tiles to attempt to walk on
        // If the tile is just one tile north, south, east, or west
        if (hypot((X - toX).toDouble(), (Y - toY).toDouble()) == 1.0)
            tilesToGlide.add(Scene!!.getDummyTileAt(toX, toY))
        // If the tile requires pathfinding to determine the series of tiles to walk on
        else {
            val list: List<SCTile>? = Scene?.getPath(X, Y, toX, toY) // From (X, Y) to (x, y)
            // If a path exists
            if (list != null) {
                tilesToGlide.addAll(list)
                // The pathfinding done by the scene to give us a path will include the tile the character is currently on (as pathfinding should) but we can't
                // glide to a tile we're already on, therefore it should be removed
                tilesToGlide.remove()
            }
            // If a path wasn't found
            else {
                Log.w("SCCharacter", "Character $Name could not find a path from ($X, $Y) to ($toX, $toY)")
                onDone.invoke(X, Y)
                return
            }
        }
        var nextTile = tilesToGlide.poll()
        var directionToNextTile = directionToTileAt(nextTile!!.X, nextTile.Y)
        // This is a helper function to do a couple ot her things in addition to calling the given "on done" callback
        val onDoneInternal: (Int, Int) -> Unit = { x, y ->
            stand() // Stop moving and center the character on the tile
            IsAnimating = false
            if (shouldCameraFollow)
                gGZ.Camera.lookAt(this) // Look at the character, now in its final location
            onDone(x, y) // Call the "when gliding is done" callback with coordinates
        }
        // Another helper function, to be called in the event that path is somehow blocked
        val onBlocked = {
            Log.w("SCCharacter", "Character $Name's path was blocked before reaching the goal")
            onDoneInternal(X, Y)
        }
        // The final helper function, to be called on each new tile to continue walking or finish if the goal is reached
        var onNewTileInternal: (Int, Int) -> Unit = { _, _ -> }
        onNewTileInternal = { x, y ->
            // If the goal has been reached
            if ((x == toX) && (y == toY))
                onDoneInternal(x, y)
            // If walking should continue
            else {
                nextTile = if (tilesToGlide.isNotEmpty()) tilesToGlide.poll() else null
                if (nextTile != null) {
                    // Gliding continues in the same direction until stand() is called so...
                    val currentDirection = directionToNextTile
                    directionToNextTile = directionToTileAt(nextTile!!.X, nextTile!!.Y)
                    // If not current gliding in the direction of the next tile
                    if (directionToNextTile != currentDirection) {
                        stand()
                        glide(directionToNextTile, shouldCameraFollow, onBlocked, onNewTileInternal)
                    }
                    // else { gliding will continue and this function will be called again }
                }
                // If something went wrong
                else {
                    Log.w("SCCharacter", "Character $Name's path was emptied before reaching the goal")
                    onDoneInternal(X, Y)
                }
            }
        }
        IsAnimating = true
        glide(directionToNextTile, shouldCameraFollow, onBlocked, onNewTileInternal)
    }

    override fun toString(): String = "{ Name = \"$Name\", (X, Y) = ($X, $Y), Direction = $Direction, Sprite = $mSprite }"

    private fun directionToTileAt(x: Int, y: Int): GZ.Cardinal {
        return when (Math.toDegrees(atan2((y - Y).toDouble(), (x - X).toDouble())).toInt()) {
            -90 -> GZ.Cardinal.NORTH
            0 -> GZ.Cardinal.EAST
            90 -> GZ.Cardinal.SOUTH
            else -> GZ.Cardinal.WEST
        }
    }
}