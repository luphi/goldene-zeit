package luphi.goldenezeit.scene

import luphi.goldenezeit.GZ

@Suppress("PropertyName")
class SCDoor(val X: Int, val Y: Int) {
    var ID = 0
    var ToScene = ""
    var ToDoor = 0
    var Direction: GZ.Cardinal? = null

    override fun toString(): String = "{ ($X, $Y), ID = $ID, ToScene = \"$ToScene\", ToDoor = \"$ToDoor\", Direction = $Direction }"
}