package luphi.goldenezeit

import android.annotation.SuppressLint
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock.uptimeMillis
import android.view.MotionEvent
import android.view.MotionEvent.ACTION_UP
import android.view.MotionEvent.ACTION_DOWN
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import luphi.goldenezeit.mode.*

@Suppress("PrivatePropertyName", "NON_EXHAUSTIVE_WHEN")
class MainActivity : AppCompatActivity() {
    private val GAME_LOOP_FREQUENCY = 100 // Hz
    private var mIsAlive = true
    private var mPreviousTicks = uptimeMillis()
    private val mTicksPerLoop = 1000 / GAME_LOOP_FREQUENCY

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gGZ.Context = this
        gGZ.Record.loadPrincipal()
        setContentView(R.layout.activity_main)
        findViewById<LinearLayout>(R.id.root_layout).setBackgroundColor(Color.parseColor(gGZ.Record.getDirect("ui_color", "#930000"))) // Set the app color
        gGZ.Mode = MMainMenu()
        a_button.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                ACTION_DOWN -> gGZ.InputQueue.add(Input(Input.Button.A, true))
                ACTION_UP -> gGZ.InputQueue.add(Input(Input.Button.A, false))
            }
            return@OnTouchListener true
        })
        b_button.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                ACTION_DOWN -> gGZ.InputQueue.add(Input(Input.Button.B, true))
                ACTION_UP -> gGZ.InputQueue.add(Input(Input.Button.B, false))
            }
            return@OnTouchListener true
        })
        select_button.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                ACTION_DOWN -> gGZ.InputQueue.add(Input(Input.Button.SELECT, true))
                ACTION_UP -> gGZ.InputQueue.add(Input(Input.Button.SELECT, false))
            }
            return@OnTouchListener true
        })
        start_button.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                ACTION_DOWN -> gGZ.InputQueue.add(Input(Input.Button.START, true))
                ACTION_UP -> gGZ.InputQueue.add(Input(Input.Button.START, false))
            }
            return@OnTouchListener true
        })
        d_pad_up.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                ACTION_DOWN -> gGZ.InputQueue.add(Input(Input.Button.DPAD_UP, true))
                ACTION_UP -> gGZ.InputQueue.add(Input(Input.Button.DPAD_UP, false))
            }
            return@OnTouchListener true
        })
        d_pad_right.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                ACTION_DOWN -> gGZ.InputQueue.add(Input(Input.Button.DPAD_RIGHT, true))
                ACTION_UP -> gGZ.InputQueue.add(Input(Input.Button.DPAD_RIGHT, false))
            }
            return@OnTouchListener true
        })
        d_pad_down.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                ACTION_DOWN -> gGZ.InputQueue.add(Input(Input.Button.DPAD_DOWN, true))
                ACTION_UP -> gGZ.InputQueue.add(Input(Input.Button.DPAD_DOWN, false))
            }
            return@OnTouchListener true
        })
        d_pad_left.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                ACTION_DOWN -> gGZ.InputQueue.add(Input(Input.Button.DPAD_LEFT, true))
                ACTION_UP -> gGZ.InputQueue.add(Input(Input.Button.DPAD_LEFT, false))
            }
            return@OnTouchListener true
        })
        gGZ.IsActivityReady = true
        if (gGZ.IsActivityReady && gGZ.IsRendererReady && gGZ.IsViewReady)
            gGZ.Mode?.init(MTransition(MTransition.Mode.MENU))
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            ACTION_DOWN -> gGZ.InputQueue.add(Input(Input.Button.NO_OP, true))
            ACTION_UP -> gGZ.InputQueue.add(Input(Input.Button.NO_OP, false))
        }
        return false
    }

    override fun onPause() {
        super.onPause()
        mIsAlive = false
        gGZ.Renderer?.IsPaused = true
        if (gGZ.Mode is MOverworld)
            gGZ.Record.autosave() // Because Android, especially on some devices, really likes to kill background processes
    }

    override fun onResume() {
        super.onResume()
        mIsAlive = true
        GlobalScope.launch { step() }
        gGZ.Renderer?.IsPaused = false
    }

    private fun step() {
        while (mIsAlive) {
            val dt = uptimeMillis() - mPreviousTicks // Determine the difference in ticks (milliseconds) since the last loop
            mPreviousTicks += dt // Store the current tick value for reference next loop
            var input: Input? = gGZ.InputQueue.poll()
            while (input != null) {
                gGZ.Mode?.handleInput(input)
                input = gGZ.InputQueue.poll()
            }
            gGZ.Mode?.step(dt)
            if (gGZ.Mode?.Transition != null) {
                val transition = gGZ.Mode?.Transition ?: MTransition(MTransition.Mode.MENU)
                gGZ.Renderer?.IsPaused = true
                // TODO consider removing the following line or preventing it from clearing the global scene (needs to be retained when returning to MOverworld)
                //gGZ.Resources.free()
                // When transitioning between overworld, battle, and card modes, we want to retain the scene because the latter two will return to the former
                // but dropping it is best for the main menu and debug modes
                when (transition.To) {
                    MTransition.Mode.MENU, MTransition.Mode.DEBUG -> gGZ.Scene = null
                }
                when (transition.To) {
                    MTransition.Mode.BATTLE -> gGZ.Mode = MBattle()
                    MTransition.Mode.CARD -> gGZ.Mode = MCard()
                    MTransition.Mode.MENU -> gGZ.Mode = MMainMenu()
                    MTransition.Mode.OVERWORLD -> gGZ.Mode = MOverworld()
                    MTransition.Mode.DEBUG -> gGZ.Mode = MDebug()
                }
                gGZ.Mode?.init(transition)
                gGZ.Renderer?.IsPaused = false
                mPreviousTicks = uptimeMillis()
            }
            // If the loop completed with time to spare, sleep
            else if (dt < mTicksPerLoop)
                Thread.sleep(mTicksPerLoop - dt)
        }
    }
}
