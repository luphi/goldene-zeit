package luphi.goldenezeit

import android.content.Context
import android.util.Log
import luphi.goldenezeit.item.ITAccessory
import luphi.goldenezeit.item.ITArmor
import luphi.goldenezeit.item.ITWeapon
import java.util.LinkedList
import kotlin.collections.HashSet

@Suppress("PropertyName", "PrivatePropertyName")
class Record {
    private val PREFERENCES_NAME = "goldenezeit"
    private var mMap = HashMap<String, String>()

    var IsAutosaveEnabled = true
    var LoadedSlot: Int? = null
    var MostRecentSlot: Int? = null
    val SlotsOnDisk = HashSet<Int>()

    // This doesn't *completely* clear what's on disk. (To do that, use the system's disk utilities.) This will remove all records on disk that are not
    // considered principal entries; that is to say preferences like UI color and the entry detailing which slots are available. The saved game slots themselves
    // are removed, including autosave. This function is intended solely for development purposes.
    fun reset() {
        // Copy some application/game preferences that should survive this near-wipe
        val principalEntries = extractPrincipal()
        principalEntries["slots"] = ";"
        LoadedSlot = null
        MostRecentSlot = null
        SlotsOnDisk.clear()
        mMap.clear()
        mMap.putAll(principalEntries)
        val editor = gGZ.Context?.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)?.edit()
        if (editor == null) {
            Log.w("Record", "Failed to get a preferences editor during reset - nothing on disk was modified")
            return
        }
        editor.clear()
        for ((key, value) in mMap)
            editor.putString(key, value)
        editor.apply()
    }

    // Read just the important things saved on disk and skip any saved game slots for now
    fun loadPrincipal() {
        val prefs = gGZ.Context?.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        if (prefs == null) {
            Log.w("Record", "Failed to get a preferences editor during principal load - nothing was read from disk")
            return
        }
        // The principal keys are 'slots', 'animations', 'card_difficulty', 'developer_options', and 'ui_color'
        mMap["slots"] = prefs.getString("slots", ";") ?: ";"
        mMap["animations"] = prefs.getString("animations", "on") ?: "on"
        mMap["card_difficulty"] = prefs.getString("card_difficulty", "easy") ?: "easy"
        mMap["developer_options"] = prefs.getString("developer_options", "off") ?: "off"
        mMap["ui_color"] = prefs.getString("ui_color", "#930000") ?: "#930000"
        for ((key, value) in mMap)
            Log.d("Record", "loadPrincipal() -> mMap.[$key] = $value")
        // The value of "slots" will be in a format like "0,1,2;1" where the numbers before the semicolon are the slots that have previously been written and
        // the the number after is the most recent slot to have been saved (used for the "CONTINUE" option in the main menu)
        val slotsValue = (mMap["slots"] ?: ";").split(";")
        val slotsOnDisk = slotsValue.getOrNull(0)?.split(",") ?: LinkedList() // Split the former string by its commas: "0,1,2" will become {"0", "1", "2"}
        // Add the slots to the list as integers
        for (slot in slotsOnDisk) {
            // This check shouldn't be needed but, for some reason, kotlin's split() behaves differently from java's and will return a list with a single,
            // empty string if no delimiters are found. Obviously, calling toInt() on it doesn't work.
            if (slot.isNotEmpty())
                SlotsOnDisk.add(slot.toInt())
        }
        MostRecentSlot = slotsValue.getOrNull(1)?.toIntOrNull() // Cast the latter string, or nullify if no saved game exists
        Log.d("Record", "loadPrincipal() -> SlotsOnDisk = ${SlotsOnDisk.joinToString(", ")}")
        Log.d("Record", "loadPrincipal() -> MostRecentSlot = $MostRecentSlot")
    }

    fun load(slot: Int) {
        if ((slot < 0) || !SlotsOnDisk.contains(slot))
            return
        val prefs = gGZ.Context?.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        if (prefs == null) {
            Log.w("Record", "Failed to get a preferences object to load slot $slot - nothing was read from disk")
            return
        }
        val fullMap = prefs.all // Retrieve all key-value pairs on disk; we'll extract just those prepended with 'slot#_' (where # is the parameter 'slot')
        val slotMap = HashMap<String, String>()
        // Extract just the entries for the given slot
        for ((key, value) in fullMap) {
            if (key.startsWith("slot${slot}_"))
                slotMap[key.substringAfter("slot${slot}_")] = value?.toString() ?: continue
        }
        gGZ.Verse = slotMap["verse"]?.toInt() ?: 1
        gGZ.Chapter = slotMap["chapter"]?.toInt() ?: 1
        gGZ.Player.X = slotMap["scene_x"]?.toInt() ?: 0
        gGZ.Player.Y = slotMap["scene_y"]?.toInt() ?: 0
        gGZ.Player.Level = slotMap["player_level"]?.toInt() ?: 1
        gGZ.Player.Cash = slotMap["player_cash"]?.toInt() ?: 0
        gGZ.Player.Experience = slotMap["player_experience"]?.toInt() ?: 0
        gGZ.Player.HP = slotMap["player_hp"]?.toInt() ?: gGZ.Player.MaxHP
        gGZ.Player.AvailableStatPoints = slotMap["player_available_stat_points"]?.toInt() ?: 0
        gGZ.Player.StrengthStatAllotted = slotMap["player_strength_stat_allotted"]?.toInt() ?: 0
        gGZ.Player.VitalityStatAllotted = slotMap["player_vitality_stat_allotted"]?.toInt() ?: 0
        gGZ.Player.AgilityStatAllotted = slotMap["player_agility_stat_allotted"]?.toInt() ?: 0
        gGZ.Player.SenseStatAllotted = slotMap["player_sense_stat_allotted"]?.toInt() ?: 0
        val inventory = LinkedHashMap<Int, Int>()
        val cards = LinkedHashMap<Int, Int>()
        for ((key, value) in slotMap) {
            if (key.startsWith("inventory_")) {
                val split = value.split(' ')
                inventory[split[0].toInt()] = split[1].toInt()
            } else if (key.startsWith("card_") && (key != "card_difficulty"))
                cards[key.substringAfter('_').toInt()] = value.toInt()
        }
        gGZ.Player.Inventory.clear()
        gGZ.Player.Inventory.putAll(inventory.toList().sortedByDescending { (key, _) -> key }.toMap())
        gGZ.Player.Cards.clear()
        gGZ.Player.Cards.putAll(cards.toList().sortedByDescending { (key, _) -> key }.toMap())
        var id = slotMap["player_equipped_weapon"]?.toInt() ?: -1
        gGZ.Player.EquippedWeapon = if (id != -1) gGZ.Resources.loadItem(id) as ITWeapon else null
        id = slotMap["player_equipped_armor"]?.toInt() ?: -1
        gGZ.Player.EquippedArmor = if (id != -1) gGZ.Resources.loadItem(id) as ITArmor else null
        id = slotMap["player_equipped_accessory_1"]?.toInt() ?: -1
        gGZ.Player.EquippedAccessory1 = if (id != -1) gGZ.Resources.loadItem(id) as ITAccessory else null
        id = slotMap["player_equipped_accessory_2"]?.toInt() ?: -1
        gGZ.Player.EquippedAccessory2 = if (id != -1) gGZ.Resources.loadItem(id) as ITAccessory else null
        id = slotMap["addition_1"]?.toInt() ?: -1
        gGZ.Player.Addition1 = if (id != -1) gGZ.Resources.loadAddition(id) else null
        id = slotMap["addition_2"]?.toInt() ?: -1
        gGZ.Player.Addition2 = if (id != -1) gGZ.Resources.loadAddition(id) else null
        id = slotMap["addition_3"]?.toInt() ?: -1
        gGZ.Player.Addition3 = if (id != -1) gGZ.Resources.loadAddition(id) else null
        id = slotMap["addition_4"]?.toInt() ?: -1
        gGZ.Player.Addition4 = if (id != -1) gGZ.Resources.loadAddition(id) else null
        mMap.putAll(slotMap)
        LoadedSlot = slot
    }

    fun unload() {
        // Copy some application/game preferences that should survive this near-wipe
        val principalEntries = extractPrincipal()
        principalEntries["slots"] = ";"
        mMap.clear()
        mMap.putAll(principalEntries)
        LoadedSlot = null
    }

    fun save(slot: Int) {
        if (slot < 0)
            return
        val prefs = gGZ.Context?.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = prefs?.edit()
        if (editor == null) {
            Log.w("Record", "Failed to get a preferences editor to save slot $slot - nothing was written to disk")
            return
        }
        val fullMap = prefs.all
        val toRemove = HashSet<String>()
        val prefix = "slot${slot}_"
        // We're going to remove all entries from disk that begin with the prefix ('slot#_' where # is the parameter 'slot'). Then we'll write the new key-value
        // pairs using what's currently in memory (mMap). This is done due to the possibility of old, unwanted entries remaining if we don't (e.g. if the user
        // saves over an existing saved game or if the item/card lists have changed at all).
        for ((key, _) in fullMap) {
            // If this entry is part of the saved game in the given slot
            if (key.startsWith(prefix))
                toRemove.add(key) // Queue it for removal
        }
        for (key in toRemove)
            editor.remove(key)
        val slotMap = HashMap<String, String>()
        var index = 0
        for ((id, quantity) in gGZ.Player.Inventory)
            slotMap["${prefix}inventory_${index++}"] = "$id $quantity"
        for ((id, quantity) in gGZ.Player.Cards)
            slotMap["${prefix}card_${id}"] = quantity.toString()
        slotMap["${prefix}verse"] = gGZ.Verse.toString()
        slotMap["${prefix}chapter"] = gGZ.Chapter.toString()
        slotMap["${prefix}scene_id"] = gGZ.Scene?.ID ?: ""
        slotMap["${prefix}scene_name"] = gGZ.Scene?.Name ?: "Unknown"
        slotMap["${prefix}scene_x"] = gGZ.Player.X.toString()
        slotMap["${prefix}scene_y"] = gGZ.Player.Y.toString()
        slotMap["${prefix}player_level"] = gGZ.Player.Level.toString()
        slotMap["${prefix}player_cash"] = gGZ.Player.Cash.toString()
        slotMap["${prefix}player_experience"] = gGZ.Player.Experience.toString()
        slotMap["${prefix}player_hp"] = gGZ.Player.HP.toString()
        slotMap["${prefix}player_available_stat_points"] = gGZ.Player.AvailableStatPoints.toString()
        slotMap["${prefix}player_strength_stat_allotted"] = gGZ.Player.StrengthStatAllotted.toString()
        slotMap["${prefix}player_vitality_stat_allotted"] = gGZ.Player.VitalityStatAllotted.toString()
        slotMap["${prefix}player_agility_stat_allotted"] = gGZ.Player.AgilityStatAllotted.toString()
        slotMap["${prefix}player_sense_stat_allotted"] = gGZ.Player.SenseStatAllotted.toString()
        slotMap["${prefix}player_equipped_weapon"] = (gGZ.Player.EquippedWeapon?.ID ?: -1).toString()
        slotMap["${prefix}player_equipped_armor"] = (gGZ.Player.EquippedArmor?.ID ?: -1).toString()
        slotMap["${prefix}player_equipped_accessory_1"] = (gGZ.Player.EquippedAccessory1?.ID ?: -1).toString()
        slotMap["${prefix}player_equipped_accessory_2"] = (gGZ.Player.EquippedAccessory2?.ID ?: -1).toString()
        slotMap["${prefix}addition_1"] = (gGZ.Player.Addition1?.ID ?: -1).toString()
        slotMap["${prefix}addition_2"] = (gGZ.Player.Addition2?.ID ?: -1).toString()
        slotMap["${prefix}addition_3"] = (gGZ.Player.Addition3?.ID ?: -1).toString()
        slotMap["${prefix}addition_4"] = (gGZ.Player.Addition4?.ID ?: -1).toString()
        // Remember cutscenes that have played and overworld items that have been picked up
        for ((key,value) in mMap) {
            if (key.startsWith("cutscene ") || key.startsWith("picked_up "))
                slotMap["$prefix$key"] = value
        }
        for ((key, value) in slotMap)
            editor.putString(key, value)
        // Keep track of the slots available and the most recent (this) one
        SlotsOnDisk.add(slot) // SlotsOnDisk is a set so there's no need to worry about duplicates
        MostRecentSlot = slot
        mMap["slots"] = "${SlotsOnDisk.joinToString(",")};$MostRecentSlot" // e.g. "0,1,2,3;2" if saving to slot 2
        editor.putString("slots", mMap["slots"])
        editor.apply()
    }

    fun autosave() {
        if (IsAutosaveEnabled)
            save(0)
    }

    fun get(key: String, defaultValue: String): String = mMap[key] ?: defaultValue

    fun getDirect(key: String, defaultValue: String): String {
        if (gGZ.Context == null)
            return defaultValue
        val prefs = gGZ.Context?.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return prefs?.getString(key, defaultValue) ?: defaultValue
    }

    fun set(key: String, value: String) = mMap.put(key, value)

    fun setDirect(key: String, value: String) {
        mMap[key] = value
        val editor = gGZ.Context?.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)?.edit() ?: return
        editor.putString(key, value)
        editor.apply()
    }

    fun remove(key: String) = mMap.remove(key)

    fun removeDirect(key: String) {
        remove(key)
        val editor = gGZ.Context?.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)?.edit() ?: return
        editor.remove(key)
        editor.apply()
    }

    fun contains(key: String): Boolean = mMap.containsKey(key)

    private fun extractPrincipal(): HashMap<String, String> {
        val principalEntries = HashMap<String, String>()
        principalEntries["slots"] = mMap["slots"] ?: ";"
        principalEntries["animations"] = mMap["animations"] ?: "on"
        principalEntries["card_difficulty"] = mMap["card_difficulty"] ?: "easy"
        principalEntries["developer_options"] = mMap["developer_options"] ?: "off"
        principalEntries["ui_color"] = mMap["ui_color"] ?: "#930000"
        return principalEntries
    }
}