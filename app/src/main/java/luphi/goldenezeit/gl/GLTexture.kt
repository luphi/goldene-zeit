package luphi.goldenezeit.gl

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.opengl.GLES20
import android.util.Log
import luphi.goldenezeit.gGZ
import java.io.IOException
import kotlin.concurrent.withLock

@Suppress("PropertyName")
class GLTexture {
    var ID = IntArray(1)
    var WidthInPixels = 1
    var HeightInPixels = 1
    private var mFile = ""

    fun init(file: String) {
        mFile = file
        val assetManager = gGZ.Context?.assets
        val bitmap: Bitmap
        try {
            bitmap = BitmapFactory.decodeStream(assetManager?.open(file))
        } catch (e: IOException) {
            Log.e("GLUtils", "Failed to open texture file \"$mFile\"", e)
            mFile += " (failed)"
            return
        }
        WidthInPixels = bitmap.width
        HeightInPixels = bitmap.height
        // OpenGL calls (GLES20.*) need to be run on the OpenGL thread so we'll queue it here in case the texture is being read from another thread.  This will
        // run on the OpenGL thread at the next opportunity.  This means it will run and generate/bind the texture before the next draw loop.  However, the
        // Width and Height are set prior to this so other objects may calculate their aspect ratios and such independent of this.
        gGZ.View?.queueEvent {
            GLES20.glGenTextures(1, ID, 0)
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, ID[0])
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST)
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST)
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)
            android.opengl.GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0)
            bitmap.recycle()
        }
    }

    fun free() {
        gGZ.View?.queueEvent { GLES20.glDeleteTextures(ID.size, ID, 0) }
    }

    override fun toString(): String {
        // Note: if using this immediately after initialization, the ID will likely be 0.  Because the OpenGL-based texture binding is queued onto the OpenGL
        // thread, its value will not have been set yet.
        gGZ.Renderer?.Lock?.withLock { return "{ ID = ${ID.contentToString()}  [$WidthInPixels x $HeightInPixels]  $mFile }" } ?: return ""
    }
}