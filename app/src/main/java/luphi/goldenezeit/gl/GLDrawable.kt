package luphi.goldenezeit.gl

import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import kotlin.concurrent.withLock

@Suppress("PropertyName")
open class GLDrawable : IDrawable {
    // IDrawable property
    override val Rect: Rect
        get() = gGZ.Renderer?.Lock?.withLock { return Rect(mX, mY, mWidth, mHeight) } ?: Rect(0f, 0f, 0f, 0f)

    protected var mIsInitialized = false
    protected var mIsMoved = false
    protected var mIsSized = false
    protected var mX = 0f
    protected var mY = 0f
    protected var mWidth = 0f
    protected var mHeight = 0f

    var Alpha = 1f
    var IsStatic = false
    var IsVisible = true

    override fun draw(dt: Long) {}

    open fun free() {}

    open fun move(x: Float, y: Float) {
        gGZ.Renderer?.Lock?.withLock {
            mX = x
            mY = y
        }
        mIsMoved = true
    }

    fun move(rect: Rect) = move(rect.X, rect.Y)

    open fun translate(dx: Float, dy: Float) {
        gGZ.Renderer?.Lock?.withLock {
            mX += dx
            mY += dy
        }
    }

    open fun size(w: Float, h: Float) {
        gGZ.Renderer?.Lock?.withLock {
            mWidth = w
            mHeight = h
        }
        mIsSized = true
    }

    fun size(rect: Rect) = size(rect.Width, rect.Height)

    open fun sizeByWidth(w: Float) {
        val rect = Rect
        size(w, rect.Height / rect.Width * w)
    }

    open fun sizeByHeight(h: Float) {
        val rect = Rect
        size(rect.Width / rect.Height * h, h)
    }

    override fun toString(): String {
        val rect = Rect
        return "{ position (${rect.X}, ${rect.Y}), dimensions [${rect.Width} x ${rect.Height}] }"
    }
}