package luphi.goldenezeit.gl

import android.opengl.Matrix
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import kotlin.concurrent.withLock
import kotlin.math.sign

@Suppress("PropertyName")
class GLCamera {
    private var mIsPanning = false
    private var mPanToX = 0f // Normalized device X coordinate the camera will eventually look at
    private var mPanToY = 0f // Normalized device Y coordinate the camera will eventually look at
    private var mPanVelocityX = 0f // Change in X (NDC) per millisecond
    private var mPanVelocityY = 0f // Change in Y (NDC) per millisecond

    var WidthInPixels = 0
    var HeightInPixels = 0
    var Width = 1f
    val WidthWithMargin
        get() = Width - (Margin * 2f)
    var Height = 1f
    val HeightWithMargin
        get() = Height - (Margin * 2f)
    var AspectRatio = 1f
    var LookAtX = 0f
    var LookAtY = 0f
    var Margin = 0.01f // A common spacing, in NDC, derived from the surface's width
    val LeftEdge
        get() = -Width / 2f
    val LeftEdgeWithMargin
        get() = LeftEdge + Margin
    val RightEdge
        get() = Width / 2f
    val RightEdgeWithMargin
        get() = RightEdge - Margin
    val BottomEdge
        get() = -Height / 2f
    val BottomEdgeWithMargin
        get() = BottomEdge + Margin
    val TopEdge
        get() = Height / 2f
    val TopEdgeWithMargin
        get() = TopEdge - Margin

    fun lookAt(x: Float, y: Float) {
        lookAt(x, y, false)
    }

    fun lookAt(rect: Rect) {
        lookAt(rect.X + (rect.Width / 2f), rect.Y + (rect.Height / 2f))
    }

    fun lookAt(drawable: IDrawable) {
        lookAt(drawable.Rect)
    }

    fun translate(dx: Float, dy: Float) {
        lookAt(LookAtX + dx, LookAtY + dy)
    }

    fun panTo(x: Float, y: Float, durationInMilliseconds: Int) {
        gGZ.Renderer?.Lock?.withLock {
            mPanToX = x
            mPanToY = y
            mPanVelocityX = (x - LookAtX) / durationInMilliseconds.toFloat()
            mPanVelocityY = (y - LookAtY) / durationInMilliseconds.toFloat()
            mIsPanning = true
        }
    }

    fun isVisible(drawable: GLDrawable): Boolean {
        val rect = drawable.Rect
        if (drawable.IsStatic) {
            if (rect.X > Width / 2f)
                return false
            if (rect.X + rect.Width < -Width / 2f)
                return false
            return if (rect.Y + rect.Height < -Height / 2f) false
            else rect.Y <= Height / 2f
        } else {
            if (rect.X > Width / 2f + LookAtX)
                return false
            if (rect.X + rect.Width < -Width / 2f + LookAtX)
                return false
            return if (rect.Y + rect.Height < -Height / 2f + LookAtY) false
            else rect.Y <= Height / 2f + LookAtY
        }
    }

    // This is the "do what cameras do" function to be used each frame. It will, when appropriate, pan at the highest possible framerate.
    internal fun film(dt: Long) {
        gGZ.Renderer?.Lock?.withLock {
            if (mIsPanning) {
                val prevX = LookAtX
                val prevY = LookAtY
                lookAt(LookAtX + (mPanVelocityX * dt), LookAtY + (mPanVelocityY * dt), true)
                // If the "look at" coordinates have passed the target (specifically, if the difference between the current coordinates have a different sign
                // from the difference between the previous coordinates), the target has been reached
                if (((sign(mPanToX - prevX) != sign(mPanToX - LookAtX)) || (sign(mPanToY - prevY) != sign(mPanToY - LookAtY)))) {
                    lookAt(mPanToX, mPanToY, true)
                    mIsPanning = false
                }
            }
        }
    }

    private fun lookAt(x: Float, y: Float, isInternal: Boolean) {
        gGZ.Renderer?.Lock?.withLock {
            if (!mIsPanning || isInternal) {
                LookAtX = x
                LookAtY = y
                Matrix.setLookAtM(
                    gGZ.Renderer?.DynamicViewMatrix, 0,
                    LookAtX, LookAtY, 1f, // Location of the "eye"
                    LookAtX, LookAtY, 0f, // Coordinates to look at
                    0f, 1f, 0f // Vector representing up
                )
            }
        }
    }
}