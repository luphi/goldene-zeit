package luphi.goldenezeit.gl

import android.content.Context
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.mode.MTransition

@Suppress("UNUSED_PARAMETER", "PropertyName")
class GLView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : GLSurfaceView(context, attrs) {
    init {
        setEGLContextClientVersion(2)
        gGZ.View = this
        gGZ.Renderer = GLRenderer()
        setRenderer(gGZ.Renderer)
        renderMode = RENDERMODE_CONTINUOUSLY
        gGZ.IsViewReady = true
        if (gGZ.IsActivityReady && gGZ.IsRendererReady && gGZ.IsViewReady)
            gGZ.Mode?.init(MTransition(MTransition.Mode.MENU))
    }
}