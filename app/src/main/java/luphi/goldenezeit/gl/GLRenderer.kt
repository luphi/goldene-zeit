package luphi.goldenezeit.gl

import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.opengl.Matrix
import android.os.SystemClock.uptimeMillis
import android.util.Log
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.mode.MTransition
import java.util.concurrent.locks.ReentrantLock
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import kotlin.concurrent.withLock

@Suppress("PropertyName")
class GLRenderer : GLSurfaceView.Renderer {
    private val mPrimitiveVSource = "uniform mat4 mat4_mvp;" +
            "attribute vec4 vec4_position;" +
            "void main() {" +
            "  gl_Position = mat4_mvp * vec4_position;" +
            "}"
    private val mPrimitiveFSource = "precision mediump float;" +
            "uniform vec4 vec4_color;" +
            "void main() {" +
            "  gl_FragColor = vec4_color;" +
            "}"
    private val mSpriteVSource = "uniform mat4 mat4_mvp;" +
            "attribute vec4 vec4_position;" +
            "attribute vec2 vec2_aTexCoord;" +
            "varying vec2 vec2_vTexCoord;" +
            "void main() {" +
            "  gl_Position = mat4_mvp * vec4_position;" +
            "  vec2_vTexCoord = vec2_aTexCoord;" +
            "}"
    private val mSpriteFSource = "precision mediump float;" +
            "varying vec2 vec2_vTexCoord;" +
            "uniform sampler2D s2d_texture;" +
            "uniform float f_alpha;" +
            "vec3 vec3_substitutionPrimary = vec3(0.721568627, 0.533333333, 0.97254902);" +
            "vec3 vec3_substitutionSecondary = vec3(0.345098039, 0.721568627, 0.97254902);" +
            "vec3 vec3_substitutionLight = vec3(0.97254902,  0.97254902,  0.97254902);" +
            "vec3 vec3_substitutionDark = vec3(0.094117647, 0.094117647, 0.094117647);" +
            "uniform vec4 vec4_colorPrimary;" +
            "uniform vec4 vec4_colorSecondary;" +
            "uniform vec4 vec4_colorLight;" +
            "uniform vec4 vec4_colorDark;" +
            "void main() {" +
            "  gl_FragColor = texture2D(s2d_texture, vec2_vTexCoord);" +
            "  if (dot(gl_FragColor.rgb - vec3_substitutionPrimary, gl_FragColor.rgb - vec3_substitutionPrimary) < 0.001) {" +
            "    gl_FragColor = vec4_colorPrimary;" +
            "  }" +
            "  else if (dot(gl_FragColor.rgb - vec3_substitutionSecondary, gl_FragColor.rgb - vec3_substitutionSecondary) < 0.001) {" +
            "    gl_FragColor = vec4_colorSecondary;" +
            "  }" +
            "  else if (dot(gl_FragColor.rgb - vec3_substitutionLight, gl_FragColor.rgb - vec3_substitutionLight) < 0.001) {" +
            "    gl_FragColor = vec4_colorLight;" +
            "  }" +
            "  else if (dot(gl_FragColor.rgb - vec3_substitutionDark, gl_FragColor.rgb - vec3_substitutionDark) < 0.001) {" +
            "    gl_FragColor = vec4_colorDark;" +
            "  }" +
            "  gl_FragColor.a *= f_alpha;" +
            "}"
    private var mPreviousTicks = uptimeMillis()

    var IsPaused: Boolean = false
        set(value) {
            field = value
            gGZ.View?.renderMode = if (value)
                GLSurfaceView.RENDERMODE_WHEN_DIRTY // Effectively pause drawing by changing the render mode to only render upon a request
            else
                GLSurfaceView.RENDERMODE_CONTINUOUSLY // Return the render mode to draw continuously on a separate thread
        }
    var ProjectionMatrix = FloatArray(16)
    var StaticViewMatrix = FloatArray(16)
    var DynamicViewMatrix = FloatArray(16)
    var PrimitiveProgram = 0
    var SpriteProgram = 0
    var Lock = ReentrantLock()
    var ClearColor: FloatArray = floatArrayOf(1f, 1f, 1f) // White
        set(value) {
            field = value
            gGZ.View?.queueEvent { GLES20.glClearColor(value[0], value[1], value[2], 0f) }
        }
    var PrimaryColor: FloatArray = floatArrayOf(0.721568627f, 0.533333333f, 0.97254902f)
        set(value) {
            field = value
            gGZ.View?.queueEvent {
                val hColor = GLES20.glGetUniformLocation(SpriteProgram, "vec4_colorPrimary")
                GLES20.glUniform4fv(hColor, 1, floatArrayOf(value[0], value[1], value[2], 1f), 0)
            }
        }
    var SecondaryColor: FloatArray = floatArrayOf(0.345098039f, 0.721568627f, 0.97254902f)
        set(value) {
            field = value
            gGZ.View?.queueEvent {
                val hColor = GLES20.glGetUniformLocation(SpriteProgram, "vec4_colorSecondary")
                GLES20.glUniform4fv(hColor, 1, floatArrayOf(value[0], value[1], value[2], 1f), 0)
            }
        }
    var LightColor: FloatArray = floatArrayOf(0.97254902f, 0.97254902f, 0.97254902f)
        set(value) {
            field = value
            gGZ.View?.queueEvent {
                val hColor = GLES20.glGetUniformLocation(SpriteProgram, "vec4_colorLight")
                GLES20.glUniform4fv(hColor, 1, floatArrayOf(value[0], value[1], value[2], 1f), 0)
            }
        }
    var DarkColor: FloatArray = floatArrayOf(0.094117647f, 0.094117647f, 0.094117647f)
        set(value) {
            field = value
            gGZ.View?.queueEvent {
                val hColor = GLES20.glGetUniformLocation(SpriteProgram, "vec4_colorDark")
                GLES20.glUniform4fv(hColor, 1, floatArrayOf(value[0], value[1], value[2], 1f), 0)
            }
        }

    init {
        Matrix.setLookAtM(
            StaticViewMatrix, 0,
            0f, 0f, 1f, // Eye/camera coordinates
            0f, 0f, 0f, // "Look at" coordinates
            0f, 1f, 0f  // Up direction (positive Y)
        )
        Matrix.setLookAtM(
            DynamicViewMatrix, 0,
            0f, 0f, 1f,
            0f, 0f, 0f,
            0f, 1f, 0f
        )
    }

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        GLES20.glEnable(GLES20.GL_BLEND)
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
        val primitiveVShader = loadShader(GLES20.GL_VERTEX_SHADER, mPrimitiveVSource)
        val primitiveFShader = loadShader(GLES20.GL_FRAGMENT_SHADER, mPrimitiveFSource)
        val spriteVShader = loadShader(GLES20.GL_VERTEX_SHADER, mSpriteVSource)
        val spriteFShader = loadShader(GLES20.GL_FRAGMENT_SHADER, mSpriteFSource)
        PrimitiveProgram = compileProgram(primitiveVShader, primitiveFShader)
        SpriteProgram = compileProgram(spriteVShader, spriteFShader)
        // Use the setters to assign the various colors with their default values
        ClearColor = ClearColor
        PrimaryColor = PrimaryColor
        SecondaryColor = SecondaryColor
        LightColor = LightColor
        DarkColor = DarkColor
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        gGZ.Camera.WidthInPixels = width
        gGZ.Camera.HeightInPixels = height
        gGZ.Camera.AspectRatio = width.toFloat() / height.toFloat()
        gGZ.Camera.Width = 1f
        gGZ.Camera.Height = 1f / gGZ.Camera.AspectRatio
        gGZ.Camera.Margin = gGZ.Camera.Width / 100f
        gGZ.FontSize = gGZ.Camera.Width / 28f
        Log.i("GLRenderer", "Surface [$width x $height] pixels -> [${gGZ.Camera.Width} x ${gGZ.Camera.Height}] normalized device coordinates")
        Log.i(
            "GLRenderer",
            "Camera [${gGZ.Camera.Width} x ${gGZ.Camera.Height}] left, right = [${gGZ.Camera.LeftEdge}, ${gGZ.Camera.RightEdge}] bottom, top = " +
                    "[${gGZ.Camera.BottomEdge}, ${gGZ.Camera.TopEdge}]"
        )
        GLES20.glViewport(0, 0, width, height)
        GLES20.glLineWidth(10f)
        Matrix.orthoM(ProjectionMatrix, 0, gGZ.Camera.LeftEdge, gGZ.Camera.RightEdge, gGZ.Camera.BottomEdge, gGZ.Camera.TopEdge, -1f /* Z near */, 1f /* far */)
        gGZ.IsRendererReady = true
        if (gGZ.IsActivityReady && gGZ.IsRendererReady && gGZ.IsViewReady)
            gGZ.Mode?.init(MTransition(MTransition.Mode.MENU))
    }

    override fun onDrawFrame(gl: GL10?) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT)
        val dt = uptimeMillis() - mPreviousTicks
        mPreviousTicks += dt
        Lock.withLock {
            gGZ.Camera.film(dt)
            gGZ.Mode?.draw(dt)
        }
    }

    private fun loadShader(type: Int, source: String): Int {
        val shader = GLES20.glCreateShader(type)
        GLES20.glShaderSource(shader, source)
        GLES20.glCompileShader(shader)
        val compilationStatus = IntArray(1)
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compilationStatus, 0)
        if (compilationStatus[0] == 0) {
            Log.e("GLRenderer", "Failed to compile a shader: " + GLES20.glGetShaderInfoLog(shader))
            GLES20.glDeleteShader(shader)
        }
        return shader
    }

    private fun compileProgram(vShader: Int, fShader: Int): Int {
        val program = GLES20.glCreateProgram()
        GLES20.glAttachShader(program, vShader)
        GLES20.glAttachShader(program, fShader)
        GLES20.glLinkProgram(program)
        val linkingStatus = IntArray(1)
        GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkingStatus, 0)
        if (linkingStatus[0] != GLES20.GL_TRUE) {
            Log.e("GLRenderer", "Failed to link a program: " + GLES20.glGetProgramInfoLog(program))
            GLES20.glDeleteProgram(program)
        }
        return program
    }
}