package luphi.goldenezeit.gl

import android.opengl.GLES20
import android.opengl.Matrix
import luphi.goldenezeit.gGZ
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer
import kotlin.concurrent.withLock

@Suppress("PropertyName")
open class GLSprite : GLDrawable() {
    protected var mTexture: GLTexture? = null
    protected var mTextureCoordinateBuffer: FloatBuffer
    private var mVertexBuffer: FloatBuffer
    private var mIndexBuffer: ShortBuffer
    private var mNumIndices = 0

    var IsMirrored = false
        set(value) {
            field = value
            setTextureCoordinates(0f, 0f, 1f, 1f, value)
        }
    var Texture: GLTexture? = null
        set(value) {
            field = value
            gGZ.Renderer?.Lock?.withLock { mTexture = value }
        }

    init {
        val vertices = floatArrayOf(
            0f, 1f, 0f, // Index 0 = top left
            1f, 1f, 0f, // Index 1 = top right
            1f, 0f, 0f, // Index 2 = bottom right
            0f, 0f, 0f // Index 3 = bottom left
        )
        val textureCoordinates = floatArrayOf(
            0f, 0f, // Top left
            1f, 0f, // Top right
            1f, 1f, // Bottom right
            0f, 1f  // Bottom left
        )
        val indices = shortArrayOf(0, 1, 2, 3)
        val byteBuffer1 = ByteBuffer.allocateDirect(vertices.size * 4)
        byteBuffer1.order(ByteOrder.nativeOrder())
        mVertexBuffer = byteBuffer1.asFloatBuffer()
        mVertexBuffer.put(vertices)
        mVertexBuffer.position(0)
        mNumIndices = indices.size
        val byteBuffer2 = ByteBuffer.allocateDirect(mNumIndices * 2)
        byteBuffer2.order(ByteOrder.nativeOrder())
        mIndexBuffer = byteBuffer2.asShortBuffer()
        mIndexBuffer.put(indices)
        mIndexBuffer.position(0)
        val byteBuffer3 = ByteBuffer.allocateDirect(textureCoordinates.size * 4)
        byteBuffer3.order(ByteOrder.nativeOrder())
        mTextureCoordinateBuffer = byteBuffer3.asFloatBuffer()
        mTextureCoordinateBuffer.put(textureCoordinates)
        mTextureCoordinateBuffer.position(0)
    }

    fun init(texture: GLTexture?) {
        if (mIsInitialized)
            return
        Texture = texture
        val width = texture?.WidthInPixels?.toFloat() ?: 1f
        val height = texture?.HeightInPixels?.toFloat() ?: 1f
        gGZ.Renderer?.Lock?.withLock {
            mWidth = width / height
            mHeight = 1f
        }
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if ((mTexture == null) || !gGZ.Camera.isVisible(this) || !IsVisible || !mIsInitialized || !mIsMoved || !mIsSized)
            return
        gGZ.Renderer?.Lock?.withLock {
            val program = gGZ.Renderer?.SpriteProgram ?: 0
            GLES20.glUseProgram(program)
            val hPosition = GLES20.glGetAttribLocation(program, "vec4_position")
            val hTextureCoordinates = GLES20.glGetAttribLocation(program, "vec2_aTexCoord")
            val hMVPMatrix = GLES20.glGetUniformLocation(program, "mat4_mvp")
            val hSampler = GLES20.glGetUniformLocation(program, "s2d_texture")
            val hAlpha = GLES20.glGetUniformLocation(program, "f_alpha")
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexture!!.ID[0])
            GLES20.glEnableVertexAttribArray(hPosition)
            GLES20.glVertexAttribPointer(hPosition, 3, GLES20.GL_FLOAT, false, 0, mVertexBuffer)
            GLES20.glEnableVertexAttribArray(hTextureCoordinates)
            GLES20.glVertexAttribPointer(hTextureCoordinates, 2, GLES20.GL_FLOAT, false, 0, mTextureCoordinateBuffer)
            val model = FloatArray(16)
            Matrix.setIdentityM(model, 0)
            Matrix.translateM(model, 0, mX, mY, 0f)
            Matrix.scaleM(model, 0, mWidth, mHeight, 0f)
            val mvpMatrix = FloatArray(16)
            if (IsStatic)
                Matrix.multiplyMM(mvpMatrix, 0, gGZ.Renderer?.StaticViewMatrix, 0, model, 0)
            else
                Matrix.multiplyMM(mvpMatrix, 0, gGZ.Renderer?.DynamicViewMatrix, 0, model, 0)
            Matrix.multiplyMM(mvpMatrix, 0, gGZ.Renderer?.ProjectionMatrix, 0, mvpMatrix, 0)
            GLES20.glUniformMatrix4fv(hMVPMatrix, 1, false, mvpMatrix, 0)
            GLES20.glUniform1i(hSampler, 0)
            GLES20.glUniform1f(hAlpha, Alpha)
            GLES20.glDrawElements(GLES20.GL_TRIANGLE_FAN, mNumIndices, GLES20.GL_UNSIGNED_SHORT, mIndexBuffer)
            GLES20.glDisableVertexAttribArray(hPosition)
            GLES20.glDisableVertexAttribArray(hTextureCoordinates)
        }
    }

    override fun sizeByWidth(w: Float) {
        gGZ.Renderer?.Lock?.withLock {
            val width = mTexture?.WidthInPixels?.toFloat() ?: mWidth
            val height = mTexture?.HeightInPixels?.toFloat() ?: mHeight
            size(w, w * (height / width))
        }
    }

    override fun sizeByHeight(h: Float) {
        gGZ.Renderer?.Lock?.withLock {
            val width = mTexture?.WidthInPixels?.toFloat() ?: mWidth
            val height = mTexture?.HeightInPixels?.toFloat() ?: mHeight
            size(h * (width / height), h)
        }
    }

    fun setTextureCoordinates(x: Float, y: Float, w: Float, h: Float, isMirrored: Boolean = false) {
        val textureCoordinates = if (isMirrored)
            floatArrayOf(
                x + w, y,    // Top left
                x, y,        // Top right
                x, y + h,    // Bottom right
                x + w, y + h // Bottom left
            )
        else
            floatArrayOf(
                x, y,         // Top left
                x + w, y,     // Top right
                x + w, y + h, // Bottom right
                x, y + h      // Bottom left
            )
        val bb = ByteBuffer.allocateDirect(textureCoordinates.size * 4)
        bb.order(ByteOrder.nativeOrder())
        gGZ.Renderer?.Lock?.withLock {
            mTextureCoordinateBuffer = bb.asFloatBuffer()
            mTextureCoordinateBuffer.put(textureCoordinates)
            mTextureCoordinateBuffer.position(0)
        }
    }

    override fun toString(): String {
        val rect = Rect
        return "{ position (${rect.X}, ${rect.Y}), dimensions [${rect.Width} x ${rect.Height}], is static = $IsStatic, texture $mTexture }"
    }
}