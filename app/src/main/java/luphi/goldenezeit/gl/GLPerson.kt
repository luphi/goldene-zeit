package luphi.goldenezeit.gl

import luphi.goldenezeit.GZ
import luphi.goldenezeit.Tasks

@Suppress("PropertyName", "PrivatePropertyName")
class GLPerson : GLSprite() {
    private enum class Frame {
        NORTH_IDLE, NORTH_WALK_FRAME_1, NORTH_WALK_FRAME_2,
        EAST_IDLE, EAST_WALK_FRAME_1, EAST_WALK_FRAME_2,
        SOUTH_IDLE, SOUTH_WALK_FRAME_1, SOUTH_WALK_FRAME_2,
        WEST_IDLE, WEST_WALK_FRAME_1, WEST_WALK_FRAME_2
    }

    private val WALK_FREQUENCY = 7 // Frequency, in Hz, at which the animation frame is changed

    private val mTasks = Tasks()
    private var mIsWalking = false

    private var mFrame = Frame.SOUTH_IDLE
        set(frame) {
            field = frame
            // Determine which of the six frames in the spritesheet are to be used, and mirror the frame
            // if needed.
            // Within the spritesheet, the indices correspond to:
            // -------------
            // |  0  |  1  |
            // -------------
            // |  2  |  3  |
            // -------------
            // |  4  |  5  |
            // -------------
            val index: Int
            val isMirrored: Boolean
            when (frame) {
                Frame.NORTH_IDLE -> {
                    index = 0
                    isMirrored = false
                }
                Frame.NORTH_WALK_FRAME_1 -> {
                    index = 1
                    isMirrored = false
                }
                Frame.NORTH_WALK_FRAME_2 -> {
                    index = 1
                    isMirrored = true
                }
                Frame.EAST_IDLE, Frame.EAST_WALK_FRAME_2 -> {
                    index = 2
                    isMirrored = false
                }
                Frame.EAST_WALK_FRAME_1 -> {
                    index = 3
                    isMirrored = false
                }
                Frame.SOUTH_IDLE -> {
                    index = 4
                    isMirrored = false
                }
                Frame.SOUTH_WALK_FRAME_1 -> {
                    index = 5
                    isMirrored = false
                }
                Frame.SOUTH_WALK_FRAME_2 -> {
                    index = 5
                    isMirrored = true
                }
                Frame.WEST_IDLE, Frame.WEST_WALK_FRAME_2 -> {
                    index = 2
                    isMirrored = true
                }
                Frame.WEST_WALK_FRAME_1 -> {
                    index = 3
                    isMirrored = true
                }
            }
            val x = (index % 2).toFloat() / 2f
            val y = (index / 2).toFloat() / 3f
            val w = 1f / 2f
            val h = 1f / 3f
            setTextureCoordinates(x, y, w, h, isMirrored)
        }

    // The north, east, south, west direction the person is facing
    var Direction = GZ.Cardinal.SOUTH
        set(direction) {
            if (field == direction)
                return
            field = direction
            mFrame = when (direction) {
                GZ.Cardinal.NORTH -> if (mIsWalking) Frame.NORTH_WALK_FRAME_1 else Frame.NORTH_IDLE
                GZ.Cardinal.SOUTH -> if (mIsWalking) Frame.SOUTH_WALK_FRAME_1 else Frame.SOUTH_IDLE
                GZ.Cardinal.EAST -> if (mIsWalking) Frame.EAST_WALK_FRAME_1 else Frame.EAST_IDLE
                GZ.Cardinal.WEST -> if (mIsWalking) Frame.WEST_WALK_FRAME_1 else Frame.WEST_IDLE
            }
        }

    init {
        IsStatic = false
        mFrame = Frame.SOUTH_IDLE
    }

    override fun draw(dt: Long) {
        if (!IsVisible || !mIsMoved || !mIsSized)
            return
        mTasks.step(dt)
        super.draw(dt)
    }

    fun walk(shouldWalk: Boolean) {
        // If already animating and animation is (redundantly) being turned on, or vice versa
        if (mIsWalking == shouldWalk)
            return
        if (shouldWalk) {
            // This is equivalent to mFrame += 1 but Kotlin/Java doesn't allow that directly
            // e.g. if (mFrame == SOUTH_IDLE) { mFrame = SOUTH_WALK_FRAME_1; }
            mFrame = Frame.values()[mFrame.ordinal + 1]
            mTasks.schedulePeriodic((1000 / WALK_FREQUENCY).toLong()) {
                mFrame = if ((mFrame.ordinal + 1) % 3 == 0)
                    Frame.values()[mFrame.ordinal - 1]
                else
                    Frame.values()[mFrame.ordinal + 1]
            }
        }
        else {
            mTasks.clear()
            // This is equivalent to mFrame = (mFrame / 3) * 3 and relies on integer division
            // e.g. if ((mFrame == SOUTH_WALK_FRAME_1) ||
            //  (mFrame == SOUTH_WALK_FRAME_2)) { mFrame = SOUTH_IDLE; }
            mFrame = Frame.values()[(mFrame.ordinal / 3) * 3]
        }
        mIsWalking = shouldWalk
    }
}