package luphi.goldenezeit.gl

import luphi.goldenezeit.gGZ
import java.util.LinkedList
import kotlin.concurrent.withLock

@Suppress("PropertyName")
class GLText : GLDrawable() {
    private val mCharacters = LinkedList<GLCharacter>()

    var Text: String = ""
        set(text) {
            field = text
            gGZ.Renderer?.Lock?.withLock {
                for (character in mCharacters)
                    character.free()
                mCharacters.clear()
                for (letter in field.toCharArray()) {
                    val character = GLCharacter()
                    character.init(letter)
                    mCharacters.add(character)
                }
                size(gGZ.FontSize * field.length, gGZ.FontSize)
            }
        }

    init {
        IsStatic = true
    }

    fun init(text: String) {
        if (mIsInitialized)
            return
        gGZ.Renderer?.Lock?.withLock {
            Text = text // Has a fairly substantial setter method
            move(0f, 0f)
        }
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!IsVisible || !mIsMoved || !mIsSized)
            return
        gGZ.Renderer?.Lock?.withLock {
            for (character in mCharacters)
                character.draw(dt)
        }
    }

    override fun move(x: Float, y: Float) {
        val rect = Rect
        translate(x - rect.X, y - rect.Y)
        mIsMoved = true
    }

    override fun translate(dx: Float, dy: Float) {
        gGZ.Renderer?.Lock?.withLock {
            super.translate(dx, dy)
            for (i in mCharacters.indices)
                mCharacters[i].translate(dx, dy)
        }
    }

    override fun size(w: Float, h: Float) {
        gGZ.Renderer?.Lock?.withLock {
            super.size(w, h)
            val rect = Rect
            val widthPerCharacter = rect.Width / mCharacters.size.toFloat()
            for (i in mCharacters.indices) {
                mCharacters[i].size(widthPerCharacter, h)
                mCharacters[i].move(rect.X + i * widthPerCharacter, rect.Y)
            }
        }
        mIsSized = true
    }

    override fun toString(): String {
        val rect = Rect
        return "{ position (${rect.X}, ${rect.Y}), dimensions [${rect.Width} x ${rect.Height}], text = \"$Text\" }"
    }
}