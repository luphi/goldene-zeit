package luphi.goldenezeit.gl

import android.opengl.GLES20
import android.opengl.Matrix
import luphi.goldenezeit.gGZ
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer
import kotlin.concurrent.withLock
import kotlin.math.cos
import kotlin.math.sin

@Suppress("PropertyName")
class GLPrimitive : GLDrawable() {
    enum class Shape { QUAD, QUAD_OUTLINE, DIAMOND, CIRCLE, CIRCLE_OUTLINE }

    private var mVertexBuffer: FloatBuffer? = null
    private var mIndexBuffer: ShortBuffer? = null
    private var mNumIndices = 0
    private var mGLDrawMode = 0
    private var mShape = Shape.QUAD

    var Color = floatArrayOf(1f, 1f, 1f)
    var Angle = 0f

    fun init(shape: Shape) {
        if (mIsInitialized)
            return
        val vertices: FloatArray
        val indices: ShortArray
        val circleVertices = 32
        mGLDrawMode = GLES20.GL_TRIANGLES
        mShape = shape
        when (mShape) {
            Shape.QUAD -> {
                vertices = floatArrayOf(
                    0f, 1f, 0f,
                    0f, 0f, 0f,
                    1f, 0f, 0f,
                    1f, 1f, 0f
                )
                indices = shortArrayOf(
                    0, 1, 2,
                    0, 2, 3
                )
            }
            Shape.QUAD_OUTLINE -> {
                vertices = floatArrayOf(
                    0f, 1f, 0f,
                    0f, 0f, 0f,
                    1f, 0f, 0f,
                    1f, 1f, 0f
                )
                indices = shortArrayOf(0, 1, 2, 3)
                mGLDrawMode = GLES20.GL_LINE_LOOP
            }
            Shape.DIAMOND -> {
                vertices = floatArrayOf(
                    0f, 0.5f, 0f,
                    0.5f, 1f, 0f,
                    1f, 0.5f, 0f,
                    0.5f, 0f, 0f
                )
                indices = shortArrayOf(
                    0, 1, 2,
                    0, 2, 3
                )
            }
            Shape.CIRCLE -> {
                mGLDrawMode = GLES20.GL_TRIANGLE_FAN
                vertices = FloatArray((circleVertices + 1) * 3)
                indices = ShortArray(circleVertices + 1)
                vertices[0] = 0.5f
                vertices[1] = 0.5f
                vertices[2] = 0f
                indices[0] = 0
            }
            Shape.CIRCLE_OUTLINE -> {
                vertices = FloatArray(circleVertices * 3)
                indices = ShortArray(circleVertices)
                for (i in 0 until circleVertices) {
                    vertices[3 * i] = (cos(i.toFloat().toDouble() * 2.0 * Math.PI / circleVertices.toFloat()).toFloat() + 1f) / 2f
                    vertices[3 * i + 1] = (sin(i.toFloat().toDouble() * 2.0 * Math.PI / circleVertices.toFloat()).toFloat() + 1f) / 2f
                    vertices[3 * i + 2] = 0f
                    indices[i] = i.toShort()
                }
                mGLDrawMode = GLES20.GL_LINE_LOOP
            }
        } // This is only the first pass, the second pass is below
        // Second pass for the circle-related shapes which have already had the vertex and index arrays created and populated with the first vertex
        if (mShape == Shape.CIRCLE)
            createCircle(0.0, 2f * Math.PI, circleVertices, vertices, indices)
        gGZ.Renderer?.Lock?.withLock {
            val byteBuffer1 = ByteBuffer.allocateDirect(vertices.size * 4)
            byteBuffer1.order(ByteOrder.nativeOrder())
            mVertexBuffer = byteBuffer1.asFloatBuffer()
            mVertexBuffer?.put(vertices)
            mVertexBuffer?.position(0)
            val byteBuffer2 = ByteBuffer.allocateDirect(indices.size * 2)
            byteBuffer2.order(ByteOrder.nativeOrder())
            mIndexBuffer = byteBuffer2.asShortBuffer()
            mIndexBuffer?.put(indices)
            mIndexBuffer?.position(0)
            mNumIndices = indices.size
            mIsInitialized = true
        }
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (!mIsInitialized || !gGZ.Camera.isVisible(this) || !IsVisible || !mIsMoved || !mIsSized)
            return
        gGZ.Renderer?.Lock?.withLock {
            val program = gGZ.Renderer?.PrimitiveProgram ?: 0
            GLES20.glUseProgram(program)
            val hPosition = GLES20.glGetAttribLocation(program, "vec4_position")
            val hMVPMatrix = GLES20.glGetUniformLocation(program, "mat4_mvp")
            val hColor = GLES20.glGetUniformLocation(program, "vec4_color")
            GLES20.glUniform4fv(hColor, 1, floatArrayOf(Color[0], Color[1], Color[2], Alpha), 0)
            GLES20.glEnableVertexAttribArray(hPosition)
            GLES20.glVertexAttribPointer(hPosition, 3, GLES20.GL_FLOAT, false, 0, mVertexBuffer)
            val model = FloatArray(16)
            Matrix.setIdentityM(model, 0)
            if (Angle != 0f)
                Matrix.rotateM(model, 0, Angle, 0f, 0f, 1f)
            Matrix.translateM(model, 0, model, 0, mX, mY, 0f)
            Matrix.scaleM(model, 0, model, 0, mWidth, mHeight, 0f)
            val mvpMatrix = FloatArray(16)
            if (IsStatic)
                Matrix.multiplyMM(mvpMatrix, 0, gGZ.Renderer?.StaticViewMatrix, 0, model, 0)
            else
                Matrix.multiplyMM(mvpMatrix, 0, gGZ.Renderer?.DynamicViewMatrix, 0, model, 0)
            Matrix.multiplyMM(mvpMatrix, 0, gGZ.Renderer?.ProjectionMatrix, 0, mvpMatrix, 0)
            GLES20.glUniformMatrix4fv(hMVPMatrix, 1, false, mvpMatrix, 0)
            GLES20.glDrawElements(mGLDrawMode, mNumIndices, GLES20.GL_UNSIGNED_SHORT, mIndexBuffer)
            GLES20.glDisableVertexAttribArray(hPosition)
        }
    }

    override fun toString(): String {
        val rect = Rect
        return "{ position (${rect.X}, ${rect.Y}), dimensions [${rect.Width} x ${rect.Height}], color ${Color.contentToString()}, angle $Angle, $mShape }"
    }

    private fun createCircle(thetaStart: Double, thetaStop: Double, count: Int, vertices: FloatArray, indices: ShortArray) {
        for (i in 0 until count) {
            val theta = thetaStart + i.toDouble() * ((thetaStop - thetaStart) / (count - 1).toDouble())
            vertices[3 * i + 3] = (cos(theta).toFloat() + 1f) / 2f
            vertices[3 * i + 4] = (sin(theta).toFloat() + 1f) / 2f
            vertices[3 * i + 5] = 0f // Z coordinate
            indices[i + 1] = (i + 1).toShort()
        }
    }
}