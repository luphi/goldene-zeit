package luphi.goldenezeit.gl

import luphi.goldenezeit.gGZ
import java.nio.ByteBuffer
import java.nio.ByteOrder
import kotlin.concurrent.withLock

@Suppress("PropertyName", "EnumEntryName")
class GLCharacter : GLSprite() {
    var Letter: Char = 'A'
        set(letter) {
            gGZ.Renderer?.Lock?.withLock {
                field = letter
                val glyph: Character
                when (letter) {
                    'A' -> glyph = Character.A
                    'B' -> glyph = Character.B
                    'C' -> glyph = Character.C
                    'D' -> glyph = Character.D
                    'E' -> glyph = Character.E
                    'F' -> glyph = Character.F
                    'G' -> glyph = Character.G
                    'H' -> glyph = Character.H
                    'I' -> glyph = Character.I
                    'J' -> glyph = Character.J
                    'K' -> glyph = Character.K
                    'L' -> glyph = Character.L
                    'M' -> glyph = Character.M
                    'N' -> glyph = Character.N
                    'O' -> glyph = Character.O
                    'P' -> glyph = Character.P
                    'Q' -> glyph = Character.Q
                    'R' -> glyph = Character.R
                    'S' -> glyph = Character.S
                    'T' -> glyph = Character.T
                    'U' -> glyph = Character.U
                    'V' -> glyph = Character.V
                    'W' -> glyph = Character.W
                    'X' -> glyph = Character.X
                    'Y' -> glyph = Character.Y
                    'Z' -> glyph = Character.Z
                    'Ä' -> glyph = Character.umlaut_A
                    'Ö' -> glyph = Character.umlaut_O
                    'Ü' -> glyph = Character.umlaut_U
                    'a' -> glyph = Character.a
                    'b' -> glyph = Character.b
                    'c' -> glyph = Character.c
                    'd' -> glyph = Character.d
                    'e' -> glyph = Character.e
                    'f' -> glyph = Character.f
                    'g' -> glyph = Character.g
                    'h' -> glyph = Character.h
                    'i' -> glyph = Character.i
                    'j' -> glyph = Character.j
                    'k' -> glyph = Character.k
                    'l' -> glyph = Character.l
                    'm' -> glyph = Character.m
                    'n' -> glyph = Character.n
                    'o' -> glyph = Character.o
                    'p' -> glyph = Character.p
                    'q' -> glyph = Character.q
                    'r' -> glyph = Character.r
                    's' -> glyph = Character.s
                    't' -> glyph = Character.t
                    'u' -> glyph = Character.u
                    'v' -> glyph = Character.v
                    'w' -> glyph = Character.w
                    'x' -> glyph = Character.x
                    'y' -> glyph = Character.y
                    'z' -> glyph = Character.z
                    '0' -> glyph = Character.zero
                    '1' -> glyph = Character.one
                    '2' -> glyph = Character.two
                    '3' -> glyph = Character.three
                    '4' -> glyph = Character.four
                    '5' -> glyph = Character.five
                    '6' -> glyph = Character.six
                    '7' -> glyph = Character.seven
                    '8' -> glyph = Character.eight
                    '9' -> glyph = Character.nine
                    'ä' -> glyph = Character.umlaut_a
                    'ö' -> glyph = Character.umlaut_o
                    'ü' -> glyph = Character.umlaut_u
                    'ẞ', 'ß' -> glyph = Character.eszett // Upper and lower case (because they look the same)
                    '!' -> glyph = Character.exclamation
                    '&' -> glyph = Character.ampersand
                    '\'' -> glyph = Character.single_quote
                    '\"' -> glyph = Character.double_quote
                    '(' -> glyph = Character.left_parenthesis
                    ')' -> glyph = Character.right_parenthesis
                    '[' -> glyph = Character.left_bracket
                    ']' -> glyph = Character.right_bracket
                    '+' -> glyph = Character.plus
                    ',' -> glyph = Character.comma
                    '-' -> glyph = Character.dash
                    '.' -> glyph = Character.period
                    '/' -> glyph = Character.slash
                    ':' -> glyph = Character.colon
                    ';' -> glyph = Character.semicolon
                    '?' -> glyph = Character.question
                    '_' -> glyph = Character.underscore
                    '<' -> glyph = Character.less_than
                    '>' -> glyph = Character.greater_than
                    '%' -> glyph = Character.percent
                    '$' -> glyph = Character.currency
                    else -> glyph = Character.A
                }
                setTextureCoordinates(glyph)
            }
        }

    init {
        IsStatic = true
        setTextureCoordinates(Character.A)
    }

    fun init(character: Char) {
        if (mIsInitialized)
            return
        if (character == ' ') {
            mIsInitialized = true
            // By exiting here, mTexture will remain null (its initial value derived from GLSprite) andGLSprite's draw() method will exit immediately due to the
            // lack of a texture
            return
        }
        gGZ.Renderer?.Lock?.withLock {
            if (mTexture == null)
                super.init(gGZ.Resources.loadTexture("spritesheets/text.png"))
            mWidth = gGZ.FontSize
            mHeight = gGZ.FontSize
            Letter = character
        }
        mIsSized = true // Defaults to FontSize for both dimensions
        mIsInitialized = true
    }

    override fun sizeByWidth(w: Float) {
        size(w, w)
    }

    override fun sizeByHeight(h: Float) {
        size(h, h)
    }

    private fun setTextureCoordinates(character: Character) {
        val index = character.ordinal
        val x = (index % Character.values().size).toFloat() / Character.values().size.toFloat()
        val y = 0f
        val w = 1f / Character.values().size.toFloat()
        val h = 1f
        val textureCoordinates = floatArrayOf(
            x, y,         // Top left
            x + w, y,     // Top right
            x + w, y + h, // Bottom right
            x, y + h      // Bottom left
        )
        val byteBuffer = ByteBuffer.allocateDirect(textureCoordinates.size * 4)
        byteBuffer.order(ByteOrder.nativeOrder())
        gGZ.Renderer?.Lock?.withLock {
            mTextureCoordinateBuffer = byteBuffer.asFloatBuffer()
            mTextureCoordinateBuffer.put(textureCoordinates)
            mTextureCoordinateBuffer.position(0)
        }
    }

    private enum class Character {
        A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
        umlaut_A, umlaut_O, umlaut_U,
        a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z,
        umlaut_a, umlaut_o, umlaut_u, eszett,
        zero, one, two, three, four, five, six, seven, eight, nine,
        exclamation, ampersand,
        single_quote, double_quote,
        left_parenthesis, right_parenthesis,
        left_bracket, right_bracket,
        plus, comma, dash, period, slash, colon, semicolon, question, underscore,
        less_than, greater_than, percent, currency
    }
}