package luphi.goldenezeit.item

import luphi.goldenezeit.GZ
import luphi.goldenezeit.`interface`.IEquipment

@Suppress("PropertyName")
class ITArmor(
    override val ID: Int,
    override val Name: String,
    override val Description: String,
    override val Cost: Int,
    val Bonus: Int,
    val Effect: ArmorEffect,
    val Style: GZ.DefenseStyle,
    val PreventsBleed: Boolean,
    val PreventsBlind: Boolean,
    val PreventsStun: Boolean,
    val PreventsFear: Boolean
) :
    Item(ID, ItemCategory.ARMOR, Name, Description, Cost), IEquipment {

    enum class ArmorEffect { NONE, VITALITY_PLUS_FIVE, REVERSE_BLEED, NULLIFY_CRUSH, NULLIFY_SLASH, NULLIFY_AILING }

    override val StrengthBonus: Int
        get() = 0
    override val VitalityBonus: Int
        get() = Bonus
    override val AgilityBonus: Int
        get() = 0
    override val SenseBonus: Int
        get() = 0
}
