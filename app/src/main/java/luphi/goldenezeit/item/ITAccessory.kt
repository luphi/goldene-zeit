package luphi.goldenezeit.item

import luphi.goldenezeit.`interface`.IEquipment

@Suppress("PropertyName")
class ITAccessory(
    override val ID: Int,
    override val Name: String,
    override val Description: String,
    override val Cost: Int,
    val Effect1: AccessoryEffect,
    val Effect2: AccessoryEffect,
    val Effect3: AccessoryEffect,
    val Effect4: AccessoryEffect
) :
    Item(ID, ItemCategory.ACCESSORY, Name, Description, Cost), IEquipment {

    enum class AccessoryEffect {
        NONE,
        // TODO
    }

    override val StrengthBonus: Int
        get() = 0
    override val VitalityBonus: Int
        get() = 0
    override val AgilityBonus: Int
        get() = 0
    override val SenseBonus: Int
        get() = 0
}