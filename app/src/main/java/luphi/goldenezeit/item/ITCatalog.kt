package luphi.goldenezeit.item

import java.util.*
import kotlin.collections.HashMap

@Suppress("PropertyName")
class ITCatalog {
    private val mEntries = HashMap<Int, Item>()

    val Keys: List<Int>
        get() = LinkedList(mEntries.keys)

    fun get(key: Int): Item? {
        if (!mEntries.containsKey(key))
            return null
        return mEntries[key]
    }

    fun set(key: Int, item: Item) {
        mEntries[key] = item
    }
}