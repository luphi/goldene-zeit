package luphi.goldenezeit.item

import luphi.goldenezeit.gGZ
import luphi.goldenezeit.mode.MBattle
import luphi.goldenezeit.mode.MOverworld

@Suppress("PropertyName")
open class Item(open val ID: Int, val Category: ItemCategory, open val Name: String, open val Description: String, open val Cost: Int) {
    enum class ItemCategory {
        POTION, // Restore HP to varying degrees
        BUFF, // Give advantages in battle, usable for only one battle
        OFFENSE, // Single-use, offensive battle items
        DEFENSE, // Single-use, defensive battle items
        HELD, // Have effects while being held in the inventory
        VITAMIN, // Permanent stat increasers
        SPECIAL,
        WEAPON, // Equipable weapons (BTWeapon)
        ARMOR, // Equipable armor (BTArmor)
        ACCESSORY // Equipable accessory (BTAccessory)
    }

    val CanUse: Boolean // True if the item can be "used" from an inventory menu (e.g. a knife can be used in battle but not walking around the overworld)
        get() = when (Category) {
            ItemCategory.BUFF, ItemCategory.OFFENSE, ItemCategory.DEFENSE -> gGZ.Mode is MBattle // Only usable from battle
            ItemCategory.HELD -> false // These items simply need to be held to have an effect
            ItemCategory.WEAPON, ItemCategory.ARMOR, ItemCategory.ACCESSORY -> false // These are equipped, not used
            ItemCategory.VITAMIN -> gGZ.Mode is MOverworld // Only usable from the overworld
            ItemCategory.POTION, ItemCategory.SPECIAL -> true // Potions may still be unusable if HP is full (the respective modes will handle it)
        }
    val IsConsumed: Boolean // True if there should be one less item after being used
        get() = when (Category) {
            ItemCategory.POTION, ItemCategory.BUFF, ItemCategory.OFFENSE, ItemCategory.DEFENSE, ItemCategory.VITAMIN -> true
            ItemCategory.HELD, ItemCategory.SPECIAL, ItemCategory.WEAPON, ItemCategory.ARMOR, ItemCategory.ACCESSORY -> false
        }

    override fun toString(): String = "{ ID = $ID, Cost = $Cost, Category = $Category, Name = \"$Name\", Description = \"$Description\" }"
}