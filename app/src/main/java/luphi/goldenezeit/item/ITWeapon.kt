package luphi.goldenezeit.item

import luphi.goldenezeit.`interface`.IEquipment

@Suppress("PropertyName")
class ITWeapon(
    override val ID: Int,
    override val Name: String,
    override val Description: String,
    override val Cost: Int,
    val Bonus: Int,
    val Effect: WeaponEffect,
    val CanCauseBleed: Boolean,
    val CanBlind: Boolean,
    val CanStun: Boolean,
    val CanCauseFear: Boolean
) :
    Item(ID, ItemCategory.WEAPON, Name, Description, Cost), IEquipment {

    enum class WeaponEffect { NONE, EXTRA_REST, SOUL_DRAIN }

    override val StrengthBonus: Int
        get() = Bonus
    override val VitalityBonus: Int
        get() = 0
    override val AgilityBonus: Int
        get() = 0
    override val SenseBonus: Int
        get() = 0
}