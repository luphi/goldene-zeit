package luphi.goldenezeit.battle

import luphi.goldenezeit.GZ
import luphi.goldenezeit.Rect
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.gl.GLSprite
import luphi.goldenezeit.item.ITArmor

@Suppress("PropertyName")
open class BTCombatant(protected var mSpriteFile: String) : IDrawable {
    // IDrawable property
    override val Rect: Rect
        get() = mSprite.Rect

    private val mSprite = GLSprite()

    var Level = 1
        set(value) {
            field = value
            HP = MaxHP
        }
    var HP = 0
        set(value) {
            field = value
            if (field > MaxHP)
                field = MaxHP
        }
    open val AttackStyle: GZ.AttackStyle = GZ.AttackStyle.NONE
    open val DefenseStyle: GZ.DefenseStyle = GZ.DefenseStyle.NONE
    open val MaxHP = 0
    open val Strength = 0
    open val Vitality = 0
    open val Agility = 0
    open val Sense = 0
    open val CanBleed = true
    open val CanBeBlinded = true
    open val CanBeStunned = true
    open val CanBeAfraid = true
    var IsBleeding = false
    var IsBlind = false
    var IsStunned = false
    var IsAfraid = false
    var IsGuarded = false
    var IsVisible = mSprite.IsVisible
        set(value) {
            field = value
            mSprite.IsVisible = value
        }

    init {
        mSprite.IsStatic = true
    }

    fun init() {
        mSprite.init(gGZ.Resources.loadTexture(mSpriteFile))
    }

    override fun draw(dt: Long) {
        mSprite.draw(dt)
    }

    fun size(rect: Rect) {
        mSprite.size(rect)
    }

    fun size(w: Float, h: Float) {
        mSprite.size(w, h)
    }

    fun move(rect: Rect) {
        mSprite.move(rect)
    }

    fun move(x: Float, y: Float) {
        mSprite.move(x, y)
    }

    fun translate(dx: Float, dy: Float) {
        mSprite.translate(dx, dy)
    }
}