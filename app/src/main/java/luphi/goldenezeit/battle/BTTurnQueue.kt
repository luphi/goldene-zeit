package luphi.goldenezeit.battle

import java.util.Random
import kotlin.math.max

@Suppress("PropertyName")
class BTTurnQueue(private val mPlayer: BTCombatant, private val mOpponent: BTCombatant) {
    private var mPlayerExhaustion = 0f
    private var mOpponentExhaustion = 0f
    private val mPlayerFatiguePerTurn: Float
    private val mOpponentFatiguePerTurn: Float

    val Next: BTCombatant
        get() {
            val combatant = if (mPlayerExhaustion != mOpponentExhaustion)
                if (mPlayerExhaustion < mOpponentExhaustion) mPlayer else mOpponent
            else if (mPlayer.Agility != mOpponent.Agility)
                if (mPlayer.Agility > mOpponent.Agility) mPlayer else mOpponent
            else
                if (Random().nextBoolean()) mPlayer else mOpponent
            mPlayerExhaustion += if (combatant === mPlayer) mPlayerFatiguePerTurn else 0f
            mOpponentExhaustion += if (combatant === mOpponent) mOpponentFatiguePerTurn else 0f
            return combatant
        }

    var IsOpponentSlowed = false
        set(value) {
            if (!field)
                mOpponentExhaustion *= 2f
            field = value
        }

    init {
        val max = max(mPlayer.Agility.toFloat(), mOpponent.Agility.toFloat())
        mPlayerFatiguePerTurn = max / mPlayer.Agility.toFloat()
        mOpponentFatiguePerTurn = max / mOpponent.Agility.toFloat()
    }
}