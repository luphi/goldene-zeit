package luphi.goldenezeit.battle

import luphi.goldenezeit.GZ
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.item.ITAccessory
import luphi.goldenezeit.item.ITArmor
import luphi.goldenezeit.item.ITWeapon

@Suppress("PropertyName")
class BTPlayer : BTCombatant("" /* The sprite file will be determined later due to the player using a couple possible sprites */) {
    private val mWeapon: ITWeapon?
    private val mArmor: ITArmor?
    private val mAccessory1: ITAccessory?
    private val mAccessory2: ITAccessory?

    init {
        val player = gGZ.Player
        mWeapon = player.EquippedWeapon
        mArmor = player.EquippedArmor
        mAccessory1 = player.EquippedAccessory1
        mAccessory2 = player.EquippedAccessory2
        mSpriteFile = if (gGZ.Chapter < 2) // TODO determine the actual "Leid becomes an adult" chapter
            "sprites/battle/back/leid_child.png"
        else
            "sprites/battle/back/leid_adult.png"
        Level = player.Level
        HP = player.HP
    }

    override val DefenseStyle
        get() = gGZ.Player.EquippedArmor?.Style ?: GZ.DefenseStyle.NONE
    override val MaxHP: Int
        get() = ((gGZ.Player.HP + gGZ.Player.HPBonus).toFloat() * if (IsHealthBuffed) 1.5f else 1f).toInt()
    override val Strength: Int
        get() = ((gGZ.Player.Strength + gGZ.Player.StrengthBonus).toFloat() * if (IsStrengthBuffed) 1.5f else 1f).toInt()
    override val Vitality: Int
        get() = ((gGZ.Player.Vitality + gGZ.Player.VitalityBonus).toFloat() * if (IsVitalityBuffed) 1.5f else 1f).toInt()
    override val Agility: Int
        get() = ((gGZ.Player.Agility + gGZ.Player.AgilityBonus).toFloat() * if (IsAgilityBuffed) 1.5f else 1f).toInt()
    override val Sense: Int
        get() = ((gGZ.Player.Sense + gGZ.Player.SenseBonus).toFloat() * if (IsSenseBuffed) 1.5f else 1f).toInt()
    var PassiveGuardTurns = 0 // Number of remaining turns for which passive guard (IsGuarded = true independent of other actions) applies
    var IsHealthBuffed = false
    var IsStrengthBuffed = false
    var IsVitalityBuffed = false
    var IsAgilityBuffed = false
    var IsSenseBuffed = false
    override val CanBleed: Boolean
        get() = true // TODO (this and the others should be affected by armor and accessories)
    override val CanBeBlinded: Boolean
        get() = true // TODO
    override val CanBeStunned: Boolean
        get() = true // TODO
    override val CanBeAfraid: Boolean
        get() = true // TODO
}