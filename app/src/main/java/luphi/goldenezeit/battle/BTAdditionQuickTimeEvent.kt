package luphi.goldenezeit.battle

import luphi.goldenezeit.Addition
import luphi.goldenezeit.Input
import luphi.goldenezeit.Rect
import luphi.goldenezeit.Tasks
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gl.GLPrimitive
import java.util.LinkedList
import kotlin.collections.HashMap
import kotlin.math.abs

@Suppress("PrivatePropertyName")
class BTAdditionQuickTimeEvent(
    private val mAddition: Addition,
    private val mOnAdd: () -> Unit, // Callback to invoke upon each successful add
    private val mOnDone: (Int) -> Unit // Callback to invoke in the event of a failure, given the number of successes as a parameter
) : IDrawable, IInteractable, IProgressive {
    // IDrawable property
    override val Rect: Rect
        get() = mBackground.Rect

    private val DURATION_PER_SPINNER = 1000L // Milliseconds, the time between the appearance of a spinner and it matching the target
    private val DURATION_PER_INDICATOR = 250L // Milliseconds, the time the squares appear within the target (with the color indicating too fast, slow, etc.)
    private val INITIAL_ROTATION = 45f // Degrees, the rotation all spinners begin with

    private val mBackground = GLPrimitive() // A partially-transparent overlay that takes attention away from the battle mode UI components
    private var mTargetWidth = 0f // Width (and height) of the target quad, compared against spinners' widths
    private val mTarget = GLPrimitive() // The target quad, indicates the time to press the button when the spinner overlaps it
    private val mSpinners = HashMap<Int, GLPrimitive>() // Task ID -> graphical spinner
    private val mIndicators = LinkedList<GLPrimitive>()
    private val mTasks = Tasks() // Various tasks that manage the size, rotations, etc. of the spinners
    private var mSuccessCount = 0 // Number of successful "adds," compared against the total to know when the addition is a complete success

    init {
        mBackground.IsStatic = true
        mBackground.Color = floatArrayOf(1f, 1f, 1f) // White
        mBackground.Alpha = 0.75f
        mTarget.IsStatic = true
        mTarget.Color = floatArrayOf(0f, 0f, 1f) // Blue
    }

    fun init() {
        mBackground.init(GLPrimitive.Shape.QUAD)
        mTarget.init(GLPrimitive.Shape.QUAD_OUTLINE)
        val frequency = 50f // Hz
        val dT = (1000f / frequency).toLong() // Milliseconds, time step
        val dTheta = (180f + abs(INITIAL_ROTATION % 90)) / frequency // Degrees, rotation (angle) step
        for (delay in mAddition.Adds)
            mTasks.scheduleOneOff(delay) {
                val spinner = GLPrimitive()
                spinner.IsStatic = true
                spinner.Color = floatArrayOf(0f, 0f, 1f) // Blue
                spinner.Angle = INITIAL_ROTATION
                spinner.init(GLPrimitive.Shape.QUAD_OUTLINE)
                val backgroundRect = mBackground.Rect
                val initialWidth = backgroundRect.Width
                spinner.size(initialWidth, initialWidth)
                spinner.move(
                    backgroundRect.X + (backgroundRect.Width / 2f) - (initialWidth / 2f),
                    backgroundRect.Y + (backgroundRect.Height / 2f) - (initialWidth / 2f)
                )
                var stopwatch = 0L
                var callbackID = 0
                callbackID = mTasks.schedulePeriodic(dT) {
                    stopwatch += dT
                    spinner.Angle += dTheta
                    val width = initialWidth - ((initialWidth - mTargetWidth) * (stopwatch.toFloat() / DURATION_PER_SPINNER))
                    if (width <= 0f) {
                        mSpinners[callbackID]?.free()
                        mSpinners.remove(callbackID)
                        mTasks.cancelByID(callbackID)
                        spawnIndicator("too late") { /* onDelay */ mOnDone.invoke(mSuccessCount) }
                    }
                    spinner.size(width, width)
                    spinner.move(backgroundRect.X + (backgroundRect.Width / 2f) - (width / 2f), backgroundRect.Y + (backgroundRect.Height / 2f) - (width / 2f))
                }
                mSpinners[callbackID] = spinner
            }
    }

    override fun draw(dt: Long) {
        mBackground.draw(dt)
        val spinners = mSpinners.values.toMutableList()
        for (quad in spinners)
            quad.draw(dt)
        val indicators = mIndicators.toMutableList()
        for (indicator in indicators)
            indicator.draw(dt)
        if (mSpinners.isNotEmpty() || mIndicators.isNotEmpty())
            mTarget.draw(dt)
    }

    fun move(x: Float, y: Float) {
        mBackground.move(x, y)
        val backgroundRect = mBackground.Rect
        val targetRect = mTarget.Rect
        mTarget.move(
            backgroundRect.X + (backgroundRect.Width / 2f) - (targetRect.Width / 2f),
            backgroundRect.Y + (backgroundRect.Height / 2f) - (targetRect.Height / 2f)
        )
    }

    fun size(w: Float, h: Float) {
        mBackground.size(w, h)
        mTargetWidth = h / 8f
        mTarget.size(mTargetWidth, mTargetWidth)
        val backgroundRect = mBackground.Rect
        move(backgroundRect.X, backgroundRect.Y)
    }

    override fun handleInput(input: Input) {
        // If the input was a press/down event anywhere within the app's surface
        if (input.IsPress) {
            var closest: MutableMap.MutableEntry<Int, GLPrimitive>? = null
            for (callbackIDToSpinnerPair in mSpinners) {
                val one = mTargetWidth
                val two = callbackIDToSpinnerPair.value.Rect.Width
                val three = closest?.value?.Rect?.Width
                // If this is the first spinner or it is closer to the target than the currently-known closest
                if ((closest == null) || (abs(one - two) < abs(one - three!!)))
                    closest = callbackIDToSpinnerPair
            }
            if (closest != null) {
                mTasks.cancelByID(closest.key)
                mSpinners[closest.key]?.free()
                mSpinners.remove(closest.key)
                val one = mTargetWidth
                val two = closest.value.Rect.Width
                // If the difference in widths, between this spinner and the target, is less than 50%
                if (abs((one - two) / one) < 0.5f) {
                    mOnAdd.invoke()
                    mSuccessCount += 1
                    if (mSuccessCount == mAddition.Adds.size)
                        spawnIndicator("success") { /* onDelay */ mOnDone.invoke(mSuccessCount) }
                    else
                        spawnIndicator("success") { /* onDelay */ /* no op */ }
                } else if (closest.value.Rect.Width < mTargetWidth)
                    spawnIndicator("too late") { /* onDelay */ mOnDone.invoke(mSuccessCount) }
                else
                    spawnIndicator("too soon") { /* onDelay */ mOnDone.invoke(mSuccessCount) }
            }
        }
    }

    override fun step(dt: Long) = mTasks.step(dt)

    private fun spawnIndicator(type: String, onDelay: () -> Unit) {
        val quad = GLPrimitive()
        quad.IsStatic = true
        quad.Alpha = 0.25f
        quad.init(GLPrimitive.Shape.QUAD)
        val targetRect = mTarget.Rect
        quad.size(targetRect)
        quad.move(targetRect)
        when (type) {
            "success" -> quad.Color = floatArrayOf(0f, 0f, 1f) // Blue
            "too soon" -> quad.Color = floatArrayOf(0.5f, 0.5f, 0.5f) // Gray
            "too late" -> quad.Color = floatArrayOf(1f, 0f, 0f) // Red
        }
        mIndicators.add(quad)
        mTasks.scheduleOneOff(DURATION_PER_INDICATOR) {
            quad.free()
            mIndicators.remove(quad)
            onDelay.invoke()
        }
    }
}