package luphi.goldenezeit.battle

import luphi.goldenezeit.gGZ

@Suppress("PropertyName")
class BTLimit(val ID: Limit) {
    enum class Limit { MUTANGRIFF, KRUEZSCHLAG, MUT, LEBENSPULS, MUNDGERUCH, OMNISCHLAG }

    val AvailableAtLevel: Int
        get() {
            return when (ID) {
                Limit.MUTANGRIFF -> 1
                Limit.KRUEZSCHLAG -> 10
                Limit.MUT -> 20
                Limit.LEBENSPULS -> 30
                Limit.MUNDGERUCH -> 40
                Limit.OMNISCHLAG -> 50
            }
        }
    val Description: String
        get() {
            return when (ID) {
                Limit.MUTANGRIFF -> "Deals 3x base damage with 100% accuracy"
                Limit.KRUEZSCHLAG -> "Deals damage equal to half of the opponents remaining HP"
                Limit.MUT -> "Buff ${gGZ.Player.Name}'s health, strength, vitality, agility, and sense"
                Limit.LEBENSPULS -> "Completely heal ${gGZ.Player.Name} and passively guard for 3 turns"
                Limit.MUNDGERUCH -> "Induce all status ailments and ignore immunities"
                Limit.OMNISCHLAG -> "Perform many consecutive attacks if you're skilled enough"
            }
        }
    val Name: String
        get() {
            return when (ID) {
                Limit.MUTANGRIFF -> "MUTANGRIFF"
                Limit.KRUEZSCHLAG -> "KRUEZSCHLAG"
                Limit.MUT -> "MUT"
                Limit.LEBENSPULS -> "LEBENSPULS"
                Limit.MUNDGERUCH -> "MUNDGERUCH"
                Limit.OMNISCHLAG -> "OMNISCHLAG"
            }
        }
}