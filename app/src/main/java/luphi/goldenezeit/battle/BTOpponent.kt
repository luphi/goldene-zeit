package luphi.goldenezeit.battle

import luphi.goldenezeit.GZ
import java.util.Random

@Suppress("PropertyName", "MemberVisibilityCanBePrivate")
class BTOpponent(
    spriteFile: String,
    val ID: String,
    val Name: String,
    val HPBase: Int,
    private val mAttackStyle: GZ.AttackStyle,
    private val mDefenseStyle: GZ.DefenseStyle,
    private val mStrengthBase: Int,
    private val mVitalityBase: Int,
    private val mAgilityBase: Int,
    private val mSenseBase: Int,
    private val mExperienceBase: Int,
    private val mHPModifier: Float,
    private val mStrengthModifier: Float,
    private val mVitalityModifier: Float,
    private val mAgilityModifier: Float,
    private val mSenseModifier: Float,
    private val mExperienceModifier: Float,
    private val mCanBleed: Boolean,
    private val mCanBeBlinded: Boolean,
    private val mCanBeStunned: Boolean,
    private val mCanBeAfraid: Boolean,
    private val mDrops: Map<Int?, Float>, // Maps an item ID to the probability (0.0 to 1.0) the drop is received for which a null key represents no drop
    private val mCashLowerBound: Int, // The lowest possible amount of cash dropped after a battle
    private val mCashUpperBound: Int // The highest possible amount of cash dropped after a battle. If 0, no cash is dropped.
) : BTCombatant(spriteFile) {
    override val AttackStyle: GZ.AttackStyle
        get() = mAttackStyle
    override val DefenseStyle
        get() = mDefenseStyle
    override val MaxHP: Int
        get() = applyBaseAndModifier(HPBase, mHPModifier)
    override val Strength: Int
        get() = applyBaseAndModifier(mStrengthBase, mStrengthModifier)
    override val Vitality: Int
        get() = applyBaseAndModifier(mVitalityBase, mVitalityModifier)
    override val Agility: Int
        get() = applyBaseAndModifier(mAgilityBase, mAgilityModifier)
    override val Sense: Int
        get() = applyBaseAndModifier(mSenseBase, mSenseModifier)
    val Experience: Int
        get() = applyBaseAndModifier(mExperienceBase, mExperienceModifier)
    override val CanBleed: Boolean
        get() = mCanBleed
    override val CanBeBlinded: Boolean
        get() = mCanBeBlinded
    override val CanBeStunned: Boolean
        get() = mCanBeStunned
    override val CanBeAfraid: Boolean
        get() = mCanBeAfraid
    val ItemDrop: Int? // A null represents no item drop
        get() {
            val roll = Random().nextFloat() // Returns a value between 0.0 and 1.0
            var probabilitySum = 0f
            // For each item, determine a probability window for the item and return the item whose window the roll falls in
            // Note: the parser will have handed probabilities whose sum is 1.0
            for ((item, probability) in mDrops) {
                // For example: consider the drop map { 0 -> 0.1f, 1 -> 0.2f, null -> 0.7f }.
                // The probability windows this creates are
                //   0 ->     0.0  -  0.1   (item ID 0 with probability 0.1)
                //   1 ->     0.1  -  0.3   (item ID 1 with probability 0.2)
                //   null ->  0.3  -  1.0   (no item received with probability 0.7)
                // with the goal being to determine which window the roll (0.0 to 1.0) falls in.
                // If the roll landed in this window
                if ((roll >= probabilitySum) && (roll < probabilitySum + probability))
                    return item
                probabilitySum += probability
            }
            return null
        }
    val CashDrop: Int? // A null represents no cash drop
        get() = if (mCashUpperBound <= 0) null else Random().nextInt(mCashUpperBound - mCashLowerBound) + mCashLowerBound

    init {
        HP = MaxHP
    }

    fun copy(): BTOpponent = BTOpponent(
        mSpriteFile, ID, Name, HPBase, AttackStyle, DefenseStyle, mStrengthBase, mVitalityBase, mAgilityBase, mSenseBase, mExperienceBase, mHPModifier,
        mStrengthModifier, mVitalityModifier, mAgilityModifier, mSenseModifier, mExperienceModifier, mCanBleed, mCanBeBlinded, mCanBeStunned,
        mCanBeAfraid, mDrops, mCashLowerBound, mCashUpperBound
    )

    override fun toString(): String =
        "{ ID \"$ID\", Name \"$Name\", HP = $HP, MaxHP = $MaxHP, AttackStyle = $AttackStyle, DefenseStyle = $DefenseStyle, HP base = $HPBase, " +
                "strength base = $mStrengthBase, vitality base = $mVitalityBase, agility base = $mAgilityBase, sense base = $mSenseBase, " +
                "HP modifier = $mHPModifier, strength modifier = $mStrengthModifier, vitality modifier = $mVitalityModifier, " +
                "agility modifier = $mAgilityModifier, sense modifier = $mSenseModifier, can bleed = $CanBleed, can be blinded = $CanBeBlinded, " +
                "can be stunned = $CanBeStunned, can be afraid = $CanBeAfraid, drops = { ${mDrops.map { (k, v) -> "$k -> $v" }.joinToString(", ")} } }"

    private fun applyBaseAndModifier(base: Int, modifier: Float): Int = base + ((Level - 1).toFloat() * modifier * base.toFloat()).toInt()
}