package luphi.goldenezeit.battle

import luphi.goldenezeit.GZ
import org.xml.sax.Attributes
import org.xml.sax.helpers.DefaultHandler

@Suppress("PropertyName")
class BTOpponentParser(file: String) : DefaultHandler() {
    private val mFile = file // TODO decide if this will ever be needed
    private var mSpriteFile = "sprites/battle/front/dev.png"
    private var mCurrentTag = ""
    private var mID = ""
    private var mName = "MISSINGNO."
    private var mHPBase = 10
    private var mStrengthBase = 1
    private var mVitalityBase = 1
    private var mAgilityBase = 1
    private var mSenseBase = 1
    private var mHPModifier = 1f
    private var mStrengthModifier = 1f
    private var mVitalityModifier = 1f
    private var mAgilityModifier = 1f
    private var mSenseModifier = 1f
    private var mAttackStyle = GZ.AttackStyle.CRUSH
    private var mDefenseStyle = GZ.DefenseStyle.CHAIN
    private var mCanBleed = true
    private var mCanBeBlinded = true
    private var mCanBeStunned = true
    private var mCanBeAfraid = true
    private var mExperienceBase = 1
    private var mExperienceModifier = 1f
    private val mWeightedDrops = HashMap<Int?, Int>()
    private val mDrops = HashMap<Int?, Float>()
    private var mCashLowerBound = 0
    private var mCashUpperBound = 0

    val Opponent: BTOpponent
        get() = BTOpponent(
            mSpriteFile, mID, mName, mHPBase, mAttackStyle, mDefenseStyle, mStrengthBase, mVitalityBase, mAgilityBase, mSenseBase, mExperienceBase, mHPModifier,
            mStrengthModifier, mVitalityModifier, mAgilityModifier, mSenseModifier, mExperienceModifier, mCanBleed, mCanBeBlinded, mCanBeStunned,
            mCanBeAfraid, mDrops, mCashLowerBound, mCashUpperBound
        )

    override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
        mCurrentTag = localName
        if (mCurrentTag == "drop")
            mWeightedDrops[atts.getValue("item")?.toIntOrNull() ?: 0] = atts.getValue("weight")?.toIntOrNull() ?: 1
        else if (mCurrentTag == "none")
            mWeightedDrops[null] = atts.getValue("weight")?.toIntOrNull() ?: 1
        else if (mCurrentTag == "cash") {
            mCashLowerBound = atts.getValue("lower")?.toIntOrNull() ?: 0
            mCashUpperBound = atts.getValue("upper")?.toIntOrNull() ?: 0
        }
    }

    override fun endElement(namespaceURI: String, localName: String, qName: String) {
        mCurrentTag = ""
        if (localName == "drops") {
            var weightSum = 0
            for (weight in mWeightedDrops.values)
                weightSum += weight
            for ((item, weight) in mWeightedDrops)
                mDrops[item] = weight / weightSum.toFloat()
        }
    }

    override fun characters(ch: CharArray, start: Int, length: Int) {
        if (mCurrentTag.isEmpty())
            return
        val string = String(ch, start, length)
        when (mCurrentTag) {
            "sprite" -> mSpriteFile = string
            "id" -> mID = string
            "name" -> mName = string
            "hpbase" -> mHPBase = string.toInt()
            "strengthbase" -> mStrengthBase = string.toInt()
            "vitalitybase" -> mVitalityBase = string.toInt()
            "agilitybase" -> mAgilityBase = string.toInt()
            "sensebase" -> mSenseBase = string.toInt()
            "hpmodifier" -> mHPModifier = string.toFloat()
            "strengthmodifier" -> mStrengthModifier = string.toFloat()
            "vitalitymodifier" -> mVitalityModifier = string.toFloat()
            "agilitymodifier" -> mAgilityModifier = string.toFloat()
            "sensemodifier" -> mSenseModifier = string.toFloat()
            "canbleed" -> mCanBleed = string.toBoolean()
            "canbeblinded" -> mCanBeBlinded = string.toBoolean()
            "canbestunned" -> mCanBeStunned = string.toBoolean()
            "canbeafraid" -> mCanBeAfraid = string.toBoolean()
            "experiencebase" -> mExperienceBase = string.toInt()
            "experiencemodifier" -> mExperienceModifier = string.toFloat()
            "attackstyle" -> mAttackStyle = when (string) {
                "crush" -> GZ.AttackStyle.CRUSH
                "stab" -> GZ.AttackStyle.STAB
                else -> GZ.AttackStyle.SLASH
            }
            "defensestyle" -> mDefenseStyle = when (string) {
                "plate" -> GZ.DefenseStyle.PLATE
                "chain" -> GZ.DefenseStyle.CHAIN
                else -> GZ.DefenseStyle.LEATHER
            }
        }
    }
}