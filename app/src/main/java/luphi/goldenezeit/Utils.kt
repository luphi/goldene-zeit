package luphi.goldenezeit

import android.util.Log
import luphi.goldenezeit.card.Card
import luphi.goldenezeit.scene.SCTileset
import kotlin.math.pow
import kotlin.math.round

class Utils {
    fun hexColorToFloatArray(hex: String): FloatArray {
        var trimmedHex = hex
        val result = floatArrayOf(1f, 1f, 1f, 1f)
        if (trimmedHex.length < 9)
            return result
        // The colors are formatted like "#FFFF0000" for pure red.  The pound symbol is unnecessary and the first two hex digits correspond to the alpha value
        // which will be ignored.
        trimmedHex = trimmedHex.substring(3)
        try {
            result[0] = Integer.parseInt(trimmedHex.substring(0, 2), 16).toFloat() / 255.0f
            result[1] = Integer.parseInt(trimmedHex.substring(2, 4), 16).toFloat() / 255.0f
            result[2] = Integer.parseInt(trimmedHex.substring(4, 6), 16).toFloat() / 255.0f
        } catch (e: Exception) {
            Log.w("Util", "The input hex color \"$trimmedHex\" is incorrectly formatted and cannot be converted to a float array (OpenGL) color")
        }
        return result
    }

    fun getTilesetByGID(tilesets: List<SCTileset>, gid: Int): SCTileset? {
        // TODO there's a lot of room for optimization here
        for (tileset in tilesets) {
            if ((gid >= tileset.FirstGid) && (gid <= tileset.LastGid))
                return tileset
        }
        return null
    }

    fun isCardUnique(card: Card): Boolean = isCardUnique(card.ID)

    fun isCardUnique(cardID: Int): Boolean = cardID >= 77

    fun experienceForLevel(level: Int): Int = if (level <= 1) 0 else (4 * level * level * level / 5) // exp = (level ^ 3) * (4 / 5)
}