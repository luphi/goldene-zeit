package luphi.goldenezeit.mode

import luphi.goldenezeit.*
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IMode
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.card.Card
import luphi.goldenezeit.gl.GLPrimitive
import luphi.goldenezeit.gl.GLSprite
import luphi.goldenezeit.gl.GLText
import luphi.goldenezeit.item.ITAccessory
import luphi.goldenezeit.item.ITArmor
import luphi.goldenezeit.item.ITWeapon
import luphi.goldenezeit.ui.*
import java.util.LinkedList
import java.util.Random

class MDebug : IMode {
    // IMode property
    override var Transition: MTransition? = null

    private var mIsInitialized = false
    private val mTasks = Tasks()
    private var mMenu = UIMenu { /* onCancel */ Transition = MTransition(MTransition.Mode.MENU) }
    private val mTooltip = UIReusableDialogue()
    private var mDialogue: UIReusableDialogue? = null
    private var mShouldDeferToMenu = true
    private var mDrawable: IDrawable? = null
    private var mInteractable: IInteractable? = null
    private var mProgressive: IProgressive? = null
    private var mTransition: UITransition? = null
    private var mQuantitySelector: UIQuantitySelector? = null

    override fun init(parameters: MTransition) {
        if (mIsInitialized)
            return
        mTooltip.init()
        mMenu.init()
        mMenu.add(UIMenuEntry("OVERWORLD MODE", {
            // onSelect
            val sceneMenu = UIMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            val transition = { sceneID: String, chapter: Int, hasEncounters: Boolean ->
                gGZ.Verse = (chapter - 1) * 1000
                gGZ.Chapter = chapter
                Transition = MTransition(MTransition.Mode.OVERWORLD)
                Transition?.ForOverworld = MTransition.OverworldParameters(sceneID, EnableEncounters = hasEncounters)
                Transition?.IsDebug = true
                gGZ.Record.IsAutosaveEnabled = false
                gGZ.Player.maximize() // Set the player's stats, items, etc. to something usable
            }
            for (sceneID in gGZ.Resources.SceneCatalog.Keys)
                sceneMenu.add(UIMenuEntry(sceneID, {
                    // onSelect
                    val verseMenu = UIMenu {
                        // onCancel
                        mDrawable = sceneMenu
                        mInteractable = sceneMenu
                        mProgressive = sceneMenu
                        sceneMenu.IsFocused = true
                    }
                    for (chapter in 1..10) {
                        verseMenu.add(UIMenuEntry("CHAPTER $chapter", {
                            // onSelect
                            val encountersMenu = UIMenu {
                                // onCancel
                                mDrawable = verseMenu
                                mInteractable = verseMenu
                                mProgressive = verseMenu
                                verseMenu.IsFocused = true
                            }
                            encountersMenu.add(
                                UIMenuEntry(
                                    "ENCOUNTERS ON",
                                    { /* onSelect */ transition.invoke(sceneID, chapter, true) })
                            )
                            encountersMenu.add(
                                UIMenuEntry(
                                    "ENCOUNTERS OFF",
                                    { /* onSelect */ transition.invoke(sceneID, chapter, false) })
                            )
                            encountersMenu.init()
                            encountersMenu.autoSize()
                            encountersMenu.move(
                                gGZ.Camera.LeftEdgeWithMargin,
                                gGZ.Camera.TopEdgeWithMargin - encountersMenu.Rect.Height
                            )
                            mDrawable = encountersMenu
                            mInteractable = encountersMenu
                            mProgressive = encountersMenu
                            verseMenu.IsFocused = false
                        }))
                    }
                    verseMenu.init()
                    verseMenu.autoSize()
                    verseMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - verseMenu.Rect.Height)
                    mDrawable = verseMenu
                    mInteractable = verseMenu
                    mProgressive = verseMenu
                    sceneMenu.IsFocused = false
                }))
            sceneMenu.init()
            sceneMenu.autoSize()
            sceneMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - sceneMenu.Rect.Height)
            mDrawable = sceneMenu
            mInteractable = sceneMenu
            mProgressive = sceneMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Open any scene with a custom verse" }))
        mMenu.add(
            UIMenuEntry(
                "CARD MODE",
                {
                    // onSelect
                    val npcMenu = UIMenu {
                        // onCancel
                        mDrawable = null
                        mInteractable = null
                        mProgressive = null
                        mMenu.IsFocused = true
                    }
                    val transition: (String) -> Unit = { npcID ->
                        gGZ.Record.load(-1) // Load nothing and, due to specifically giving it -1, disable autosave
                        gGZ.Player.maximize() // Set the player's stats, items, etc. to something usable
                        Transition = MTransition(MTransition.Mode.CARD)
                        Transition!!.ForCard = MTransition.CardParameters(
                            if
                                    (npcID.isNotEmpty()) gGZ.Resources.loadNpc(gGZ.Resources.NpcCatalog[npcID])!!.Cards
                            else
                                HashSet()
                        )
                        Transition!!.IsDebug = true
                    }
                    for (npcID in gGZ.Resources.NpcCatalog.Keys) {
                        val npc = gGZ.Resources.loadNpc(gGZ.Resources.NpcCatalog[npcID])
                        // If the NPC has cards (and isn't null), add it to the list
                        if (npc?.Cards?.isNotEmpty() == true)
                            npcMenu.add(UIMenuEntry("NPC: $npcID", { /* onSelect */ transition(npcID) }))
                    }
                    npcMenu.add(UIMenuEntry("Random", { /* onSelect */ transition("") }))
                    npcMenu.init()
                    npcMenu.autoSize()
                    npcMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - npcMenu.Rect.Height)
                    mDrawable = npcMenu
                    mInteractable = npcMenu
                    mProgressive = npcMenu
                    mMenu.IsFocused = false
                },
                { /* onHover */ mTooltip.Text =
                    "Begin a card game with a specific NPC, or none, and a full deck for the player"
                })
        )
        mMenu.add(UIMenuEntry("BATTLE MODE", {
            // onSelect
            val opponentMenu = UIMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            val transition = { opponentID: String, isWeak: Boolean ->
                Transition = MTransition(MTransition.Mode.BATTLE)
                Transition!!.ForBattle =
                    MTransition.BattleParameters(OpponentID = opponentID, OpponentLevel = 1, CanEscape = false)
                Transition!!.IsDebug = true
                if (isWeak)
                    gGZ.Player.minimize() // Set the player's stats, items, etc. to something barely usable (weak)
                else
                    gGZ.Player.maximize() // Set the player's stats, items, etc. to something excessive (strong)
            }
            for (opponentID in gGZ.Resources.OpponentCatalog.Keys)
                opponentMenu.add(UIMenuEntry(opponentID, {
                    // onSelect
                    val playerMenu = UIMenu {
                        // onCancel
                        mDrawable = opponentMenu
                        mInteractable = opponentMenu
                        mProgressive = opponentMenu
                        opponentMenu.IsFocused = true
                    }
                    playerMenu.add(
                        UIMenuEntry("WEAK PLAYER",
                            { /* onSelect */ transition.invoke(opponentID, true) },
                            { /* onHover */ mTooltip.Text =
                                "Set the player's stats, items, etc. to something barely usable (weak)"
                            })
                    )
                    playerMenu.add(
                        UIMenuEntry("STRONG PLAYER",
                            { /* onSelect */ transition.invoke(opponentID, false) },
                            { /* onHover */ mTooltip.Text =
                                "Set the player's stats, items, etc. to something excessive (strong)"
                            })
                    )
                    playerMenu.init()
                    playerMenu.autoSize()
                    playerMenu.move(
                        gGZ.Camera.LeftEdgeWithMargin,
                        gGZ.Camera.TopEdgeWithMargin - playerMenu.Rect.Height
                    )
                    mDrawable = playerMenu
                    mInteractable = playerMenu
                    mProgressive = playerMenu
                    opponentMenu.IsFocused = false
                }, { /* onHover */ mTooltip.Text = "" }))
            opponentMenu.init()
            opponentMenu.autoSize()
            opponentMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - opponentMenu.Rect.Height)
            mDrawable = opponentMenu
            mInteractable = opponentMenu
            mProgressive = opponentMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Open a battle with a selectable opponent" }))
        mMenu.add(UIMenuEntry("MENU", {
            // onSelect
            val menu = UIMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            menu.init()
            var clearAddSizeAndMove = {}
            clearAddSizeAndMove = {
                menu.clear()
                menu.add(UIMenuEntry("ZERO", { /* onSelect */ }))
                menu.add(UIMenuEntry("ONE", { /* onSelect */ }))
                menu.add(UIMenuEntry("ADD", { /* onSelect */
                    menu.add(UIMenuEntry("REMOVE", {/* onSelect */ menu.remove("REMOVE") }))
                }))
                menu.add(UIMenuEntry("CLEAR", { /* onSelect */ clearAddSizeAndMove.invoke() }))
                menu.sizeByHeight(gGZ.Camera.HeightWithMargin / 2f)
                val menuRect = menu.Rect
                menu.move(-(menuRect.Width / 2f), -(menuRect.Height / 2f))
            }
            clearAddSizeAndMove.invoke()
            mDrawable = menu
            mInteractable = menu
            mProgressive = menu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Create a generic menu for testing" }))
        mMenu.add(UIMenuEntry("OVERFLOW MENU", {
            // onSelect
            val overflowMenu = UIJustifiedMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            var char = 'A'
            while (char <= 'Z')
                overflowMenu.add(UIJustifiedMenuEntry("$char$char", "$char${char++}", { /* onSelect */ }))
            overflowMenu.init()
            overflowMenu.autoSize()
            val overflowMenuRect = overflowMenu.Rect
            overflowMenu.move(-(overflowMenuRect.Width / 2f), -(overflowMenuRect.Height / 2f))
            mDrawable = overflowMenu
            mInteractable = overflowMenu
            mProgressive = overflowMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Create a (justified) menu with more entries than can be shown at once" }))
        mMenu.add(UIMenuEntry("JUSTIFIED MENU", {
            // onSelect
            val justifiedMenu = UIJustifiedMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            justifiedMenu.init()
            val useTwoLines = Random().nextBoolean()
            justifiedMenu.add(
                UIJustifiedMenuEntry("ONE", "TWO", { /* onSelect */ }, { /* onHover */ }, useTwoLines)
            )
            justifiedMenu.add(
                UIJustifiedMenuEntry("THREE", "FOUR", { /* onSelect */ }, { /* onHover */ }, useTwoLines)
            )
            justifiedMenu.add(
                UIJustifiedMenuEntry("FIVE", "SIX", { /* onSelect */ }, { /* onHover */ }, useTwoLines)
            )
            justifiedMenu.add(
                UIJustifiedMenuEntry("SEVEN", "EIGHT", { /* onSelect */ }, { /* onHover */ }, useTwoLines)
            )
            justifiedMenu.autoSize()
            justifiedMenu.size(gGZ.Camera.WidthWithMargin * 0.6f, justifiedMenu.Rect.Height)
            val justifiedMenuRect = justifiedMenu.Rect
            justifiedMenu.move(-(justifiedMenuRect.Width / 2f), -(justifiedMenuRect.Height / 2f))
            mDrawable = justifiedMenu
            mInteractable = justifiedMenu
            mProgressive = justifiedMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Create a generic justified menu for testing" }))
        mMenu.add(UIMenuEntry("INVENTORY MENU", {
            // onSelect
            val inventoryMenu = UIInventoryMenu(
                UIInventoryMenu.Behavior.values()[Random().nextInt(UIInventoryMenu.Behavior.values().size)], {
                    // onCancel
                    mDrawable = null
                    mInteractable = null
                    mProgressive = null
                    mMenu.IsFocused = true
                }
            )
            inventoryMenu.init()
            inventoryMenu.add(UIInventoryMenuEntry("KNIFE", 15, { /* onUse */ }, { /* onToss */ }))
            inventoryMenu.add(UIInventoryMenuEntry("SPOON", 3, { /* onUse */ }, { /* onToss */ }))
            inventoryMenu.add(UIInventoryMenuEntry("KEY", /* quantity == 0 -> unique */ 0, { /* onUse */ }))
            inventoryMenu.add(UIInventoryMenuEntry("HOTDOG", 1, { /* onUse */ }, { /* onToss */ }))
            inventoryMenu.autoSize()
            inventoryMenu.move(gGZ.Camera.LeftEdge + gGZ.Camera.Margin, gGZ.Camera.BottomEdge + gGZ.Camera.Margin)
            mDrawable = inventoryMenu
            mInteractable = inventoryMenu
            mProgressive = inventoryMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Create a generic inventory menu, with randomized behavior, for testing" }))
        mMenu.add(UIMenuEntry("OPTIONS MENU", {
            // onSelect
            val optionsMenu = UIOptionMenu({
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            })
            optionsMenu.init()
            optionsMenu.add(
                UIOptionMenuEntry("ANIMATIONS", arrayOf("ON", "OFF"), 0, { /* onChanged */ }, /* onHover */{})
            )
            optionsMenu.add(
                UIOptionMenuEntry("CARDS", arrayOf("EASY", "HARD", "BEST"), 0, { /* onChanged */ }, /* onHover */{})
            )
            optionsMenu.add(UIOptionMenuEntry("DEV", arrayOf("ON", "OFF"), 1, { /* onChanged */ }, /* onHover */ {}))
            optionsMenu.autoSize()
            optionsMenu.size(
                gGZ.Camera.WidthWithMargin * 0.75f,
                optionsMenu.Rect.Height
            ) // Resize in order to test both sizing options
            optionsMenu.move(gGZ.Camera.LeftEdge + gGZ.Camera.Margin, gGZ.Camera.BottomEdge + gGZ.Camera.Margin)
            mDrawable = optionsMenu
            mInteractable = optionsMenu
            mProgressive = optionsMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Create a generic options menu for testing" }))
        mMenu.add(UIMenuEntry("SAVE MENU", {
            // onSelect
            val saveMenu = UISaveLoadMenu(UISaveLoadMenu.Behavior.SAVE, {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }, {/* onSelect */ })
            saveMenu.init()
            mDrawable = saveMenu
            mInteractable = saveMenu
            mProgressive = saveMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Create a standard save menu that does not save games" }))
        mMenu.add(UIMenuEntry("LOAD MENU", {
            // onSelect
            val loadMenu = UISaveLoadMenu(UISaveLoadMenu.Behavior.LOAD, {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }, {/* onSelect */ })
            loadMenu.init()
            mDrawable = loadMenu
            mInteractable = loadMenu
            mProgressive = loadMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Create a standard load menu that does not load games" }))
        mMenu.add(UIMenuEntry("DIALOGUE", {
            // onSelect
            val createDialogue = { words: String ->
                val originalDialogue = mDialogue
                val dialogue = UIDialogue {
                    // onDone
                    mDialogue = originalDialogue
                    mInteractable = mMenu
                    mProgressive = mMenu
                    mMenu.IsFocused = true
                }
                dialogue.init(words)
                mDialogue = dialogue
                mInteractable = dialogue
                mProgressive = null
                mDrawable = null
            }
            val wordsMenu = UIMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            wordsMenu.add(UIMenuEntry("1 WORD", { /* onSelect */ createDialogue.invoke("LOREM") }))
            wordsMenu.add(UIMenuEntry("10 WORDS", {
                // onSelect
                createDialogue.invoke("LOREM IPSUM DOLOR SIT AMET CONSECTETUR ADIPISCING ELIT SED DO")
            }))
            wordsMenu.init()
            wordsMenu.autoSize()
            wordsMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - wordsMenu.Rect.Height)
            mDrawable = wordsMenu
            mInteractable = wordsMenu
            mProgressive = wordsMenu
            mMenu.IsFocused = false
        }))
        mMenu.add(UIMenuEntry("TITLE SCREEN", {
            // onSelect
            val originalChapter = gGZ.Chapter
            gGZ.Chapter = 1
            val titleScreen = UITitleScreen {
                // onDone
                mDrawable = null
                mInteractable = null
                gGZ.Chapter = originalChapter
            }
            titleScreen.init()
            mDrawable = titleScreen
            mInteractable = titleScreen
        }, { /* onHover */ mTooltip.Text = "Display a title screen for chapter 1" }))
        mMenu.add(UIMenuEntry("CARD SELECTOR", {
            // onSelect
            val opponentDeck = LinkedList<Card>()
            val playerDeck = LinkedList<Card>()
            val random = Random()
            val cardCatalog: Catalog = gGZ.Resources.CardCatalog
            for (i in 0..9) {
                val card =
                    gGZ.Resources.loadCard(cardCatalog.Keys[random.nextInt(cardCatalog.Keys.size - 1)].toInt()).copy()
                card.sizeByWidth(gGZ.Camera.Width / 6f)
                if (i < 5) {
                    card.IsPlayerOwned = true
                    playerDeck.add(card)
                } else {
                    card.IsPlayerOwned = false
                    opponentDeck.add(card)
                }
            }
            val selector = UICardSelector(playerDeck, opponentDeck, random.nextBoolean()) {
                // onSelect
                mShouldDeferToMenu = true
                mDrawable = null
                mInteractable = null
                mProgressive = null
            }
            selector.init()
            mShouldDeferToMenu = false
            mDrawable = selector
            mInteractable = selector
            mProgressive = selector
        }, { /* onHover */ mTooltip.Text = "Generate a card selector with randomized cards and win status" }))
        mMenu.add(UIMenuEntry("INCREMENT VERSE", {
            // onSelect
            gGZ.Verse += 1
            val text = GLText()
            text.init("Verse " + gGZ.Verse)
            val textRect = text.Rect
            text.move(-(textRect.Width / 2f), -(textRect.Height / 2f))
            gGZ.Camera.lookAt(text)
            mDrawable = text
            delayedReturnToMenu()
        }, { /* onHover */ mTooltip.Text = "Increment the current verse in memory by 1" }))
        mMenu.add(
            UIMenuEntry(
                "REMOVE ENTRY",
                { /* onSelect */ mMenu.remove(mMenu.CurrentlySelected) },
                { /* onHover */ mTooltip.Text = "Remove this entry from this menu" })
        )
        mMenu.add(UIMenuEntry("LOAD SHOP", {
            // onSelect
            val shop = UIShop(gGZ.Resources.loadShop(0), { /* onBuy */ }, { /* onSell */ }, {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            })
            shop.init()
            mDrawable = shop
            mInteractable = shop
            mProgressive = shop
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Create a shop menu using the dev shop XML" }))
        mMenu.add(UIMenuEntry("LOAD ITEM", {
            // onSelect
            val idMenu = UIMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            val itemsIds = gGZ.Resources.ItemCatalog.Keys.map { it.toInt() }
            for (itemID in itemsIds.sorted()) {
                idMenu.add(UIMenuEntry("$itemID", {
                    // onSelect
                    val item = gGZ.Resources.loadItem(itemID)
                    val text = GLText()
                    text.init("\"${item.Name}\"")
                    val textRect = text.Rect
                    text.move(-(textRect.Width / 2f), -(textRect.Height / 2f))
                    gGZ.Camera.lookAt(text)
                    mDrawable = text
                    delayedReturnToMenu()
                }))
            }
            idMenu.init()
            idMenu.autoSize()
            idMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - idMenu.Rect.Height)
            mDrawable = idMenu
            mInteractable = idMenu
            mProgressive = idMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Parse an item with a selectable ID and display its name" }))
        mMenu.add(UIMenuEntry("LOAD WEAPON", {
            // onSelect
            val idMenu = UIMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            val itemsIds = gGZ.Resources.ItemCatalog.Keys.map { it.toInt() }
            val weaponIds = itemsIds.filter { it in 100..199 } // Range [100, 200) is reserved for weapons
            for (weaponID in weaponIds.sorted()) {
                idMenu.add(UIMenuEntry("$weaponID", {
                    // onSelect
                    val weapon = gGZ.Resources.loadItem(weaponID) as ITWeapon
                    blurbAndDelayedReturnToMenu("${weapon.Name}\nBonus: ${weapon.Bonus}\nEffect: ${weapon.Effect}\n"+
                            "Bleed: ${weapon.CanCauseBleed}\nBlind: ${weapon.CanBlind}\nStun: ${weapon.CanStun}\n" +
                            "Fear: ${weapon.CanCauseFear}")
                }))
            }
            idMenu.init()
            idMenu.autoSize()
            idMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - idMenu.Rect.Height)
            mDrawable = idMenu
            mInteractable = idMenu
            mProgressive = idMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Parse a weapon and display its weapon-only information"}))
        mMenu.add(UIMenuEntry("LOAD ARMOR", {
            // onSelect
            val idMenu = UIMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            val itemsIds = gGZ.Resources.ItemCatalog.Keys.map { it.toInt() }
            val armorIds = itemsIds.filter { it in 200..299 } // Range [200, 300) is reserved for armors
            for (armorID in armorIds.sorted()) {
                idMenu.add(UIMenuEntry("$armorID", {
                    // onSelect
                    val armor = gGZ.Resources.loadItem(armorID) as ITArmor
                    blurbAndDelayedReturnToMenu("${armor.Name}\nBonus: ${armor.Bonus}\nEffect: ${armor.Effect}\n" +
                            "Style: ${armor.Style}\nBleed: ${armor.PreventsBleed}\nBlind: ${armor.PreventsBlind}\n" +
                            "Stun: ${armor.PreventsStun}\nFear: ${armor.PreventsFear}")
                }))
            }
            idMenu.init()
            idMenu.autoSize()
            idMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - idMenu.Rect.Height)
            mDrawable = idMenu
            mInteractable = idMenu
            mProgressive = idMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Parse an armor and display its armor-only information"}))
        mMenu.add(UIMenuEntry("LOAD ACCESSORY", {
            // onSelect
            // onSelect
            val idMenu = UIMenu {
                // onCancel
                mDrawable = null
                mInteractable = null
                mProgressive = null
                mMenu.IsFocused = true
            }
            val itemsIds = gGZ.Resources.ItemCatalog.Keys.map { it.toInt() }
            val accessoryIds = itemsIds.filter { it in 300..399 } // Range [300, 400) is reserved for accessories
            for (accessoryID in accessoryIds.sorted()) {
                idMenu.add(UIMenuEntry("$accessoryID", {
                    // onSelect
                    val accessory = gGZ.Resources.loadItem(accessoryID) as ITAccessory
                    blurbAndDelayedReturnToMenu("${accessory.Name}\nEffect 1: ${accessory.Effect1}\n" +
                            "Effect 2: ${accessory.Effect2}\nEffect 3: ${accessory.Effect3}\n" +
                            "Effect 4: ${accessory.Effect4}")
                }))
            }
            idMenu.init()
            idMenu.autoSize()
            idMenu.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - idMenu.Rect.Height)
            mDrawable = idMenu
            mInteractable = idMenu
            mProgressive = idMenu
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Parse an accessory and display its accessory-only information"}))
        mMenu.add(UIMenuEntry("LOAD ADDITION", {
            // onSelect
            val addition = gGZ.Resources.loadAddition(3)
            val text = GLText()
            text.init("\"${addition.Name}\"")
            val textRect = text.Rect
            text.move(-(textRect.Width / 2f), -(textRect.Height / 2f))
            mDrawable = text
            delayedReturnToMenu()
        }, { /* onHover */ mTooltip.Text = "Parse an addition (ID 3) and display its name" }))
        mMenu.add(UIMenuEntry("LOAD OPPONENT", {
            // onSelect
            val opponent = gGZ.Resources.loadOpponent("opponents/dev.xml")
            if (opponent != null) {
                opponent.init()
                opponent.size(gGZ.Camera.Width / 2f, gGZ.Camera.Width / 2f)
                val opponentRect = opponent.Rect
                opponent.move(-opponentRect.Width / 2f, -opponentRect.Height / 2f)
                mDrawable = opponent
                delayedReturnToMenu()
            }
        }, { /* onHover */ mTooltip.Text = "Parse and display an opponent" }))
        mMenu.add(UIMenuEntry("SPIN QUAD", {
            // onSelect
            val quad = GLPrimitive()
            quad.IsStatic = true
            quad.Color = floatArrayOf(0f, 0f, 1f)
            quad.init(GLPrimitive.Shape.QUAD_OUTLINE)
            quad.size(gGZ.Camera.Height / 2f, gGZ.Camera.Height / 4f)
            quad.move(gGZ.Camera.RightEdge, gGZ.Camera.TopEdge)
            gGZ.Camera.lookAt(quad)
            val quadRect = quad.Rect
            quad.move(-quadRect.Width / 2f, -quadRect.Height / 2f)
            quad.Angle = 0f
            val frequency = 50f // Hz
            mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                quad.Angle += (180f / frequency)
            }
            mDrawable = quad
            delayedReturnToMenu()
        }, { /* onHover */ mTooltip.Text = "Rotate a primitive quad around the center of the screen" }))
        mMenu.add(UIMenuEntry("SHOW SPRITE", {
            // onSelect
            val sprite = GLSprite()
            sprite.init(gGZ.Resources.loadTexture("sprites/battle/back/leid_adult.png"))
            sprite.sizeByHeight(gGZ.Camera.Height / 2f)
            val spriteRect = sprite.Rect
            sprite.move(-spriteRect.Width / 2f, -spriteRect.Height / 2f)
            gGZ.Camera.lookAt(sprite)
            mDrawable = sprite
            delayedReturnToMenu()
        }, { /* onHover */ mTooltip.Text = "Display an OpenGL sprite" }))
        mMenu.add(UIMenuEntry("MIRROR SPRITE", {
            // onSelect
            val sprite = GLSprite()
            sprite.IsMirrored = true
            sprite.init(gGZ.Resources.loadTexture("sprites/battle/back/leid_adult.png"))
            sprite.sizeByHeight(gGZ.Camera.Height / 2f)
            val spriteRect = sprite.Rect
            sprite.move(-spriteRect.Width / 2f, -spriteRect.Height / 2f)
            gGZ.Camera.lookAt(sprite)
            mDrawable = sprite
            delayedReturnToMenu()
        }, { /* onHover */ mTooltip.Text = "Display a horizontally-flipped OpenGL sprite" }))
        mMenu.add(UIMenuEntry("SHOW CARD", {
            // onSelect
            val card = gGZ.Resources.loadCard(0)
            card.sizeByHeight(gGZ.Camera.Height / 2f)
            val cardRect = card.Rect
            card.move(-cardRect.Width / 2f, -cardRect.Height / 2f)
            mDrawable = card
            mShouldDeferToMenu = false
            var callbackID = 0
            callbackID = mTasks.schedulePeriodic(300) {
                if (card.Level < 10) {
                    card.Level += 1
                    card.IsPlayerOwned = !card.IsPlayerOwned
                } else {
                    mTasks.cancelByID(callbackID)
                    mTasks.scheduleOneOff(500) {
                        mDrawable = null
                        mShouldDeferToMenu = true
                    }
                }
            }
        }, { /* onHover */ mTooltip.Text = "Display a card, including identicon, values, etc." }))
        mMenu.add(UIMenuEntry("SHOW NPC", {
            // onSelect
            val npc = gGZ.Resources.loadNpc("NPCs/dev.xml")
            if (npc != null) {
                npc.size(gGZ.Camera.Width / 2f, gGZ.Camera.Width / 2f)
                val npcRect = npc.Rect
                npc.move(-npcRect.Width / 2f, -npcRect.Height / 2f)
                gGZ.Camera.lookAt(npc)
                npc.IsAnimating = true
                mDrawable = npc
                mShouldDeferToMenu = false
                var i = 0
                var callbackID = 0
                callbackID = mTasks.schedulePeriodic(500) {
                    when (i) {
                        0 -> npc.Direction = GZ.Cardinal.EAST
                        1 -> npc.Direction = GZ.Cardinal.NORTH
                        2 -> npc.Direction = GZ.Cardinal.WEST
                        3 -> npc.Direction = GZ.Cardinal.SOUTH
                        4 -> {
                            npc.IsAnimating = false
                            mDrawable = null
                            mShouldDeferToMenu = true
                            mTasks.cancelByID(callbackID)
                        }
                    }
                    i += 1
                }
            }
        }, { /* onHover */ mTooltip.Text = "Display and animate an NPC and change its direction several times" }))
        mMenu.add(UIMenuEntry("SHOW BLURB", {
            // onSelect
            blurbAndDelayedReturnToMenu("YOU ARE A LITTLE SOUL\nCARRYING ABOUT A CORPSE,\nAS EPICTETUS USED TO SAY")
        }, { /* onHover */ mTooltip.Text = "Display a blurb drawable with a message" }))
        mMenu.add(UIMenuEntry("RAND. TRANSITION", {
            // onSelect
            mTransition = UITransition {
                mTransition = null
                mShouldDeferToMenu = true
                mMenu.IsFocused = true
            }
            mTransition?.init(UITransition.Type.values()[Random().nextInt(UITransition.Type.values().size)])
            mShouldDeferToMenu = false
            mMenu.IsFocused = false
        }, { /* onHover */ mTooltip.Text = "Overlay a random transition" }))
        mMenu.add(UIMenuEntry("DRAIN HEALTH", {
            // onSelect
            val healthBar = UIHealthBar()
            healthBar.init()
            healthBar.sizeByWidth(gGZ.Camera.Width * 0.75f)
            val rect = healthBar.Rect
            healthBar.move(-rect.Width / 2f, -rect.Height / 2f)
            mShouldDeferToMenu = false
            mDrawable = healthBar
            val duration = 1500L // Milliseconds
            var sum = 0L // Milliseconds
            val frequency = 50f // Hz
            val dt = (1000f / frequency).toLong()
            var callbackID = 0
            callbackID = mTasks.schedulePeriodic(dt) {
                if (sum >= (duration + 500)) {
                    mDrawable = null
                    mShouldDeferToMenu = true
                    mTasks.cancelByID(callbackID)
                }
                healthBar.Percentage = 1f - (sum.toFloat() / duration.toFloat())
                sum += dt
            }
        }, { /* onHover */ mTooltip.Text = "Display a health bar and drain it from 100% to 0%" }))
        mMenu.add(
            UIMenuEntry(
                "RESET DISK",
                { /* onSelect */ gGZ.Record.reset() },
                { /* onHover */ mTooltip.Text =
                    "Reset the data on disk saving just the principal key-value pairs. THIS WILL ERASE ALL SAVED GAMES."
                })
        )
        mMenu.add(
            UIMenuEntry(
                "EXIT",
                { /* onSelect */ Transition = MTransition(MTransition.Mode.MENU) },
                { /* onHover */ mTooltip.Text = "Return to the main menu" })
        )
        mMenu.autoSize()
        val tooltipUsableSurface = mTooltip.UsableSurface
        // Resize the menu to use the best fit width and greatest possible space between the top edge and tooltip
        // dialogue. This is done to prevent the tooltip from overlapping any important parts of the menu
        // (if it's displayed).
        mMenu.size(
            mMenu.Rect.Width,
            gGZ.Camera.TopEdgeWithMargin - tooltipUsableSurface.Y - tooltipUsableSurface.Height
        )
        val menuRect = mMenu.Rect
        mMenu.move(
            gGZ.Camera.RightEdgeWithMargin - menuRect.Width,
            tooltipUsableSurface.Y + tooltipUsableSurface.Height
        )
        mMenu.IsFocused = true
        gGZ.Renderer?.ClearColor = floatArrayOf(1f, 1f, 1f) // White
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        if (mShouldDeferToMenu || (mTransition != null))
            mMenu.draw(dt)
        mDrawable?.draw(dt)
        mQuantitySelector?.draw(dt)
        mDialogue?.draw(dt)
        mTransition?.draw(dt)
    }

    override fun step(dt: Long) {
        mTasks.step(dt)
        mMenu.step(dt)
        mProgressive?.step(dt)
        mTransition?.step(dt)
    }

    override fun handleInput(input: Input) {
        // If select was pressed (this is a special case as it's designated for the tooltip which we always want to be
        // showable/hideable)
        if (input.IsPress && (input.Opcode == Input.Button.SELECT))
            mDialogue = if (mDialogue == null) mTooltip else null
        when {
            (mInteractable != null) -> mInteractable?.handleInput(input)
            (mQuantitySelector != null) -> mQuantitySelector?.handleInput(input)
            mShouldDeferToMenu -> mMenu.handleInput(input)
        }
    }

    private fun delayedReturnToMenu() {
        mShouldDeferToMenu = false
        mTasks.scheduleOneOff(2000) {
            mDrawable = null
            mInteractable = null
            mProgressive = null
            mMenu.IsFocused = true
            mShouldDeferToMenu = true
            mTasks.clear()
        }
    }

    private fun blurbAndDelayedReturnToMenu(string: String) {
        val blurb = UIBlurb()
        blurb.init(string)
        val blurbRect = blurb.Rect
        blurb.move(-blurbRect.Width / 2f, -blurbRect.Height / 2f)
        mDrawable = blurb
        delayedReturnToMenu()
    }
}