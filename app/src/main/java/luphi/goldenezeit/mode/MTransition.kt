package luphi.goldenezeit.mode

@Suppress("PropertyName")
class MTransition(val To: Mode) {
    enum class Mode { MENU, OVERWORLD, BATTLE, CARD, DEBUG }

    class BattleParameters(val OpponentID: String, val OpponentLevel: Int, val CanEscape: Boolean = true)

    class CardParameters(val NpcCards: Set<Int>)

    class OverworldParameters(val SceneID: String, val EnableEncounters: Boolean = true, val Landing: Pair<Int, Int>? = null)

    var ForBattle: BattleParameters? = null
    var ForCard: CardParameters? = null
    var ForOverworld: OverworldParameters? = null
    var IsDebug = false
}