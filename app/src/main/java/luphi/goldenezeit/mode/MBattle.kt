package luphi.goldenezeit.mode

import luphi.goldenezeit.*
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IMode
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.battle.*
import luphi.goldenezeit.gl.GLSprite
import luphi.goldenezeit.ui.*
import java.util.*
import kotlin.math.*

@Suppress("PropertyName")
class MBattle : IMode {
    // IMode property
    override var Transition: MTransition? = null

    private var mIsInitialized = false
    private var mIsDebug = false
    private val mTasks = Tasks()
    private val mBackgroundBubble = UIBubble()
    private val mTooltip = UIReusableDialogue()
    private var mDialogue: UIReusableDialogue? = null
    private var mBattleMenu = UIBattleMenu({
        // onAttack
        mDrawable = mAttackMenu
        mInteractable = mAttackMenu
        mAttackMenu.IsFocused = true
    }, { /* onAttackHover */ mTooltip.Text = "Choose an addition to attack with" }, {
        // onGuard
        val oldHP = mPlayer.HP
        healPlayer((mPlayer.MaxHP.toFloat() * 0.1f).toInt()) // 10% of the player's maximum HP
        mPlayer.IsGuarded = true // Will be assigned to false on the player's next turn
        if (gGZ.Record.get("animations", "on") == "on")
        // Animate the guard, then the HP gain, and then proceed to the opponent's turn
            animateGuard(/* isPlayer */ true) { /* onDone */ animateHPChange(mPlayerStatus, oldHP, mPlayer.HP, { /* onDone */ nextTurn() }) }
        else {
            // Immediately change the visual representation of the player's HP and move onto the opponent's turn
            mPlayerStatus.HP = mPlayer.HP
            nextTurn()
        }
    }, { /* onGuardHover */ mTooltip.Text = "Halve damage for one turn and recover some HP" }, {
        // onItem
        val inventoryMenu = UIInventoryMenu(UIInventoryMenu.Behavior.NO_SELECTOR, {
            // onCancel
            mDrawable = mBattleMenuRef
            mInteractable = mBattleMenuRef
            mProgressive = null
            mBattleMenuRef.IsFocused = true
            mTooltip.IsBottomAligned = false // Move the tooltip back up to the top
        }, { /* onHoverCancel */ mTooltip.Text = "" })
        inventoryMenu.init()
        for (idQuantityPair in gGZ.Player.Inventory) {
            val item = gGZ.Resources.loadItem(idQuantityPair.key)
            inventoryMenu.add(
                UIInventoryMenuEntry(/* label */ item.Name.toUpperCase(Locale.getDefault()), /* quantity */ idQuantityPair.value, {
                    // onUse
                    mDrawable = null
                    mInteractable = null
                    mProgressive = null
                    mTooltip.IsBottomAligned = false // Move the tooltip back up to the top
                    // Prepare a helper routine for displaying a dialogue with a given message and proceed to the next turn
                    val showDialogueAndNext = { message: String ->
                        val originalDialogue = mDialogue
                        val dialogue = UIDialogue {
                            // onDone
                            mDialogue = originalDialogue
                            nextTurn()
                        }
                        dialogue.init(message)
                        mDialogue = dialogue
                        mInteractable = dialogue
                    }
                    // Prepare a similar helper method that tells the player "that won't work, bruh" before returning to the inventory menu
                    val showDialogueAndReturn = {
                        val originalDialogue = mDialogue
                        val dialogue = UIDialogue {
                            // onDone
                            mDialogue = originalDialogue
                            mDrawable = inventoryMenu
                            mInteractable = inventoryMenu
                            mProgressive = inventoryMenu
                        }
                        dialogue.init("It won't have any effect.")
                        mDialogue = dialogue
                        mInteractable = dialogue
                    }
                    // One last helper method to consume (remove one copy of it) if appropriate
                    val tryConsume = {
                        // If there should be one less item after being used
                        if (item.IsConsumed) {
                            // If removing this quantity of the item removed the remainder of that item
                            if (gGZ.Player.removeItem(/* id */ idQuantityPair.key, /* quantity */ 1))
                                inventoryMenu.remove(item.Name.toUpperCase(Locale.getDefault()))
                            // If there are still some remaining
                            else
                                inventoryMenu.setEntryQuantity(item.Name.toUpperCase(Locale.getDefault()), gGZ.Player.Inventory[idQuantityPair.key] ?: 0)
                        }
                    }
                    if (item.CanUse) {
                        when (item.ID) {
                            0, 1, 2, 3 -> {
                                if (mPlayer.HP != mPlayer.MaxHP) {
                                    tryConsume.invoke()
                                    val healAmount = when (item.ID) {
                                        0 /* Potion */ -> 20
                                        1 /* Super potion */ -> 50
                                        2 /* Hyper potion */ -> 200
                                        else /* Fairy dust */ -> mPlayer.MaxHP
                                    }
                                    val oldHP = mPlayer.HP
                                    healPlayer(healAmount) // Will set mPlayer.HP
                                    if (gGZ.Record.get("animations", "on") == "on")
                                        animateHPChange(mPlayerStatus, oldHP, mPlayer.HP, { /* onDone */ nextTurn() })
                                    else {
                                        mPlayerStatus.HP = mPlayer.HP
                                        nextTurn()
                                    }
                                } else
                                    showDialogueAndReturn.invoke()
                            }
                            10 /* X Health */ -> {
                                if (!mPlayer.IsHealthBuffed) {
                                    tryConsume.invoke()
                                    val oldMaxHP = mPlayer.MaxHP
                                    mPlayer.IsHealthBuffed = true // Affects MaxHP
                                    mPlayerStatus.MaxHP = mPlayer.MaxHP
                                    val oldHP = mPlayer.HP
                                    healPlayer(mPlayer.MaxHP - oldMaxHP) // Increment by the potential HP gained
                                    if (gGZ.Record.get("animations", "on") == "on")
                                        animateHPChange(mPlayerStatus, oldHP, mPlayer.HP, {
                                            /* onDone */ showDialogueAndNext.invoke("${gGZ.Player.Name}'s HEALTH rose!")
                                        })
                                    else
                                        showDialogueAndNext.invoke("${gGZ.Player.Name}'s HEALTH rose!")
                                } else
                                    showDialogueAndReturn.invoke()
                            }
                            11 /* X Strength */ -> {
                                if (!mPlayer.IsStrengthBuffed) {
                                    tryConsume.invoke()
                                    mPlayer.IsStrengthBuffed = true
                                    showDialogueAndNext.invoke("${gGZ.Player.Name}'s STRENGTH rose!")
                                } else
                                    showDialogueAndReturn.invoke()
                            }
                            12 /* X Vitality */ -> {
                                if (!mPlayer.IsVitalityBuffed) {
                                    tryConsume.invoke()
                                    mPlayer.IsVitalityBuffed = true
                                    showDialogueAndNext.invoke("${gGZ.Player.Name}'s VITALITY rose!")
                                } else
                                    showDialogueAndReturn.invoke()
                            }
                            13 /* X Agility */ -> {
                                if (!mPlayer.IsAgilityBuffed) {
                                    tryConsume.invoke()
                                    mPlayer.IsAgilityBuffed = true
                                    // With the change in agility, the turn queue needs to reinitialize to calculate some stuff (this could be refactored)
                                    mTurnQueue = BTTurnQueue(mPlayer, mOpponent!!)
                                    showDialogueAndNext.invoke("${gGZ.Player.Name}'s AGILITY rose!")
                                } else
                                    showDialogueAndReturn.invoke()
                            }
                            14 /* X Sense */ -> {
                                if (!mPlayer.IsSenseBuffed) {
                                    tryConsume.invoke()
                                    mPlayer.IsSenseBuffed = true
                                    showDialogueAndNext.invoke("${gGZ.Player.Name}'s SENSE rose!")
                                } else
                                    showDialogueAndReturn.invoke()
                            }
                            20 /* Dart */ -> {
                                val isMiss = Random().nextBoolean()
                                val doDart = {
                                    tryConsume.invoke()
                                    if (mOpponent!!.CanBleed) {
                                        if (!isMiss || mOpponent!!.IsBleeding) {
                                            mOpponent!!.IsBleeding = true
                                            showDialogueAndNext.invoke("${mOpponent!!.Name} is bleeding!")
                                        } else
                                            showDialogueAndNext.invoke("The ${item.Name.toUpperCase(Locale.getDefault())} missed!")
                                    } else
                                        showDialogueAndNext("${mOpponent!!.Name} isn't affected!")
                                }
                                if (gGZ.Record.get("animations", "on") == "on") {
                                    animateItemThrow(/* onDone */ doDart)
                                    if (isMiss)
                                        animateEvade(mOpponent!!)
                                } else
                                    doDart.invoke()
                            }
                            21 /* Sand */ -> {
                                val isMiss = Random().nextBoolean()
                                val doSand = {
                                    tryConsume.invoke()
                                    if (mOpponent!!.CanBeBlinded) {
                                        if (!isMiss || mOpponent!!.IsBlind) {
                                            mOpponent!!.IsBlind = true
                                            showDialogueAndNext.invoke("${mOpponent!!.Name} is blind!")
                                        } else
                                            showDialogueAndNext.invoke("The ${item.Name.toUpperCase(Locale.getDefault())} missed!")
                                    } else
                                        showDialogueAndNext("${mOpponent!!.Name} isn't affected!")
                                }
                                if (gGZ.Record.get("animations", "on") == "on") {
                                    animateItemThrow(/* onDone */ doSand)
                                    if (isMiss)
                                        animateEvade(mOpponent!!)
                                } else
                                    doSand.invoke()
                            }
                            22 /* Mini bomb */ -> {
                                val isMiss = Random().nextBoolean()
                                val doMiniBomb = {
                                    tryConsume.invoke()
                                    if (mOpponent!!.CanBeStunned) {
                                        if (!isMiss || mOpponent!!.IsStunned) {
                                            mOpponent!!.IsStunned = true
                                            showDialogueAndNext.invoke("${mOpponent!!.Name} is stunned!")
                                        } else
                                            showDialogueAndNext.invoke("The ${item.Name.toUpperCase(Locale.getDefault())} missed!")
                                    } else
                                        showDialogueAndNext("${mOpponent!!.Name} isn't affected!")
                                }
                                if (gGZ.Record.get("animations", "on") == "on") {
                                    animateItemThrow(/* onDone */ doMiniBomb)
                                    if (isMiss)
                                        animateEvade(mOpponent!!)
                                } else
                                    doMiniBomb.invoke()
                            }
                            23 /* Mask */ -> {
                                val isMiss = Random().nextBoolean()
                                val doMask = {
                                    tryConsume.invoke()
                                    if (mOpponent!!.CanBeAfraid) {
                                        if (!isMiss || mOpponent!!.IsAfraid) {
                                            mOpponent!!.IsAfraid = true
                                            showDialogueAndNext.invoke("${mOpponent!!.Name} is afraid!")
                                        } else
                                            showDialogueAndNext.invoke("The ${item.Name.toUpperCase(Locale.getDefault())} failed!")
                                    } else
                                        showDialogueAndNext("${mOpponent!!.Name} isn't affected!")
                                }
                                if (gGZ.Record.get("animations", "on") == "on") {
                                    animateItemThrow(/* onDone */ doMask)
                                    if (isMiss)
                                        animateEvade(mOpponent!!)
                                } else
                                    doMask.invoke()
                            }
                            24 /* Caltrops */ -> {
                                val doCaltrops = {
                                    tryConsume.invoke()
                                    mTurnQueue!!.IsOpponentSlowed = true
                                    showDialogueAndNext.invoke("${mOpponent!!.Name}'s agility fell")
                                }
                                if (gGZ.Record.get("animations", "on") == "on")
                                    animateItemThrow(/* onDone */ doCaltrops)
                                else
                                    doCaltrops.invoke()
                            }
                            25 /* Knife */ -> {
                                val isMiss = Random().nextBoolean() // Knives have a 50% chance of doing critical damage or missing
                                val doKnife = {
                                    tryConsume.invoke()
                                    if (!isMiss)
                                        doDamage(mOpponent!!, calculateDamage(mPlayer, mOpponent!!), /* isCritical */ true) { /* onDone */ nextTurn() }
                                    else
                                        showDialogueAndNext("The ${item.Name.toUpperCase(Locale.getDefault())} missed!")
                                }
                                if (gGZ.Record.get("animations", "on") == "on") {
                                    animateItemThrow(/* onDone */ doKnife)
                                    if (isMiss)
                                        animateEvade(mOpponent!!)
                                } else
                                    doKnife.invoke()
                            }
                            30 /* Bandage */ -> {
                                if (mPlayer.IsBleeding) {
                                    tryConsume.invoke()
                                    mPlayer.IsBleeding = false
                                    showDialogueAndNext("${gGZ.Player.Name} is no longer bleeding.")
                                } else
                                    showDialogueAndReturn()
                            }
                            31 /* Eye drops */ -> {
                                if (mPlayer.IsBlind) {
                                    tryConsume.invoke()
                                    mPlayer.IsBlind = false
                                    showDialogueAndNext("${gGZ.Player.Name} is no longer blinded.")
                                } else
                                    showDialogueAndReturn()
                            }
                            32 /* Smelling salts */ -> {
                                if (mPlayer.IsStunned) {
                                    tryConsume.invoke()
                                    mPlayer.IsStunned = false
                                    showDialogueAndNext("${gGZ.Player.Name} is no longer stunned.")
                                } else
                                    showDialogueAndReturn()
                            }
                            33 /* Bravery charm */ -> {
                                if (mPlayer.IsAfraid) {
                                    tryConsume.invoke()
                                    mPlayer.IsAfraid = false
                                    showDialogueAndNext("${gGZ.Player.Name} is no longer afraid.")
                                } else
                                    showDialogueAndReturn()
                            }
                        }
                    } else {
                        val originalDialogue = mDialogue
                        val dialogue = UIDialogue {
                            // onDone
                            mDialogue = originalDialogue
                            mDrawable = inventoryMenu
                            mInteractable = inventoryMenu
                            mProgressive = inventoryMenu
                        }
                        dialogue.init("Now isn't the time to use that!")
                        mDialogue = dialogue
                        mInteractable = dialogue
                    }
                }, { /* onToss */ }, { /* onHover */ mTooltip.Text = item.Description })
            )
        }
        inventoryMenu.autoSize() // Let the menu determine the best width
        var inventoryMenuRect = inventoryMenu.Rect
        val cashBlurb = UIBlurb()
        cashBlurb.init("asdf")
        val blurbRect = cashBlurb.Rect
        cashBlurb.move(gGZ.Camera.RightEdgeWithMargin - blurbRect.Width, gGZ.Camera.TopEdgeWithMargin - blurbRect.Height)
        val cashBlurbUsableSurface = cashBlurb.UsableSurface
        mTooltip.IsBottomAligned = true // Move the tooltip to the bottom, mainly for the purpose of maintaining common menu usage with other modes
        val tooltipUsableSurface = mTooltip.UsableSurface
        // Resize the menu to use the best fit width and greatest possible space between the (would-be) cash blurb and dialogue. This is done to match the
        // dimensions of the menu that would appear in the shop UI.
        inventoryMenu.size(inventoryMenuRect.Width, cashBlurbUsableSurface.Y - tooltipUsableSurface.Y - tooltipUsableSurface.Height)
        inventoryMenuRect = inventoryMenu.Rect
        inventoryMenu.move(gGZ.Camera.RightEdgeWithMargin - inventoryMenuRect.Width, cashBlurbUsableSurface.Y - inventoryMenuRect.Height)
        mDrawable = inventoryMenu
        mInteractable = inventoryMenu
        mProgressive = inventoryMenu
        mBattleMenuRef.IsFocused = false
        inventoryMenu.IsFocused = true
    }, { /* onItemHover */ mTooltip.Text = "Open the inventory and select an item to use" },
        { /* onRun */ run() }, { /* onRunHover */ mTooltip.Text = "Attempt to escape" })
    private var mBattleMenuRef: UIBattleMenu = mBattleMenu
    private var mAttackMenu: UIAttackMenu = UIAttackMenu {
        // onCancel
        mDrawable = mBattleMenu
        mInteractable = mBattleMenu
    }
    private val mLimitMenu = UIMenu { /* onCancel */ } // The battle menu overrides input and will manage a cancel (B press) itself
    private val mPlayer = BTPlayer()
    private var mOpponent: BTOpponent? = null
    private val mPlayerStatus = UIPlayerBattleStatus()
    private val mOpponentStatus = UIOpponentBattleStatus()
    private var mTurnQueue: BTTurnQueue? = null
    private var mArmedMenu: UIArmedMenu? = null
    private var mDrawable: IDrawable? = null
    private var mTransientDrawable: IDrawable? = null
    private var mInteractable: IInteractable? = null
    private var mProgressive: IProgressive? = null
    private var mCanEscape = false
    private var mEscapeAttempts = 0

    override fun init(parameters: MTransition) {
        if (mIsInitialized) return else mIsInitialized = true
        if (parameters.ForBattle != null) {
            mOpponent = gGZ.Resources.loadOpponent(gGZ.Resources.OpponentCatalog[parameters.ForBattle!!.OpponentID])?.copy()
            mOpponent!!.Level = parameters.ForBattle!!.OpponentLevel
            mCanEscape = parameters.ForBattle!!.CanEscape
            mTurnQueue = BTTurnQueue(mPlayer, mOpponent!!)
            mIsDebug = parameters.IsDebug
        } else /* if (parameter.ForBattle == null) */ {
            // This is hopefully an impossible scenario but, if it does occur, just return to the main menu with nothing lost nor gained
            Transition = MTransition(MTransition.Mode.MENU)
            Transition!!.IsDebug = mIsDebug
            return
        }
        mTooltip.init()
        mTooltip.IsBottomAligned = false // Because the battle, attack, etc. menus are all on the bottom, move the tooltip to the top (for most things)
        mBattleMenu.init()
        mLimitMenu.init()
        // Add (available) limits by iterating through the enumeration of their IDs
        for (id in BTLimit.Limit.values()) {
            val limit = BTLimit(id)
            // If the player's level is high enough for this limit to have been unlocked
            if (limit.AvailableAtLevel <= gGZ.Player.Level)
                mLimitMenu.add(UIMenuEntry(limit.Name, {
                    // onSelect
                    mDrawable = null
                    mInteractable = null
                    doLimit(limit.ID)
                }, { /* onHover */ mTooltip.Text = limit.Description }))
        }
        val armedMenu = UIMenu { /* onCancel */ } // The battle menu overrides input and will manage a cancel (B press) itself
        armedMenu.init()
        armedMenu.add(UIMenuEntry("ARMED", {
            // onSelect
            mArmedMenu = UIArmedMenu {
                // onCancel
                mArmedMenu = null
                mInteractable = mBattleMenu
                mBattleMenu.IsFocused = true
            }
            mArmedMenu?.init()
            mInteractable = mArmedMenu
            mBattleMenu.IsFocused = false
        }))
        mBattleMenu.ItemSubmenu = armedMenu
        mAttackMenu = UIAttackMenu {
            // onCancel
            mDrawable = mBattleMenu
            mInteractable = mBattleMenu
            mBattleMenu.IsFocused = true
        }
        mAttackMenu.add(
            UIAttackMenuEntry(gGZ.Player.Addition1, { /* onSelect */ doAddition(gGZ.Player.Addition1) },
                { /* onHover */ mTooltip.Text = gGZ.Player.Addition1?.toString() ?: "" })
        )
        mAttackMenu.add(
            UIAttackMenuEntry(gGZ.Player.Addition2, { /* onSelect */ doAddition(gGZ.Player.Addition2) },
                { /* onHover */ mTooltip.Text = gGZ.Player.Addition2?.toString() ?: "" })
        )
        mAttackMenu.add(
            UIAttackMenuEntry(gGZ.Player.Addition3, { /* onSelect */ doAddition(gGZ.Player.Addition3) },
                { /* onHover */ mTooltip.Text = gGZ.Player.Addition2?.toString() ?: "" })
        )
        mAttackMenu.add(
            UIAttackMenuEntry(gGZ.Player.Addition4, { /* onSelect */ doAddition(gGZ.Player.Addition4) },
                { /* onHover */ mTooltip.Text = gGZ.Player.Addition4?.toString() ?: "" })
        )
        mAttackMenu.init()
        mBackgroundBubble.init()
        mBackgroundBubble.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mBackgroundBubble.size(gGZ.Camera.Width, mBattleMenu.Rect.Height)
        // The following areas divide the available surface into this general layout:
        // --------------------------------------------------------------------------
        // |                                      | |                               |
        // |                                      | |                               |
        // |                                      | |                               |
        // |                 C                    | |                               |
        // |                                      | |                               |
        // |                                      | |             D                 |
        // |                                      | |                               |
        // |----------------------------------------|                               |
        // |                               |        |                               |
        // |                               |   E    |                               |
        // |                               |        |                               |
        // |                               |----------------------------------------|
        // |                               | |                                      |
        // |              A                | |                                      |
        // |                               | |                                      |
        // |                               | |                B                     |
        // |                               | |                                      |
        // |                               | |                                      |
        // |                               | |                                      |
        // --------------------------------------------------------------------------
        // with A being the player's "back" sprite, B being the player's status (health and stuff), C being the opponent's status, D being the opponent's sprite
        // and E being essentially unused (although animations may intrude into it). The margins between A and B and between C and D are just for aesthetics.
        // The sprites placed in areas A and D, although not the same resolution, all have a 1:1 aspect ratio.
        val backgroundBubbleRect = mBackgroundBubble.Rect
        val usableSurface = Rect(
            gGZ.Camera.LeftEdgeWithMargin, // X
            backgroundBubbleRect.Y + backgroundBubbleRect.Height + (gGZ.Camera.Margin * 2f), // Y
            gGZ.Camera.WidthWithMargin, // Width
            gGZ.Camera.TopEdgeWithMargin - (backgroundBubbleRect.Y + backgroundBubbleRect.Height + (gGZ.Camera.Margin * 2f)) // Height
        )
        val playerSpriteArea = Rect(usableSurface.X, usableSurface.Y, usableSurface.Width * 0.4f, usableSurface.Width * 0.4f)
        val opponentSpriteArea = Rect(
            usableSurface.X + usableSurface.Width - (usableSurface.Width * 0.4f),
            usableSurface.Y + usableSurface.Height - (usableSurface.Width * 0.4f),
            usableSurface.Width * 0.4f,
            usableSurface.Width * 0.4f
        )
        val playerStatusArea = Rect(
            playerSpriteArea.X + playerSpriteArea.Width + gGZ.Camera.Margin,
            usableSurface.Y,
            usableSurface.Width - playerSpriteArea.Width - gGZ.Camera.Margin,
            usableSurface.Height - opponentSpriteArea.Height
        )
        val opponentStatusArea = Rect(
            usableSurface.X,
            playerSpriteArea.Y + playerSpriteArea.Height,
            usableSurface.Width - opponentSpriteArea.Width - gGZ.Camera.Margin,
            usableSurface.Height - playerSpriteArea.Height
        )
        mPlayer.init() // Grabs information from the global Player object
        mPlayer.size(playerSpriteArea)
        mPlayer.move(playerSpriteArea)
        // If the player's HP is 25% or less, make limits available
        if (mPlayer.HP <= (mPlayer.MaxHP / 4))
            mBattleMenu.AttackSubmenu = mLimitMenu
        mOpponent?.init()
        mOpponent?.size(opponentSpriteArea)
        mOpponent?.move(opponentSpriteArea)
        mPlayerStatus.IsVisible = false // Hidden until the fly-in animation completes (or made visible immediately if animations are off)
        mPlayerStatus.init()
        mPlayerStatus.sizeByWidth(playerStatusArea.Width * 0.8f)
        val playerStatusRect = mPlayerStatus.Rect
        mPlayerStatus.move(playerStatusArea.X + (playerStatusArea.Width / 2f) - (playerStatusRect.Width / 2f), playerStatusArea.Y)
        mPlayerStatus.HP = mPlayer.HP
        mPlayerStatus.MaxHP = mPlayer.MaxHP
        mPlayerStatus.Level = mPlayer.Level
        mPlayerStatus.ExperiencePercentage = (gGZ.Player.Experience - gGZ.Utils.experienceForLevel(gGZ.Player.Level)).toFloat() /
                (gGZ.Utils.experienceForLevel(gGZ.Player.Level + 1) - gGZ.Utils.experienceForLevel(gGZ.Player.Level)).toFloat()
        mOpponentStatus.IsVisible = false // Hidden until the fly-in animation completes (or made visible immediately if animations are off)
        mOpponentStatus.init()
        mOpponentStatus.sizeByWidth(playerStatusRect.Width)
        val opponentStatusRect = mOpponentStatus.Rect
        mOpponentStatus.move(
            opponentStatusArea.X + (opponentStatusArea.Width / 2f) - (opponentStatusRect.Width / 2f),
            opponentStatusArea.Y + opponentStatusArea.Height - opponentStatusRect.Height
        )
        mOpponentStatus.HP = mOpponent!!.HP
        mOpponentStatus.MaxHP = mOpponent!!.MaxHP
        mOpponentStatus.Level = mOpponent!!.Level
        mOpponentStatus.Name = mOpponent!!.Name
        mOpponentStatus.AttackStyle = mOpponent!!.AttackStyle.toString()
        mOpponentStatus.DefenseStyle = mOpponent!!.DefenseStyle.toString()
        gGZ.Renderer?.ClearColor = floatArrayOf(1f, 1f, 1f) // White
        val finishInit = {
            mPlayerStatus.IsVisible = true
            mOpponentStatus.IsVisible = true
            val somethingAppearedDialogue = UIDialogue {
                // onDone
                mDialogue = null
                mInteractable = null
                // If the player's agility is higher or agilities are equal and the coin flip was lucky, it's the player's turn
                if ((mPlayer.Agility > mOpponent!!.Agility) || ((mPlayer.Agility == mOpponent!!.Agility) && Random().nextBoolean()))
                    playerTurn()
                else // When agilities are equal, there's a 50/50 chance of this case as well
                    opponentTurn()
            }
            somethingAppearedDialogue.init("${mOpponent!!.Name.toUpperCase(Locale.getDefault())} appeared!")
            mDialogue = somethingAppearedDialogue
            mInteractable = somethingAppearedDialogue
        }
        if (gGZ.Record.get("animations", "on") == "on")
            animateFlyIn(playerSpriteArea, opponentSpriteArea, /* onDone */ finishInit)
        else
            finishInit.invoke()
    }

    override fun draw(dt: Long) {
        // The armed menu takes up the whole screen so there's no point in drawing anything else while it's up
        if (mArmedMenu != null) {
            mArmedMenu?.draw(dt)
            return
        }
        mPlayer.draw(dt)
        mOpponent?.draw(dt)
        mTransientDrawable?.draw(dt)
        mPlayerStatus.draw(dt)
        mOpponentStatus.draw(dt)
        mBackgroundBubble.draw(dt)
        mDrawable?.draw(dt)
        mDialogue?.draw(dt)
    }

    override fun step(dt: Long) {
        mTasks.step(dt)
        mProgressive?.step(dt)
    }

    override fun handleInput(input: Input) {
        // If select was pressed (this is a special case as it's designated for the tooltip which we always want to be showable/hideable)
        if (input.IsPress && (input.Opcode == Input.Button.SELECT))
            mDialogue = if (mDialogue == null) mTooltip else if (mDialogue === mTooltip) null else mDialogue
        mInteractable?.handleInput(input)
    }

    private fun run() {
        val displayCantEscapeDialogue = {
            val originalDialogue = mDialogue
            val cantEscapeDialogue = UIDialogue {
                // onDone
                mDialogue = originalDialogue
                mInteractable = mBattleMenu
                nextTurn()
            }
            cantEscapeDialogue.init("Can't escape!")
            mDialogue = cantEscapeDialogue
            mInteractable = cantEscapeDialogue
        }
        if (mCanEscape) {
            // F = (player.Agility * 32) / ((opponent.Agility / 4) % 256) + (30 * attempts)
            // If F > 255, guaranteed escape
            // Else if random() % 255 < F, escape
            // Else, can't escape
            val b = if (mOpponent!!.Agility / 4 > 255) 255 else if (mOpponent!!.Agility < 4) 1 else mOpponent!!.Agility / 4
            val f = ((mPlayer.Agility * 32) / (b % 256)) + (30 * mEscapeAttempts)
            if ((f > 255) || (Random().nextInt(255) < f)) {
                val gotAwayDialogue = UIDialogue {
                    // onDone
                    Transition = MTransition(MTransition.Mode.OVERWORLD)
                    Transition!!.IsDebug = mIsDebug
                }
                gotAwayDialogue.init("Got away safely!")
                mDialogue = gotAwayDialogue
                mInteractable = gotAwayDialogue
            } else {
                mEscapeAttempts += 1
                displayCantEscapeDialogue.invoke()
            }
        } else
            displayCantEscapeDialogue.invoke()
    }

    private fun nextTurn() {
        val combatant = mTurnQueue!!.Next
        // Stunned combatants have a 50% chance of failing their turn
        if (combatant.IsStunned && Random().nextBoolean()) {
            val originalDialogue = mDialogue
            val stunnedDialogue = UIDialogue {
                // onDone
                mDialogue = originalDialogue
                mInteractable = null
                nextTurn()
            }
            stunnedDialogue.init("${if (combatant === mPlayer) gGZ.Player.Name else "Enemy ${mOpponent!!.Name}"} is stunned!")
            mDialogue = stunnedDialogue
            mInteractable = stunnedDialogue
        } else {
            if (combatant === mPlayer)
                playerTurn()
            else
                opponentTurn()
        }
    }

    private fun playerTurn() {
        mPlayer.IsGuarded = false
        // If passive guard is being applied due to using the lebenspuls limit
        if (mPlayer.PassiveGuardTurns > 0) {
            mPlayer.PassiveGuardTurns -= 1
            mPlayer.IsGuarded = true
        }
        // Prepare the turn as a callback to either be invoked immediately or followed by bleeding (if animations are on)
        val turn = {
            // The player's turn essentially consists of handing control over to the menu which can spawn various other actions
            mDrawable = mBattleMenu
            mInteractable = mBattleMenu
            mBattleMenu.IsFocused = true
        }
        if (mPlayer.IsBleeding || mPlayer.IsAfraid)
            doAilments(mPlayer, turn)
        else
            turn.invoke()
    }

    private fun opponentTurn() {
        mOpponent!!.IsGuarded = false
        // Prepare the turn as a callback to either be invoked immediately or followed by bleeding (if animations are on)
        val turn = {
            val random = Random()
            val randomInt = random.nextInt(10) // Random number from 0 to 9, inclusive
            // If the opponent will attack
            if (randomInt < 9) { // 90% chance
                val isEvaded = random.nextFloat() < calculateEvasionProbability(mOpponent!!, mPlayer)
                if (gGZ.Record.get("animations", "on") == "on") {
                    animateAttack(mOpponent!!)
                    if (isEvaded)
                        animateEvade(mPlayer)
                }
                val originalDialogue = mDialogue
                val attackDialogue = UIDialogue {
                    // onDone
                    if (!isEvaded) {
                        mDialogue = originalDialogue
                        mInteractable = null
                        val isCritical = Random().nextFloat() < calculateCriticalProbability()
                        doDamage(mPlayer, calculateDamage(mOpponent!!, mPlayer), isCritical) { /* onDone */ nextTurn() }
                    } else {
                        val missedDialogue = UIDialogue {
                            // onDone
                            mDialogue = originalDialogue
                            mInteractable = null
                            nextTurn()
                        }
                        // Either "Enemy {name}'s attack missed!" or "Enemy {name} is blind and missed!"
                        missedDialogue.init(
                            "Enemy ${mOpponent!!.Name.toUpperCase(Locale.getDefault())}" +
                                    "${if (mOpponent!!.IsBlind) " is blind and" else "'s attack"} missed!"
                        )
                        mDialogue = missedDialogue
                        mInteractable = missedDialogue
                    }
                }
                attackDialogue.init(
                    "Enemy ${mOpponent!!.Name.toUpperCase(Locale.getDefault())} ${
                        when (mOpponent!!.AttackStyle) {
                            GZ.AttackStyle.NONE -> "attacked"
                            GZ.AttackStyle.STAB -> "stabbed"
                            GZ.AttackStyle.SLASH -> "slashed at"
                            GZ.AttackStyle.CRUSH -> "swung at" // Don't want to say "crushed" but "swung at" may not be the best wording either
                        }
                    } ${gGZ.Player.Name}!"
                )
                mDialogue = attackDialogue
                mInteractable = attackDialogue
            }
            // If the opponent will guard
            else { // 10% chance
                val oldHP = mOpponent!!.HP
                mOpponent!!.HP += (mOpponent!!.MaxHP.toFloat() * 0.1f).toInt() // 10% of the opponent's maximum HP
                if (mOpponent!!.HP > mOpponent!!.MaxHP)
                    mOpponent!!.HP = mOpponent!!.MaxHP
                mOpponent!!.IsGuarded = true // Will be assigned to false on the opponent's next turn
                if (gGZ.Record.get("animations", "on") == "on")
                // Animate the guard, then the HP gain, and then proceed to the player's turn
                    animateGuard(/* isPlayer */ false) { /* onDone */ animateHPChange(mOpponentStatus, oldHP, mOpponent!!.HP, { /* onDone */ nextTurn() }) }
                else {
                    // Immediately change the visual representation of the opponent's HP and move onto the opponent's turn
                    mOpponentStatus.HP = mOpponent!!.HP
                    nextTurn()
                }
            }
        }
        if (mOpponent!!.IsBleeding || mOpponent!!.IsAfraid)
            doAilments(mOpponent!!, turn)
        else
            turn.invoke()
    }

    private fun doAilments(combatant: BTCombatant, onDone: () -> Unit) {
        var showDialogue = { _: Boolean -> } // Forward declaration
        // Either animate the fear status (shows skulls) or just go straight to the next turn
        val fear = {
            if (gGZ.Record.get("animations", "on") == "on")
                animateAilment(isBleed = false, isPlayer = combatant === mPlayer, onDone = onDone)
            else
                onDone.invoke()
        }
        // Either animate the bleed status (shows blood drops) or just go straight to doing damage. After doing damage, either alert the player to a fear
        // status or go straight to the opponent's turn.
        val bleed = {
            val next = { if (combatant.IsAfraid) showDialogue.invoke(/* isBleed */ false) else onDone.invoke() }
            if (gGZ.Record.get("animations", "on") == "on")
                animateAilment(isBleed = true, isPlayer = combatant === mPlayer) {
                    /* onDone */ doDamage(combatant, (combatant.MaxHP.toFloat() * 0.1f).toInt(), /* isCritical */ false, /*onDone */ next)
                }
            else
                doDamage(combatant, (combatant.MaxHP.toFloat() * 0.1f).toInt(), /* isCritical */ false, /* onDone */ next)
        }
        showDialogue = { isBleed: Boolean ->
            val originalDialogue = mDialogue
            val bleedDialogue = UIDialogue {
                // onDone
                mDialogue = originalDialogue
                mInteractable = null
                if (isBleed)
                    bleed.invoke()
                else
                    fear.invoke()
            }
            bleedDialogue.init(
                "${if (combatant === mPlayer) gGZ.Player.Name else mOpponent!!.Name.toUpperCase(Locale.getDefault())} is " +
                        "${if (isBleed) "bleeding" else "afraid"}!"
            )
            mDialogue = bleedDialogue
            mInteractable = bleedDialogue
        }
        // For either ailment (bleed or fear), display a dialogue alerting the player. If bleeding, it begins with a bleed alert before checking for fear.
        // If afraid, it only alerts to fear. In other words, there are three cases: 1) bleed, 2) fear, 3) bleed then fear.
        showDialogue.invoke(mOpponent!!.IsBleeding)
    }

    private fun doAddition(addition: Addition?) {
        if (addition == null)
            return
        val isEvaded = Random().nextFloat() < calculateEvasionProbability(mPlayer, mOpponent!!)
        val quickTimeEvent = BTAdditionQuickTimeEvent(addition, {
            // onAdd
            // If animations are turned on
            if (gGZ.Record.get("animations", "on") == "on") {
                animateAttack(mPlayer)
                if (isEvaded)
                    animateEvade(mOpponent!!)
            }
        }, {
            /* onDone */ successCount ->
            mDrawable = mBattleMenu
            mInteractable = mBattleMenu
            mProgressive = null
            if (!isEvaded) {
                val isCritical = Random().nextFloat() < calculateCriticalProbability(successCount)
                doDamage(mOpponent!!, calculateDamage(mPlayer, mOpponent!!, addition, successCount), isCritical) {
                    // onDone
                    val effectiveness = calculateEffectivenessFactor(addition.Style, mOpponent!!.DefenseStyle)
                    // If the addition, when performed on this opponent, is either "super effective" or "not very effective"
                    if (effectiveness != 1f) {
                        // Display a dialogue stating the effectiveness
                        val originalDialogue = mDialogue
                        val effectivenessDialogue = UIDialogue {
                            // onDone
                            mDialogue = originalDialogue
                            mInteractable = mBattleMenu
                            nextTurn()
                        }
                        effectivenessDialogue.init(if (effectiveness > 1f) "It's super effective!" else "It's not very effective...")
                        mDialogue = effectivenessDialogue
                        mInteractable = effectivenessDialogue
                    } else
                        nextTurn()
                }
            } else {
                val originalDialogue = mDialogue
                val missedDialogue = UIDialogue {
                    // onDone
                    mDialogue = originalDialogue
                    mInteractable = mBattleMenu
                    nextTurn()
                }
                // Either just "Leid missed!" or "Leid is blind and missed!"
                missedDialogue.init("${gGZ.Player.Name.toUpperCase(Locale.getDefault())}${if (mPlayer.IsBlind) " is blind and" else ""} missed!")
                mDialogue = missedDialogue
                mInteractable = missedDialogue
            }
        })
        quickTimeEvent.init()
        quickTimeEvent.size(gGZ.Camera.Width, gGZ.Camera.Height)
        quickTimeEvent.move(gGZ.Camera.LeftEdge, gGZ.Camera.BottomEdge)
        mDrawable = quickTimeEvent
        mInteractable = quickTimeEvent
        mProgressive = quickTimeEvent
    }

    private fun doLimit(id: BTLimit.Limit) {
        when (id) {
            BTLimit.Limit.MUTANGRIFF -> { // Deals 3x base damage with 100% accuracy, available at all levels
                if (gGZ.Record.get("animations", "on") == "on")
                    animateMutangriff { /* onDone */ doDamage(mOpponent!!, calculateDamage(mPlayer, mOpponent!!) * 3, false) { /* onDone */ nextTurn() } }
                else
                    doDamage(mOpponent!!, calculateDamage(mPlayer, mOpponent!!) * 3, false) { /* onDone */ nextTurn() }
            }
            BTLimit.Limit.KRUEZSCHLAG -> { // Deals damage equal to half of the opponents remaining HP, available at level 10+
                if (gGZ.Record.get("animations", "on") == "on")
                    animateKruezschlag { /* onDone */ doDamage(mOpponent!!, mOpponent!!.HP / 2, false) { /* onDone */ nextTurn() } }
                else
                    doDamage(mOpponent!!, mOpponent!!.HP / 2, false) { /* onDone */ nextTurn() }
            }
            BTLimit.Limit.MUT -> { // Applies all buffs, available at level 20+
                // If already 100% fully buffed, this won't do anything so tell the player and return to the limit menu
                if (mPlayer.IsHealthBuffed && mPlayer.IsStrengthBuffed && mPlayer.IsVitalityBuffed && mPlayer.IsAgilityBuffed && mPlayer.IsStrengthBuffed) {
                    val originalDialogue = mDialogue
                    val dialogue = UIDialogue {
                        // onDone
                        mDialogue = originalDialogue
                        mDrawable = mBattleMenu
                        mInteractable = mBattleMenu
                    }
                    dialogue.init("It won't have any effect.")
                    mDialogue = dialogue
                    mInteractable = dialogue
                    return
                }
                mPlayer.IsStrengthBuffed = true
                mPlayer.IsVitalityBuffed = true
                mPlayer.IsAgilityBuffed = true
                mPlayer.IsSenseBuffed = true
                if (!mPlayer.IsHealthBuffed) {
                    val oldMaxHP = mPlayer.MaxHP
                    mPlayer.IsHealthBuffed = true // Affects MaxHP
                    mPlayerStatus.MaxHP = mPlayer.MaxHP
                    val oldHP = mPlayer.HP
                    healPlayer(mPlayer.MaxHP - oldMaxHP) // Increment by the potential HP gained
                    if (gGZ.Record.get("animations", "on") == "on")
                        animateHPChange(mPlayerStatus, oldHP, mPlayer.HP, {/* onDone */ /* nothing to do */ }, isQuick = true)
                    else
                        mPlayerStatus.HP = mPlayer.HP
                }
                if (gGZ.Record.get("animations", "on") == "on")
                    animateMut { /* onDone */ nextTurn() }
                else
                    nextTurn()
            }
            BTLimit.Limit.LEBENSPULS -> { // Complete heal (HP and ailments) and passive guard for three turns, available at level 30+
                val onDone = {
                    mPlayer.PassiveGuardTurns = 3 // Will be handled by playerTurn() after this
                    mPlayer.IsAfraid = false
                    mPlayer.IsBlind = false
                    mPlayer.IsBleeding = false
                    mPlayer.IsStunned = false
                    val oldHP = mPlayer.HP
                    healPlayer(mPlayer.MaxHP - mPlayer.HP) // Will set mPlayer.HP
                    if (gGZ.Record.get("animations", "on") == "on")
                        animateHPChange(mPlayerStatus, oldHP, mPlayer.HP, { /* onDone */ nextTurn() })
                    else {
                        mPlayerStatus.HP = mPlayer.HP
                        nextTurn()
                    }
                }
                if (gGZ.Record.get("animations", "on") == "on")
                    animateLebenspuls(/* onDone */ onDone)
                else
                    onDone.invoke()
            }
            BTLimit.Limit.MUNDGERUCH -> { // Induce all status ailments in the opponent even if immune, available at level 40+
                val onDone = {
                    mOpponent!!.IsAfraid = true
                    mOpponent!!.IsBlind = true
                    mOpponent!!.IsBleeding = true
                    mOpponent!!.IsStunned = true
                    nextTurn()
                }
                if (gGZ.Record.get("animations", "on") == "on")
                    animateAilment(isBleed = false, isPlayer = false, onDone = onDone)
                else
                    onDone.invoke()
            }
            BTLimit.Limit.OMNISCHLAG -> { // Perform many consecutive attacks if you're skilled enough, available at level 50+
                doAddition(gGZ.Resources.loadAddition(100))
            }
        }
        mBattleMenu.default() // Causes the menu to hover on "ATTACK"
    }

    private fun doDamage(combatant: BTCombatant, damageInHP: Int, isCritical: Boolean, onDone: () -> Unit) {
        var onDoneInternal = { onDone.invoke() } // Wrap the "on done" callback because we might override it (if the battle is over)
        val oldHP = combatant.HP
        val effectiveDamageInHP = if (isCritical) damageInHP * 2 else damageInHP
        combatant.HP -= if (combatant.IsGuarded) (effectiveDamageInHP / 2f).roundToInt() else effectiveDamageInHP
        // If the player's HP is 25% or less, make limits available
        if ((combatant === mPlayer) && (mPlayer.HP <= (mPlayer.MaxHP / 4)))
            mBattleMenu.AttackSubmenu = mLimitMenu
        // If the battle is over
        if (combatant.HP <= 0) {
            combatant.HP = 0
            // Override the "on done" callback to end the battle with some messages before transitioning to another mode
            onDoneInternal = if (combatant === mPlayer) {
                { onPlayerLost() }
            } else {
                { onPlayerWon() }
            }
        }
        val status = if (combatant === mPlayer) mPlayerStatus else mOpponentStatus
        // If animations are on, show the health bar decrease over time before proceeding to the "on done" response
        if (gGZ.Record.get("animations", "on") == "on") {
            if (isCritical)
                animateCriticalDamage(combatant)
            animateHPChange(status, oldHP, combatant.HP, onDoneInternal)
        } else {
            // Immediately apply the visual changes
            status.HP = combatant.HP
            onDoneInternal.invoke()
        }
    }

    private fun healPlayer(amount: Int) {
        mPlayer.HP += amount // The HP setter also limits HP to MaxHP
        // If the player's HP is now greater than 25%, make limits available
        if (mPlayer.HP > (mPlayer.MaxHP / 4))
            mBattleMenuRef.AttackSubmenu = null
    }

    private fun onPlayerWon() {
        gGZ.Player.HP = mPlayer.HP // The setter limits the HP to MaxHP
        val oldLevel = gGZ.Player.Level
        val oldExperience = gGZ.Player.Experience
        gGZ.Player.addExperience(mOpponent!!.Experience)
        val itemDrop = mOpponent!!.ItemDrop
        if (itemDrop != null)
            gGZ.Player.addItem(itemDrop)
        val cashDrop = mOpponent!!.CashDrop
        if (cashDrop != null)
            gGZ.Player.Cash += cashDrop
        // Make a list of messages to display. These will be shown in the form of a series of a dialogues. The second element in the pair is (nullable) lambda
        // to call when the dialogue showing the message is done.
        val messages: Queue<Pair<String, (() -> Unit)?>> = LinkedList()
        var nextDialogue = {}
        // Create a routine to sort-of recursively display the dialogues. Each dialogue's "on done" callback will call this, or leave the mode.
        nextDialogue = {
            when {
                messages.isNotEmpty() -> {
                    val (message, lambda) = messages.remove()
                    val dialogue = UIDialogue { /* onDone */ lambda?.invoke() ?: nextDialogue.invoke() }
                    dialogue.init(message)
                    mDialogue = dialogue
                    mInteractable = dialogue
                }
                // If there is no scene to return to, head back to debug mode where we came from
                gGZ.Scene == null -> Transition = MTransition(MTransition.Mode.DEBUG)
                else -> {
                    Transition = MTransition(MTransition.Mode.OVERWORLD)
                    Transition!!.IsDebug = mIsDebug
                }
            }
        }
        // Add the messages in the order in which they should be shown
        messages.add(Pair("Enemy ${mOpponent!!.Name.toUpperCase(Locale.getDefault())} fainted!", null)) // No callback to associate with "blah blah fainted"
        messages.add(Pair("${gGZ.Player.Name.toUpperCase(Locale.getDefault())} gained ${mOpponent!!.Experience} EXP. Points!", {
            if (gGZ.Record.get("animations", "on") == "on")
                animateExperienceGain(oldLevel, gGZ.Player.Level, oldExperience, gGZ.Player.Experience, /* onDone */ nextDialogue)
            else {
                // Immediately change the visual representation of the player's experience and move onto the next dialogue
                mPlayerStatus.ExperiencePercentage = (gGZ.Player.Experience - gGZ.Utils.experienceForLevel(gGZ.Player.Level)).toFloat() /
                        (gGZ.Utils.experienceForLevel(gGZ.Player.Level + 1) - gGZ.Utils.experienceForLevel(gGZ.Player.Level)).toFloat()
                nextDialogue.invoke()
            }
        })) // Along with the "blah blah has gained blah exp points" dialogue, when it's done, animate the experience gain (if animations are on)
        if (gGZ.Player.Level != oldLevel)
            messages.add(Pair("${gGZ.Player.Name.toUpperCase(Locale.getDefault())} grew to level ${gGZ.Player.Level}!", {
                mPlayerStatus.Level = gGZ.Player.Level // Display the new level, even if we're just about to exit this mode
                nextDialogue.invoke()
            }))
        if (itemDrop != null)
            messages.add(Pair("Found a ${gGZ.Resources.loadItem(itemDrop).Name}", null)) // No callback to associate
        if (cashDrop != null)
            messages.add(Pair("Found $$cashDrop", null)) // No callback to associate
        nextDialogue.invoke() // Begin the chain of dialogues. This will end with a mode transition.
    }

    private fun onPlayerLost() {
        val blackedOutDialogue = UIDialogue {
            // onDone
            Transition = if (mIsDebug)
                MTransition(MTransition.Mode.DEBUG)
            else {
                gGZ.Record.load(gGZ.Record.MostRecentSlot ?: -1) // Return to where the player last saved, 'cause he/she just died
                MTransition(MTransition.Mode.OVERWORLD)
            }
            Transition!!.IsDebug = mIsDebug
        }
        blackedOutDialogue.init("${gGZ.Player.Name.toUpperCase(Locale.getDefault())} blacked out!")
        mDialogue = blackedOutDialogue
        mInteractable = blackedOutDialogue
    }

    private fun calculateDamage(from: BTCombatant, to: BTCombatant, addition: Addition? = null, successCount: Int? = null): Int {
        val effectivenessModifier = calculateEffectivenessFactor(addition?.Style ?: from.AttackStyle, to.DefenseStyle)
        var fearModifier = 1f // If neither the attacker nor defender is afraid, this will remain 1.0
        if (from.IsAfraid)
            fearModifier *= 0.5f // Attacking while afraid halves damage done
        if (to.IsAfraid)
            fearModifier *= 2f // Being attacked while afraid doubles damage done
        var additionModifier = 1f
        // If the player is attacking, an addition will be passed and there may be additional damage to add from it (range: [1.0, addition.Multiplier])
        if (addition != null)
            additionModifier = 1f + ((addition.Modifier - 1f) * ((successCount?.toFloat() ?: 0f) / addition.Adds.size.toFloat()))
        //     (attacker's level + 5) * attacker's strength * effectiveness (style) modifier * fear modifier * addition modifier
        // D = -----------------------------------------------------------------------------------------------------------------
        //                                              5 * defender's vitality
        return ((from.Level + 5f) * from.Strength / (5f * to.Vitality) * effectivenessModifier * fearModifier * additionModifier).roundToInt()
    }

    private fun calculateEffectivenessFactor(attackStyle: GZ.AttackStyle, defenseStyle: GZ.DefenseStyle): Float = when (attackStyle) {
        GZ.AttackStyle.NONE -> 1f
        GZ.AttackStyle.CRUSH -> if (defenseStyle == GZ.DefenseStyle.CHAIN) 2f else if (defenseStyle == GZ.DefenseStyle.PLATE) 0.5f else 1f
        GZ.AttackStyle.SLASH -> if (defenseStyle == GZ.DefenseStyle.LEATHER) 2f else if (defenseStyle == GZ.DefenseStyle.CHAIN) 0.5f else 1f
        GZ.AttackStyle.STAB -> if (defenseStyle == GZ.DefenseStyle.PLATE) 2f else if (defenseStyle == GZ.DefenseStyle.LEATHER) 0.5f else 1f
    }

    private fun calculateEvasionProbability(attacker: BTCombatant, defender: BTCombatant): Float {
        // Blind combatants have a 50% chance of missing
        if (attacker.IsBlind && Random().nextBoolean())
            return 1f // Half of the time, it misses 100% of the tme (that's not a joke, this return value means a 100% chance of missing)
        //               1
        // P = -----------------------
        //     1 + e^(4.59512 - S + A)
        // where S is the defender's sense and A is the attacker's agility such that the probability of evasion will range from 0 to 1, increasing sense
        // increases the probability of evasion, increasing agility lowers the probability, and the case where S == A results in a ~0.01 probability
        return 1f / (1f + exp(4.59512f - defender.Sense.toFloat() + attacker.Agility.toFloat()))
    }

    private fun calculateCriticalProbability(adds: Int = 1): Float {
        // P = 0.01 * N
        // where N is the number of successfully-executed adds in an addition such that the probability of a critical increases with the number of adds, is 0
        // for a completely failed addition, and opponents have a constant 0.01 probablity of a critical
        return 0.01f * adds.toFloat()
    }

    private fun animateFlyIn(playerGoal: Rect, opponentGoal: Rect, onDone: () -> Unit) {
        mPlayer.move(gGZ.Camera.RightEdge, playerGoal.Y) // Move the player just off screen, to the right
        mOpponent!!.move(gGZ.Camera.LeftEdge - opponentGoal.Width, opponentGoal.Y) // Move the opponent just off screen, to the left
        val duration = 1f // Seconds
        val frequency = 50f // Hz
        val dxdt = (abs(mPlayer.Rect.X - playerGoal.X)) / (frequency * duration) // Change in X per callback
        var t = 0f // Accumulated seconds
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            mPlayer.translate(-dxdt, 0f) // Translate the player leftward
            mOpponent!!.translate(dxdt, 0f) // Translate the opponent rightward
            t += (1f / frequency)
            if (t >= duration) {
                mPlayer.move(playerGoal)
                mOpponent!!.move(opponentGoal)
                mTasks.cancelByID(callbackID)
                onDone.invoke()
            }
        }
    }

    private fun animateAttack(combatant: BTCombatant) {
        val duration = 0.2f // Seconds
        val frequency = 50f // Hz
        val origin = combatant.Rect
        val dxdt = (origin.Width / 2f) / (frequency * duration) * if (combatant === mPlayer) 1f else -1f // Change in X per callback
        var t = 0f // Accumulated seconds
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            when {
                t < duration / 2f -> combatant.translate(dxdt, 0f)
                t < duration -> combatant.translate(-dxdt, 0f)
                else -> {
                    combatant.move(origin)
                    mTasks.cancelByID(callbackID)
                }
            }
            t += (1 / frequency)
        }
    }

    private fun animateGuard(isPlayer: Boolean, onDone: () -> Unit) {
        val sprite = GLSprite()
        if (isPlayer) {
            // The player and opponent use different sprites for their guards (different resolutions and orientations) but they both have the same general idea.
            // The sprite is the same size as the opponent/player and is offset from those sprites slightly. The task will be the same for both and simply flash
            // the sprite a few times before calling the "on done" callback when it's finished.
            sprite.init(gGZ.Resources.loadTexture("sprites/battle/back/guard.png"))
            val playerRect = mPlayer.Rect
            sprite.size(playerRect)
            sprite.move(playerRect)
            sprite.translate(playerRect.Width * 0.1f, playerRect.Height * 0.1f) // Offset 10% towards the upper right
        } else {
            sprite.init(gGZ.Resources.loadTexture("sprites/battle/front/guard.png"))
            val opponentRect = mOpponent!!.Rect
            sprite.size(opponentRect)
            sprite.move(opponentRect)
            sprite.translate(-opponentRect.Width * 0.1f, -opponentRect.Height * 0.1f) // Offset 10% toward the bottom left
        }
        var i = 0
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic(100L /* every 100 milliseconds */) {
            sprite.IsVisible = !sprite.IsVisible
            i += 1
            if (i == 6) {
                mDrawable = null
                mTasks.cancelByID(callbackID)
                onDone.invoke()
            }
        }
        mDrawable = sprite
    }

    private fun animateEvade(combatant: BTCombatant) {
        // This is essentially the reversal of animateAttack() in that it shifts the combatant in the other direction with the same duration, frequency, etc.
        val duration = 0.2f // Seconds
        val frequency = 50f // Hz
        val origin = combatant.Rect
        val dxdt = (origin.Width / 2f) / (frequency * duration) * if (combatant === mPlayer) 1f else -1f // Change in X per step
        var t = 0f // Accumulated seconds
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            when {
                t < duration / 2f -> combatant.translate(-dxdt, 0f)
                t < duration -> combatant.translate(dxdt, 0f)
                else -> {
                    combatant.move(origin)
                    mTasks.cancelByID(callbackID)
                }
            }
            t += (1 / frequency)
        }
    }

    private fun animateAilment(isBleed: Boolean, isPlayer: Boolean, onDone: () -> Unit) {
        val sprite = GLSprite()
        val combatantRect: Rect
        combatantRect = if (isPlayer) {
            // Much like the guard animation, the player and opponent use different sprites. The sprite is flashed near the player with a varying offset here.
            sprite.init(gGZ.Resources.loadTexture(if (isBleed) "sprites/battle/back/bleed.png" else "sprites/battle/back/fear.png"))
            mPlayer.Rect
        } else {
            sprite.init(gGZ.Resources.loadTexture(if (isBleed) "sprites/battle/front/bleed.png" else "sprites/battle/front/fear.png"))
            mOpponent!!.Rect
        }
        sprite.size(combatantRect)
        sprite.move(
            combatantRect.X + (combatantRect.Width * 0.1f * if (isPlayer) -1f else 1f),
            combatantRect.Y + (combatantRect.Height * 0.1f * if (isPlayer) 1f else -1f)
        )
        var i = 0
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic(250 /* every 250 milliseconds */) {
            i += 1
            if (i == 6) {
                mDrawable = null
                mTasks.cancelByID(callbackID)
                onDone.invoke()
            } else {
                when (i) {
                    // Because the callback's first execution is after the delay, it effectively begins at i = 1 and the 0 case is just for readability
                    0, 3 -> sprite.move(
                        combatantRect.X + (combatantRect.Width * 0.1f * if (isPlayer) -1f else 1f),
                        combatantRect.Y + (combatantRect.Height * 0.1f * if (isPlayer) 1f else -1f)
                    )
                    1, 4 -> sprite.move(combatantRect)
                    else -> sprite.move(
                        combatantRect.X + (combatantRect.Width * 0.1f * if (isPlayer) 1f else -1f),
                        combatantRect.Y + (combatantRect.Height * 0.1f * if (isPlayer) -1f else 1f)
                    )
                }
            }
        }
        mDrawable = sprite
    }

    private fun animateCriticalDamage(combatant: BTCombatant) {
        val duration = 0.2f // Seconds
        val flashes = 2 // Number of times the sprite will be made invisible
        var i = 0
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((duration * 1000f / (flashes.toFloat() * 2f)).toLong()) {
            combatant.IsVisible = !combatant.IsVisible
            i += 1
            if (i == (flashes * 2))
                mTasks.cancelByID(callbackID)
        }
    }

    private fun animateHPChange(status: UIBattleStatus, oldHP: Int, newHP: Int, onDone: () -> Unit, isQuick: Boolean = false) {
        // If there's nothing to animate
        if (oldHP == newHP) {
            onDone.invoke()
            return
        }
        val frequency = 50f // Hz
        val dhdt = if (isQuick) (newHP - oldHP).toFloat() else 50f // Change in HP per second
        val isLoss = newHP < oldHP // Flag indicating if the change in HP is a loss (true) or a gain (false)
        var hpAsFloat = oldHP.toFloat() // Decreasing HP as a float to allow for fine steps (integers would be too coarse)
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            hpAsFloat += (dhdt / frequency) * if (isLoss) -1f else 1f // Increase or decrease the intermediary value
            // If the HP has reached or passed its goal value, we're done
            if ((isLoss && (hpAsFloat <= newHP.toFloat())) || (!isLoss && (hpAsFloat >= newHP.toFloat()))) {
                status.HP = newHP
                mTasks.cancelByID(callbackID)
                onDone.invoke()
            } else
                status.HP = hpAsFloat.roundToInt()
        }
    }

    private fun animateExperienceGain(oldLevel: Int, newLevel: Int, oldExperience: Int, newExperience: Int, onDone: () -> Unit) {
        // If there's nothing to animate
        if ((oldLevel == newLevel) && (oldExperience == newExperience)) {
            onDone.invoke()
            return
        }
        val duration = 1f // Seconds
        val frequency = 50f // Hz
        val dedt = (newExperience - oldExperience).toFloat() / (frequency * duration) // Change in experience per step
        var experience = oldExperience.toFloat() // Experience counter, increases at a constant rate
        var level = oldLevel // Level counter
        var t = 0f // Accumulated seconds
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            experience += dedt
            if (experience >= gGZ.Utils.experienceForLevel(level + 1)) {
                level += 1
                mPlayerStatus.Level = level
            }
            mPlayerStatus.ExperiencePercentage = (experience - gGZ.Utils.experienceForLevel(level).toFloat()) /
                    (gGZ.Utils.experienceForLevel(level + 1).toFloat() - gGZ.Utils.experienceForLevel(level).toFloat())
            t += (1f / frequency)
            if (t >= duration) {
                mPlayerStatus.ExperiencePercentage = (newExperience - gGZ.Utils.experienceForLevel(newLevel)).toFloat() /
                        (gGZ.Utils.experienceForLevel(newLevel + 1) - gGZ.Utils.experienceForLevel(newLevel)).toFloat()
                mPlayerStatus.Level = newLevel
                mTasks.cancelByID(callbackID)
                onDone.invoke()
            }
        }
    }

    private fun animateItemThrow(onDone: () -> Unit) {
        // Load a sprite to represent the item being thrown
        val sprite = GLSprite()
        sprite.init(gGZ.Resources.loadTexture("sprites/battle/other/item.png"))
        // Size and position the sprite (initially) in the center of the player's sprite
        val playerRect = mPlayer.Rect
        sprite.sizeByWidth(playerRect.Width / 10f)
        var spriteRect = sprite.Rect
        sprite.move(playerRect.X + (playerRect.Width / 2f) - (spriteRect.Width / 2f), playerRect.Y + (playerRect.Height / 2f) - (spriteRect.Height / 2f))
        spriteRect = sprite.Rect
        // Create a Rect to represent the goal position of the sprite in the center of the opponent's sprite
        val opponentRect = mOpponent!!.Rect
        val goalRect = Rect(0f, 0f, opponentRect.Width / 2f, opponentRect.Width / 2f)
        goalRect.X = opponentRect.X + (opponentRect.Width / 2f) - (goalRect.Width / 2f)
        goalRect.Y = opponentRect.Y + (opponentRect.Height / 2f) - (goalRect.Height / 2f)
        mTransientDrawable = sprite
        val duration = 0.25f // Seconds
        val frequency = 50f // Hz
        val dxdt = (goalRect.X - spriteRect.X) / (frequency * duration) // Change in X (position) per callback
        val dydt = (goalRect.Y - spriteRect.Y) / (frequency * duration) // Change in Y per callback
        var lx = spriteRect.X // X coordinate if moved in a straight line
        var ly = spriteRect.Y // Y coordinate if moved in a straight line
        var t = 0L // Total time counter
        val dt = (1000f / frequency).toLong()
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            // Increment lx and ly as if the card were moving in a perfectly linear path from its origin to the goal
            lx += dxdt
            ly += dydt
            val yOffset = sin(PI.toFloat() * t / (duration * 1000f)) * (gGZ.Camera.Height * 0.25f) // Y coordinate offset from the linear path
            // Add the Y offset to the linear path to effectively create a curved path
            sprite.move(lx, ly + yOffset)
            t += dt
            if (t >= (duration * 1000L)) {
                mTransientDrawable = null
                mTasks.cancelByID(callbackID)
                onDone.invoke()
            }
        }
    }

    private fun animateMutangriff(onDone: () -> Unit) {
        // This animation displays a star-based icon on the opponent a few times
        val sprite = GLSprite()
        sprite.init(gGZ.Resources.loadTexture("sprites/battle/front/mutangriff.png"))
        val opponentRect = mOpponent!!.Rect
        sprite.size(opponentRect)
        sprite.move(opponentRect)
        val duration = 1f // Seconds
        val flashes = 3f
        var i = 0
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((1000f * duration / (flashes + 1f)).toLong()) {
            i += 1
            if (i == (flashes.toInt() + 1)) {
                mDrawable = null
                mTasks.cancelByID(callbackID)
                onDone.invoke()
            } else
                sprite.translate(opponentRect.Width / flashes, opponentRect.Height / flashes)
        }
        mDrawable = sprite
    }

    private fun animateKruezschlag(onDone: () -> Unit) {
        // This animation grows and shrinks the opponent sprite, in that order, smoothly. It's meant to resemble a certain magic attack from other games.
        val originalRect = mOpponent!!.Rect
        val bigRect = Rect(originalRect.X, originalRect.Y, originalRect.Width * 1.1f, originalRect.Height * 1.1f) // 10% bigger should do
        val smallRect = Rect(originalRect.X, originalRect.Y, originalRect.Width / 2f, originalRect.Height / 2f) // Half the size of the original
        var pastRect = originalRect // Reassigned variable pointing to whichever rect the sprite is being resized from
        var goalRect = originalRect // Reassigned variable pointing to whichever rect the sprite is being resized towards
        val duration = 1f // Seconds
        val frequency = 50f // Hz
        var i = 0 // Phase counter, increments when switching from growing to shrinking and vice versa
        var t = duration // Time counter, initialized to the animation's duration to trigger the if case in the callback
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            if (t >= (duration / 3f)) {
                when (i) {
                    0 -> { // Growing from the original rect dimensions to the 'big' rect's
                        pastRect = originalRect
                        goalRect = bigRect
                    }
                    1 -> { // Shrinking from the 'big' rect's dimensions to the 'small' rect's
                        pastRect = goalRect
                        goalRect = smallRect
                    }
                    2 -> { // Growing from the 'small' rect's dimension to hte original rect
                        pastRect = goalRect
                        goalRect = originalRect
                    }
                    3 -> { // With the sprite back to its original size, the animation is done
                        mOpponent!!.size(originalRect)
                        mTasks.cancelByID(callbackID)
                        onDone.invoke()
                    }
                }
                i += 1
                t = 0f
            } else {
                t += (1f / frequency)
                mOpponent!!.size(
                    pastRect.Width + ((goalRect.Width - pastRect.Width) * t / (duration / 3f)),
                    pastRect.Height + ((goalRect.Height - pastRect.Height) * t / (duration / 3f))
                )
                val currentRect = mOpponent!!.Rect
                mOpponent!!.move(
                    originalRect.X + ((originalRect.Width - currentRect.Width) / 2f),
                    originalRect.Y + ((originalRect.Height - currentRect.Height) / 2f)
                )
            }
        }
    }

    private fun animateMut(onDone: () -> Unit) {
        // This will abruptly enlarge and revert the sprite a few times. It's a little like how a certain italian plumber eating a mushroom.
        val originalRect = mPlayer.Rect
        val enlargedRect = Rect(originalRect.X, originalRect.Y, originalRect.Width * 1.1f, originalRect.Height * 1.1f) // 10% bigger should do
        val duration = 1f // Seconds
        val growths = 3 // Number of times the player's sprite will be enlarged
        var i = 0
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((duration * 1000f / (growths.toFloat() * 2f)).toLong()) {
            mPlayer.size(if (i % 2 == 0) enlargedRect else originalRect)
            i += 1
            if (i == (growths * 2)) {
                mTasks.cancelByID(callbackID)
                onDone.invoke()
            }
        }
    }

    private fun animateLebenspuls(onDone: () -> Unit) {
        // This is part 1 of the animation which simply flashes the player a few times (just like critical damage animations)
        val duration = 0.5f // Seconds
        val flashes = 4 // Number of times the sprite will be made invisible
        var i = 0
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((duration * 1000f / (flashes.toFloat() * 2f)).toLong()) {
            mPlayer.IsVisible = !mPlayer.IsVisible
            i += 1
            if (i == (flashes * 2)) {
                mTasks.cancelByID(callbackID)
                animateLebenspulsPart2(onDone)
            }
        }
    }

    private fun animateLebenspulsPart2(onDone: () -> Unit) {
        // This is part 2 of the animation which displays a moving orb over the player, spirally inward towards the player's center
        val playerRect = mPlayer.Rect
        val orb = GLSprite()
        val drawable = LebenspulsChain(orb)
        orb.init(gGZ.Resources.loadTexture("sprites/battle/other/orb.png"))
        orb.sizeByWidth(playerRect.Width / 6f)
        val orbRadius = orb.Rect.Width / 2f
        val duration = 1f // Seconds
        val frames = 18 // Number of times the orb's position will change
        var frame = 0 // Frame counter, determines when the animation is complete
        var theta = 0f
        var callbackID = 0
        callbackID = mTasks.schedulePeriodic((1000f * duration / frames.toFloat()).toLong()) {
            drawable.Tail = drawable.Last
            drawable.Last = drawable.Head
            val r = (playerRect.Width / 3f) - ((playerRect.Width / 3f) * (frame.toFloat() / frames.toFloat()))
            orb.move(
                playerRect.X + (playerRect.Width / 2f) + (r * sin(theta)) - orbRadius,
                playerRect.Y + (playerRect.Height / 2f) + (r * cos(theta)) - orbRadius
            )
            orb.IsVisible = true
            theta -= (4.0 * Math.PI / frames.toDouble()).toFloat() // Subtracting results in a counterclockwise movement
            frame += 1
            if (frame == frames) {
                mTasks.cancelByID(callbackID)
                onDone.invoke()
            }
        }
        orb.IsVisible = false // Hidden until it's properly positioned by the task
        mDrawable = drawable
    }

    // Helper class solely used for the second part of the Lebenspuls animation in order to draw up to three drawables at once
    private class LebenspulsChain(var Head: GLSprite) : IDrawable {
        // IDrawable property
        override val Rect: Rect
            get() = Rect(0f, 0f, 0f, 0f) // This is lazy but, to be fair, this rect is never used

        var Last: GLSprite? = null  // The "last" orb that was drawn, as in the most recent rather than final
            set(value) {
                field = copySprite(value)
            }
        var Tail: GLSprite? = null // The trailing or final orb in the chain
            set(value) {
                field = copySprite(value)
            }

        override fun draw(dt: Long) {
            Head.draw(dt)
            Last?.draw(dt)
            Tail?.draw(dt)
        }

        private fun copySprite(source: GLSprite?): GLSprite? {
            if (source == null)
                return null
            val sprite = GLSprite()
            sprite.init(source.Texture)
            sprite.size(source.Rect)
            sprite.move(source.Rect)
            sprite.IsVisible = source.IsVisible
            return sprite
        }
    }
}