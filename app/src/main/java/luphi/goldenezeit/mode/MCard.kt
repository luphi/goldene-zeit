package luphi.goldenezeit.mode

import luphi.goldenezeit.*
import luphi.goldenezeit.`interface`.IMode
import luphi.goldenezeit.card.CDTable
import luphi.goldenezeit.card.Card
import luphi.goldenezeit.gl.GLCharacter
import luphi.goldenezeit.ui.*
import java.util.LinkedList
import java.util.Queue
import java.util.Random
import kotlin.collections.LinkedHashMap

@Suppress("PropertyName")
class MCard : IMode {
    // IMode property
    override var Transition: MTransition? = null

    private var mIsInitialized = false
    private var mIsDebug = false // If running independent of the main game (i.e. launched from debug mode)
    private val mTasks = Tasks()
    private var mCardWidth = 0f // Width of an individual card; most drawables in the mode are sized relative to this value
    private var mState = CardState.CHOOSE_FIRST_MOVE
    private var mAnimatedCard: Card? = null // Points to a card while being played so it may be drawn between removal from the deck and addition to the table
    private var mChooseCardsMenu: UIInventoryMenu? = null // Lists cards available to the player from which he/she will choose five
    private var mPostGameCardSelector: UICardSelector? = null // Let the user, or opponent, select a card he/she/it won (takes up the whole screen)
    private var mGameOverBlurb: UIBlurb? = null // "You Win!" or "You Lose..." or "Draw" shown when the game is over

    // Card-holding variables - references are shared between the decks and the table whereas the "original" lists hold their own instances
    private val mPlayerOriginalCards = LinkedList<Card>()
    private val mOpponentOriginalCards = LinkedList<Card>()
    private val mPlayerDeck = UICardDeck(/* isPlayer = */ true) { /* onSelect */ mState = CardState.PLAYER_SELECT_TABLE }
    private val mOpponentDeck = UICardDeck(/* isPlayer = */ false) { /* onSelect */ }
    private val mTable = CDTable( // Where cards are placed when in play, located in the center of the screen, handles most of the (card) game logic
        { /* onSelect */ playCard(mPlayerDeck.remove(mPlayerDeck.HoveredIndex), mTableRef.HoveredColumn, mTableRef.HoveredRow) },
        { /* onCancel */ mState = CardState.PLAYER_SELECT_CARD }
    )
    private val mTableRef: CDTable = mTable // Exists to overcome some recursive references in the above onCancel callback

    // Score variable(s)
    private var mPlayerScore = 5
    private var mOpponentScore = 5
    private val mPlayerScoreVisual = GLCharacter()
    private val mOpponentScoreVisual = GLCharacter()

    override fun init(parameters: MTransition) {
        if (mIsInitialized) return else mIsInitialized = true
        // Cards are sized such that there is room for the two decks (one card width each with cards laid out vertically) and 3x3, thus three card widths,
        // table. The remaining space is to be used for the cursor and general aesthetic appeal.
        mCardWidth = gGZ.Camera.Width / 6f
        // Choose the cards, add them to the "original" lists, then add to the deck, and apply proper sizes
        chooseCards(
            parameters.ForCard?.NpcCards ?: HashSet(),
            /* isRandom */ gGZ.Record.get("card_rule_random", "off") == "on",
            /* isOpen */ gGZ.Record.get("card_rule_open", "on") == "on"
        )
        for (card in mPlayerOriginalCards) {
            card.sizeByWidth(mCardWidth)
            mPlayerDeck.add(card.copy()) // Make a copy instance to give to the deck
        }
        for (card in mOpponentOriginalCards) {
            card.sizeByWidth(mCardWidth)
            mOpponentDeck.add(card.copy())
        }
        // The visuals are laid out such that
        // -------------------------------------------------------------------------
        // |           |                                               |           |
        // |           |                                               |           |
        // |           |                                               |           |
        // |           |                                               |           |
        // |           | |-------------------------------------------| |           |
        // |           | |                                           | |           |
        // |           | |                                           | |           |
        // |           | |                                           | |           |
        // |     B     | |                                           | |     C     |
        // |           | |                                           | |           |
        // |           | |                   A                       | |           |
        // |           | |                                           | |           |
        // |           | |                                           | |           |
        // |           | |                                           | |           |
        // |           | |-------------------------------------------| |           |
        // |___________|                                               |___________|
        //  _______                                                          ______|
        // |      |                                                         |      |
        // |  E   |                                                         |   F  |
        // -------------------------------------------------------------------------
        // With A being the table, B being the opponent's deck, C being the player's deck, E being the opponent's score, and F being the player's score
        mTable.init()
        var ruleFlags = 0
        ruleFlags += if (gGZ.Record.get("card_rule_combo", "on") == "on") mTable.COMBO_RULE else 0
        ruleFlags += if (gGZ.Record.get("card_rule_same", "on") == "on") mTable.SAME_RULE else 0
        ruleFlags += if (gGZ.Record.get("card_rule_plus", "on") == "on") mTable.PLUS_RULE else 0
        ruleFlags += if (gGZ.Record.get("card_rule_same_wall", "off") == "on") mTable.SAME_WALL_RULE else 0
        ruleFlags += if (gGZ.Record.get("card_rule_plus_wall", "off") == "on") mTable.PLUS_WALL_RULE else 0
        mTable.Rules = ruleFlags
        mIsDebug = parameters.IsDebug
        val cardRect = mOpponentOriginalCards[0].Rect // Grab a card that will definitely exist and use it as a representation of all card dimensions
        mTable.size(cardRect.Width * 3f, cardRect.Height * 3f) // Fits 3x3 cards
        var tableRect = mTable.Rect
        mTable.move(-tableRect.Width / 2f, -tableRect.Height / 2f) // Centered in the surface
        mPlayerScoreVisual.init('5')
        // Position the player's score at the bottom of the surface in line with the center of the player's deck
        mPlayerScoreVisual.sizeByHeight(gGZ.FontSize * 2f)
        mPlayerScoreVisual.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.BottomEdge)
        mOpponentScoreVisual.init('5')
        mOpponentScoreVisual.sizeByHeight(mPlayerScoreVisual.Rect.Height)
        mOpponentScoreVisual.move(gGZ.Camera.RightEdgeWithMargin - mOpponentScoreVisual.Rect.Width, gGZ.Camera.BottomEdgeWithMargin)
        mPlayerDeck.init()
        tableRect = mTable.Rect
        val playerScoreRect = mPlayerScoreVisual.Rect
        mPlayerDeck.size(
            gGZ.Camera.RightEdgeWithMargin - (tableRect.X + tableRect.Width) - gGZ.Camera.Margin,
            gGZ.Camera.TopEdgeWithMargin - (playerScoreRect.Y + playerScoreRect.Height) - gGZ.Camera.Margin
        )
        var playerDeckRect = mPlayerDeck.Rect
        mPlayerDeck.move(gGZ.Camera.RightEdgeWithMargin - mPlayerDeck.Rect.Width, gGZ.Camera.TopEdgeWithMargin - playerDeckRect.Height)
        mOpponentDeck.init()
        playerDeckRect = mPlayerDeck.Rect
        mOpponentDeck.size(playerDeckRect.Width, playerDeckRect.Height)
        mOpponentDeck.move(gGZ.Camera.LeftEdgeWithMargin, playerDeckRect.Y)
        // The perfect white and black colors used by the various card sprites are close enough to the light and/or dark substitution colors that they are
        // substituted.  In order to effectively undo those substitutions performed by the shader, the light and dark substitutions are set here.
        gGZ.Renderer?.LightColor = floatArrayOf(1f, 1f, 1f) // White
        gGZ.Renderer?.DarkColor = floatArrayOf(0f, 0f, 0f) // Black
        gGZ.Renderer?.ClearColor = floatArrayOf(1f, 1f, 1f) // White
        if (mState == CardState.OPPONENT_MOVE)
            opponentMove()
        else if (mState == CardState.PLAYER_SELECT_CARD)
            mPlayerDeck.hoverOnFirstOrDefault()
    }

    override fun draw(dt: Long) {
        if (mPostGameCardSelector != null) {
            mPostGameCardSelector!!.draw(dt)
            return
        }
        // Always draw the table, decks, and scores
        mTable.IsFocused = mState == CardState.PLAYER_SELECT_TABLE
        mTable.draw(dt)
        mPlayerDeck.IsFocused = mState == CardState.PLAYER_SELECT_CARD
        mPlayerDeck.draw(dt)
        mOpponentDeck.IsFocused = (mState == CardState.ANIMATE_OPPONENT_MOVE) || (mState == CardState.OPPONENT_MOVE)
        mOpponentDeck.draw(dt)
        mAnimatedCard?.draw(dt)
        mPlayerScoreVisual.draw(dt)
        mOpponentScoreVisual.draw(dt)
        mChooseCardsMenu?.draw(dt)
        mGameOverBlurb?.draw(dt)
    }

    override fun step(dt: Long) {
        if (mPostGameCardSelector != null) {
            mPostGameCardSelector!!.step(dt)
            return
        }
        mTasks.step(dt)
        mTable.step(dt)
        mChooseCardsMenu?.step(dt)
    }

    override fun handleInput(input: Input) {
        if (mPostGameCardSelector != null)
            mPostGameCardSelector!!.handleInput(input)
        else
            when (mState) {
                CardState.CHOOSE_CARDS -> mChooseCardsMenu?.handleInput(input)
                CardState.PLAYER_SELECT_CARD -> mPlayerDeck.handleInput(input)
                CardState.PLAYER_SELECT_TABLE -> mTable.handleInput(input)
                else -> {
                }
            }
    }

    private fun chooseCards(npcCards: Set<Int>, isRandom: Boolean, isOpen: Boolean) {
        val random = Random()
        ///////////////////////////////////////////////////////////////////////
        // Choose the opponent's card
        ///////////////////////////////////////////////////////////////////////
        // If the list of the NPC's (opponent's) card IDs is empty, just choose randomly from the catalog of all cards
        if (npcCards.isEmpty()) {
            val cardCatalog: Catalog = gGZ.Resources.CardCatalog
            for (i in 0..4) {
                // Load the card and make a copy, just in case the player and opponent have an identical card
                val card = gGZ.Resources.loadCard(cardCatalog.Keys[random.nextInt(cardCatalog.Keys.size - 1)].toInt()).copy()
                card.IsPlayerOwned = false
                mOpponentOriginalCards.add(card)
            }
            mState = if (random.nextBoolean()) CardState.PLAYER_SELECT_CARD else CardState.OPPONENT_MOVE
        } else {
            val opponentCardIDs = LinkedList<Int>(npcCards) // Don't want to modify the NPC's list so we'll make a copy
            // Choose the opponent's five cards
            for (i in 0..4) {
                val id = opponentCardIDs[random.nextInt(opponentCardIDs.size - 1)]
                val card = gGZ.Resources.loadCard(id).copy()
                card.IsPlayerOwned = false
                card.IsFaceDown = !isOpen
                mOpponentOriginalCards.add(card)
                // If the card is one of the rare one-of-a-kind cards, remove it so it can't be selected again
                if (gGZ.Utils.isCardUnique(card))
                    opponentCardIDs.remove(id)
            }

        }
        ///////////////////////////////////////////////////////////////////////
        // Choose the opponent's card
        ///////////////////////////////////////////////////////////////////////
        val playerCards = LinkedHashMap<Int, Int>(gGZ.Player.Cards).toSortedMap() // Don't want to modify the player's copy
        // If the random rule is in effect, randomly choose cards from what the player has on hand
        if (isRandom) {
            // Choose the player's five cards
            for (i in 0..4) {
                val cardID = playerCards.entries.elementAt(random.nextInt(playerCards.size)).key
                val card = gGZ.Resources.loadCard(cardID).copy()
                card.IsPlayerOwned = true
                mPlayerOriginalCards.add(card)
                // If a one-of-a-kind-card
                if (gGZ.Utils.isCardUnique(card))
                    playerCards.remove(cardID)
                else {
                    // Decrement the remaining number of cards with that ID and remove the entry if all are gone
                    playerCards[cardID] = (playerCards[cardID] ?: 0) - 1
                    if (playerCards[cardID] ?: 0 <= 0)
                        playerCards.remove(cardID)
                }
            }
            mState = if (random.nextBoolean()) CardState.PLAYER_SELECT_CARD else CardState.OPPONENT_MOVE
        }
        // Let the player choose the cards from a menu
        else {
            val entryQueue: Queue<UIMenuEntry> = LinkedList<UIMenuEntry>() // A queue of menu entries allowing the player to undo card selections
            mChooseCardsMenu = UIInventoryMenu(UIInventoryMenu.Behavior.NO_SELECTOR, { // Lists entries with quantities but no "how many?" selector
                // onCancel
                // If there is at least one card in the player's deck to remove
                if (mPlayerDeck.Count > 0) {
                    // Remove the card, pop the corresponding menu entry, determine the quantity of the card available, and apply the old menu entry
                    val card = mPlayerDeck.remove()
                    val entry = entryQueue.remove()
                    val quantity = (playerCards[card.ID] ?: 0) + 1 // 1 if no entry exists for the ID, or the existing quantity + 1
                    playerCards[card.ID] = quantity
                    // If the entry would have been removed, add it again at the appropriate index
                    if (quantity == 1)
                        mChooseCardsMenu?.add(entry, playerCards.keys.sorted().indexOf(card.ID)) // Use of the index of the card's ID
                    else
                        mChooseCardsMenu?.setEntryQuantity(entry.Label, quantity)
                }
            })
            for (cardID in playerCards.keys) {
                val card = gGZ.Resources.loadCard(cardID)
                var entry: UIMenuEntry? = null // A separate variable is needed because the onSelect routine needs a reference to add the entry to the queue
                entry = UIInventoryMenuEntry(card.Shorthand, playerCards[cardID] ?: 0, {
                    // onSelect
                    val entryCard = card.copy() // New instances are needed due to the possibility of selecting multiple copies of the same card
                    entryCard.sizeByWidth(mCardWidth) // Cards don't have a uniform size so it must be set (to match others in this mode)
                    entryCard.IsPlayerOwned = true
                    mPlayerOriginalCards.add(entryCard)
                    mPlayerDeck.add(entryCard.copy()) // Make a copy instance to give to the deck
                    entryQueue.add(entry) // Add this menu entry to the queue so it can be re-added if the player hits B
                    playerCards[cardID] = (playerCards[cardID] ?: 0) - 1 // Remove one from the card ID -> quantity map
                    // If the card being chosen is the last card of this ID the player has available
                    if (playerCards[cardID] ?: 0 <= 0) {
                        // Remove it from both the map and menu
                        playerCards.remove(cardID)
                        mChooseCardsMenu?.remove(entryCard.Shorthand) // Remove by label with the label being the shorthand
                    } else
                    // Set the menu's quantity to match what the player now has available
                        mChooseCardsMenu?.setEntryQuantity(entryCard.Shorthand, playerCards[cardID]!!) // Set by label with the label being the shorthand
                    // If the player has chosen all cards he/she will play with
                    if (mPlayerDeck.Count == 5) {
                        mChooseCardsMenu = null // Don't need it anymore
                        mState = if (random.nextBoolean()) CardState.PLAYER_SELECT_CARD else CardState.OPPONENT_MOVE
                        if (mState == CardState.OPPONENT_MOVE)
                            opponentMove()
                        else if (mState == CardState.PLAYER_SELECT_CARD)
                            mPlayerDeck.hoverOnFirstOrDefault()
                    }
                })
                mChooseCardsMenu?.add(entry)
            }
            mChooseCardsMenu?.init()
            mChooseCardsMenu?.autoSize()
            mChooseCardsMenu?.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.TopEdgeWithMargin - mChooseCardsMenu!!.Rect.Height) // Top left corner
            mState = CardState.CHOOSE_CARDS
        }
    }

    private fun playCard(card: Card?, column: Int, row: Int) {
        if ((card == null) || (column < 0) || (column > 2) || (row < 0) || (row > 2))
            return
        val destination = mTable.getRectForSlot(column, row) ?: return
        // Add some more steps to the "on placed" event to apply the change in score, if there is one, and choose the next action via onPlayComplete()
        val onDone = {
            mTable.playCard(card, column, row) { result ->
                mPlayerScore += result.PlayerScoreDiff
                mOpponentScore += result.OpponentScoreDiff
                mPlayerScoreVisual.Letter = Character.forDigit(mPlayerScore, 10)
                mOpponentScoreVisual.Letter = Character.forDigit(mOpponentScore, 10)
                onPlayComplete()
            }
        }
        // If animations are turned on or unset
        if (gGZ.Record.get("animations", "on") == "on")
            animatePlay(card, destination, onDone)
        // If animations are turned off
        else {
            // Immediately play the card and place it on the table
            card.move(destination.X, destination.Y)
            onDone.invoke()
        }
    }

    private fun animatePlay(card: Card, destinationRect: Rect, onDone: () -> Unit) {
        // Animate the placement by having the card fly up and back down
        mState = if (mState == CardState.PLAYER_SELECT_TABLE) CardState.ANIMATE_PLAYER_MOVE else CardState.ANIMATE_OPPONENT_MOVE
        mAnimatedCard = card
        val duration = 0.5f // Seconds
        val frequency = 50f // Hz
        val dydt = gGZ.Camera.Height * 2f / (frequency * duration) // NDC per second
        var callbackID = 0
        // Schedule a periodic callback that will glide the card up from the deck
        callbackID = mTasks.schedulePeriodic((1000f / frequency).toLong()) {
            card.translate(0f, dydt)
            if (card.Rect.Y > gGZ.Camera.TopEdge) {
                // Cancel the first gliding callback and schedule a single callback following a 500 ms delay
                mTasks.cancelByID(callbackID)
                mTasks.scheduleOneOff(500) {
                    // Move the card above its destination on the X axis and flip it face up (if it wasn't already)
                    card.move(destinationRect.X, gGZ.Camera.TopEdge)
                    card.IsFaceDown = false
                    // Schedule a final periodic callback to glide the card from above the screen to its final location on the board
                    callbackID = mTasks.schedulePeriodic((1000f / frequency).toLong()) {
                        card.translate(0f, -dydt)
                        if (card.Rect.Y < destinationRect.Y) {
                            card.move(destinationRect.X, destinationRect.Y)
                            mTasks.cancelByID(callbackID)
                            mAnimatedCard = null
                            onDone.invoke()
                        }
                    }
                }
            }
        }
    }

    private fun opponentMove() {
        val moves = LinkedList<OpponentMove>()
        val columns = arrayListOf(0, 1, 2)
        columns.shuffle()
        val rows = arrayListOf(0, 1, 2)
        rows.shuffle()
        for (i in mOpponentDeck.Cards.indices) {
            for (j in columns.indices) {
                for (k in rows.indices) {
                    val result = mTable.playCardDryRun(mOpponentDeck.Cards[i], j, k) ?: continue // Continue the loop on if we get a null result
                    // If the move could not be performed (e.g. the coordinate already has a card placed on it)
                    moves.add(OpponentMove(Column = j, Row = k, Index = i, Result = result))
                }
            }
        }
        // Sort the moves such that the better moves, for the opponent, are at higher indexes.  In other words, moves[0] is the worst possible move for the
        // opponent and moves[moves.size - 1] is the best based on the number of points gained or lost with the move. Sort the moves such that the better moves,
        // for the opponent, are at higher indexes.  In other words, moves[0] is the worst possible move for the opponent and moves[moves.size - 1] is the best
        // based on the number of points gained or lost.
        moves.sort()
        val move = if (gGZ.Record.get("card_difficulty", "easy") == "easy")
            if (moves.size >= 3) moves[moves.size - 3] else moves[moves.size - 1]
        else if (gGZ.Record.get("card_difficulty", "easy") == "hard")
            if (moves.size >= 2) moves[moves.size - 2] else moves[moves.size - 1]
        else // "card_difficulty" == "best"
            moves[moves.size - 1]
        if (gGZ.Record.get("animations", "on") == "on") {
            mOpponentDeck.hoverOnIndex(move.Index) // Protrude the card
            mTasks.scheduleOneOff(500) {
                var callbackID = 0
                callbackID = mTasks.scheduleOneOff(500) {
                    playCard(mOpponentDeck.remove(move.Index), move.Column, move.Row)
                    mTasks.cancelByID(callbackID)
                }
            }
        } else
            playCard(mOpponentDeck.remove(move.Index), move.Column, move.Row)
    }

    private fun onPlayComplete() {
        // If the game is over
        if (mTable.IsFull) {
            val isDraw = mPlayerScore == mOpponentScore
            val isPlayerWin = (mPlayerScore > 5) && !isDraw
            val transition = if (mIsDebug) MTransition(MTransition.Mode.DEBUG) else MTransition(MTransition.Mode.OVERWORLD)
            transition.IsDebug = mIsDebug
            val postGame = {
                if (isDraw)
                    Transition = transition
                else {
                    // Display the card selector (interactable, like a menu visually unique) and either add (player won) or remove (player lost) the chosen
                    // card. In the event that the player lost, the card selector will select a card for the opponent. The logic isn't anything special; it just
                    // chooses whichever card amongst the player's has the highest ID (higher IDs indicating better cards).
                    // Maybe: move that logic to this mode to keep the ui package's code purely to UI logic.
                    mPostGameCardSelector = UICardSelector(mPlayerOriginalCards, mOpponentOriginalCards, isPlayerWin) { /* onSelect */ card ->
                        if (isPlayerWin)
                            gGZ.Player.addCard(card.ID)
                        else
                            gGZ.Player.removeCard(card.ID)
                        Transition = transition
                    }
                    mPostGameCardSelector!!.init()
                }
            }
            // Display a blurb briefly before bringing up the card selector and/or leaving this mode
            mGameOverBlurb = UIBlurb()
            mGameOverBlurb?.init(if (isDraw) "Draw" else if (isPlayerWin) "You Win!" else "You Lose...")
            val blurbRect = mGameOverBlurb!!.Rect
            mGameOverBlurb!!.move(-blurbRect.Width / 2f, -blurbRect.Height / 2f)
            mTasks.scheduleOneOff(1500L, postGame) // Display the blurb for 1.5 seconds before invoking the post-game lambda
        }
        // If coming from the "player is/was selecting where to play a card" or "animate the player's play" states
        else if ((mState == CardState.PLAYER_SELECT_TABLE) || (mState == CardState.ANIMATE_PLAYER_MOVE)) {
            // The player's turn is over and we move onto the opponent's
            mState = CardState.OPPONENT_MOVE
            opponentMove()
        }
        // If coming from the "opponent is/was selecting where to play a carD" or "animate the opponent's play" states
        else if ((mState == CardState.OPPONENT_MOVE) || (mState == CardState.ANIMATE_OPPONENT_MOVE)) {
            // The opponent's turn is over and we move onto the player's
            mState = CardState.PLAYER_SELECT_CARD
            mPlayerDeck.hoverOnFirstOrDefault()
        }
    }

    private enum class CardState {
        CHOOSE_CARDS, CHOOSE_FIRST_MOVE, PLAYER_SELECT_CARD, PLAYER_SELECT_TABLE, OPPONENT_MOVE, ANIMATE_PLAYER_MOVE, ANIMATE_OPPONENT_MOVE
    }

    private inner class OpponentMove(var Column: Int, var Row: Int, var Index: Int, var Result: CDTable.Result?) : Comparable<OpponentMove> {
        override fun equals(other: Any?): Boolean {
            if (other === this)
                return true
            if (other !is OpponentMove)
                return false
            return (Column == other.Column) && (Row == other.Row) && (Index == other.Index) && (Result?.equals(other.Result) ?: false)
        }

        override operator fun compareTo(other: OpponentMove): Int = (Result?.OpponentScoreDiff ?: 0) - (other.Result?.OpponentScoreDiff ?: 0)

        override fun hashCode(): Int {
            var result = Column
            result = 31 * result + Row
            result = 31 * result + Index
            result = 31 * result + (Result?.hashCode() ?: 0)
            return result
        }

        override fun toString(): String = "{ [Column][Row] = [$Column][$Row], Index = $Index, Result = $Result }"
    }
}