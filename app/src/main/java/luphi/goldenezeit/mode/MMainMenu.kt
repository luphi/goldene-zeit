package luphi.goldenezeit.mode

import android.graphics.Color
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import luphi.goldenezeit.Input
import luphi.goldenezeit.R
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IMode
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.ui.*
import java.util.Locale

class MMainMenu : IMode {
    // IMode property
    override var Transition: MTransition? = null

    private var mIsInitialized = false
    private var mShouldDrawContinueBlurb = false
    private var mRootMenu = UIMenu { /* onCancel */ }
    private var mDrawable: IDrawable? = null
    private var mInteractable: IInteractable? = mRootMenu
    private var mProgressive: IProgressive? = mRootMenu
    private var mOptionsMenu = UIOptionMenu({
        // onCancel
        mDrawable = null
        mInteractable = mRootMenu
        mProgressive = mRootMenu
        mRootMenu.IsFocused = true
    })
    private var mContinueBlurb = UIBlurb()

    override fun init(parameters: MTransition) {
        if (mIsInitialized)
            return
        // The very first thing the activity does when this app is launched is read the principal records. They include preference, which saved game slots are
        // available, and so on. If there is a game to be continued, it will also have read the "chapter" and "player_level" keys of the most recent save.
        mContinueBlurb.init(
            "CHAPTER ${gGZ.Record.getDirect("slot${gGZ.Record.MostRecentSlot}_chapter", "1")}\n   " +
                    "LEVEL ${gGZ.Record.getDirect("slot${gGZ.Record.MostRecentSlot}_player_level", "1")}"
        )
        mContinueBlurb.move(gGZ.Camera.LeftEdgeWithMargin, gGZ.Camera.BottomEdgeWithMargin)
        mRootMenu.init()
        val hasSavedGame = gGZ.Record.MostRecentSlot != null
        mShouldDrawContinueBlurb = hasSavedGame
        if (hasSavedGame)
            mRootMenu.add(UIMenuEntry("CONTINUE  ", { /* onSelect */
                gGZ.Record.IsAutosaveEnabled = true
                gGZ.Record.load(gGZ.Record.MostRecentSlot!!)
                Transition = MTransition(MTransition.Mode.OVERWORLD)
                Transition?.ForOverworld = MTransition.OverworldParameters(SceneID = gGZ.Record.get("scene_id", ""), Landing = Pair(gGZ.Player.X, gGZ.Player.Y))
                gGZ.Player.maximize() // TODO remove this when no longer useful
            }))
        mRootMenu.add(UIMenuEntry("NEW GAME  ", {
            // onSelect
            gGZ.Chapter = 1
            gGZ.Player.zeroize()
            gGZ.Verse = 0
            gGZ.Record.IsAutosaveEnabled = true
            Transition = MTransition(MTransition.Mode.OVERWORLD)
            Transition?.ForOverworld = MTransition.OverworldParameters(SceneID = "leben von tod")
        }))
        if (hasSavedGame)
            mRootMenu.add(UIMenuEntry("LOAD GAME  ", {
                // onSelect
                val onCancel = {
                    mDrawable = null
                    mInteractable = mRootMenu
                    mProgressive = mRootMenu
                }
                val loadMenu = UISaveLoadMenu(UISaveLoadMenu.Behavior.LOAD, {
                    /* onCancel */ onCancel.invoke()
                }, {
                    /* onSelect */ slot ->
                    gGZ.Record.IsAutosaveEnabled = true
                    gGZ.Record.load(slot)
                    Transition = MTransition(MTransition.Mode.OVERWORLD)
                    Transition?.ForOverworld =
                        MTransition.OverworldParameters(SceneID = gGZ.Record.get("scene_id", ""), Landing = Pair(gGZ.Player.X, gGZ.Player.Y))
                    onCancel.invoke()
                })
                loadMenu.init()
                mDrawable = loadMenu
                mInteractable = loadMenu
                mProgressive = loadMenu
            }))
        mRootMenu.add(UIMenuEntry("OPTIONS  ", {
            // onSelect
            mDrawable = mOptionsMenu
            mInteractable = mOptionsMenu
            mProgressive = mOptionsMenu
            mRootMenu.IsFocused = false
        }))
        if (gGZ.Record.get("developer_options", "off") == "on")
            mRootMenu.add(UIMenuEntry("DEBUG MENU  ", { /* onSelect */ Transition = MTransition(MTransition.Mode.DEBUG) }))
        mRootMenu.autoSize()
        mRootMenu.move(gGZ.Camera.LeftEdge + gGZ.Camera.Margin, gGZ.Camera.TopEdge - mRootMenu.Rect.Height - gGZ.Camera.Margin)
        mOptionsMenu.init()
        mOptionsMenu.add(UIOptionMenuEntry("ANIMATIONS", arrayOf("ON", "OFF"), if (gGZ.Record.get("animations", "on") == "on") 0 else 1, {
            /* onChanged */ option ->
            gGZ.Record.setDirect("animations", option.toLowerCase(Locale.getDefault()))
        }))
        mOptionsMenu.add(
            UIOptionMenuEntry("UI COLOR", arrayOf("RED", "PURPLE", "ORANGE", "SILVER", "TEAL", "KIWI", "FUCHSIA"),
                when (gGZ.Record.get("ui_color", "#930000")) {
                    "#930000" -> 0
                    "#8A2BE2" -> 1
                    "#FF8C00" -> 2
                    "#C0C0C0" -> 3
                    "#008080" -> 4
                    "#9BBC88" -> 5
                    "#FF00FF" -> 6
                    else -> 0
                }, { /* onChanged */ option ->
                    val hex = when (option) {
                        "RED" -> "#930000"
                        "PURPLE" -> "#8A2BE2"
                        "ORANGE" -> "#FF8C00"
                        "SILVER" -> "#C0C0C0"
                        "TEAL" -> "#008080"
                        "KIWI" -> "#9BBC88"
                        "FUCHSIA" -> "#FF00FF"
                        else -> "#930000" // Red
                    }
                    if (gGZ.Context != null) { // Should be impossible for it to be null but still
                        val activity = gGZ.Context as AppCompatActivity
                        val layout = activity.findViewById<LinearLayout>(R.id.root_layout)
                        activity.runOnUiThread {
                            layout.setBackgroundColor(Color.parseColor(hex))
                        }
                    }
                    gGZ.Record.setDirect("ui_color", hex)
                })
        )
        mOptionsMenu.add(UIOptionMenuEntry("CARD DIFFICULT", arrayOf("EASY", "HARD", "BEST"), 0, {
            /*onChanged */ option ->
            gGZ.Record.setDirect("card_difficulty", option.toLowerCase(Locale.getDefault()))
        }))
        mOptionsMenu.add(UIOptionMenuEntry("CARD RANDOM RULE", arrayOf("ON", "OFF"), if (gGZ.Record.get("card_rule_random", "off") == "on") 0 else 1, {
            /*onChanged */ option ->
            gGZ.Record.setDirect("card_rule_random", option.toLowerCase(Locale.getDefault()))
        }))
        mOptionsMenu.add(UIOptionMenuEntry("CARD OPEN RULE", arrayOf("ON", "OFF"), if (gGZ.Record.get("card_rule_open", "on") == "on") 0 else 1, {
            /*onChanged */ option ->
            gGZ.Record.setDirect("card_rule_open", option.toLowerCase(Locale.getDefault()))
        }))
        mOptionsMenu.add(UIOptionMenuEntry("CARD COMBO RULE", arrayOf("ON", "OFF"), if (gGZ.Record.get("card_rule_combo", "on") == "on") 0 else 1, {
            /*onChanged */ option ->
            gGZ.Record.setDirect("card_rule_random", option.toLowerCase(Locale.getDefault()))
        }))
        mOptionsMenu.add(UIOptionMenuEntry("CARD SAME RULE", arrayOf("ON", "OFF"), if (gGZ.Record.get("card_rule_same", "on") == "on") 0 else 1, {
            /*onChanged */ option ->
            gGZ.Record.setDirect("card_rule_same", option.toLowerCase(Locale.getDefault()))
        }))
        mOptionsMenu.add(UIOptionMenuEntry("CARD PLUS RULE", arrayOf("ON", "OFF"), if (gGZ.Record.get("card_rule_plus", "on") == "on") 0 else 1, {
            /*onChanged */ option ->
            gGZ.Record.setDirect("card_rule_plus", option.toLowerCase(Locale.getDefault()))
        }))
        mOptionsMenu.add(UIOptionMenuEntry("CARD SAME WALL RULE", arrayOf("ON", "OFF"), if (gGZ.Record.get("card_rule_same_wall", "off") == "on") 0 else 1, {
            /*onChanged */ option ->
            gGZ.Record.setDirect("card_rule_same_wall", option.toLowerCase(Locale.getDefault()))
        }))
        mOptionsMenu.add(UIOptionMenuEntry("CARD PLUS WALL RULE", arrayOf("ON", "OFF"), if (gGZ.Record.get("card_rule_plus_wall", "off") == "on") 0 else 1, {
            /*onChanged */ option ->
            gGZ.Record.setDirect("card_rule_plus_wall", option.toLowerCase(Locale.getDefault()))
        }))
        mOptionsMenu.add(UIOptionMenuEntry("DEBUG", arrayOf("OFF", "ON"), if (gGZ.Record.get("developer_options", "off") == "on") 1 else 0, {
            /* onChanged */ option ->
            gGZ.Record.setDirect("developer_options", option.toLowerCase(Locale.getDefault()))
            if (gGZ.Record.get("developer_options", "off") == "on") {
                mRootMenu.add(UIMenuEntry("DEBUG MENU  ", { /* onSelect */ Transition = MTransition(MTransition.Mode.DEBUG) })) // Add the debug menu entry
                mRootMenu.autoSize() // Resize to avoid empty space where the entry was previously
                mRootMenu.move(gGZ.Camera.LeftEdge + gGZ.Camera.Margin, gGZ.Camera.TopEdge - mRootMenu.Rect.Height - gGZ.Camera.Margin) // Snap to the borders
            } else {
                mRootMenu.remove("DEBUG MENU  ")
                mRootMenu.autoSize()
                mRootMenu.move(gGZ.Camera.LeftEdge + gGZ.Camera.Margin, gGZ.Camera.TopEdge - mRootMenu.Rect.Height - gGZ.Camera.Margin)
            }
        }))
        mOptionsMenu.autoSize()
        val optionsMenuRect = mOptionsMenu.Rect
        mOptionsMenu.move(gGZ.Camera.RightEdge - optionsMenuRect.Width - gGZ.Camera.Margin, gGZ.Camera.TopEdge - optionsMenuRect.Height - gGZ.Camera.Margin)
        gGZ.Renderer?.ClearColor = floatArrayOf(1f, 1f, 1f) // White
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        mRootMenu.draw(dt)
        mDrawable?.draw(dt)
        if (mShouldDrawContinueBlurb)
            mContinueBlurb.draw(dt)
    }

    override fun step(dt: Long) {
        mProgressive?.step(dt)
    }

    override fun handleInput(input: Input) {
        mInteractable?.handleInput(input)
        mShouldDrawContinueBlurb = mRootMenu.CurrentlySelected.contains("CONTINUE")
    }
}