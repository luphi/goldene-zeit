package luphi.goldenezeit.mode

import android.util.Log
import luphi.goldenezeit.GZ
import luphi.goldenezeit.Input
import luphi.goldenezeit.`interface`.IDrawable
import luphi.goldenezeit.`interface`.IMode
import luphi.goldenezeit.`interface`.IInteractable
import luphi.goldenezeit.`interface`.IProgressive
import luphi.goldenezeit.gGZ
import luphi.goldenezeit.scene.*
import luphi.goldenezeit.ui.*
import java.util.*

@Suppress("PropertyName")
class MOverworld : IMode {
    // IMode property
    override var Transition: MTransition? = null

    private var mIsInitialized = false
    private var mIsDebug = false
    private val mPC = SCCharacter()
    private val mRootMenu = UIMenu {
        // onCancel
        mDrawablePriorityHigh = null
        mInteractable = null
        mProgressive = null
    }
    private var mDrawablePriorityAbsolute: IDrawable? = null
    private var mDrawablePriorityHigh: IDrawable? = null
    private var mDrawablePriorityMedium: IDrawable? = null
    private val mTooltip = UIReusableDialogue()
    private var mDialogue: UIReusableDialogue? = null
    private var mInteractable: IInteractable? = null
    private var mProgressive: IProgressive? = null
    private var mDPadHeld: GZ.Cardinal? = null
    private var mShouldIgnoreInput = false
    private var mAreEncountersEnabled = true // May be disabled if launching from the debug menu
    private val mStepCountdowns = LinkedList<StepCountdown>()

    override fun init(parameters: MTransition) {
        if (mIsInitialized)
            return
        mAreEncountersEnabled = parameters.ForOverworld?.EnableEncounters ?: true
        mTooltip.init()
        mRootMenu.init()
        mRootMenu.add(UIMenuEntry("ITEM", {
            // onSelect
            val inventoryMenu = UIInventoryMenu(UIInventoryMenu.Behavior.USE_AND_TOSS, {
                // onCancel
                mDrawablePriorityHigh = mRootMenu
                mInteractable = mRootMenu
                mProgressive = mRootMenu
                mTooltip.Text = ""
                mRootMenu.IsFocused = true
            }, { /* onHoverCancel */ mTooltip.Text = "" })
            inventoryMenu.init()
            for (idQuantityPair in gGZ.Player.Inventory) {
                val item = gGZ.Resources.loadItem(idQuantityPair.key)
                inventoryMenu.add(
                    UIInventoryMenuEntry(/* label */ item.Name.toUpperCase(Locale.getDefault()), /* quantity */ idQuantityPair.value, {
                        // onUse
                        mTooltip.Text = ""
                        // Prepare a helper routine for displaying a dialogue with a given message and return to the root menu
                        val showDialogueAndReturn = { message: String ->
                            val originalDialogue = mDialogue
                            val dialogue = UIDialogue {
                                // onDone
                                mDialogue = originalDialogue
                                mDrawablePriorityHigh = mRootMenu
                                mInteractable = mRootMenu
                                mProgressive = mRootMenu
                                mRootMenu.IsFocused = true
                            }
                            dialogue.init(message)
                            mDialogue = dialogue
                            mInteractable = dialogue
                        }
                        val showDialogAndRetry = {
                            val originalDialogue = mDialogue
                            val dialogue = UIDialogue {
                                // onDone
                                mDialogue = originalDialogue
                                mInteractable = inventoryMenu
                            }
                            dialogue.init("It won't have any effect.")
                            mDialogue = dialogue
                            mInteractable = dialogue
                        }
                        // One last helper method to consume (remove one copy of it) if appropriate
                        val tryConsume = {
                            // If there should be one less item after being used
                            if (item.IsConsumed) {
                                // If removing this quantity of the item removed the remainder of that item
                                if (gGZ.Player.removeItem(/* id */ idQuantityPair.key, /* quantity */ 1))
                                    inventoryMenu.remove(item.Name.toUpperCase(Locale.getDefault()))
                                // If there are still some remaining
                                else
                                    inventoryMenu.setEntryQuantity(
                                        item.Name.toUpperCase(Locale.getDefault()),
                                        gGZ.Player.Inventory[idQuantityPair.key] ?: 0
                                    )
                            }
                        }
                        if (item.CanUse) {
                            when (item.ID) {
                                0, 1, 2, 3 -> {
                                    if (gGZ.Player.HP != gGZ.Player.MaxHP) {
                                        tryConsume.invoke()
                                        val newHP = when (item.ID) {
                                            0 /* Potion */ -> gGZ.Player.HP + 20
                                            1 /* Super potion */ -> gGZ.Player.HP + 50
                                            2 /* Hyper potion */ -> gGZ.Player.HP + 200
                                            else /* Fairy dust */ -> gGZ.Player.MaxHP
                                        }
                                        val oldHP = gGZ.Player.HP
                                        gGZ.Player.HP = newHP // The HP setter also limits HP to MaxHP
                                        showDialogueAndReturn.invoke("${gGZ.Player.Name} recovered by ${gGZ.Player.HP - oldHP}!")
                                    } else
                                        showDialogAndRetry.invoke()
                                }
                                50 /* Protein */ -> {
                                    tryConsume.invoke()
                                    gGZ.Player.StrengthStatAllotted += 1
                                    showDialogueAndReturn.invoke("${gGZ.Player.Name}'s STRENGTH rose.")
                                }
                                51 /* Calcium */ -> {
                                    tryConsume.invoke()
                                    gGZ.Player.VitalityStatAllotted += 1
                                    showDialogueAndReturn.invoke("${gGZ.Player.Name}'s VITALITY rose.")
                                }
                                52 /* Carbohydrate */ -> {
                                    tryConsume.invoke()
                                    gGZ.Player.AgilityStatAllotted += 1
                                    showDialogueAndReturn.invoke("${gGZ.Player.Name}'s AGILITY rose.")
                                }
                                53 /* Caffeine */ -> {
                                    tryConsume.invoke()
                                    gGZ.Player.SenseStatAllotted += 1
                                    showDialogueAndReturn.invoke("${gGZ.Player.Name}'s SENSE rose.")
                                }
                                54 /* Rare candy */ -> {
                                    tryConsume.invoke()
                                    gGZ.Player.addExperience(gGZ.Utils.experienceForLevel(gGZ.Player.Level + 1) - gGZ.Player.Experience)
                                    showDialogueAndReturn.invoke("${gGZ.Player.Name} grew to level ${gGZ.Player.Level}!")
                                }
                            }
                        } else {
                            val originalDialogue = mDialogue
                            val dialogue = UIDialogue {
                                // onDone
                                mDialogue = originalDialogue
                                mInteractable = inventoryMenu
                            }
                            dialogue.init("Now isn't the time to use that!")
                            mDialogue = dialogue
                            mInteractable = dialogue
                        }
                    }, {
                        /* onToss */ quantityToRemove ->
                        if (gGZ.Player.removeItem(/* id */ idQuantityPair.key, /* quantity */ quantityToRemove))
                            inventoryMenu.remove(item.Name.toUpperCase(Locale.getDefault()))
                        else
                            inventoryMenu.setEntryQuantity(item.Name.toUpperCase(Locale.getDefault()), gGZ.Player.Inventory[idQuantityPair.key] ?: 0)
                    },
                        { /* onHover */ mTooltip.Text = item.Description })
                )
            }
            inventoryMenu.autoSize() // Let the menu determine the best width
            var inventoryMenuRect = inventoryMenu.Rect
            val cashBlurb = UIBlurb()
            cashBlurb.init("asdf")
            val blurbRect = cashBlurb.Rect
            cashBlurb.move(gGZ.Camera.RightEdgeWithMargin - blurbRect.Width, gGZ.Camera.TopEdgeWithMargin - blurbRect.Height)
            val cashBlurbUsableSurface = cashBlurb.UsableSurface
            val tooltipUsableSurface = mTooltip.UsableSurface
            // Resize the menu to use the best fit width and greatest possible space between the (would-be) cash blurb and dialogue. This is done to match the
            // dimensions of the menu that would appear in the shop UI.
            inventoryMenu.size(inventoryMenuRect.Width, cashBlurbUsableSurface.Y - tooltipUsableSurface.Y - tooltipUsableSurface.Height)
            inventoryMenuRect = inventoryMenu.Rect
            inventoryMenu.move(gGZ.Camera.RightEdgeWithMargin - inventoryMenuRect.Width, cashBlurbUsableSurface.Y - inventoryMenuRect.Height)
            mDrawablePriorityHigh = inventoryMenu
            mInteractable = inventoryMenu
            mProgressive = inventoryMenu
            mRootMenu.IsFocused = false
        }))
        mRootMenu.add(
            UIMenuEntry("ARMED", {
                // onSelect
                val armedMenu = UIArmedMenu {
                    mDrawablePriorityAbsolute = null
                    mInteractable = mRootMenu
                }
                armedMenu.init()
                mDrawablePriorityAbsolute = armedMenu
                mInteractable = armedMenu
            })
        )
        mRootMenu.add(
            UIMenuEntry("ADDITION", {
                // onSelect
                val additionMenu = UIAdditionMenu {
                    mDrawablePriorityAbsolute = null
                    mInteractable = mRootMenu
                }
                additionMenu.init()
                mDrawablePriorityAbsolute = additionMenu
                mInteractable = additionMenu
            })
        )
        mRootMenu.add(
            UIMenuEntry("CARD", {
                // onSelect
                val cardCatalog = UICardCatalog {
                    mDrawablePriorityAbsolute = null
                    mInteractable = mRootMenu
                    mProgressive = mRootMenu
                }
                cardCatalog.init()
                mDrawablePriorityAbsolute = cardCatalog
                mInteractable = cardCatalog
                mProgressive = cardCatalog
            })
        )
        mRootMenu.add(
            UIMenuEntry("STATUS", {
                // onSelect
                val playerStatus = UIPlayerStatus {
                    // onDone
                    val playerStatAllotment = UIPlayerStatAllotment {
                        // onDone
                        mDrawablePriorityAbsolute = null
                        mInteractable = mRootMenu
                    }
                    playerStatAllotment.init()
                    mDrawablePriorityAbsolute = playerStatAllotment
                    mInteractable = playerStatAllotment
                }
                playerStatus.init()
                mDrawablePriorityAbsolute = playerStatus
                mInteractable = playerStatus
            })
        )
        mRootMenu.add(
            UIMenuEntry("MAP", { // onSelect
                val townMap = UITownMap(
                    { // onCancel
                        mDrawablePriorityAbsolute = null
                        mInteractable = mRootMenu
                    },
                    { /* onSelected */ town ->
                        Log.d("MOverworld", "Fast travel to $town")
                    })
                townMap.init()
                mDrawablePriorityAbsolute = townMap
                mInteractable = townMap
            })
        )
        // If not launched from the developer/debug mode
        if (!parameters.IsDebug)
            mRootMenu.add(UIMenuEntry("SAVE", {
                // onSelect
                val onCancel = {
                    mDrawablePriorityAbsolute = null
                    mInteractable = mRootMenu
                    mProgressive = mRootMenu
                }
                val saveMenu = UISaveLoadMenu(UISaveLoadMenu.Behavior.SAVE, { /* onCancel */ onCancel.invoke() }, {
                    /* onSelect */ slot ->
                    gGZ.Record.save(slot)
                    onCancel.invoke()
                })
                saveMenu.init()
                mDrawablePriorityAbsolute = saveMenu
                mInteractable = saveMenu
                mProgressive = saveMenu
            }))
        else
            mIsDebug = true
        mRootMenu.add(UIMenuEntry("EXIT", {
            // onSelect
            gGZ.Record.unload()
            gGZ.Scene?.setTileWalkableAt(mPC.X, mPC.Y, true)
            Transition = MTransition(MTransition.Mode.MENU)
            Transition!!.IsDebug = mIsDebug
        }))
        mRootMenu.autoSize()
        val rootMenuRect = mRootMenu.Rect
        mRootMenu.move(gGZ.Camera.RightEdge - rootMenuRect.Width - gGZ.Camera.Margin, gGZ.Camera.TopEdge - rootMenuRect.Height - gGZ.Camera.Margin)
        gGZ.Renderer?.ClearColor = floatArrayOf(0f, 0f, 0f) // Black
        mPC.Name = gGZ.Resources.PCCatalog["name"]
        mPC.init(gGZ.Resources.PCCatalog["spritesheet"])
        if (parameters.ForOverworld == null)
            loadScene(null, landing = Pair(gGZ.Player.X, gGZ.Player.Y))
        else
            loadScene(parameters.ForOverworld?.SceneID, landing = parameters.ForOverworld?.Landing)
        mIsInitialized = true
    }

    override fun draw(dt: Long) {
        // If there is an existing absolute (full screen overlay) drawable, draw it and nothing else
        if (mDrawablePriorityAbsolute != null) {
            mDrawablePriorityAbsolute?.draw(dt)
            return
        }
        gGZ.Scene?.BackgroundLayer?.draw(dt)
        gGZ.Scene?.ItemLayer?.draw(dt)
        gGZ.Scene?.NpcsDrawable?.draw(dt)
        mPC.draw(dt)
        mDrawablePriorityMedium?.draw(dt)
        gGZ.Scene?.ForegroundLayer?.draw(dt)
        mDialogue?.draw(dt)
        // If the high priority drawable is the inventory menu, draw the root menu underneath it
        if (mDrawablePriorityHigh is UIInventoryMenu)
            mRootMenu.draw(dt)
        mDrawablePriorityHigh?.draw(dt)
    }

    override fun step(dt: Long) {
        if (mProgressive != null)
            mProgressive?.step(dt)
        else {
            gGZ.Scene?.step(dt)
            mPC.step(dt)
        }
    }

    override fun handleInput(input: Input) {
        // If all input should be ignored (used, for example, when transitioning to the battle mode)
        if (mShouldIgnoreInput)
            return
        // If select was pressed (this is a special case as it's designated for the tooltip which we always want to be showable/hideable)
        if (input.IsPress && (input.Opcode == Input.Button.SELECT))
            mDialogue = if (mDialogue == null) mTooltip else if (mDialogue === mTooltip) null else mDialogue
        if (mInteractable != null) {
            mInteractable?.handleInput(input)
            return
        }
        if (input.IsPress) {
            val adjacentX = mPC.X + if (mPC.Direction == GZ.Cardinal.WEST) -1 else if (mPC.Direction == GZ.Cardinal.EAST) 1 else 0
            val adjacentY = mPC.Y + if (mPC.Direction == GZ.Cardinal.NORTH) -1 else if (mPC.Direction == GZ.Cardinal.SOUTH) 1 else 0
            var npc: SCNpc? = null // Potentially an NPC on the adjacent tile in the direction the player is facing
            // If either A or B was pressed
            if ((input.Opcode == Input.Button.A) || (input.Opcode == Input.Button.B)) {
                // Try to find an NPC on the adjacent tile and make it look at the player if found
                npc = gGZ.Scene?.getNpcAt(adjacentX, adjacentY)
                if (npc != null)
                    npc.Direction = when (mPC.Direction) {
                        GZ.Cardinal.NORTH -> GZ.Cardinal.SOUTH
                        GZ.Cardinal.EAST -> GZ.Cardinal.WEST
                        GZ.Cardinal.SOUTH -> GZ.Cardinal.NORTH
                        GZ.Cardinal.WEST -> GZ.Cardinal.EAST
                    }
            }
            when (input.Opcode) {
                Input.Button.A -> { // Generic interaction (with an NPC, sign, item, etc.)
                    // 'npc' was set above, though it may still be null
                    val sign = gGZ.Scene?.getSignAt(adjacentX, adjacentY)
                    val itemID = gGZ.Scene?.pickUpItemAt(adjacentX, adjacentY) // Removes the item from the scene, if it exists at that location, and returns it
                    if (itemID != null) {
                        val item = gGZ.Resources.loadItem(itemID.toInt())
                        gGZ.Record.set("picked_up ${gGZ.Scene!!.ID} ${item.ID}", "true")
                        gGZ.Player.addItem(itemID.toInt())
                        val originalDialogue = mDialogue
                        val dialogue = UIDialogue {
                            // onDone
                            mDialogue = originalDialogue
                            mDrawablePriorityMedium = null
                            mInteractable = null
                        }
                        dialogue.init(
                            "You found ${if (item.Name.first() in arrayOf('a', 'e', 'i', 'o', 'u')) "an" else "a"} " +
                                    "${item.Name.toUpperCase(Locale.getDefault())}!"
                        )
                        mDialogue = dialogue
                        mInteractable = dialogue
                    } else if (npc != null) {
                        val cutscene = npc.Cutscene
                        if (cutscene != null)
                            playCutscene(cutscene)
                    } else if (sign != null) {
                        // Create a cutscene with a single event to represent the sign
                        val cutscene = SCCutscene()
                        val event = SCEvent(SCEvent.Opcode.THOUGHT)
                        event.String = sign
                        cutscene.Acts.add(listOf(event))
                        playCutscene(cutscene)
                    }
                }
                Input.Button.B -> { // Card game request with an NPC
                    // If trying to interact with an NPC that has a deck of cards
                    if (npc?.Cards?.isNotEmpty() == true) {
                        npc.IsPaused = true
                        val originalDialogue = mDialogue
                        val dialogue = UIDialogue {
                            // onDone
                            val onCancel = {
                                mDialogue = originalDialogue
                                mDrawablePriorityMedium = null
                                mInteractable = null
                                npc.IsPaused = false
                            }
                            val menu = UIMenu { /* onCancel */ onCancel.invoke() }
                            menu.add(UIMenuEntry("YES", {
                                // onSelect
                                Transition = MTransition(MTransition.Mode.CARD)
                                Transition!!.ForCard = MTransition.CardParameters(npc.Cards)
                                Transition!!.IsDebug = mIsDebug
                            }))
                            menu.add(UIMenuEntry("NO", { /* onSelect */ onCancel.invoke() }))
                            menu.init()
                            menu.autoSize()
                            val tooltipRect = mTooltip.Rect // Has an identical position and size compared to the dialogue
                            // Place the menu just above the dialogue
                            menu.move(gGZ.Camera.RightEdgeWithMargin - menu.Rect.Width, tooltipRect.Y + tooltipRect.Height + gGZ.Camera.Margin)
                            mDrawablePriorityMedium = menu
                            mInteractable = menu
                        }
                        dialogue.init(npc.CardMessage)
                        mDialogue = dialogue
                        mInteractable = dialogue
                    }
                }
                Input.Button.START -> {
                    mInteractable = mRootMenu
                    mDrawablePriorityHigh = mRootMenu
                    mDPadHeld = null
                }
                Input.Button.DPAD_UP -> {
                    mDPadHeld = GZ.Cardinal.NORTH
                    walk()
                }
                Input.Button.DPAD_RIGHT -> {
                    mDPadHeld = GZ.Cardinal.EAST
                    walk()
                }
                Input.Button.DPAD_DOWN -> {
                    mDPadHeld = GZ.Cardinal.SOUTH
                    walk()
                }
                Input.Button.DPAD_LEFT -> {
                    mDPadHeld = GZ.Cardinal.WEST
                    walk()
                }
                else -> {
                }
            }
        } else // if (!uiEvent.IsPress) -- meaning a button was released
            when (input.Opcode) {
                Input.Button.DPAD_UP, Input.Button.DPAD_RIGHT, Input.Button.DPAD_DOWN, Input.Button.DPAD_LEFT -> {
                    mDPadHeld = null
                    // The walking animation should continue until A) the PC is done walking or B) the PC cannot walk in the chosen direction, preventing any
                    // actual movement, but the user has released the D pad button. Case (B) is handled here. Case (A) is handled by walk().
                    if (!mPC.IsGliding)
                        mPC.IsAnimating = false
                }
                else -> {
                }
            }
    }

    fun registerStepCountdown(stepCount: Int, callback: () -> Unit) = mStepCountdowns.add(StepCountdown(stepCount - 1, callback))

    private fun loadScene(id: String? = null, exitDoor: SCDoor? = null, landing: Pair<Int, Int>? = null) {
        // If a scene was identified, load it. Otherwise, we will continue under the assumption that we're resuming with the existing global scene.
        if (id != null)
            gGZ.Scene = gGZ.Resources.loadScene(id)
        if (gGZ.Scene == null)
            return
        // Change the four variable colors with the renderer using the colors specified by the scene
        gGZ.Renderer?.PrimaryColor = gGZ.Scene!!.PrimaryColor
        gGZ.Renderer?.SecondaryColor = gGZ.Scene!!.SecondaryColor
        gGZ.Renderer?.LightColor = gGZ.Scene!!.LightColor
        gGZ.Renderer?.DarkColor = gGZ.Scene!!.DarkColor
        mPC.Scene = gGZ.Scene
        // There are cases during story progression where the PC will change/age. These changes are always associated with a new scene making this the best time
        // to update the spritesheet. In most cases, though, the spritesheet won't change and this setter will return immediately.
        mPC.Spritesheet = gGZ.Resources.PCCatalog["spritesheet"]
        val entryDoor = gGZ.Scene?.getDoorByID(exitDoor?.ToDoor ?: -1)
        when {
            id == null -> {
                mPC.placeOnTile(gGZ.Player.X, gGZ.Player.Y)
            }
            // If the player walked into/onto a door and the new scene has a connected door
            entryDoor != null -> {
                mPC.placeOnTile(entryDoor.X, entryDoor.Y)
                gGZ.Player.X = mPC.X
                gGZ.Player.Y = mPC.Y
                // Begin walking one tile away from the door (the door specifies the direction the player should walk)
                mPC.IsAnimating = true
                val dpadHeld = mDPadHeld // Remember which D pad button is held
                mDPadHeld = null // Flag all D pad buttons are held so the PC's walk action isn't messed with
                mDPadHeld = entryDoor.Direction // Have the player walk one tile away based on the direction indicated by the door
                walk() // Trick walk() into moving the player in the appropriate direction
                mDPadHeld = dpadHeld // Restore the flags to their previous values
                gGZ.Camera.lookAt(mPC)
            }
            // If a specific tile is being forced, such as when returning from a battle or loading from a save file
            landing != null -> {
                mPC.placeOnTile(landing)
                gGZ.Camera.lookAt(mPC)
                onNewTile(mPC.X, mPC.Y, shouldIgnoreEncounters = true)
            }
            // If no other tile was given, use the scene's default landing tile, if it has one, or place the PC in the center of hte scene and hope for the best
            gGZ.Scene != null -> {
                val landingTile = gGZ.Scene!!.Landing
                mPC.placeOnTile(landingTile?.X ?: gGZ.Scene!!.WidthInTiles / 2, landingTile?.Y ?: gGZ.Scene!!.HeightInTiles / 2)
                gGZ.Camera.lookAt(mPC)
                onNewTile(mPC.X, mPC.Y, shouldIgnoreEncounters = true)
            }
        }
        if (gGZ.Scene != null)
            gGZ.Record.autosave() // Unrelated to scenes but this was arbitrarily chosen as a good time to do an autosave
    }

    private fun walk() {
        // If walking should start or continue
        if (mDPadHeld != null) {
            mPC.IsAnimating = true
            mPC.glide(mDPadHeld!!, /* shouldCameraFollow */ true, { /* onBlocked */ mPC.Direction = mDPadHeld!! },
                { /* onNewTile */ x, y ->
                    mPC.stand() // Center the PC on the new tile and cancel the previous gliding
                    gGZ.Camera.lookAt(mPC)
                    // If the new tile event was not consumed
                    if (!onNewTile(x, y))
                        walk() // Recursively call this method to continue walking if a D pad button is held
                })
        } else {
            mPC.stand()
            mPC.IsAnimating = false
        }
    }

    private fun playCutscene(cutscene: SCCutscene) {
        mPC.stand() // Stop any PC movement
        mPC.IsAnimating = false // Stop the walk animation
        mDPadHeld = null
        mDrawablePriorityMedium = cutscene // Draw the cutscene (what is actually drawn varies) with the same priority as the PC
        mInteractable = cutscene
        mProgressive = cutscene
        cutscene.Scene = gGZ.Scene
        cutscene.PC = mPC
        cutscene.play { newSceneID -> // onDone
            mDrawablePriorityMedium = null
            mInteractable = null
            mProgressive = null
            // If the cutscene contained a scene transition, move to the new scene
            if (newSceneID != null)
                loadScene(newSceneID)
        }
        // If this cutscene should play only once per game
        if (cutscene.Once)
            gGZ.Record.set("cutscene ${cutscene.ID}", "true") // Record the fact that the cutscene with this ID has played
    }

    // To be called in the event of the PC being placed on or walking to a new tile
    // Returns true if the event was consumed (made use of), indicating that walking should cease
    // Returns false if there's nothing special about this tile
    private fun onNewTile(x: Int, y: Int, shouldIgnoreEncounters: Boolean = false): Boolean {
        gGZ.Player.X = x // Update the global player representation (mainly so Record knows where to record the player's location)
        gGZ.Player.Y = y
        countdown()
        val trap = gGZ.Scene?.getTrapAt(x, y)
        val door = gGZ.Scene?.getDoorAt(x, y)
        // If the player stepped on a trap (cutscene) tile
        if (trap != null) {
            val cutscene = gGZ.Resources.loadCutscene(gGZ.Resources.CutsceneCatalog[trap])
            if (cutscene != null) {
                // If this cutscene is not a once per game type or, if it is, it has not already been played
                if (!cutscene.Once || (gGZ.Record.get("cutscene ${cutscene.ID}", "false") != "true")) {
                    playCutscene(cutscene)
                    return true
                }
            } else
                Log.w("MOverworld", "A trap was triggered for cutscene \"$trap\" but no cutscene with that ID could be found")
        }
        // If the player walked into a door, gate, entrance, etc. leading to another scene
        else if (door != null) {
            loadScene(door.ToScene, exitDoor = door)
            return true
        }
        // If a randomly-chosen value between 0f and 1f is less than the scene's encounter rate (itself being being in the (0, 1] if the scene has encounters)
        // and if we haven't been instructed to ignore encounters (done on the initial placement of the player after loading the scene)
        else if (mAreEncountersEnabled && !shouldIgnoreEncounters && (Random().nextFloat() < gGZ.Scene?.EncounterRate ?: 0f)) {
            val weightTotal = gGZ.Scene!!.Encounters.sumBy { it.Weight }
            val random = Random()
            val roll = random.nextInt(weightTotal) + 1 // nextInt returns a random number in the range [0, weightTotal) so + 1 = [1, weightTotal]
            var weightCounter = 0
            for (encounter in gGZ.Scene!!.Encounters) {
                weightCounter += encounter.Weight
                if (roll <= weightCounter) {
                    val levelMargin = random.nextInt(encounter.LevelMargin)
                    val level = encounter.CenterLevel + if (random.nextBoolean()) levelMargin else -levelMargin
                    val onDone = {
                        Transition = MTransition(MTransition.Mode.BATTLE)
                        Transition!!.ForBattle = MTransition.BattleParameters(OpponentID = encounter.ID, OpponentLevel = level)
                        Transition!!.IsDebug = mIsDebug
                    }
                    if (gGZ.Record.get("animations", "on") == "on") {
                        val transition = UITransition(/* onDone */ onDone)
                        // TODO select the transition based on level difference
                        transition.init(UITransition.Type.values()[Random().nextInt(UITransition.Type.values().size)])
                        mDrawablePriorityHigh = transition
                        mProgressive = transition
                        mShouldIgnoreInput = true
                        mPC.IsAnimating = false
                    } else
                        onDone.invoke()
                    break
                }
            }
            return true
        }
        return false
    }

    // Helper method to process the step countdowns
    private fun countdown() {
        val countdownsToRemove = LinkedList<StepCountdown>()
        // Decrement one step from each counter and remember the counters that have reached 0
        for (stepCountdown in mStepCountdowns) {
            stepCountdown.StepCount -= 1
            if (stepCountdown.StepCount <= 0)
                countdownsToRemove.add(stepCountdown)
        }
        // Execute and remove, in that order, any countdown that just hit 0 with this new tile
        for (stepCountdown in countdownsToRemove) {
            stepCountdown.Callback.invoke()
            mStepCountdowns.remove(stepCountdown)
        }
    }

    private class StepCountdown(var StepCount: Int, val Callback: () -> Unit)
}